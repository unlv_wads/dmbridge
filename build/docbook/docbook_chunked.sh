#!/bin/sh
#
# Generates HTML documentation from DocBook.
#
# Remember to keep this command in sync with the one in build.xml.
#
# @author Alex Dolski <alex.dolski@unlv.edu>
#

xsltproc --stringparam chunk.section.depth 0 \
	--stringparam admon.graphics 1 \
	--stringparam highlight.source 1 \
	--stringparam chunker.output.encoding UTF-8 \
	--stringparam base.dir /Library/WebServer/Documents/dmbridge/doc/manual/html-chunked/ \
	--stringparam html.stylesheet styles/manual.css \
	/Library/WebServer/docbook-xsl/html/chunk.xsl \
	/Library/WebServer/Documents/dmbridge/doc/manual/index.xml
