#!/bin/sh
#
# Generates HTML documentation from DocBook.
#
# Remember to keep this command in sync with the one in build.xml.
#
# @author Alex Dolski <alex.dolski@unlv.edu>
#

xsltproc --stringparam admon.graphics 1 \
	--stringparam highlight.source 1 \
	--stringparam chunker.output.encoding UTF-8 \
	--stringparam html.stylesheet styles/manual.css \
	--output /Library/WebServer/Documents/dmbridge/doc/manual/html-onepage/index.html \
	/Library/WebServer/docbook-xsl/html/docbook.xsl \
	/Library/WebServer/Documents/dmbridge/doc/manual/index.xml

