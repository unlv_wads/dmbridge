<?php

define("TEST_ALIAS", "/static");
define("TEST_URL", "http://localhost/dmbridge");
define("TEST_COLLECTION_COUNT", 2);
define("TEST_COLLECTION_NAME", "Static Test Collection");
define("TEST_COMMENT_ID", 20);
define("TEST_COMPOUND_PDF_PAGE_PTR", 510);
define("TEST_COMPOUND_PDF_PTR", 10);
define("TEST_COMPOUND_PTR", 1);
define("TEST_COMPOUND_PAGE_PTR", 501);
define("TEST_DATA_COUNT", 150);
define("TEST_DYNAMIC_ALIAS", "/dynamic");
define("TEST_NORMAL_PTR", 2);
define("TEST_PTR", 1);
define("TEST_SEARCH_STRING", "Sample");
define("TEST_SITEMAP_URL", "http://localhost/dmbridge/sitemap");
define("TEST_TAG_ID", 20);
define("TEST_TEMPLATE_ID", 1);
define("TEST_USERNAME", "admin"); // dmulator default
define("TEST_PASSWORD", "admin"); // dmulator default

define("TEST_DB_HOST", "localhost");
define("TEST_DB_NAME", "dmbridge");
define("TEST_DB_PASS", null);
define("TEST_DB_PORT", 3306);
define("TEST_DB_USER", null);
