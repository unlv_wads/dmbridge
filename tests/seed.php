<?php

require_once("bootstrap.php");
require_once("DMTestDataSeeder.php");

$seeder = new DMTestDataSeeder();
$seeder->wipe();
$seeder->seed();

die("Done\n");

