<?php

require_once "DmBridgeSystemTest.php";

/**
 * dmBridge Atom output system test plan
 * (for PHPUnit)
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class AtomFeedTest extends DmBridgeSystemTest {

	/** @var DOMDocument */
	private $dxml;
	/** @var DMHTTPClient */
	private $http_client;

	private static function getURI() {
		return TEST_URL;
	}

	protected function setUp() {
		parent::setUp();
		$this->dxml = new DOMDocument("1.0", "utf-8");
		$this->http_client = new DMHTTPClient(new DMHTTPRequest());
	}

	protected function tearDown() {
		$this->dxml = null;
	}


	////////////////////////////////////////////////////////////////////////
	// results view
	////////////////////////////////////////////////////////////////////////

	function testResultsFeedReturns200() {
		$uri = new DMURI(self::getURI() . "/objects/static.atom");
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testResultsFeedContentTypeIsAtom() {
		$uri = new DMURI(self::getURI() . "/objects/static.atom");
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$expected_type = new DMMediaType("application", "atom+xml");
		$actual_type = $response->getRepresentation()->getMediaType();
		$this->assertTrue($expected_type->equals($actual_type));
	}

	function testResultsFeedReturnsValidAtom() {
		$uri = new DMURI(self::getURI() . "/objects/static.atom");
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$this->assertTrue($this->dxml->loadXML($response));
		$this->assertTrue($this->dxml->schemaValidate(
				dirname(__FILE__) . "/atom.xsd"));
	}


	////////////////////////////////////////////////////////////////////////
	// object view
	////////////////////////////////////////////////////////////////////////

	function testObjectFeedReturns200() {
		$uri = new DMURI(self::getURI() . "/objects/static/3.atom");
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testObjectFeedContentTypeIsAtom() {
		$uri = new DMURI(self::getURI() . "/objects/static/3.atom");
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$expected_type = new DMMediaType("application", "atom+xml");
		$actual_type = $response->getRepresentation()->getMediaType();
		$this->assertTrue($expected_type->equals($actual_type));
	}

	function testObjectFeedReturnsValidAtom() {
		$uri = new DMURI(self::getURI() . "/objects/static/3.atom");
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$this->assertTrue($this->dxml->loadXML($response));
		$this->assertTrue($this->dxml->schemaValidate(
				dirname(__FILE__) . "/atom.xsd"));
	}


	////////////////////////////////////////////////////////////////////////
	// object comments view
	////////////////////////////////////////////////////////////////////////

	function testObjectCommentsFeedReturns200() {
		$uri = new DMURI(self::getURI() . "/objects/static/1/comments.atom");
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testObjectCommentsFeedContentTypeIsAtom() {
		$uri = new DMURI(self::getURI() . "/objects/static/1/comments.atom");
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$expected_type = new DMMediaType("application", "atom+xml");
		$actual_type = $response->getRepresentation()->getMediaType();
		$this->assertTrue($expected_type->equals($actual_type));
	}

	function testObjectCommentsFeedReturnsValidAtom() {
		$uri = new DMURI(self::getURI() . "/objects/static/1/comments.atom");
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$this->assertTrue($this->dxml->loadXML($response));
		$this->assertTrue($this->dxml->schemaValidate(
				dirname(__FILE__) . "/atom.xsd"));
	}

}

