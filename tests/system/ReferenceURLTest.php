<?php

require_once("DmBridgeSystemTest.php");

/**
 * dmBridge reference URL system test plan
 * (for PHPUnit)
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class ReferenceURLTest extends DmBridgeSystemTest {

	private static function getURI() {
		return TEST_URL;
	}

	function testReferenceUrlOfNormalObjectRoutedToCdm() {
		$col = new DMCollection(TEST_ALIAS);
		$col->setRedirectingReferenceURLs(false);
		$this->config_xml->saveCollection($col);

		$uri = new DMURI(self::getURI() . "/u/?" . TEST_ALIAS . "," . TEST_NORMAL_PTR);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertEquals(302, $response->getStatusCode());
		$this->assertStringContains(
				$request->getRedirectedURI(),
				"/cdm4/item_viewer.php?CISOROOT=" . TEST_ALIAS . "&CISOPTR=" . TEST_NORMAL_PTR);
		$this->assertNoError($response->getRepresentation()->getBody());
	}

	function testReferenceUrlOfCompoundObjectRoutedToCdm() {
		$col = new DMCollection(TEST_ALIAS);
		$col->setRedirectingReferenceURLs(false);
		$this->config_xml->saveCollection($col);

		$uri = new DMURI(self::getURI() . "/u/?" . TEST_ALIAS . "," . TEST_PTR);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertEquals(302, $response->getStatusCode());
		$this->assertStringContains(
				$request->getRedirectedURI(),
				"/cdm4/document.php?CISOROOT=" . TEST_ALIAS . "&CISOPTR=" . TEST_PTR);
		$this->assertNoError($response->getRepresentation()->getBody());
	}

	function testReferenceUrlRoutedToDmBridge() {
		$col = new DMCollection(TEST_ALIAS);
		$col->setRedirectingReferenceURLs(true);
		$this->config_xml->saveCollection($col);

		$uri = new DMURI(self::getURI() . "/u/?" . TEST_ALIAS . "," . TEST_PTR);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertEquals(302, $response->getStatusCode());
		$this->assertStringContains(
				$request->getRedirectedURI(),
				"/objects" . TEST_ALIAS . "/" . TEST_PTR);
		$this->assertNoError($response->getRepresentation()->getBody());
	}

}
