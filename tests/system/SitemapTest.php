<?php

require_once "DmBridgeSystemTest.php";

/**
 * dmBridge sitemap output system test plan
 * (for PHPUnit)
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class SitemapTest extends DmBridgeSystemTest {

	/** @var DOMDocument */
	private $dxml;

	/** @var DMHTTPClient */
	private $http_client;

	private static function getSitemapURI() {
		return TEST_SITEMAP_URL;
	}

	protected function setUp() {
		parent::setUp();
		$this->dxml = new DOMDocument("1.0", "utf-8");
		$this->http_client = new DMHTTPClient(new DMHTTPRequest());
	}

	protected function tearDown() {
		$this->dxml = null;
	}

	function testMainSitemapReturns200() {
		$uri = new DMURI(self::getSitemapURI());
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testMainSitemapContentTypeIsXML() {
		$uri = new DMURI(self::getSitemapURI());
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$expected_type = new DMMediaType("application", "xml");
		$actual_type = $response->getRepresentation()->getMediaType();
		$this->assertTrue($expected_type->equals($actual_type));
	}

	function testMainSitemapValidity() {
		$uri = new DMURI(self::getSitemapURI());
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$this->assertTrue($this->dxml->loadXML($response));
		$this->assertTrue($this->dxml->schemaValidate(
				dirname(__FILE__) . "/sitemap.xsd"));
	}

	function testMainSitemapLinksToValidSitemaps() {
		$uri = new DMURI(self::getSitemapURI());
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$this->dxml->loadXML($response);
		foreach ($this->dxml->getElementsByTagName("loc") as $node) {
			$req2 = new DMHTTPRequest();
			$req2->setURI(new DMURI($node->nodeValue));
			$client2 = new DMHTTPClient($req2);
			$response = $client2->send();
			$dxml2 = new DOMDocument("1.0", "utf-8");
			$dxml2->loadXML($response);

			$this->assertEquals(200, $response->getStatusCode());

			$expected_type = new DMMediaType("application", "xml");
			$actual_type = $response->getRepresentation()->getMediaType();
			$this->assertTrue($expected_type->equals($actual_type));

			$this->assertTrue($dxml2->schemaValidate(
					dirname(__FILE__) . "/sitemap.xsd"));
		}
	}

}

