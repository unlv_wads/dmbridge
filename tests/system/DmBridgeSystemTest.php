<?php

require_once(dirname(__FILE__) . "/../DMTestDataSeeder.php");

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DmBridgeSystemTest extends PHPUnit_Framework_TestCase {

	/** @var DMConfigXML */
	protected $config_xml;
	/** @var string */
	protected $temp_config_xml_pathname;

	protected function setUp() {
		$seeder = new DMTestDataSeeder();
		$seeder->seed();

		$this->temp_config_xml_pathname =
				dirname(__FILE__) . "/../../data/config.test.xml";
		copy(dirname(__FILE__) . "/../../data/config.default.xml",
				$this->temp_config_xml_pathname);
		$this->config_xml = DMConfigXML::getInstance(
				$this->temp_config_xml_pathname);
	}

	protected function tearDown() {
		unlink($this->temp_config_xml_pathname);
	}

	protected function assertNoError($string) {
		$this->assertFalse(strpos($string, "on line") !== false);
	}

	protected function assertStringContains($string, $contains) {
		$this->assertTrue(strpos($string, $contains) !== false);
	}

	protected function assertStringNotContains($string, $not_contains) {
		$this->assertFalse(strpos($string, $not_contains));
	}

}

