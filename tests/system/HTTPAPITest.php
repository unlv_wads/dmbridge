<?php

require_once "DmBridgeSystemTest.php";

/**
 * dmBridge HTTP API functional test plan for PHPUnit. Although this plan
 * checks the well-formedness of returned representations, this is not its
 * focus; fine-grained checking is handled instead in the unit tests of the
 * DMRepresentationTransformers.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class HTTPAPITest extends DmBridgeSystemTest {

	private static $has_run_once = false;

	/** @var DOMDocument */
	private $dxml;

	private static function getAPIURI() {
		return TEST_URL;
	}

	private static function getXSDRoot() {
		return dirname(__FILE__) . "/../../includes/schema/1/xml";
	}

	public function __construct() {
		parent::__construct();
		if (!self::$has_run_once) {
			if (!function_exists("json_last_error")) {
				echo "FYI: json_last_error() is not available; you need PHP 5.3 "
					. "for JSON well-formedness checking.\n";
			}
			self::$has_run_once = true;
		}
	}

	protected function setUp() {
		parent::setUp();
		$this->dxml = new DOMDocument("1.0", "utf-8");
	}

	protected function tearDown() {
		parent::tearDown();
		$this->dxml = null;
	}

	protected function assertValidJSON($json) {
		$this->assertTrue(strlen($json) > 0);
		json_decode($json);
		if (function_exists("json_last_error")) {
			$this->assertFalse((bool) json_last_error());
		}
	}

	protected function assertValidXML($xml, $schema = null) {
		$this->assertTrue($this->dxml->loadXML($xml));
		if ($schema) {
			$this->assertTrue(
				$this->dxml->schemaValidate(self::getXSDRoot() . "/" . $schema));
		}
	}

	function testContentNegotiation() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/collections");
		$request = new DMHTTPRequest($uri);

		$request->addHeader("Accept",
				"application/xml,application/json;q=0.9");
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$expected = new DMMediaType("application", "xml");
		$this->assertTrue(
				$expected->equals($response->getRepresentation()->getMediaType()));

		$request->unsetHeaders();
		$request->addHeader("Accept",
				"application/json,application/xml;q=0.9");
		$response = $client->send();
		$expected = new DMMediaType("application", "json");
		$this->assertTrue(
				$expected->equals($response->getRepresentation()->getMediaType()));
	}

	function testDebugModeOnShowsDebugInfo() {
		$path = dirname(__FILE__) . "/../../config.ini";
		$ini = file_get_contents($path);
		$ini = str_replace("dmbridge.debug_mode = false",
				"dmbridge.debug_mode = true", $ini);
		file_put_contents($path, $ini);
		DMConfigIni::destroyInstance(DMConfigIni::getInstance());

		$uri = new DMURI(self::getAPIURI() . "/api/pizza/is/yummy.xml");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertStringContains(
				$response->getRepresentation()->getBody(), "stackTrace");
	}

	function testDebugModeOffHidesDebugInfo() {
		$path = dirname(__FILE__) . "/../../config.ini";
		$ini = file_get_contents($path);
		$ini = str_replace("dmbridge.debug_mode = true",
				"dmbridge.debug_mode = false", $ini);
		file_put_contents($path, $ini);
		DMConfigIni::destroyInstance(DMConfigIni::getInstance());

		$uri = new DMURI(self::getAPIURI() . "/api/pizza/is/yummy.xml");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertStringNotContains(
				$response->getRepresentation()->getBody(), "stackTrace");
	}

	function testInvalidRouteJSON() {
		$uri = new DMURI(self::getAPIURI() . "/api/pizza/is/yummy.json");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(404, $response->getStatusCode());
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testInvalidRouteXML() {
		$uri = new DMURI(self::getAPIURI() . "/api/pizza/is/yummy.xml");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(404, $response->getStatusCode());
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"exception.xsd");
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/collections
	//////////////////////////////////////////////////////////////////////////

	function testCollectionsListJSONValidity() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/collections.json");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testCollectionsListXMLValidity() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/collections.xml");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response, "collections_list.xsd");
	}

	function testCollectionsListReturns200() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/collections.xml");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/collections/[alias]
	//////////////////////////////////////////////////////////////////////////

	function testCollectionViewJSONValidity() {
		$uri = new DMURI(self::getAPIURI()
			. sprintf("/api/1/collections%s.json", TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testCollectionViewXMLValidity() {
		$uri = new DMURI(self::getAPIURI()
			. sprintf("/api/1/collections%s.xml", TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"collection_view.xsd");
	}

	function testCollectionViewOfValidCollectionReturns200() {
		$uri = new DMURI(self::getAPIURI()
			. sprintf("/api/1/collections%s.xml", TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testCollectionViewOfInvalidCollectionReturns404() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/collections/rosebud.xml");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(404, $response->getStatusCode());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/comments
	//////////////////////////////////////////////////////////////////////////

	function testCommentSearchJSONValidity() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/comments.json");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testCommentSearchXMLValidity() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/comments.xml");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"comments_list.xsd");
	}

	function testValidCommentSearchReturnsReturns200() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/comments?collections=%s",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testCommentSearchTerm() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/comments.json?term=a");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertTrue(count($json['dmBridgeResponse']['comments']) > 0);
	}

	function testCommentSearchCollections() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/comments.json?collections=/static&rpp=30");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertEquals(30, count($json['dmBridgeResponse']['comments']));
	}

	function testCommentSearchObjects() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/comments.json?objects=/static/1");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertEquals(10, count($json['dmBridgeResponse']['comments']));
	}

	function testCommentSearchPage() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/comments.json?page=1",
				TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$first_result = $json['dmBridgeResponse']['comments'][0]['id'];

		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/comments.json?page=2",
				TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertNotEquals(
				$json['dmBridgeResponse']['comments'][0]['id'],
				$first_result);
	}

	function testCommentSearchMinResultsPerPage() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/comments.json?rpp=2",
				TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertEquals(10, count($json['dmBridgeResponse']['comments']));
	}

	function testCommentSearchMaxResultsPerPage() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/comments.json?rpp=200",
				TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertEquals(100, count($json['dmBridgeResponse']['comments']));
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/comments/[id]
	//////////////////////////////////////////////////////////////////////////

	function testCommentViewJSONValidity() {
		$uri = new DMURI(self::getAPIURI()
			. sprintf("/api/1/comments/%d.json", TEST_COMMENT_ID));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testCommentViewXMLValidity() {
		$uri = new DMURI(self::getAPIURI()
			. sprintf("/api/1/comments/%d.xml", TEST_COMMENT_ID));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"comment_view.xsd");
	}

	function testCommentViewOfValidCommentReturns200() {
		$uri = new DMURI(self::getAPIURI()
			. sprintf("/api/1/comments/%d.xml", TEST_COMMENT_ID));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testCommentViewOfValidCommentReturns404() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/comments/243224.xml");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(404, $response->getStatusCode());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/doc
	//////////////////////////////////////////////////////////////////////////

	function testDocumentation() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/doc");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertTrue(strlen($response) > 10000);
		$this->assertTrue(
				strpos($response, "API Documentation</title>") !== false);
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/objects/[alias]
	//////////////////////////////////////////////////////////////////////////

	function testObjectsListJSONValidity() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/objects%s.json", TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testObjectsListXMLValidity() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/objects%s.xml", TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"objects_list.xsd");
	}

	function testObjectsListPage() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/objects%s.json?page=1&rpp=10",
				TEST_DYNAMIC_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$first_result = $json['dmBridgeResponse']['results'][0]['pointer'];

		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/objects%s.json?page=2&rpp=10",
				TEST_DYNAMIC_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertNotEquals(
				$json['dmBridgeResponse']['results'][0]['pointer'],
				$first_result);
	}

	function testObjectsListMinResultsPerPage() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/objects%s.json?rpp=2", TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertEquals(10, count($json['dmBridgeResponse']['results']));
	}

	function testObjectsListMaxResultsPerPage() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/objects%s.json?rpp=200",
				TEST_DYNAMIC_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertEquals(100, count($json['dmBridgeResponse']['results']));
	}

	function testObjectsListSort() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/objects%s.json", TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$first_result = $json['dmBridgeResponse']['results'][0]['pointer'];

		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/objects%s.json?sort=subjec", TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertNotEquals(
				$json['dmBridgeResponse']['results'][0]['pointer'],
				$first_result);
	}

	function testAvailableCollectionReturns200() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s.xml",
				TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testUnavailableCollectionReturns404() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/objects/blablabla");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(404, $response->getStatusCode());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/objects/[alias]/[pointer]
	//////////////////////////////////////////////////////////////////////////

	function testObjectViewJSONValidity() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d.json",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testObjectViewXMLValidity() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d.xml",
				TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"object_view.xsd");
	}

	function testAvailableObjectReturns200() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d.xml",
				TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testUnavailableObjectReturns404() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/2094024.json",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(404, $response->getStatusCode());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/objects/[alias]/[pointer]/comments
	//////////////////////////////////////////////////////////////////////////

	function testObjectCommentsJSONValidity() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/comments.json",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testObjectCommentsXMLValidity() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/comments.xml",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"object_comments.xsd");
	}

	function testObjectCommentsOnValidObjectReturns200() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/comments.xml",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testObjectCommentsOnInvalidObjectReturns404() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/objects%s/234234/comments.xml",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(404, $response->getStatusCode());
	}

	function testObjectCommentsPage() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/comments.json?page=1",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$first_result = $json['dmBridgeResponse']['dmObject']['comments'][0]['id'];

		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/comments.xml?page=2",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertNotEquals(
				$json['dmBridgeResponse']['dmObject']['comments'][0]['id'],
				$first_result);
	}

	function testObjectCommentsMinResultsPerPage() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/comments.json?rpp=2",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertEquals(10, count($json['dmBridgeResponse']['dmObject']['comments']));
	}

	function testObjectCommentsMaxResultsPerPage() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/comments.json?rpp=200",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertEquals(100, count($json['dmBridgeResponse']['dmObject']['comments']));
	}


	//////////////////////////////////////////////////////////////////////////
	// POST api/1/objects/[alias]/[pointer]/comments
	//////////////////////////////////////////////////////////////////////////

	function testPostValidCommentOnValidObjectWithModerationEnabledReturns202() {
		DMConfigXML::getInstance()->setCommentModerationEnabled(true);
		DMConfigXML::getInstance()->save();
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/comments",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("name", "santa claus");
		$request->getRepresentation()->addFormData("email", "santa@northpole.com");
		$request->getRepresentation()->addFormData("text", "asflaskjflkjsf");
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(202, $response->getStatusCode());
	}

	function testPostValidCommentOnValidObjectWithModerationDisabledReturns201() {
		DMConfigXML::getInstance()->setCommentModerationEnabled(false);
		DMConfigXML::getInstance()->save();
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/comments",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("name", "santa claus");
		$request->getRepresentation()->addFormData("email", "santa@northpole.com");
		$request->getRepresentation()->addFormData("text", "adfljasdfl;jksdf");
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(201, $response->getStatusCode());
	}

	function testPostValidCommentOnValidObjectWithModerationDisabledReturnsLocationHeader() {
		DMConfigXML::getInstance()->setCommentModerationEnabled(false);
		DMConfigXML::getInstance()->save();
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/comments",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("name", "santa claus");
		$request->getRepresentation()->addFormData("email", "santa@northpole.com");
		$request->getRepresentation()->addFormData("text", "adfljasdfl;jksdf");
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$found = false;
		foreach ($response->getHeaders() as $k => $v) {
			if ($k == "Location") {
				$found = true;
				break;
			}
		}
		$this->assertTrue($found);
	}

	function testPostValidCommentOnInvalidObjectReturns404() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/2424535/comments",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("name", "santa claus");
		$request->getRepresentation()->addFormData("email", "santa@northpole.com");
		$request->getRepresentation()->addFormData("text", "asdfjkasdfljk");
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(404, $response->getStatusCode());
	}

	function testPostingCommentWithMissingTextReturns400() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/comments",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("name", "santa claus");
		$request->getRepresentation()->addFormData("email", "santa@northpole.com");
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}

	function testPostingCommentWithMissingNameReturns400() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/comments",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("text", "asdfljasfdl;jkasf");
		$request->getRepresentation()->addFormData("email", "santa@northpole.com");
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}

	function testPostingCommentWithInvalidEmailReturns400() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/comments",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("name", "santa claus");
		$request->getRepresentation()->addFormData("text", "wekjrwlekrj");
		$request->getRepresentation()->addFormData("email", "santa");
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}


	//////////////////////////////////////////////////////////////////////////
	// POST api/1/objects/[alias]/[pointer]/tags
	//////////////////////////////////////////////////////////////////////////

	function testPostValidTagOnValidObjectWithModerationEnabledReturns202() {
		DMConfigXML::getInstance()->setTagModerationEnabled(true);
		DMConfigXML::getInstance()->save();
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/tags",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("value", "asflaskjflkjsf");
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(202, $response->getStatusCode());
	}

	function testPostValidTagOnValidObjectWithModerationDisabledReturns201() {
		DMConfigXML::getInstance()->setTagModerationEnabled(false);
		DMConfigXML::getInstance()->save();
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/tags",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("value", "adfljasdfl;jksdf");
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(201, $response->getStatusCode());
	}

	function testPostValidTagOnInvalidObjectReturns404() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/24234234/tags",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("value", "asdfjkasdfljk");
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(404, $response->getStatusCode());
	}

	function testPostingTagWithMissingValueReturns400() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/tags",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/objects/[alias]/[pointer]/rating
	//////////////////////////////////////////////////////////////////////////

	function testObjectRatingJSONValidity() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/rating.json",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testObjectRatingXMLValidity() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/rating.xml",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"object_rating.xsd");
	}

	function testAvailableObjectRatingReturns200() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/rating.xml",
				TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testUnavailableObjectRatingReturns404() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/2094024/rating.json",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(404, $response->getStatusCode());
	}

	//////////////////////////////////////////////////////////////////////////
	// POST api/1/objects/[alias]/[pointer]/rating
	//////////////////////////////////////////////////////////////////////////

	function testPostingValidRatingOnValidObjectReturns204() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/rating",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("value", 45);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(204, $response->getStatusCode());
	}

	function testPostingValidRatingOnInvalidObjectReturns404() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/242342/rating",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("value", 45);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(404, $response->getStatusCode());
	}

	function testPostingInvalidRatingOnValidObjectReturns400() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/rating",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("value", 245);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/objects/[alias]/highest_rated
	//////////////////////////////////////////////////////////////////////////

	function testHighestRatedObjectsJSONValidity() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/highest_rated.json",
			TEST_DYNAMIC_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testHighestRatedObjectsXMLValidity() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/highest_rated.xml",
			TEST_DYNAMIC_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"objects_list_ratings.xsd");
	}

	function testHighestRatedObjectsMinResultsPerPage() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/highest_rated.json?rpp=2",
			TEST_DYNAMIC_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertTrue(count($json['dmBridgeResponse']['results']) > 2);
		$this->assertTrue(count($json['dmBridgeResponse']['results']) <= 10);
	}

	function testHighestRatedObjectsMaxResultsPerPage() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/highest_rated.json?rpp=250",
			TEST_DYNAMIC_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertTrue(count($json['dmBridgeResponse']['results']) <= 100);
	}

	function testAvailableHighestRatedObjectsReturns200() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/highest_rated.xml",
			TEST_DYNAMIC_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/objects/[alias]/[pointer]/tags
	//////////////////////////////////////////////////////////////////////////

	function testObjectTagsJSONValidity() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/tags.json",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testObjectTagsXMLValidity() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/tags.xml",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"object_tags.xsd");
	}

	function testObjectTagsOnValidObjectReturns200() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/tags",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testObjectTagsOnInvalidObjectReturns404() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/23408923/tags",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(404, $response->getStatusCode());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/search
	//////////////////////////////////////////////////////////////////////////

	function testMetadataSearchJSONValidity() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/search.json?CISOROOT=%s&CISOOP1=all&CISOBOX1=cats&CISOFIELD1=title",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testMetadataSearchXMLValidity() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/search.xml?CISOROOT=%s&CISOOP1=all&CISOBOX1=cats&CISOFIELD1=title",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"objects_list.xsd");
	}

	function testValidMetadataSearchReturnsReturns200() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/search.xml?CISOROOT=%s&CISOOP1=all&CISOBOX1=cats&CISOFIELD1=title",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testInvalidMetadataSearchMatchingModeReturnsReturns400() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/search.xml?CISOROOT=%s&CISOOP1=cats&CISOBOX1=cats&CISOFIELD1=title",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}

	function testMissingMetadataSearchMatchingModeReturnsReturns400() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/search.xml?CISOROOT=%s&CISOBOX1=cats&CISOFIELD1=title",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}

	function testMissingMetadataSearchSearchStringReturnsReturns400() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/search.xml?CISOROOT=%s&CISOOP1=all&CISOFIELD1=title",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}

	function testMissingMetadataSearchCollectionAliasReturnsReturns400() {
		$uri = new DMURI(
				self::getAPIURI() . "/api/1/search.xml?CISOOP1=cats&CISOBOX1=cats&CISOFIELD1=title");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}

	function testMetadataSearchPage() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/search.json?CISOROOT=%s"
				. "&CISOOP1=all&CISOBOX1=cats&CISOFIELD1=title&page=1",
				TEST_DYNAMIC_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$first_result = $json['dmBridgeResponse']['results'][0]['pointer'];

		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/search.json?CISOROOT=%s"
				. "&CISOOP1=all&CISOBOX1=cats&CISOFIELD1=title&page=2",
				TEST_DYNAMIC_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertNotEquals(
				$json['dmBridgeResponse']['results'][0]['pointer'],
				$first_result);
	}

	function testMetadataSearchMinResultsPerPage() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/search.json?CISOROOT=%s"
				. "&CISOOP1=all&CISOBOX1=cats&CISOFIELD1=title&rpp=2",
				TEST_DYNAMIC_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertEquals(10, count($json['dmBridgeResponse']['results']));
	}

	function testMetadataSearchMaxResultsPerPage() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/search.json?CISOROOT=%s"
				. "&CISOOP1=all&CISOBOX1=cats&CISOFIELD1=title&rpp=200",
				TEST_DYNAMIC_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response->getRepresentation()->getBody(), true);
		$this->assertEquals(100, count($json['dmBridgeResponse']['results']));
	}

	function testMetadataSearchSortDirection() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/search.json?CISOROOT=%s"
				. "&CISOOP1=all&CISOBOX1=Sample&CISOFIELD1=title&sort=title",
				TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response->getRepresentation()->getBody(), true);
		$first_result = $json['dmBridgeResponse']['results'][0]['pointer'];

		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/search.json?CISOROOT=%s"
				. "&CISOOP1=all&CISOBOX1=Sample&CISOFIELD1=title&sort=subjec",
				TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response->getRepresentation()->getBody(), true);
		$this->assertNotEquals(
				$json['dmBridgeResponse']['results'][0]['pointer'],
				$first_result);
	}

	function testMetadataSearchSortOrder() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/search.json?CISOROOT=%s"
				. "&CISOOP1=all&CISOBOX1=Sample&CISOFIELD1=title&order=asc",
				TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response->getRepresentation()->getBody(), true);
		$first_result = $json['dmBridgeResponse']['results'][0]['pointer'];

		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/search.json?CISOROOT=%s"
				. "&CISOOP1=all&CISOBOX1=Sample&CISOFIELD1=title&order=desc",
				TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response->getRepresentation()->getBody(), true);
		$this->assertNotEquals(
				$json['dmBridgeResponse']['results'][0]['pointer'],
				$first_result);
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/search_by_rating
	//////////////////////////////////////////////////////////////////////////

	function testRatingSearchJSONValidity() {
		$uri = new DMURI(self::getAPIURI()
			. "/api/1/search_by_rating.json?min_rating=0&max_rating=1");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testRatingSearchXMLValidity() {
		$uri = new DMURI(self::getAPIURI()
			. "/api/1/search_by_rating.xml?min_rating=0&max_rating=1");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"objects_list_ratings.xsd");
	}

	function testValidRatingSearchReturnsReturns200() {
		$uri = new DMURI(self::getAPIURI()
			. "/api/1/search_by_rating.xml?min_rating=0&max_rating=1");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testInvalidRatingSearchMinRatingReturnsReturns400() {
		$uri = new DMURI(self::getAPIURI()
			. "/api/1/search_by_rating.xml?min_rating=1&max_rating=1");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}

	function testInvalidRatingSearchMaxRatingReturnsReturns400() {
		$uri = new DMURI(self::getAPIURI()
			. "/api/1/search_by_rating.xml?min_rating=0&max_rating=0");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}

	function testMissingRatingSearchMinRatingReturnsReturns400() {
		$uri = new DMURI(self::getAPIURI()
			. "/api/1/search_by_rating.xml?max_rating=1");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}

	function testMissingRatingSearchMaxRatingReturnsReturns400() {
		$uri = new DMURI(self::getAPIURI()
			. "/api/1/search_by_rating.xml?min_rating=1");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}

	function testRatingSearchPage() {
		$uri = new DMURI(self::getAPIURI()
			. "/api/1/search_by_rating.json?min_rating=0&max_rating=1&rpp=30");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$first_result = $json['dmBridgeResponse']['results'][0]['pointer'];

		$uri = new DMURI(self::getAPIURI()
			. "/api/1/search_by_rating.json?min_rating=0&max_rating=1&page=2");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertNotEquals(
				$json['dmBridgeResponse']['results'][0]['pointer'],
				$first_result);
	}

	function testRatingSearchMinResultsPerPage() {
		$uri = new DMURI(self::getAPIURI()
			. "/api/1/search_by_rating.json?min_rating=0&max_rating=1&rpp=2");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertEquals(10, count($json['dmBridgeResponse']['results']));
	}

	function testRatingSearchMaxResultsPerPage() {
		$uri = new DMURI(self::getAPIURI()
			. "/api/1/search_by_rating.json?min_rating=0&max_rating=1&rpp=200");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertEquals(100, count($json['dmBridgeResponse']['results']));
	}

	function testSearchSimpleJSON() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/search.json?q=cats");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testSearchSimpleXML() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/search.xml?q=cats");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"objects_list.xsd");
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/objects/[alias]/[pointer]/search
	//////////////////////////////////////////////////////////////////////////

	function testCompoundObjectSearchJSONValidity() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/search.json?term=cats",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testCompoundObjectSearchXMLValidity() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/search.xml?term=cats",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"objects_list.xsd");
	}

	function testValidCompoundObjectSearchReturnsReturns200() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/search.json?term=cats",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testMissingCompoundObjectSearchTermReturnsReturns400() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/objects%s/%d/search.json",
			TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/tags
	//////////////////////////////////////////////////////////////////////////

	function testTagSearchJSONValidity() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/tags.json");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testTagSearchXMLValidity() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/tags.xml");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"tag_counts.xsd");
	}

	function testValidTagSearchReturnsReturns200() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/tags?collections=%s",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testTagSearchTerm() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/tags.json?term=a");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertTrue(count($json['dmBridgeResponse']['tags']) > 0);
	}

	function testTagSearchCollections() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/tags.json?collections=%s&rpp=30",
				TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertEquals(30, count($json['dmBridgeResponse']['tags']));
	}

	function testTagSearchObjects() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/tags.json?objects=%s/%d&rpp=30",
				TEST_ALIAS, TEST_PTR));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertEquals(30, count($json['dmBridgeResponse']['tags']));
	}

	function testTagSearchSort() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/tags.json");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$first_result = $json['dmBridgeResponse']['tags'];

		$uri = new DMURI(self::getAPIURI() . "/api/1/tags.json?sort=random");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertNotEquals($json['dmBridgeResponse']['tags'], $first_result);
	}

	function testTagSearchPage() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/tags.json?page=1",
				TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$first_result = array_keys($json['dmBridgeResponse']['tags']);
		$first_result = $first_result[0];

		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/tags.json?page=2",
				TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$compared_result = array_keys($json['dmBridgeResponse']['tags']);
		$compared_result = $compared_result[0];
		$this->assertNotEquals($compared_result, $first_result);
	}

	function testTagSearchMinResultsPerPage() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/tags.json?rpp=2",
				TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertEquals(10, count($json['dmBridgeResponse']['tags']));
	}

	function testTagSearchMaxResultsPerPage() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/tags.json?rpp=200",
				TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$json = json_decode($response, true);
		$this->assertEquals(100, count($json['dmBridgeResponse']['tags']));
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/tags/[id]
	//////////////////////////////////////////////////////////////////////////

	function testTagViewJSONValidity() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/tags/%d.json", TEST_TAG_ID));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testTagViewXMLValidity() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/tags/%d.xml", TEST_TAG_ID));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"tag_view.xsd");
	}

	function testViewingValidTagReturns200() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/tags/%d.xml", TEST_TAG_ID));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testViewingInvalidTagReturns404() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/tags/9999999.xml"));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(404, $response->getStatusCode());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/vocabulary/[alias]/[nick]/frequency
	//////////////////////////////////////////////////////////////////////////

	function testVocabularyFrequencyJSONValidity() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/vocabulary%s/subjec.json",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testVocabularyFrequencyXMLValidity() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/vocabulary%s/subjec.xml",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"vocabulary_frequency.xsd");
	}

	function testVocabularyFrequencyOfValidFieldReturns200() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/vocabulary%s/subjec.xml",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testVocabularyFrequencyOfInvalidFieldReturns404() {
		$uri = new DMURI(self::getAPIURI() . sprintf("/api/1/vocabulary%s/dfsadfasdf.xml",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(404, $response->getStatusCode());
	}

	function testVocabularyFrequencyOfValidFieldInInvalidCollectionReturns404() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/vocabulary/adfkljasdf/subjec.xml");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(404, $response->getStatusCode());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/vocabulary/suggest
	//////////////////////////////////////////////////////////////////////////

	function testVocabularySuggestionsJSONValidity() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/vocabulary/suggest.json?CISOROOT=%s&CISOFIELD1=title&CISOBOX1=%s",
			TEST_ALIAS, TEST_SEARCH_STRING));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testVocabularySuggestionsXMLValidity() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/vocabulary/suggest.xml?CISOROOT=%s&CISOFIELD1=title&CISOBOX1=%s",
			TEST_ALIAS, TEST_SEARCH_STRING));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"vocabulary_suggestions.xsd");
	}

	function testVocabularySuggestionWithMissingCollectionAlias() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/vocabulary/suggest.xml?CISOFIELD1=title&CISOBOX1=%s",
			TEST_SEARCH_STRING));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}

	function testVocabularySuggestionWithMissingSearchString() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/vocabulary/suggest.xml?CISOROOT=%s&CISOFIELD1=title",
			TEST_ALIAS));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}

	function testVocabularySuggestionWithMissingFieldNick() {
		$uri = new DMURI(
				self::getAPIURI() . sprintf("/api/1/vocabulary/suggest.xml?CISOROOT=%s&CISOBOX1=%s",
			TEST_ALIAS, TEST_SEARCH_STRING));
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(400, $response->getStatusCode());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/status
	//////////////////////////////////////////////////////////////////////////

	function testStatusJSONValidity() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/status.json");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidJSON($response->getRepresentation()->getBody());
	}

	function testStatusXMLValidity() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/status.xml");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertValidXML($response->getRepresentation()->getBody(),
				"status.xsd");
	}

	function testStatusReturns200() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/status.json");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

}

