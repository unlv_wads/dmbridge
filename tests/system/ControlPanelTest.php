<?php

require_once("DmBridgeSystemTest.php");

/**
 * dmBridge Control Panel test plan
 * (for PHPUnit)
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class ControlPanelTest extends DmBridgeSystemTest {

	/** @var DOMDocument */
	private $dxml;

	private static function getURI() {
		return TEST_URL;
	}

	protected function setUp() {
		parent::setUp();
		$this->dxml = new DOMDocument("1.0", "utf-8");
	}

	/**
	 * @return DMHTTPClient A logged-in HTTP client
	 */
	protected function login() {
		$uri = new DMURI(self::getURI() . "/admin/login");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$client->send();
		return $client;
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin
	//////////////////////////////////////////////////////////////////////////

	function testMainPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/basic
	//////////////////////////////////////////////////////////////////////////

	function testBasicSettingsPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/basic");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// POST admin/basic
	//////////////////////////////////////////////////////////////////////////

	/**
	 * @todo Write testBasicSettingsForm()
	 */
	function testBasicSettingsForm() {
		$this->markTestIncomplete();
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/modules
	//////////////////////////////////////////////////////////////////////////

	function testModulesPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/modules");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}

	/**
	 * @todo Write testNewModuleShowsUp()
	 */
	function testNewModuleShowsUp() {
		$this->markTestIncomplete();
	}

	/**
	 * @todo Write testRemovedModuleDisappears()
	 */
	function testRemovedModuleDisappears() {
		$this->markTestIncomplete();
	}


	//////////////////////////////////////////////////////////////////////////
	// POST admin/modules
	//////////////////////////////////////////////////////////////////////////

	/**
	 * @todo Write testEnableModule()
	 */
	function testEnableModule() {
		$this->markTestIncomplete();
	}

	/**
	 * @todo Write testDisableModule()
	 */
	function testDisableModule() {
		$this->markTestIncomplete();
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/comments
	//////////////////////////////////////////////////////////////////////////

	function testCommentsPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/comments");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// POST admin/comments
	//////////////////////////////////////////////////////////////////////////

	/**
	 * @todo Write testDeleteComment()
	 */
	function testDeleteComment() {
		$this->markTestIncomplete();
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/comments/edit
	//////////////////////////////////////////////////////////////////////////

	function testEditCommentsPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/comments/edit");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// POST admin/comments/edit
	//////////////////////////////////////////////////////////////////////////

	/**
	 * @todo Write testEditCommentsForm()
	 */
	function testEditCommentsForm() {
		$this->markTestIncomplete();
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/comments/moderate
	//////////////////////////////////////////////////////////////////////////

	function testModerateCommentsPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/comments/moderate");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// POST admin/comments/moderate
	//////////////////////////////////////////////////////////////////////////

	/**
	 * @todo Write testApproveComment()
	 */
	function testApproveComment() {
		$this->markTestIncomplete();
	}

	/**
	 * @todo Write testRejectComment()
	 */
	function testRejectComment() {
		$this->markTestIncomplete();
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/comments/statistics
	//////////////////////////////////////////////////////////////////////////

	function testCommentStatisticsPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/comments/statistics");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/ratings/edit
	//////////////////////////////////////////////////////////////////////////

	function testEditRatingsPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/ratings/edit");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// POST admin/ratings/edit
	//////////////////////////////////////////////////////////////////////////

	/**
	 * @todo Write testEditRatingsForm()
	 */
	function testEditRatingsForm() {
		$this->markTestIncomplete();
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/ratings/statistics
	//////////////////////////////////////////////////////////////////////////

	function testRatingStatistics() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/ratings/statistics");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/tags
	//////////////////////////////////////////////////////////////////////////

	function testTagsPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/tags");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// POST admin/tags
	//////////////////////////////////////////////////////////////////////////

	/**
	 * @todo Write testDeleteTag()
	 */
	function testDeleteTag() {
		$this->markTestIncomplete();
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/tags/edit
	//////////////////////////////////////////////////////////////////////////

	function testEditTagsPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/tags/edit");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// POST admin/tags/edit
	//////////////////////////////////////////////////////////////////////////

	/**
	 * @todo Write testEditTagsForm()
	 */
	function testEditTagsForm() {
		$this->markTestIncomplete();
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/tags/moderate
	//////////////////////////////////////////////////////////////////////////

	function testModerateTagsPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/tags/moderate");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// POST admin/tags/moderate
	//////////////////////////////////////////////////////////////////////////

	/**
	 * @todo Write testApproveTag()
	 */
	function testApproveTag() {
		$this->markTestIncomplete();
	}

	/**
	 * @todo Write testRejectTag()
	 */
	function testRejectTag() {
		$this->markTestIncomplete();
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/tags/statistics
	//////////////////////////////////////////////////////////////////////////

	function testTagStatisticsPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/tags/statistics");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/collections
	//////////////////////////////////////////////////////////////////////////

	function testCollectionsPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/collections");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/collections/:alias/edit
	//////////////////////////////////////////////////////////////////////////

	function testCollectionPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin" . TEST_ALIAS . "/edit");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/reference_urls
	//////////////////////////////////////////////////////////////////////////

	function testReferenceURLsPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/reference_urls");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// POST admin/reference_urls
	//////////////////////////////////////////////////////////////////////////

	/**
	 * @todo Write testReferenceURLsForm()
	 */
	function testReferenceURLsForm() {
		$this->markTestIncomplete();
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/templates
	//////////////////////////////////////////////////////////////////////////

	function testTemplatesPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/templates");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// POST admin/templates
	//////////////////////////////////////////////////////////////////////////

	/**
	 * @todo Write testDeleteTemplate()
	 */
	function testDeleteTemplate() {
		$this->markTestIncomplete();
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/templates/add
	//////////////////////////////////////////////////////////////////////////

	/**
	 * @todo Write testAddTemplatesPage()
	 */
	function testAddTemplatesPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/templates/add");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// POST admin/templates/add
	//////////////////////////////////////////////////////////////////////////

	/**
	 * @todo Write testAddTemplatesForm()
	 */
	function testAddTemplatesForm() {
		$this->markTestIncomplete();
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin/feeds
	//////////////////////////////////////////////////////////////////////////

	function testFeedsPage() {
		$client = $this->login();
		$uri = new DMURI(self::getURI() . "/admin/feeds");
		$client->getRequest()->setURI($uri);
		$response = $client->send();
		$this->assertEquals(200, $response->getStatusCode());
		$dhtml = $this->dxml->loadHTML($response);
		$this->assertTrue($dhtml);
		$this->assertNoError($dhtml);
	}


	//////////////////////////////////////////////////////////////////////////
	// POST admin/feeds
	//////////////////////////////////////////////////////////////////////////

	/**
	 * @todo Write testFeedsForm()
	 */
	function testFeedsForm() {
		$this->markTestIncomplete();
	}

}
