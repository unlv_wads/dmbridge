<?php

require_once("DmBridgeSystemTest.php");

/**
 * dmBridge template engine test plan
 * (for PHPUnit)
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class TemplateTest extends DmBridgeSystemTest {

	/** @var DOMDocument */
	private $dxml;

	private static function getAnswerFromCaptchaInResponse(
			DMHTTPResponse $response) {
		$dxml = new DOMDocument("1.0", "utf-8");
		$dxml->loadHTML($response->getRepresentation()->getBody());
		$node = $dxml->getElementById("dmCaptchaChallenge");
		$parts = explode("+", $node->nodeValue);
		$answer = 0;
		foreach ($parts as $part) {
			$answer += preg_replace("/[^0-9]/", "", $part);
		}
		return $answer;
	}

	private static function getURI() {
		return TEST_URL;
	}

	protected function setUp() {
		parent::setUp();
		$this->dxml = new DOMDocument("1.0", "utf-8");
		$this->config_xml->setCommentingEnabled(true);
		$this->config_xml->setRatingEnabled(true);
		$this->config_xml->setTaggingEnabled(true);
		$this->config_xml->save();
	}


	//////////////////////////////////////////////////////////////////////////
	// GET /
	//////////////////////////////////////////////////////////////////////////

	function testWelcomePage() {
		$uri = new DMURI(self::getURI() . "/");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertStringContains(
				$response->getRepresentation()->getBody(),
				"Welcome to dmBridge");
		$this->assertNoError($response->getRepresentation()->getBody());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET objects
	//////////////////////////////////////////////////////////////////////////

	function testObjectsPage() {
		$uri = new DMURI(self::getURI() . "/objects");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertNoError($response->getRepresentation()->getBody());
	}

	/**
	 * @todo Write testObjectsPagePagination()
	 */
	function testObjectsPagePagination() {
		$this->markTestIncomplete();
	}

	function testObjectsPageResultsPerPage() {
		$uri = new DMURI(self::getURI() . "/objects?rpp=23");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$body = $response->getRepresentation()->getBody();
		$dxml = new DOMDocument("1.0", "utf-8");
		$dxml->loadHTML($body);
		$dxp = new DOMXPath($dxml);
		$result = $dxp->query("//table[@class = 'dmResults dmGridResults']/tbody/tr");
		$this->assertEquals(24, $result->length);
	}


	//////////////////////////////////////////////////////////////////////////
	// GET objects/favorites
	//////////////////////////////////////////////////////////////////////////

	function testFavoritesPageWithFavorites() {
		// @todo add a favorite

		$uri = new DMURI(self::getURI() . "/objects/favorites");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertStringNotContains(
				$response->getRepresentation()->getBody(),
				"You have no favorites");
		$this->assertNoError($response->getRepresentation()->getBody());
	}

	function testFavoritesPageWithNoFavorites() {
		$uri = new DMURI(self::getURI() . "/objects/favorites");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertStringContains(
				$response->getRepresentation()->getBody(),
				"You have no favorites");
		$this->assertNoError($response->getRepresentation()->getBody());
	}


	//////////////////////////////////////////////////////////////////////////
	// POST objects/favorites
	//////////////////////////////////////////////////////////////////////////

	function testAddingSingleFavorite() {
		$uri = new DMURI(self::getURI() . "/objects/favorites");
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("alias", TEST_ALIAS);
		$request->getRepresentation()->addFormData("ptr", TEST_PTR);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertNoError($response->getRepresentation()->getBody());
	}

	function testAddingMultipleFavorites() {
		$uri = new DMURI(self::getURI() . "/objects/favorites");
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData(
				"data[]", TEST_ALIAS . "|" . TEST_PTR);
		$request->getRepresentation()->addFormData(
				"data[]", TEST_ALIAS . "|" . TEST_NORMAL_PTR);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertNoError($response->getRepresentation()->getBody());
	}

	function testDeletingSingleFavorite() {
		$uri = new DMURI(self::getURI() . "/objects/favorites");
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData(
				"data[]", TEST_ALIAS . "|" . TEST_PTR);
		$request->getRepresentation()->addFormData("method", "DELETE");
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertNoError($response->getRepresentation()->getBody());
	}

	function testDeletingMultipleFavorites() {
		$uri = new DMURI(self::getURI() . "/objects/favorites");
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData(
				"data[]", TEST_ALIAS . "|" . TEST_PTR);
		$request->getRepresentation()->addFormData(
				"data[]", TEST_ALIAS . "|" . TEST_NORMAL_PTR);
		$request->getRepresentation()->addFormData("method", "DELETE");
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertNoError($response->getRepresentation()->getBody());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET objects/login
	//////////////////////////////////////////////////////////////////////////

	function testLoginPage() {
		$uri = new DMURI(self::getURI() . "/objects/login");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertStringContains(
				$response->getRepresentation()->getBody(), "Username");
		$this->assertNoError($response->getRepresentation()->getBody());
	}


	//////////////////////////////////////////////////////////////////////////
	// POST objects/login
	//////////////////////////////////////////////////////////////////////////

	function testLoggingIn() {
		$uri = new DMURI(self::getURI() . "/objects/login");
		$request = new DMHTTPRequest($uri, DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("username", TEST_USERNAME);
		$request->getRepresentation()->addFormData("password", TEST_PASSWORD);
		$client = new DMHTTPClient($request);
		$client->setFollowRedirects(true);
		$response = $client->send();

		$this->assertNotNull($request->getRedirectedURI());
		$this->assertNoError($response->getRepresentation()->getBody());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET objects/logout
	//////////////////////////////////////////////////////////////////////////

	function testLoggingOut() {
		// @todo login

		$uri = new DMURI(self::getURI() . "/objects/logout");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertNotNull($request->getRedirectedURI());
		$this->assertStringContains($response->getRepresentation()->getBody(),
				DMLocalizedString::getString("LOGOUT_SUCCESS"));
		$this->assertNoError($response->getRepresentation()->getBody());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET objects/search
	//////////////////////////////////////////////////////////////////////////

	function testSearchView() {
		$uri = new DMURI(self::getURI() . "/objects/search");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertNoError($response->getRepresentation()->getBody());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET objects/:alias
	//////////////////////////////////////////////////////////////////////////

	function testCollectionObjectsPage() {
		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertNoError($response->getRepresentation()->getBody());
	}

	/**
	 * @todo Write testCollectionObjectsPagePagination()
	 */
	function testCollectionObjectsPagePagination() {
		$this->markTestIncomplete();
	}

	function testCollectionObjectsPageResultsPerPage() {
		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "?rpp=23");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$body = $response->getRepresentation()->getBody();
		$dxml = new DOMDocument("1.0", "utf-8");
		$dxml->loadHTML($body);
		$dxp = new DOMXPath($dxml);
		$result = $dxp->query("//table[@class = 'dmResults dmGridResults']/tbody/tr");
		$this->assertEquals(24, $result->length);
	}


	//////////////////////////////////////////////////////////////////////////
	// GET objects/:alias/favorites
	//////////////////////////////////////////////////////////////////////////

	function testCollectionFavoritesPageWithFavorites() {
		// @todo add favorites

		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/favorites");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertStringNotContains(
				$response->getRepresentation()->getBody(),
				"You have no favorites");
		$this->assertNoError($response->getRepresentation()->getBody());
	}

	function testCollectionFavoritesPageWithNoFavorites() {
		$uri = new DMURI(self::getURI() . "/objects/favorites");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertStringContains(
				$response->getRepresentation()->getBody(),
				"You have no favorites");
		$this->assertNoError($response->getRepresentation()->getBody());
	}


	//////////////////////////////////////////////////////////////////////////
	// POST objects/:alias/favorites
	//////////////////////////////////////////////////////////////////////////

	function testAddingSingleCollectionFavorite() {
		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/favorites");
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("alias", TEST_ALIAS);
		$request->getRepresentation()->addFormData("ptr", TEST_PTR);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertNoError($response->getRepresentation()->getBody());
	}

	function testAddingMultipleCollectionFavorites() {
		$uri = new DMURI(self::getURI() . "/objects/favorites");
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData(
				"data[]", TEST_ALIAS . "|" . TEST_PTR);
		$request->getRepresentation()->addFormData(
				"data[]", TEST_ALIAS . "|" . TEST_NORMAL_PTR);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertNoError($response->getRepresentation()->getBody());
	}

	function testDeletingSingleCollectionFavorite() {
		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/favorites");
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData(
				"data[]", TEST_ALIAS . "|" . TEST_PTR);
		$request->getRepresentation()->addFormData("method", "DELETE");
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertNoError($response->getRepresentation()->getBody());
	}

	function testDeletingMultipleCollectionFavorites() {
		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/favorites");
		$request = new DMHTTPRequest($uri);
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData(
				"data[]", TEST_ALIAS . "|" . TEST_PTR);
		$request->getRepresentation()->addFormData(
				"data[]", TEST_ALIAS . "|" . TEST_NORMAL_PTR);
		$request->getRepresentation()->addFormData("method", "DELETE");
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertNoError($response->getRepresentation()->getBody());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET objects/:alias/login
	//////////////////////////////////////////////////////////////////////////

	function testCollectionLoginPage() {
		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/login");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$client->setFollowRedirects(true);
		$response = $client->send();

		$this->assertNoError($response->getRepresentation()->getBody());
	}


	//////////////////////////////////////////////////////////////////////////
	// POST objects/:alias/login
	//////////////////////////////////////////////////////////////////////////

	function testCollectionLoggingIn() {
		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/login");
		$request = new DMHTTPRequest($uri, DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("username", TEST_USERNAME);
		$request->getRepresentation()->addFormData("password", TEST_PASSWORD);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertNotNull($request->getRedirectedURI());
		$this->assertNoError($response->getRepresentation()->getBody());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET objects/:alias/logout
	//////////////////////////////////////////////////////////////////////////

	function testCollectionLoggingOut() {
		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/logout");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$client->setFollowRedirects(true);
		$response = $client->send();

		$this->assertStringContains($response->getRepresentation()->getBody(),
				DMLocalizedString::getString("LOGOUT_SUCCESS"));
		$this->assertNotNull($request->getRedirectedURI());
		$this->assertNoError($response->getRepresentation()->getBody());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET objects/:alias/search
	//////////////////////////////////////////////////////////////////////////

	function testCollectionSearchView() {
		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/search");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertNoError($response->getRepresentation()->getBody());
	}


	//////////////////////////////////////////////////////////////////////////
	// GET objects/:alias/:ptr
	//////////////////////////////////////////////////////////////////////////

	function testCollectionObjectView() {
		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/" . TEST_PTR);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertNoError($response->getRepresentation()->getBody());
	}

	function testPostingCommentWithInvalidCaptcha() {
		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/" . TEST_PTR);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		// submit comment
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("dmForm[alias]", TEST_ALIAS);
		$request->getRepresentation()->addFormData("dmForm[ptr]", TEST_PTR);
		$request->getRepresentation()->addFormData("dmForm[name]", "Test Poster");
		$request->getRepresentation()->addFormData("dmForm[email]", "test@localhost");
		$request->getRepresentation()->addFormData("dmForm[test]", "test comment");
		$request->getRepresentation()->addFormData("dmForm[response]", 24324);
		$request->getRepresentation()->addFormData("action", "comment");
		$response = $client->send();

		$this->assertStringContains($response->getRepresentation()->getBody(),
				DMLocalizedString::getString("INCORRECT_MATH_RESPONSE"));
		$this->assertNoError($response->getRepresentation()->getBody());
	}

	function testPostingCommentWithModerationEnabled() {
		$this->config_xml->setCommentModerationEnabled(true);
		$this->config_xml->save();

		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/" . TEST_PTR);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$answer = self::getAnswerFromCaptchaInResponse($response);

		// submit comment
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("dmForm[alias]", TEST_ALIAS);
		$request->getRepresentation()->addFormData("dmForm[ptr]", TEST_PTR);
		$request->getRepresentation()->addFormData("dmForm[name]", "Test Poster");
		$request->getRepresentation()->addFormData("dmForm[email]", "test@localhost");
		$request->getRepresentation()->addFormData("dmForm[test]", "test comment");
		$request->getRepresentation()->addFormData("dmForm[response]", $answer);
		$request->getRepresentation()->addFormData("action", "comment");
		$response = $client->send();

		$this->assertStringContains($response->getRepresentation()->getBody(),
				DMLocalizedString::getString("MODERATED_COMMENT_ADDED"));
		$this->assertNoError($response->getRepresentation()->getBody());

		$ds = DMDataStoreFactory::getDataStore();
		$sql = "SELECT COUNT(id) AS count FROM comment WHERE value = 'test comment'";
		$result = $ds->read($sql, array());
		$this->assertEquals(1, $result[0]['count']);
	}

	function testPostingCommentWithModerationDisabled() {
		$this->config_xml->setCommentModerationEnabled(false);
		$this->config_xml->save();

		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/" . TEST_PTR);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$answer = self::getAnswerFromCaptchaInResponse($response);

		// submit comment
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("dmForm[alias]", TEST_ALIAS);
		$request->getRepresentation()->addFormData("dmForm[ptr]", TEST_PTR);
		$request->getRepresentation()->addFormData("dmForm[name]", "Test Poster");
		$request->getRepresentation()->addFormData("dmForm[email]", "test@localhost");
		$request->getRepresentation()->addFormData("dmForm[test]", "test comment");
		$request->getRepresentation()->addFormData("dmForm[response]", $answer);
		$request->getRepresentation()->addFormData("action", "comment");
		$response = $client->send();

		$this->assertStringContains($response->getRepresentation()->getBody(),
				DMLocalizedString::getString("COMMENT_ADDED"));
		$this->assertNoError($response->getRepresentation()->getBody());

		$ds = DMDataStoreFactory::getDataStore();
		$sql = "SELECT COUNT(id) AS count FROM comment WHERE value = 'test comment'";
		$result = $ds->read($sql, array());
		$this->assertEquals(1, $result[0]['count']);
	}

	function testPostingMultipleCommentsNotAllowed() {
		$this->config_xml->setCommentModerationEnabled(false);
		$this->config_xml->save();

		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/" . TEST_PTR);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$answer = self::getAnswerFromCaptchaInResponse($response);

		// submit comment
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("dmForm[alias]", TEST_ALIAS);
		$request->getRepresentation()->addFormData("dmForm[ptr]", TEST_PTR);
		$request->getRepresentation()->addFormData("dmForm[name]", "Test Poster");
		$request->getRepresentation()->addFormData("dmForm[email]", "test@localhost");
		$request->getRepresentation()->addFormData("dmForm[test]", "test comment");
		$request->getRepresentation()->addFormData("dmForm[response]", $answer);
		$request->getRepresentation()->addFormData("action", "comment");
		$response = $client->send();

		$this->assertStringContains($response->getRepresentation()->getBody(),
				DMLocalizedString::getString("COMMENT_ADDED"));
		$this->assertNoError($response->getRepresentation()->getBody());

		$response = $client->send();
		$this->assertStringContains($response->getRepresentation()->getBody(),
				DMLocalizedString::getString("MULTIPLE_COMMENTS_NOT_ALLOWED"));
		$this->assertNoError($response->getRepresentation()->getBody());
	}

	function testCommentFormInvisibleWhenCommentsAreDisabled() {
		$this->config_xml->setCommentingEnabled(false);
		$this->config_xml->save();

		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/" . TEST_PTR);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertStringNotContains($response->getRepresentation()->getBody(),
				"dmCommentName");
	}

	function testPostingRating() {
		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/" . TEST_PTR);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		// submit rating
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("value", 19);
		$request->getRepresentation()->addFormData("max", 100);
		$request->getRepresentation()->addFormData("action", "rate");
		$response = $client->send();

		$this->assertStringContains($response->getRepresentation()->getBody(),
				DMLocalizedString::getString("RATING_ADDED"));
		$this->assertNoError($response->getRepresentation()->getBody());

		// assert rating was stored
		$ds = DMDataStoreFactory::getDataStore();
		$sql = "SELECT COUNT(id) AS count FROM rating WHERE value = 19";
		$result = $ds->read($sql, array());
		$this->assertEquals(1, $result[0]['count']);
	}

	function testPostingInvalidRating() {
		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/" . TEST_PTR);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		// submit rating
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("value", 342);
		$request->getRepresentation()->addFormData("max", 100);
		$request->getRepresentation()->addFormData("action", "rate");
		$response = $client->send();

		$this->assertStringContains($response->getRepresentation()->getBody(),
				DMLocalizedString::getString("RATING_LARGER_THAN_MAX"));
		$this->assertNoError($response->getRepresentation()->getBody());

		// assert rating was stored
		$ds = DMDataStoreFactory::getDataStore();
		$sql = "SELECT COUNT(id) AS count FROM rating WHERE value = 19";
		$result = $ds->read($sql, array());
		$this->assertEquals(1, $result[0]['count']);
	}

	function testPostingMultipleRatingsNotAllowed() {
		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/" . TEST_PTR);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		// submit rating
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("value", 19);
		$request->getRepresentation()->addFormData("max", 100);
		$request->getRepresentation()->addFormData("action", "rate");
		$response = $client->send();

		$request->getRepresentation()->setFormData("value", 37);
		$response = $client->send();

		$this->assertStringContains($response->getRepresentation()->getBody(),
				DMLocalizedString::getString("ALREADY_RATED"));
		$this->assertNoError($response->getRepresentation()->getBody());
	}

	function testRatingFormInvisibleWhenRatingsAreDisabled() {
		$this->config_xml->setRatingEnabled(false);
		$this->config_xml->save();

		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/" . TEST_PTR);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertStringContains($response->getRepresentation()->getBody(),
				"Rating section not inserted");
	}

	function testPostingTagWithModerationEnabled() {
		$this->config_xml->setTagModerationEnabled(true);
		$this->config_xml->save();

		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/" . TEST_PTR);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		// submit tag
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("dmForm[alias]", TEST_ALIAS);
		$request->getRepresentation()->addFormData("dmForm[ptr]", TEST_PTR);
		$request->getRepresentation()->addFormData("dmForm[value]", "test tag");
		$request->getRepresentation()->addFormData("action", "tag");
		$response = $client->send();

		$this->assertStringContains($response->getRepresentation()->getBody(),
				DMLocalizedString::getString("MODERATED_TAG_ADDED"));
		$this->assertNoError($response->getRepresentation()->getBody());

		// assert tag was stored
		$ds = DMDataStoreFactory::getDataStore();
		$sql = "SELECT COUNT(id) AS count FROM tag WHERE value = 'test tag'";
		$result = $ds->read($sql, array());
		$this->assertEquals(1, $result[0]['count']);
	}

	function testPostingTagWithModerationDisabled() {
		$this->config_xml->setTagModerationEnabled(false);
		$this->config_xml->save();

		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/" . TEST_PTR);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		// submit tag
		$request->setMethod(DMHTTPMethod::POST);
		$request->getRepresentation()->addFormData("dmForm[alias]", TEST_ALIAS);
		$request->getRepresentation()->addFormData("dmForm[ptr]", TEST_PTR);
		$request->getRepresentation()->addFormData("dmForm[value]", "test tag");
		$request->getRepresentation()->addFormData("action", "tag");
		$response = $client->send();

		$this->assertStringContains($response->getRepresentation()->getBody(),
				DMLocalizedString::getString("TAG_ADDED"));
		$this->assertNoError($response->getRepresentation()->getBody());

		// assert tag was stored
		$ds = DMDataStoreFactory::getDataStore();
		$sql = "SELECT COUNT(id) AS count FROM tag WHERE value = 'test tag'";
		$result = $ds->read($sql, array());
		$this->assertEquals(1, $result[0]['count']);
	}

	function testTagFormInvisibleWhenTagsAreDisabled() {
		$this->config_xml->setTaggingEnabled(false);
		$this->config_xml->save();

		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/" . TEST_PTR);
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();

		$this->assertStringContains($response->getRepresentation()->getBody(),
				"Tagging section not inserted");
	}


	//////////////////////////////////////////////////////////////////////////
	// GET objects/:alias/:ptr/search
	//////////////////////////////////////////////////////////////////////////

	function testCompoundObjectSearch() {
		$uri = new DMURI(self::getURI() . "/objects" . TEST_ALIAS . "/" . TEST_PTR . "/search");
		$request = new DMHTTPRequest($uri);
		$client = new DMHTTPClient($request);
		$response = $client->send();
		$this->assertNoError($response->getRepresentation()->getBody());
	}

}
