This folder houses the dmBridge test suite. The folder structure is as follows:

tests/          Bootstrap and configuration
    system/     System tests
    unit/       Unit tests

To run the tests, PHPUnit needs to be run with the --bootstrap option pointed
to bootstrap.php in this folder.

Many of the tests depend on the data fixtures supplied with dmulator
(http://code.google.com/p/dmulator/).

Individual modules (located in the ../extensions/modules folder) may have their
own accompanying tests. These, if they exist, are bundled with the module and
have to be run separately.
