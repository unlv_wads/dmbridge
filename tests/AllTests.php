<?php

class AllTests extends PHPUnit_Framework_TestSuite {

	protected function setUp() {
	}

    protected function tearDown() {
	}

	public static function suite() {
		$suite = new PHPUnit_Framework_TestSuite("dmBridge Test Suite");

		$it = new RecursiveDirectoryIterator(dirname(__FILE__));
		foreach (new RecursiveIteratorIterator($it) as $filename) {
			if (substr($filename->__toString(), -8, 8) == "Test.php") {
				$suite->addTestFile($filename->getPathname());
			}
		}
		return $suite;
	}

}
