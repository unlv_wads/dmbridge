<?php

class DMTextFieldTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMTextField
	 */
	protected $object;

	protected function setUp() {
		$this->object = new DMTextField("cats");
		$form = new DMObjectCommentForm();
		$this->object->setForm($form);
	}

	function testEnabled() {
		$this->object->setEnabled(false);
		$this->assertFalse($this->object->isEnabled());
	}

	function testSize() {
		$this->object->setSize(234);
		$this->assertEquals(234, $this->object->getSize());
	}

	function testGetHTMLTag() {
		$this->object->getHTMLTag();
	}

}
