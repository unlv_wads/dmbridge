<?php

class DMPasswordFieldTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMPasswordField
	 */
	protected $object;

	protected function setUp() {
		$this->object = new DMPasswordField("cats");
		$form = new DMObjectCommentForm();
		$this->object->setForm($form);
	}

	function testEnabled() {
		$this->object->setEnabled(false);
		$this->assertFalse($this->object->isEnabled());
	}

	function testGetHTMLTag() {
		$this->object->getHTMLTag();
	}


}
