<?php

class DMRadioFieldTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMRadioField
	 */
	protected $object;

	protected function setUp() {
		$this->object = new DMRadioField("cats");
		$form = new DMObjectCommentForm();
		$this->object->setForm($form);
	}

	function testChecked() {
		$this->object->setChecked(true);
		$this->assertTrue($this->object->isChecked());
	}

	function testEnabled() {
		$this->object->setEnabled(false);
		$this->assertFalse($this->object->isEnabled());
	}

	function testGetHTMLTag() {
		$this->object->getHTMLTag();
	}

}
