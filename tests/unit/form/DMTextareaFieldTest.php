<?php

class DMTextareaFieldTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMTextareaField
	 */
	protected $object;

	protected function setUp() {
		$this->object = new DMTextareaField("cats");
		$form = new DMObjectCommentForm();
		$this->object->setForm($form);
	}

	function testEnabled() {
		$this->object->setEnabled(false);
		$this->assertFalse($this->object->isEnabled());
	}

	function testNumColumns() {
		$this->object->setNumColumns(43);
		$this->assertEquals(43, $this->object->getNumColumns());
	}

	function testNumRows() {
		$this->object->setNumRows(43);
		$this->assertEquals(43, $this->object->getNumRows());
	}

	function testGetHTMLTag() {
		$this->object->getHTMLTag();
	}

}
