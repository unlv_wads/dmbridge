<?php

class DMSelectFieldTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMSelectField
	 */
	protected $object;

	protected function setUp() {
		$this->object = new DMSelectField("cats");
		$form = new DMObjectCommentForm();
		$this->object->setForm($form);
	}

	function testEnabled() {
		$this->object->setEnabled(false);
		$this->assertFalse($this->object->isEnabled());
	}

	function testOptions() {
		$this->object->addOption("lewjrk");
		$this->assertEquals(1, count($this->object->getOptions()));
	}

	function testGetHTMLTag() {
		$this->object->getHTMLTag();
	}

}
