<?php

class DMResultsViewTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMResultsView
	 */
	protected $object;

	protected function setUp() {
		$ts = new DMTemplateSet();
		$tpl = new DMTemplate($ts, "/bla/bla/bla");
		$col = new DMCollection(TEST_ALIAS);
		$query = new DMObjectQuery();
		$this->object = new DMResultsView($tpl, new DMSession(), $col, $query);
	}


	function testCollection() {
		$col = new DMCollection(TEST_ALIAS);
		$this->assertTrue($col->equals($this->object->getCollection()));
	}

	function testFavoritesView() {
		$view = $this->object->getFavoritesView();
		$this->assertInstanceOf("DMFavoritesView", $view);
		$this->assertInstanceOf("DMTemplate", $view->getTemplate());
		$this->assertInstanceOf("DMCollection", $view->getCollection());
	}

	function testGetFields() {
		$this->markTestSkipped("Implemented in DMCollectionTest::testGetResultsViewFields()");
	}

	function getLoginView() {
		$view = $this->object->getLoginView();
		$this->assertInstanceOf("DMLoginView", $view);
		$this->assertInstanceOf("DMTemplate", $view->getTemplate());
		$this->assertInstanceOf("DMCollection", $view->getCollection());
	}

	/**
	 * @todo Write testLogoutURI()
	 */
	function testLogoutURI() {
		$this->markTestIncomplete();
	}

	function testGetNumTileViewColumns() {
		$set = $this->object->getTemplate()->getTemplateSet();
		$set->setNumTileViewColumns(8);
		$this->assertEquals(8, $set->getNumTileViewColumns());
	}

	function testGetQuery() {
		$ts = new DMTemplateSet();
		$tpl = new DMTemplate($ts, "/bla/bla/bla");
		$col = new DMCollection(TEST_ALIAS);
		$query = new DMObjectQuery();
		$view = new DMResultsView($tpl, new DMSession(), $col, $query);
		$this->assertSame($query, $view->getQuery());
	}

	function testRecentlyViewedObjects() {
		$this->markTestSkipped("Tested in DMSessionTest");
	}

	function testSearchView() {
		$view = $this->object->getSearchView();
		$this->assertInstanceOf("DMSearchView", $view);
		$this->assertInstanceOf("DMTemplate", $view->getTemplate());
		$this->assertInstanceOf("DMCollection", $view->getCollection());
	}

	function testGetURIWithNoCollection() {
		$ts = new DMTemplateSet();
		$tpl = new DMTemplate($ts, "/bla/bla/bla");
		$col = new DMCollection(TEST_ALIAS);
		$this->object = new DMResultsView($tpl, new DMSession(), $col);
		$this->assertEquals(DMInternalURI::getURIWithParams("objects"),
				$this->object->getURI());
	}

	function testGetURIWithDefaultCollection() {
		$ts = new DMTemplateSet();
		$tpl = new DMTemplate($ts, "/bla/bla/bla");
		$col = DMCollectionFactory::getCollection("/dmdefault");
		$this->object = new DMResultsView($tpl, new DMSession(), $col);
		$this->assertEquals(DMInternalURI::getURIWithParams("objects"),
				$this->object->getURI());
	}

	function testGetURIWithConcreteCollection() {
		$ts = new DMTemplateSet();
		$tpl = new DMTemplate($ts, "/bla/bla/bla");
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$this->object = new DMResultsView($tpl, new DMSession(), $col);
		$this->assertEquals(
				DMInternalURI::getURIWithParams("objects" . TEST_ALIAS),
				$this->object->getURI());
	}

}

