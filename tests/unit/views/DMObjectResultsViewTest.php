<?php

class DMObjectResultsViewTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMObjectResultsView
	 */
	protected $object;

	protected function setUp() {
		$ts = new DMTemplateSet();
		$tpl = new DMTemplate($ts, "/bla/bla/bla");
		$obj = new DMObject(new DMCollection(TEST_ALIAS), TEST_NORMAL_PTR,
				DMDataStoreFactory::getDataStore(), DMConfigIni::getInstance());
		$query = new DMObjectQuery();
		$this->object = new DMObjectResultsView($tpl, $obj, $query);
	}


	function testCollection() {
		$col = new DMCollection(TEST_ALIAS);
		$this->assertTrue($col->equals($this->object->getCollection()));
	}

	function testFavoritesView() {
		$view = $this->object->getFavoritesView();
		$this->assertInstanceOf("DMFavoritesView", $view);
		$this->assertInstanceOf("DMTemplate", $view->getTemplate());
		$this->assertInstanceOf("DMCollection", $view->getCollection());
	}

	function getLoginView() {
		$view = $this->object->getLoginView();
		$this->assertInstanceOf("DMLoginView", $view);
		$this->assertInstanceOf("DMTemplate", $view->getTemplate());
		$this->assertInstanceOf("DMCollection", $view->getCollection());
	}

	/**
	 * @todo Write testLogoutURI()
	 */
	function testLogoutURI() {
		$this->markTestIncomplete();
	}

	function testQuery() {
		$ts = new DMTemplateSet();
		$tpl = new DMTemplate($ts, "/bla/bla/bla");
		$obj = new DMObject(new DMCollection(TEST_ALIAS), TEST_NORMAL_PTR,
				DMDataStoreFactory::getDataStore(), DMConfigIni::getInstance());
		$query = new DMObjectQuery();
		$view = new DMObjectResultsView($tpl, $obj, $query);
		$this->assertSame($query, $view->getQuery());
	}

	function testRecentlyViewedObjects() {
		$this->markTestSkipped("Tested in DMSessionTest");
	}

	function testSearchView() {
		$view = $this->object->getSearchView();
		$this->assertInstanceOf("DMSearchView", $view);
		$this->assertInstanceOf("DMTemplate", $view->getTemplate());
		$this->assertInstanceOf("DMCollection", $view->getCollection());
	}

	function testGetURI() {
		$this->assertEquals("objects" . TEST_ALIAS . "/search",
				$this->object->getURI()->getParams());
	}

}

