<?php

class DMExternalViewTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMExternalView
	 */
	protected $object;

	protected function setUp() {
		$col = new DMCollection(TEST_ALIAS);
		$this->object = new DMExternalView($col);
	}

	public function testGetCollection() {
		$col = new DMCollection(TEST_ALIAS);
		$view = new DMExternalView($col);
		$this->assertSame($col, $view->getCollection());
	}

}

