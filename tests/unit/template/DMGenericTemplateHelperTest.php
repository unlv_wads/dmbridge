<?php

class DMGenericTemplateHelperTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMGenericTemplateHelper
	 */
	protected $object;

	protected function setUp() {
		$ts = new DMTemplateSet();
		$tpl = new DMTemplate($ts, "/bla");
		$obj = DMObjectFactory::getObject(new DMCollection(TEST_ALIAS), TEST_PTR);
		$view = new DMObjectView($tpl, new DMSession(), $obj);
		$this->object = new DMGenericTemplateHelper($view, new DMSession());
	}

	function testCallingNonexistingModuleMethod() {
		$this->setExpectedException("DMNoSuchMethodException");
		$this->object->sldfjkaslfjk();
	}

	function testCallingExistingModuleMethod() {
		$dmm = DMModuleManager::getInstance();
		$module = $dmm->getModuleByName("UNLVSpatial");
		$dmm->setModuleEnabled($module, true);
		$this->object->getHtmlGoogleMapsSpatialSearch(50, -130);
	}

	function testGetHtmlAtomFeedURL() {
		$this->object->getHtmlAtomFeedURL();
	}

	function testHtmlBodyScripts() {
		$src = "/cats";
		$type = "dogs/arf";
		$this->object->addBodyScriptTag($src, $type);
		$actual = $this->object->getHtmlBodyScriptTags();
		$expected = sprintf('<script type="%s" src="%s"></script>', $type, $src);
		$this->assertEquals($actual, $expected);

		$this->object->removeBodyScriptTag($src);
		$this->assertFalse(strpos($this->object->getHtmlBodyScriptTags(), $expected));
	}

	function testGetHtmlCollectionsAsPulldown() {
		$this->object->getHtmlCollectionsAsPulldown();
	}

	function testGetHtmlFormattedFlash() {
		$this->object->getHtmlFormattedFlash();
	}

	function testHtmlHeadScripts() {
		$src = "/cats";
		$type = "dogs/arf";
		$this->object->addHeadScriptTag($src, $type);
		$actual = $this->object->getHtmlHeadScriptTags();
		$expected = sprintf('<script type="%s" src="%s"></script>', $type, $src);
		$this->assertEquals($actual, $expected);

		$this->object->removeHeadScriptTag($src);
		$this->assertFalse(strpos($this->object->getHtmlHeadScriptTags(), $expected));
	}

	function testGetHtmlLoginPageLink() {
		$this->object->getHtmlLoginPageLink();
	}

	function testGetHtmlLoginForm() {
		$this->object->getHtmlLoginForm();
	}

	function testHtmlMetaTags() {
		$name = "aldfjwerwer";
		$content = "alsdkfjalsjk";
		$equiv = "whatever";
		$this->object->addMetaTag($content, $name, $equiv);
		$actual = $this->object->getHtmlMetaTags();
		$expected = sprintf('<meta content="%s" name="%s" http-equiv="%s">',
				$content, $name, $equiv);
		$this->assertTrue(strpos($actual, $expected) !== false);

		$this->object->removeMetaTag($name);
		$this->assertFalse(strpos($this->object->getHtmlMetaTags(), $expected));
	}

	function testGetHtmlRecentlyViewedObjectsAsList() {
		$this->object->getHtmlRecentlyViewedObjectsAsList(5);
	}

	function testGetHtmlRecentlyViewedObjectsAsListWithThumbs() {
		$this->object->getHtmlRecentlyViewedObjectsAsList(5, true);
	}

	function testHtmlStylesheets() {
		$type = "dogs/arf";
		$media = "youtube";
		$href = "/cats";
		$this->object->addStylesheetTag($href, $type, $media);
		$actual = $this->object->getHtmlStylesheetTags();
		$expected = sprintf(
				'<link rel="stylesheet" type="%s" media="%s" href="%s">',
				$type, $media, $href);
		$this->assertEquals($actual, $expected);

		$this->object->removeStylesheetTag($href);
		$this->assertFalse(strpos($this->object->getHtmlStylesheetTags(), $expected));
	}

	function testGetHtmlTagsAsCloud() {
		$this->object->getHtmlTagsAsCloud();
	}

	function testGetHtmlTagsAsCloudWithCustomLimit() {
		$this->object->getHtmlTagsAsCloud(3);
	}

	function testGetHtmlTermLinkedToSearch() {
		$this->object->getHtmlTermLinkedToSearch("bla", new DMObjectQuery());
	}

	function testGetHtmlVocabularyAsCloud() {
		$this->object->getHtmlVocabularyAsCloud(array(new DMDCElement("title")));
	}

	function testGetHtmlVocabularyAsList() {
		$this->object->getHtmlVocabularyAsList(new DMDCElement("cats"));
	}

	function testGetHtmlVocabularyForSelect() {
		$this->object->getHtmlVocabularyForSelect(new DMDCElement("cats"));
	}

	function testView() {
		$view = new DMExternalView(DMCollectionFactory::getCollection(TEST_ALIAS));
		$this->object->setView($view);
		$this->assertSame($view, $this->object->getView());
	}

}
