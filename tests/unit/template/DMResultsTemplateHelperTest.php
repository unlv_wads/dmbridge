<?php

class DMResultsTemplateHelperTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMResultsTemplateHelper
	 */
	protected $object;

	protected function setUp() {
		$ts = new DMTemplateSet();
		$tpl = new DMTemplate($ts, "/bla");
		$col = new DMCollection(TEST_ALIAS);
		$query = new DMObjectQuery();
		$query->addCollection($col);
		$view = new DMResultsView($tpl, new DMSession(), $col, $query);
		$this->object = new DMResultsTemplateHelper($view, new DMSession());
	}

	function testHtmlBodyScripts() {
		$src = "/cats";
		$type = "dogs/arf";
		$this->object->addBodyScriptTag($src, $type);
		$actual = $this->object->getHtmlBodyScriptTags();
		$expected = sprintf('<script type="%s" src="%s"></script>', $type, $src);
		$this->assertEquals($actual, $expected);

		$this->object->removeBodyScriptTag($src);
		$this->assertFalse(strpos($this->object->getHtmlBodyScriptTags(), $expected));
	}

	function testGetHtmlFacetsAsUL() {
		$this->object->getHtmlFacetsAsUL();
	}

	function testGetHtmlFacetsAsULWithCustomCutoff() {
		$this->object->getHtmlFacetsAsUL(3);
	}

	function testHtmlHeadScripts() {
		$src = "/cats";
		$type = "dogs/arf";
		$this->object->addHeadScriptTag($src, $type);
		$actual = $this->object->getHtmlHeadScriptTags();
		$expected = sprintf('<script type="%s" src="%s"></script>', $type, $src);
		$this->assertEquals($actual, $expected);

		$this->object->removeHeadScriptTag($src);
		$this->assertFalse(strpos($this->object->getHtmlHeadScriptTags(), $expected));
	}

	function testGetHtmlLinkedSuggestion() {
		$this->object->getHtmlLinkedSuggestion();
	}

	function testMetaTags() {
		$name = "aldfjwerwer";
		$content = "alsdkfjalsjk";
		$equiv = "whatever";
		$this->object->addMetaTag($content, $name, $equiv);
		$actual = $this->object->getHtmlMetaTags();
		$expected = sprintf('<meta content="%s" name="%s" http-equiv="%s">',
				$content, $name, $equiv);
		$this->assertTrue(strpos($actual, $expected) !== false);

		$this->object->removeMetaTag($name);
		$this->assertFalse(strpos($this->object->getHtmlMetaTags(), $expected));
	}

	function testGetHtmlPageLinks() {
		$this->object->getHtmlPageLinks();
	}

	function testGetHtmlResults() {
		$this->object->getHtmlResults();
	}

	function tetHtmlResultsAsGridWithThumbs() {
		$this->object->getHtmlResultsAsGrid();
	}

	function tetHtmlResultsAsGridWithoutThumbs() {
		$this->object->getHtmlResultsAsGrid(false);
	}

	function testGetHtmlResultsAsListWithThumbs() {
		$this->object->getHtmlResultsAsList();
	}

	function testGetHtmlResultsAsListWithoutThumbs() {
		$this->object->getHtmlResultsAsList(false);
	}

	function testGetHtmlResultsAsTilesWithThumbs() {
		$this->object->getHtmlResultsAsTiles();
	}

	function testGetHtmlResultsAsTilesWithoutThumbs() {
		$this->object->getHtmlResultsAsList(false);
	}

	function testGetHtmlSearchTerms() {
		$this->object->getHtmlSearchTerms();
	}

	function testGetHtmlSortLinksAsUL() {
		$this->object->getHtmlSortLinksAsUL();
	}

	function testHtmlStylesheets() {
		$type = "dogs/arf";
		$media = "youtube";
		$href = "/cats";
		$this->object->addStylesheetTag($href, $type, $media);
		$actual = $this->object->getHtmlStylesheetTags();
		$expected = sprintf(
				'<link rel="stylesheet" type="%s" media="%s" href="%s">',
				$type, $media, $href);
		$this->assertEquals($actual, $expected);

		$this->object->removeStylesheetTag($href);
		$this->assertFalse(strpos($this->object->getHtmlStylesheetTags(), $expected));
	}

	function testGetHtmlViewLinks() {
		$this->object->getHtmlViewLinks();
	}

}

