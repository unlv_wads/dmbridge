<?php

class DMTemplateSetTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMTemplateSet
	 */
	protected $object;

	protected function setUp() {
		$this->object = new DMTemplateSet();
	}


	function testConstructorWithNewTemplateSet() {
		$this->assertNull($this->object->getName());
	}

	function testConstructorWithExistingTemplateSet() {
		$ts = new DMTemplateSet(1);
		$this->assertEquals("Basic", $ts->getName());
	}

	function testToString() {
		$expected = "asldfjaslfdjk";
		$this->object->setName($expected);
		$this->assertEquals($expected, $this->object->__toString());
	}

	function testAuthorizedCollections() {
		$col = new DMCollection(TEST_ALIAS);
		$this->object->addAuthorizedCollection($col);
		$collections = $this->object->getAuthorizedCollections();
		$this->assertSame($col, $collections[0]);
		$this->object->unsetAuthorizedCollections();
		$this->assertEquals(0, count($this->object->getAuthorizedCollections()));
	}

	function testIsAuthorizedToViewAuthorizedCollectionWhenExplicitlyAuthorized() {
		$col1 = new DMCollection(TEST_ALIAS);
		$this->object->addAuthorizedCollection($col1);
		$this->assertTrue($this->object->isAuthorizedToViewCollection($col1));
	}

	function testIsAuthorizedToViewCollectionWhenNoneExplicitlyAuthorized() {
		$col1 = new DMCollection(TEST_ALIAS);
		$this->assertTrue($this->object->isAuthorizedToViewCollection($col1));
	}

	function testIsAuthorizedToViewUnauthorizedCollection() {
		$col1 = new DMCollection(TEST_ALIAS);
		$col2 = new DMCollection(TEST_DYNAMIC_ALIAS);
		$this->object->addAuthorizedCollection($col2);
		$this->assertFalse($this->object->isAuthorizedToViewCollection($col1));
	}

	function testDefaultCollection() {
		$col = new DMCollection(TEST_ALIAS);
		$this->object->setDefaultCollection($col);
		$this->assertSame($col, $this->object->getDefaultCollection());
	}

	function testSettingDmdefaultCollectionAsDefaultCollectionThrowsException() {
		$col = new DMCollection("/dmdefault");
		$this->setExpectedException("DMIllegalArgumentException");
		$this->object->setDefaultCollection($col);
	}

	function testGetErrorTemplateWhenCustomTemplateExists() {
		$this->object->setName("Basic");
		$tpl = $this->object->getErrorTemplate();
		$this->assertTrue(strpos($tpl->getAbsolutePathname(), "basic") !== false);
	}

	function testGetErrorTemplateWhenCustomTemplateDoesNotExist() {
		$this->object->setName("adsfljasf");
		$tpl = $this->object->getErrorTemplate();
		$this->assertTrue(strpos($tpl->getAbsolutePathname(), "system") !== false);
	}

	function testGridViewFields() {
		$this->markTestIncomplete();
		$f = new DMDCElement("cats");
		$this->object->addGridViewField($f);
		$this->assertEquals(1, count($this->object->getGridViewFields()));
		$this->object->unsetGridViewFields();
		$this->assertEquals(0, count($this->object->getGridViewFields()));
	}

	function testID() {
		$this->object->setID(24);
		$this->assertEquals(24, $this->object->getID());
	}

	function testName() {
		$expected = "asldfjaslfd;jk";
		$this->object->setName($expected);
		$this->assertEquals($expected, $this->object->getName());
	}

	function testNumResultsPerPage() {
		$this->object->setNumResultsPerPage(43);
		$this->assertEquals(43, $this->object->getNumResultsPerPage());
	}

	function testNumTileViewColumns() {
		$this->object->setNumTileViewColumns(6);
		$this->assertEquals(6, $this->object->getNumTileViewColumns());
	}

	function testGetPath() {
		$this->object->setName("Cats N Stuff");
		$expected = dirname($_SERVER['PHP_SELF']) . "/templates/cats_n_stuff";
		$this->assertEquals($expected, $this->object->getPath());
	}

	function testGetPathWithCustomPath() {
		$expected = "/cats/cats/cats";
		$this->object->setPath($expected);
		$this->assertEquals($expected, $this->object->getPath());
	}

	function testGetTemplates() {
		$templates = $this->object->getTemplates();
		$this->assertEquals(9, count($templates));
	}

	function testGetTemplateAtValidPathname() {
		$path = "/templates/object/results_faceted.html.php";
		$tpl = $this->object->getTemplateAtPathname($path);
		$this->assertInstanceOf("DMTemplate", $tpl);
	}

	function testGetTemplateAtInvalidPathname() {
		$path = "/cats";
		$tpl = $this->object->getTemplateAtPathname($path);
		$this->assertNull($tpl);
	}

}
