<?php

class DMSearchTemplateHelperTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMSearchTemplateHelper
	 */
	protected $object;

	protected function setUp() {
		$ts = new DMTemplateSet();
		$tpl = new DMTemplate($ts, "/bla");
		$col = new DMCollection(TEST_ALIAS);
		$view = new DMSearchView($tpl, new DMSession(), $col);
		$this->object = new DMSearchTemplateHelper($view, new DMSession());
	}

	function testHtmlBodyScripts() {
		$src = "/cats";
		$type = "dogs/arf";
		$this->object->addBodyScriptTag($src, $type);
		$actual = $this->object->getHtmlBodyScriptTags();
		$expected = sprintf('<script type="%s" src="%s"></script>', $type, $src);
		$this->assertEquals($actual, $expected);

		$this->object->removeBodyScriptTag($src);
		$this->assertFalse(strpos($this->object->getHtmlBodyScriptTags(), $expected));
	}

	function testGetHtmlDateSearch() {
		$this->object->getHtmlDateSearch();
	}

	function testGetHtmlFieldSearch() {
		$this->object->getHtmlFieldSearch();
	}

	function testHtmlHeadScripts() {
		$src = "/cats";
		$type = "dogs/arf";
		$this->object->addHeadScriptTag($src, $type);
		$actual = $this->object->getHtmlHeadScriptTags();
		$expected = sprintf('<script type="%s" src="%s"></script>', $type, $src);
		$this->assertEquals($actual, $expected);

		$this->object->removeHeadScriptTag($src);
		$this->assertFalse(strpos($this->object->getHtmlHeadScriptTags(), $expected));
	}

	function testHtmlMetaTags() {
		$name = "aldfjwerwer";
		$content = "alsdkfjalsjk";
		$equiv = "whatever";
		$this->object->addMetaTag($content, $name, $equiv);
		$actual = $this->object->getHtmlMetaTags();
		$expected = sprintf('<meta content="%s" name="%s" http-equiv="%s">',
				$content, $name, $equiv);
		$this->assertTrue(strpos($actual, $expected) !== false);

		$this->object->removeMetaTag($name);
		$this->assertFalse(strpos($this->object->getHtmlMetaTags(), $expected));
	}

	function testGetHtmlProximitySearch() {
		$this->object->getHtmlProximitySearch();
	}

	function testHtmlStylesheets() {
		$type = "dogs/arf";
		$media = "youtube";
		$href = "/cats";
		$this->object->addStylesheetTag($href, $type, $media);
		$actual = $this->object->getHtmlStylesheetTags();
		$expected = sprintf(
				'<link rel="stylesheet" type="%s" media="%s" href="%s">',
				$type, $media, $href);
		$this->assertEquals($actual, $expected);

		$this->object->removeStylesheetTag($href);
		$this->assertFalse(strpos($this->object->getHtmlStylesheetTags(), $expected));
	}

}

