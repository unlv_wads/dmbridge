<?php

require_once(dirname(__FILE__) . "/../../DMTestDataSeeder.php");

/**
 * Most of these tests aren't super thorough - the idea is just to check for
 * errors.
 */
class DMControlPanelTemplateHelperTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMObjectTemplateHelper
	 */
	protected $object;

	protected function setUp() {
		$ts = new DMTemplateSet();
		$tpl = new DMTemplate($ts, "/bla");
		$obj = DMObjectFactory::getObject(new DMCollection(TEST_ALIAS), TEST_PTR);
		$view = new DMObjectView($tpl, new DMSession(), $obj);
		$this->object = new DMObjectTemplateHelper($view, new DMSession());
	}

	function testGetHtmlAddFavoriteButton() {
		$result = $this->object->getHtmlAddFavoriteButton();
	}

	function testHtmlBodyScripts() {
		$src = "/cats";
		$type = "dogs/arf";
		$this->object->addBodyScriptTag($src, $type);
		$actual = $this->object->getHtmlBodyScriptTags();
		$expected = sprintf('<script type="%s" src="%s"></script>', $type, $src);
		$this->assertEquals($actual, $expected);

		$this->object->removeBodyScriptTag($src);
		$this->assertFalse(strpos($this->object->getHtmlBodyScriptTags(), $expected));
	}

	function testGetHtmlChildLinksAsListWithThumbs() {
		$result = $this->object->getHtmlChildLinksAsList();
	}

	function testGetHtmlChildLinksAsListWithoutThumbs() {
		$result = $this->object->getHtmlChildLinksAsList(false);
	}

	function testGetHtmlChildLinksAsPulldown() {
		$result = $this->object->getHtmlChildLinksAsPulldown();
	}

	function testGetHtmlCommentsSectionWithCommentingDisabled() {
		DMConfigXML::getInstance()->setCommentingEnabled(false);
		$result = $this->object->getHtmlCommentsSection();
	}

	function testGetHtmlCommentsSectionWithCommentingEnabledAndComments() {
		$seeder = new DMTestDataSeeder();
		$seeder->seed();
		DMConfigXML::getInstance()->setCommentingEnabled(true);
		$result = $this->object->getHtmlCommentsSection();
	}

	function testGetHtmlCommentsSectionWithCommentingEnabledAndNoComments() {
		$seeder = new DMTestDataSeeder();
		$seeder->wipe();
		DMConfigXML::getInstance()->setCommentingEnabled(true);
		$this->object->getHtmlCommentsSection();
	}

	function testGetHtmlCompoundObjectSearchForm() {
		$result = $this->object->getHtmlCompoundObjectSearchForm();
	}

	function testGetHtmlCompoundObjectSearchFormWithNonCompoundObject() {
		$this->object->getView()->setObject(
				DMObjectFactory::getObject(new DMCollection(TEST_ALIAS), TEST_NORMAL_PTR));
		$result = $this->object->getHtmlCompoundObjectSearchForm("Cats!");
		$this->assertNull($result);
	}

	function testGetHtmlCompoundObjectSearchFormWithNoFullText() {
		$this->object->getView()->setObject(
				DMObjectFactory::getObject(new DMCollection(TEST_ALIAS), TEST_NORMAL_PTR));
		$result = $this->object->getHtmlCompoundObjectSearchForm("Cats!");
		$this->assertNull($result);
	}

	function testGetHtmlCompoundObjectSearchFormWithCustomButtonText() {
		$result = $this->object->getHtmlCompoundObjectSearchForm("Cats!");
		$this->assertTrue(strpos($result, "Cats!") !== false);
	}

	function testGetHtmlCompoundObjectPageNumbersInSearchResultsAsString() {
		$result = $this->object->getHtmlCompoundObjectPageNumbersInSearchResultsAsString();
	}

	function testGetHtmlCompoundPageFlipLinks() {
		$result = $this->object->getHtmlCompoundPageFlipLinks();
	}

	function testGetHtmlDownloadImageLink() {
		$result = $this->object->getHtmlDownloadImageLink();
	}

	function testGetHtmlDownloadImageLinkWithCustomResolutions() {
		$result = $this->object->getHtmlDownloadImageLink(array(300, 400));
	}

	function testHtmlHeadScripts() {
		$src = "/cats";
		$type = "dogs/arf";
		$this->object->addHeadScriptTag($src, $type);
		$actual = $this->object->getHtmlHeadScriptTags();
		$expected = sprintf('<script type="%s" src="%s"></script>', $type, $src);
		$this->assertEquals($actual, $expected);

		$this->object->removeHeadScriptTag($src);
		$this->assertFalse(strpos($this->object->getHtmlHeadScriptTags(), $expected));
	}

	function testGetHtmlHighlightedFullText() {
		$result = $this->object->getHtmlHighlightedFullText();
	}

	function testGetHtmlMetadataAsDL() {
		$result = $this->object->getHtmlMetadataAsDL();
	}

	function testGetHtmlMetadataAsMetaTags() {
		$result = $this->object->getHtmlMetadataAsMetaTags();
	}

	function testHtmlMetaTags() {
		$name = "aldfjwerwer";
		$content = "alsdkfjalsjk";
		$equiv = "whatever";
		$this->object->addMetaTag($content, $name, $equiv);
		$actual = $this->object->getHtmlMetaTags();
		$expected = sprintf('<meta content="%s" name="%s" http-equiv="%s">',
				$content, $name, $equiv);
		$this->assertTrue(strpos($actual, $expected) !== false);

		$this->object->removeMetaTag($name);
		$this->assertFalse(strpos($this->object->getHtmlMetaTags(), $expected));
	}

	function testGetHtmlPreviousNextLinks() {
		$result = $this->object->getHtmlPreviousNextLinks();
	}

	function testGetHtmlRatingsSectionWithRatingsDisabled() {
		DMConfigXML::getInstance()->setRatingEnabled(false);
		$result = $this->object->getHtmlRatingsSection(0, 100, 20);
	}

	function testGetHtmlRatingsSectionWithRatingsEnabledAndRatings() {
		$seeder = new DMTestDataSeeder();
		$seeder->seed();
		DMConfigXML::getInstance()->setRatingEnabled(true);
		$result = $this->object->getHtmlRatingsSection(0, 100, 20);
	}

	function testGetHtmlRatingsSectionWithRatingsEnabledAndNoRatings() {
		$seeder = new DMTestDataSeeder();
		$seeder->wipe();
		DMConfigXML::getInstance()->setRatingEnabled(true);
		$this->object->getHtmlRatingsSection(0, 100, 20);
	}

	function testGetHtmlResultsAsUL() {
		$this->object->getHtmlResultsAsUL();
	}

	function testGetHtmlResultsAsULWithoutThumbs() {
		$this->object->getHtmlResultsAsUL(false);
	}

	function testGetHtmlResultsAsULWithMax10() {
		$this->object->getHtmlResultsAsUL(true, 10);
	}

	function testHtmlStylesheets() {
		$type = "dogs/arf";
		$media = "youtube";
		$href = "/cats";
		$this->object->addStylesheetTag($href, $type, $media);
		$actual = $this->object->getHtmlStylesheetTags();
		$expected = sprintf(
				'<link rel="stylesheet" type="%s" media="%s" href="%s">',
				$type, $media, $href);
		$this->assertEquals($actual, $expected);

		$this->object->removeStylesheetTag($href);
		$this->assertFalse(strpos($this->object->getHtmlStylesheetTags(), $expected));
	}

	function testGetHtmlTaggingSectionWithTaggingDisabled() {
		DMConfigXML::getInstance()->setTaggingEnabled(false);
		$result = $this->object->getHtmlTaggingSection();
	}

	function testGetHtmlTaggingSectionWithTaggingEnabledAndTags() {
		$seeder = new DMTestDataSeeder();
		$seeder->seed();
		DMConfigXML::getInstance()->setTaggingEnabled(true);
		$this->object->getHtmlTaggingSection();
	}

	function testGetHtmlTaggingSectionWithTaggingEnabledAndNoTags() {
		$seeder = new DMTestDataSeeder();
		$seeder->wipe();
		DMConfigXML::getInstance()->setTaggingEnabled(true);
		$this->object->getHtmlTaggingSection();
	}

	function testGetHtmlViewer() {
		$this->object->getHtmlViewer();
	}

	function testGetHtmlViewerWithCustomDimensions() {
		$this->object->getHtmlViewer(234, 205);
	}

}

