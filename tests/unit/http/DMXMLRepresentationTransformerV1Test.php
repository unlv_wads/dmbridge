<?php

class DMXMLRepresentationTransformerV1Test extends PHPUnit_Framework_TestCase {

	/**
	 * @var DOMDocument
	 */
	protected $dxml;

	/**
	 * @var DMXMLRepresentationTransformerV1
	 */
	protected $object;

	private static function getXSDRoot() {
		return dirname(__FILE__) . "/../../../includes/schema/1/xml";
	}

	protected function setUp() {
		$this->object = new DMXMLRepresentationTransformerV1();
		$this->dxml = new DOMDocument("1.0", "utf-8");
	}

	protected function assertValidXML($xml, $schema = null) {
		$this->assertTrue($this->dxml->loadXML($xml));
		if ($schema) {
			$this->assertTrue(
				$this->dxml->schemaValidate(self::getXSDRoot() . "/" . $schema));
		}
	}

	function testTransformCollection() {
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$result = $this->object->transformCollection($col);
		$this->assertValidXML($result, "collection_view.xsd");
	}

	function testTransformCollections() {
		$collections = array(
			DMCollectionFactory::getCollection(TEST_ALIAS)
		);
		$result = $this->object->transformCollections($collections);
		$this->assertValidXML($result, "collections_list.xsd");
	}

	function testTransformComment() {
		$comment = new DMComment();
		$comment->setID(5);
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$comment->setObject($obj);

		$result = $this->object->transformComment($comment);
		$this->assertValidXML($result, "comment_view.xsd");
	}

	function testTransformComments() {
		$c1 = new DMComment();
		$c1->setId(50);
		$c2 = new DMComment();
		$c2->setId(520);
		$comments = array($c1, $c2);
		$result = $this->object->transformComments($comments, 1, 20, 2);
		$this->assertValidXML($result, "comments_list.xsd");
	}

	function testTransformCommentsPagination() {
		$comments = array();
		for ($i = 1; $i < 100; $i++) {
			$comment = new DMComment();
			$comment->setID($i);
			$comments[] = $comment;
		}
		// page 1
		$result = $this->object->transformComments(
				array_slice($comments, 0, 20), 1, 20, 100);
		$this->dxml->loadXML($result);
		$dxp = new DOMXPath($this->dxml);
		$dxp->registerNamespace("unlv", "http://digital.library.unlv.edu");
		$result = $dxp->query("//unlv:comments");
		$this->assertEquals(1, $result->item(0)->getAttribute("page"));
		$this->assertEquals(20, $result->item(0)->getAttribute("perPage"));
		$this->assertEquals(5, $result->item(0)->getAttribute("numPages"));
		$this->assertEquals(100, $result->item(0)->getAttribute("total"));
		$result = $dxp->query("//unlv:dmComment[1]/@id");
		$this->assertEquals(1, $result->item(0)->value);

		// page 2
		$result = $this->object->transformComments(
				array_slice($comments, 20, 20), 2, 20, 100);
		$this->dxml->loadXML($result);
		$dxp = new DOMXPath($this->dxml);
		$dxp->registerNamespace("unlv", "http://digital.library.unlv.edu");
		$result = $dxp->query("//unlv:comments");
		$this->assertEquals(2, $result->item(0)->getAttribute("page"));
		$this->assertEquals(20, $result->item(0)->getAttribute("perPage"));
		$this->assertEquals(5, $result->item(0)->getAttribute("numPages"));
		$this->assertEquals(100, $result->item(0)->getAttribute("total"));
		$result = $dxp->query("//unlv:dmComment[1]/@id");
		$this->assertEquals(21, $result->item(0)->value);
	}

	function testTransformException() {
		$e = new DMException("uh oh");
		$result = $this->object->transformException($e);
		$this->assertValidXML($result, "exception.xsd");
	}

	function testTransformObject() {
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$result = $this->object->transformObject($obj);
		$this->assertValidXML($result, "object_view.xsd");
	}

	function testTransformObjectComments() {
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$result = $this->object->transformObjectComments($obj, 1, 20);
		$this->assertValidXML($result, "object_comments.xsd");
	}

	function testTransformObjectRating() {
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$result = $this->object->transformObjectRating($obj);
		$this->assertValidXML($result, "object_rating.xsd");
	}

	function testTransformObjectTags() {
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$result = $this->object->transformObjectTags($obj);
		$this->assertValidXML($result, "object_tags.xsd");
	}

	function testTransformRatings() {
		$q = new DMObjectQuery();
		$result = $this->object->transformRatings($q);
		$this->assertValidXML($result, "objects_list_ratings.xsd");
	}

	function testTransformResults() {
		$q = new DMObjectQuery();
		$result = $this->object->transformResults($q);
		$this->assertValidXML($result, "objects_list.xsd");
	}

	function testTransformStatus() {
		$result = $this->object->transformStatus();
		$this->assertValidXML($result, "status.xsd");
	}

	function testTransformTag() {
		$tag = new DMTag();
		$tag->setID(50);
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$tag->setObject($obj);
		$result = $this->object->transformTag($tag);
		$this->assertValidXML($result, "tag_view.xsd");
	}

	function testTransformTagCounts() {
		$tags = array('cats' => 5, 'dogs' => 10);
		$result = $this->object->transformTagCounts($tags, 1, 20, 2);
		$this->assertValidXML($result, "tag_counts.xsd");
	}

	function testTransformTagCountsPagination() {
		$tags = array();
		for ($i = 0; $i < 100; $i++) {
			$tags[$i] = md5($i);
		}

		// page 1
		$result = $this->object->transformTagCounts($tags, 1, 20, 100);
		$this->dxml->loadXML($result);
		$dxp = new DOMXPath($this->dxml);
		$dxp->registerNamespace("unlv", "http://digital.library.unlv.edu");
		$result = $dxp->query("//unlv:tags");
		$this->assertEquals(1, $result->item(0)->getAttribute("page"));
		$this->assertEquals(20, $result->item(0)->getAttribute("perPage"));
		$this->assertEquals(5, $result->item(0)->getAttribute("numPages"));
		$this->assertEquals(100, $result->item(0)->getAttribute("total"));

		// page 2
		$result = $this->object->transformTagCounts($tags, 2, 20, 100);
		$this->dxml->loadXML($result);
		$dxp = new DOMXPath($this->dxml);
		$dxp->registerNamespace("unlv", "http://digital.library.unlv.edu");
		$result = $dxp->query("//unlv:tags");
		$this->assertEquals(2, $result->item(0)->getAttribute("page"));
		$this->assertEquals(20, $result->item(0)->getAttribute("perPage"));
		$this->assertEquals(5, $result->item(0)->getAttribute("numPages"));
		$this->assertEquals(100, $result->item(0)->getAttribute("total"));
	}

	function testTransformVocabularyFrequencies() {
		$freqs = array();
		$result = $this->object->transformVocabularyFrequencies($freqs);
		$this->assertValidXML($result, "vocabulary_frequency.xsd");
	}

	function testTransformVocabularySuggestions() {
		$words = array('cats', 'dogs');
		$result = $this->object->transformVocabularySuggestions($words);
		$this->assertValidXML($result, "vocabulary_suggestions.xsd");
	}

}
