<?php

class DMHTTPRequestTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMHTTPRequest
	 */
	protected $object;

	protected function setUp() {
		$this->object = new DMHTTPRequest();
	}

	function testGetCurrent() {
		// not testable
	}

	function testRedirectToParams() {
		// not testable
	}

	function testRedirectToURI() {
		// not testable
	}

	function testReload() {
		// not testable
	}

	function testHeaders() {
		$this->object->addHeader("head", "shoulders");
		$expected = array(
			array(
				'key' => 'head',
				'value' => 'shoulders'
			)
		);
		$this->assertEquals($expected, $this->object->getHeaders());
		$this->object->unsetHeaders();
		$this->assertEquals(0, count($this->object->getHeaders()));
	}

	function testMethod() {
		$method = DMHTTPMethod::TRACE;
		$this->object->setMethod($method);
		$this->assertEquals($method, $this->object->getMethod());
	}

	function testPassword() {
		$password = "adfkjaslfdj";
		$this->object->setPassword($password);
		$this->assertEquals($password, $this->object->getPassword());
	}

	function testRedirectedURI() {
		$uri = new DMInternalURI("http://blabla.com/");
		$this->object->setURI($uri);
		$this->assertSame($uri, $this->object->getRedirectedURI());
	}

	function testSession() {
		$session = new DMSession();
		$this->object->setSession($session);
		$this->assertSame($session, $this->object->getSession());
	}

	function testURI() {
		$uri = new DMURI("http://blablabla.com");
		$this->object->setURI($uri);
		$this->assertTrue($uri->equals($this->object->getURI()));
	}

	function testUsername() {
		$expected = "adfkjaslfdj";
		$this->object->setUsername($expected);
		$this->assertEquals($expected, $this->object->getUsername());
	}

}
