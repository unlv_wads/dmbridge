<?php

class DMInternalURITest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMInternalURI
	 */
	protected $object;

	/**
	 * @var string
	 */
	protected $uri = "http://user:pass@www.bla.com:8080/bla/bla/cheese.html?q=cats&q=dogs#whee";

	protected function setUp() {
		$this->object = new DMInternalURI($this->uri);
	}

	function testGetResourceURIInTemplateEngine() {
		$params = "bla/bla";
		$uri = DMInternalURI::getResourceURI(DMBridgeComponent::TemplateEngine, $params);
		$this->assertEquals($uri, DMInternalURI::getURIWithParams($params));
	}

	function testGetResourceURIInHTTPAPI() {
		$params = "bla/bla";
		$uri = DMInternalURI::getResourceURI(DMBridgeComponent::HTTPAPI, $params);
		$this->assertEquals($uri, DMInternalURI::getURIWithParams("api/1/" . $params));
	}

	function testGetResourceURIWithExtension() {
		$params = "bla/bla";
		$uri = DMInternalURI::getResourceURI(DMBridgeComponent::TemplateEngine, $params, "bla");
		$this->assertEquals($uri, DMInternalURI::getURIWithParams($params, array(), "bla"));
	}

	function testGetURIWithParams() {
		$path = "/dmbridge";
		DMConfigIni::getInstance()->setString("dmbridge.base_uri_path", $path);

		$params = "a/b/c";
		$query = array(
			'cats' => 'no',
			'dogs' => 'yes'
		);
		$expected_query = array(
			array(
				'key' => 'cats',
				'value' => 'no'
			),
			array(
				'key' => 'dogs',
				'value' => 'yes'
			)
		);
		$extension = "asdf";
		
		$uri = DMInternalURI::getURIWithParams($params, $query, $extension);		
		$this->assertEquals($path . "/" . $params, $uri->getPath());
		$this->assertEquals($params, $uri->getParams());
		$this->assertEquals($expected_query, $uri->getQuery());
		$this->assertEquals($extension, $uri->getExtension());
	}

	function testGetAbsoluteURIAsString() {
		$this->assertEquals($this->uri, $this->object->getAbsoluteURIAsString());
	}

	function testGetAbsoluteURIAsStringWithNoHostReturnsNull() {
		$this->object->setHost("");
		$this->assertNull($this->object->getAbsoluteURIAsString());
	}

	function testGetAbsoluteURIAsStringWithNoSchemeReturnsNull() {
		$this->object->setScheme("");
		$this->assertNull($this->object->getAbsoluteURIAsString());
	}

	function testExtension() {
		$this->assertEquals("html", $this->object->getExtension());
	}

	function testParams() {
		$this->assertEquals("bla/bla/cheese", $this->object->getParams());
	}

	function testGetRoute() {
		$this->markTestSkipped("getRoute() will be tested in the system tests");
	}

}
