<?php

class DMRouteTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMRoute
	 */
	protected $object;

	protected function setUp() {
		$this->object = new DMRoute("class", "method");
	}

	function testConstructor() {
		$this->assertEquals("class", $this->object->getController());
		$this->assertEquals("method", $this->object->getMethod());
	}

	/**
	 * @todo Write testAvailableRepresentations()
	 */
	function testAvailableRepresentations() {
		$this->markTestIncomplete();
	}

	function testGetBestRepresentation() {
		$this->markTestSkipped("getBestRepresentation() will be tested in the system tests");
	}

	function testController() {
		$this->object->setController("carrots");
		$this->assertEquals("carrots", $this->object->getController());
	}

	function testEqualsWithEqualRoute() {
		$route = new DMRoute("class", "method");
		$this->assertTrue($route->equals($this->object));
	}

	function testEqualsWithUnequalRoute() {
		$route = new DMRoute("aldfjksf", "alejrlajkser");
		$this->assertFalse($route->equals($this->object));
	}

	function testMethod() {
		$this->object->setMethod("carrots");
		$this->assertEquals("carrots", $this->object->getMethod());
	}

	function testModuleRoute() {
		$this->assertFalse($this->object->isModuleRoute());
		$this->object->setModuleRoute(true);
		$this->assertTrue($this->object->isModuleRoute());
	}

	function testParameters() {
		$params = "asldfjaslj";
		$this->object->setParameters($params);
		$this->assertEquals($params, $this->object->getParameters());
	}

	function testIsValidWithInvalidRoute() {
		$this->assertFalse($this->object->isValid());
	}

	function testIsValidWithValidRoute() {
		$this->object->setController("DMTEObjectController");
		$this->object->setMethod("index");
		$this->assertTrue($this->object->isValid());
	}

}
