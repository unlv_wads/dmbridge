<?php

class DMJSONRepresentationTransformerV1Test extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMJSONRepresentationTransformerV1
	 */
	protected $object;

	protected function setUp() {
		$this->object = new DMJSONRepresentationTransformerV1();
	}

	protected function assertValidMetaInfo($json) {
		$result = json_decode($json);
		$this->assertEquals(DMConfigXML::getInstance()->getFeedCopyright(),
				$result->dmBridgeResponse->copyright);
		$this->assertInternalType("float", $result->dmBridgeResponse->querySeconds);
		$this->assertInternalType("int", $result->dmBridgeResponse->version);
	}

	function testTransformCollection() {
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$json = $this->object->transformCollection($col);
		$result = json_decode($json);
		$this->assertEquals(TEST_ALIAS,
				$result->dmBridgeResponse->dmCollection->alias);
		$this->assertEquals(TEST_COLLECTION_NAME,
				$result->dmBridgeResponse->dmCollection->name);
		$this->assertEquals(13,
				$result->dmBridgeResponse->dmCollection->num_objects);
		$this->assertNull(
				$result->dmBridgeResponse->dmCollection->overview_href);
		$this->assertEquals(15,
				count((array)$result->dmBridgeResponse->dmCollection->fields));
		$field = $result->dmBridgeResponse->dmCollection->fields->title;
		$this->assertEquals("Title", $field->name);
		$this->assertEquals(0, $field->isControlled);
		$this->assertEquals(1, $field->isSearchable);
		$this->assertEquals("title", $field->dc);
		$this->assertEquals("TEXT", $field->type);
		$this->assertValidMetaInfo($json);
	}

	function testTransformCollections() {
		$collections = array(DMCollectionFactory::getCollection(TEST_ALIAS));
		$json = $this->object->transformCollections($collections);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;
		// will throw an exception if the URI is invalid
		$uri = new DMURI($response->dmCollections->{"/static"}->href);
		$this->assertEquals(TEST_COLLECTION_NAME,
				$response->dmCollections->{"/static"}->name);
		$this->assertEquals(13,
				$response->dmCollections->{"/static"}->num_objects);
		$this->assertValidMetaInfo($json);
	}

	function testTransformComment() {
		$comment = new DMComment();
		$comment->setID(5);
		$comment->setName("bob");
		$comment->setEmail("bob@google.com");
		$comment->setValue("wooo");
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$comment->setObject($obj);
		$json = $this->object->transformComment($comment);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;

		$this->assertEquals(TEST_ALIAS, $response->dmObject->alias);
		$this->assertEquals(TEST_PTR, $response->dmObject->ptr);
		// will throw an exception if the URI is invalid
		$uri = new DMURI($response->dmObject->href);
		$this->assertEquals(5, $response->dmObject->dmComment->id);
		$date = new DMDateTime($response->dmObject->dmComment->timestamp);
		$this->assertEquals($date->asISO8601(),
				$response->dmObject->dmComment->timestamp);
		$this->assertEquals("bob",
				$response->dmObject->dmComment->poster->name);
		$this->assertEquals("bob@google.com",
				$response->dmObject->dmComment->poster->email);
		$this->assertEquals("wooo", $response->dmObject->dmComment->body);
		$this->assertValidMetaInfo($json);
	}

	function testTransformComments() {
		$c1 = new DMComment();
		$c1->setId(50);
		$c2 = new DMComment();
		$c2->setId(520);
		$comments = array($c1, $c2);
		$json = $this->object->transformComments($comments, 1, 20, 2);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;

		$this->assertEquals(2, count($response->comments));
		$this->assertEquals(50, $response->comments[0]->id);
		// will throw an exception if the URI is invalid
		$uri = new DMURI($response->comments[0]->href);
		$this->assertValidMetaInfo($json);
	}

	function testTransformCommentsPagination() {
		$comments = array();
		for ($i = 1; $i < 100; $i++) {
			$comment = new DMComment();
			$comment->setID($i);
			$comments[] = $comment;
		}
		// page 1
		$json = $this->object->transformComments(
				array_slice($comments, 0, 20), 1, 20, 100);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;

		$this->assertEquals(1, $response->page);
		$this->assertEquals(20, $response->resultsPerPage);
		$this->assertEquals(5, $response->numPages);
		$this->assertEquals(100, $response->total);
		$this->assertEquals(1, $response->comments[0]->id);

		// page 2
		$json = $this->object->transformComments(
				array_slice($comments, 20, 20), 2, 20, 100);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;
		$this->assertEquals(2, $response->page);
		$this->assertEquals(20, $response->resultsPerPage);
		$this->assertEquals(5, $response->numPages);
		$this->assertEquals(100, $response->total);
		$this->assertEquals(21, $response->comments[0]->id);
	}

	function testTransformExceptionWithDebugModeDisabled() {
		DMConfigIni::getInstance()->setString("dmbridge.debug_mode", false);
		$e = new DMException("uh oh");
		$json = $this->object->transformException($e);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;
		$this->assertEquals("uh oh", $response->dmException->message);

		// will throw an exception if the URI is invalid
		$uri = new DMURI($response->dmException->documentation);
		$this->assertValidMetaInfo($json);
	}

	function testTransformExceptionWithDebugModeEnabled() {
		DMConfigIni::getInstance()->setString("dmbridge.debug_mode", true);
		$e = new DMException("uh oh");
		$json = $this->object->transformException($e);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;
		$this->assertEquals("uh oh", $response->dmException->message);

		$this->assertEquals("DMException", $response->dmException->type);
		$this->assertNotNull($response->dmException->stack_trace);
		// will throw an exception if the URI is invalid
		$uri = new DMURI($response->dmException->documentation);
		$this->assertValidMetaInfo($json);
	}

	function testTransformObject() {
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$json = $this->object->transformObject($obj);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;

		$this->assertEquals(TEST_ALIAS, $response->dmObject->alias);
		$this->assertEquals(1, $response->dmObject->ptr);
		$dt = new DMDateTime($response->dmObject->created);
		$this->assertEquals($dt->asISO8601(), $response->dmObject->created);
		$dt = new DMDateTime($response->dmObject->last_updated);
		$this->assertEquals($dt->asISO8601(), $response->dmObject->last_updated);
		$this->assertEquals(1, $response->dmObject->is_compound);
		// will throw an exception if the URI is invalid
		$uri = new DMURI($response->dmObject->web_template->href);
		$this->assertNotNull($response->dmObject->file->filename);
		$this->assertEquals("image/jpeg", $response->dmObject->file->mime_type);
		$uri = new DMURI($response->dmObject->file->href);
		$this->assertInternalType("int", $response->dmObject->file->height);
		$this->assertInternalType("int", $response->dmObject->file->width);
		$uri = new DMURI($response->dmObject->image_generator);
		$uri = new DMURI($response->dmObject->image->small);
		$uri = new DMURI($response->dmObject->image->medium);
		$uri = new DMURI($response->dmObject->image->large);
		$uri = new DMURI($response->dmObject->thumbnail->href);
		$this->assertInternalType("int", $response->dmObject->rating->num_ratings);
		$this->assertInternalType("float", $response->dmObject->rating->average);

		$this->assertNull($response->dmObject->parent);

		$this->assertEquals(5, count($response->dmObject->children));
		$this->assertInternalType("int", $response->dmObject->children[0]->pointer);
		$this->assertInternalType("string", $response->dmObject->children[0]->title);
		$uri = new DMURI($response->dmObject->children[0]->thumbnail->href);
		$uri = new DMURI($response->dmObject->children[0]->href);

		$this->assertEquals(6, count($response->dmObject->metadata));
		$this->assertEquals("title", $response->dmObject->metadata[0]->nick);
		$this->assertEquals("Title", $response->dmObject->metadata[0]->name);
		$this->assertEquals("Sample JPEG Object", $response->dmObject->metadata[0]->value);
		$this->assertEquals("title", $response->dmObject->metadata[0]->dc_map);
		$this->assertEquals(1, $response->dmObject->metadata[0]->searchable);
		$this->assertEquals(0, $response->dmObject->metadata[0]->controlled);

		$this->assertEquals(20, count($response->dmObject->tags));
		$this->assertInternalType("int", $response->dmObject->tags[0]->id);
		$uri = new DMURI($response->dmObject->tags[0]->href);
		$this->assertInternalType("string", $response->dmObject->tags[0]->text);
		$this->assertTrue(strlen($response->dmObject->tags[0]->text) > 0);

		$this->assertEquals(20, count($response->dmObject->comments));
		$this->assertInternalType("int", $response->dmObject->comments[0]->id);
		$uri = new DMURI($response->dmObject->comments[0]->href);

		$this->assertValidMetaInfo($json);
	}

	function testTransformChildObject() {
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_COMPOUND_PAGE_PTR);
		$json = $this->object->transformObject($obj);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;

		$uri = new DMURI($response->dmObject->parent);

		$this->assertValidMetaInfo($json);
	}

	function testTransformObjectComments() {
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$json = $this->object->transformObjectComments($obj, 1, 20);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;

		$this->assertEquals(TEST_ALIAS, $response->dmObject->alias);
		$this->assertEquals(TEST_PTR, $response->dmObject->pointer);
		$uri = new DMURI($response->dmObject->href);
		$this->assertEquals(20, count($response->dmObject->comments));
		$this->assertInternalType("int", $response->dmObject->comments[0]->id);
		$uri = new DMURI($response->dmObject->comments[0]->href);
		$dt = new DMDateTime($response->dmObject->comments[0]->timestamp);
		$this->assertEquals($dt->asISO8601(), $response->dmObject->comments[0]->timestamp);
		$this->assertInternalType("string", $response->dmObject->comments[0]->poster->name);
		$this->assertInternalType("string", $response->dmObject->comments[0]->body);
		$this->assertValidMetaInfo($json);
	}

	function testTransformObjectRating() {
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$json = $this->object->transformObjectRating($obj);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;
		$this->assertEquals(TEST_ALIAS, $response->dmObject->alias);
		$this->assertEquals(TEST_PTR, $response->dmObject->pointer);
		$uri = new DMURI($response->dmObject->href);
		$this->assertInternalType("int", $response->dmObject->rating->num_ratings);
		$this->assertInternalType("float", $response->dmObject->rating->average);
		$this->assertValidMetaInfo($json);
	}

	function testTransformObjectTags() {
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$json = $this->object->transformObjectTags($obj);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;
		$this->assertEquals(TEST_ALIAS, $response->dmObject->alias);
		$this->assertEquals(TEST_PTR, $response->dmObject->pointer);
		$uri = new DMURI($response->dmObject->href);
		$this->assertEquals(20, count($response->dmObject->tags));
		$uri = new DMURI($response->dmObject->tags[0]->href);
		$this->assertInternalType("string", $response->dmObject->tags[0]->text);
		$this->assertValidMetaInfo($json);
	}

	function testTransformRatings() {
		$q = new DMObjectQuery();
		$q->getSearchResults();
		$json = $this->object->transformRatings($q);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;

		$this->assertEquals(20, $response->resultsPerPage);
		$this->assertEquals(13, $response->total);
		$this->assertEquals(13, count($response->results));
		$this->assertEquals(TEST_ALIAS, $response->results[0]->alias);
		$this->assertEquals(TEST_PTR, $response->results[0]->pointer);
		$uri = new DMURI($response->results[0]->href);
		$this->assertInternalType("float", $response->results[0]->rating);
		$this->assertValidMetaInfo($json);
	}

	function testTransformResults() {
		$q = new DMObjectQuery();
		$q->getSearchResults();
		$json = $this->object->transformResults($q);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;

		$this->assertInternalType("string", $response->suggestion);
		$this->assertEquals(1, $response->page);
		$this->assertEquals(20, $response->resultsPerPage);
		$this->assertEquals(1, $response->numPages);
		$this->assertEquals(13, $response->total);
		$this->assertEquals(13, count($response->results));
		$this->assertEquals(TEST_ALIAS, $response->results[0]->alias);
		$this->assertEquals(TEST_PTR, $response->results[0]->pointer);
		$uri = new DMURI($response->results[0]->href);

		// @todo test this with facets
		$this->assertInternalType("array", $response->facets);
		
		$this->assertValidMetaInfo($json);
	}

	function testTransformStatusWithEverythingEnabled() {
		$cfg = DMConfigXML::getInstance();
		$cfg->setCommentingEnabled(1);
		$cfg->setCommentModerationEnabled(1);
		$cfg->setTaggingEnabled(1);
		$cfg->setTagModerationEnabled(1);
		$cfg->setRatingEnabled(1);

		$json = $this->object->transformStatus();
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;

		$this->assertInternalType("string", $response->dmStatus->institution->name);
		$this->assertEquals(1, $response->dmStatus->comments->are_available);
		$this->assertEquals(1, $response->dmStatus->comments->are_moderated);
		$this->assertEquals(1, $response->dmStatus->tags->are_available);
		$this->assertEquals(1, $response->dmStatus->tags->are_moderated);
		$this->assertEquals(1, $response->dmStatus->ratings->are_available);

		$this->assertInternalType("string", $response->dmStatus->version);
		$this->assertInternalType("int", $response->dmStatus->version_sequence);
		$this->assertValidMetaInfo($json);
	}

	function testTransformStatusWithEverythingDisabled() {
		$cfg = DMConfigXML::getInstance();
		$cfg->setCommentingEnabled(0);
		$cfg->setCommentModerationEnabled(0);
		$cfg->setTaggingEnabled(0);
		$cfg->setTagModerationEnabled(0);
		$cfg->setRatingEnabled(0);

		$json = $this->object->transformStatus();
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;

		$this->assertInternalType("string", $response->dmStatus->institution->name);
		$this->assertEquals(0, $response->dmStatus->comments->are_available);
		$this->assertEquals(0, $response->dmStatus->comments->are_moderated);
		$this->assertEquals(0, $response->dmStatus->tags->are_available);
		$this->assertEquals(0, $response->dmStatus->tags->are_moderated);
		$this->assertEquals(0, $response->dmStatus->ratings->are_available);

		$this->assertInternalType("string", $response->dmStatus->version);
		$this->assertInternalType("int", $response->dmStatus->version_sequence);
		$this->assertValidMetaInfo($json);
	}

	function testTransformTag() {
		$tag = new DMTag();
		$tag->setID(50);
		$tag->setValue("cats");
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$tag->setObject($obj);
		$json = $this->object->transformTag($tag);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;

		$this->assertEquals(TEST_ALIAS, $response->dmObject->alias);
		$this->assertEquals(TEST_PTR, $response->dmObject->ptr);
		$uri = new DMURI($response->dmObject->href);
		$this->assertInternalType("int", $response->dmObject->dmTag->id);
		$dt = new DMDateTime($response->dmObject->dmTag->timestamp);
		$this->assertEquals($dt->asISO8601(), $response->dmObject->dmTag->timestamp);
		$this->assertEquals("cats", $response->dmObject->dmTag->value);
		$this->assertValidMetaInfo($json);
	}

	function testTransformTagCounts() {
		$tags = array(2 => "cats", 1 => "dogs");
		$json = $this->object->transformTagCounts($tags, 1, 20, 2);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;

		$this->assertEquals(1, $response->page);
		$this->assertEquals(20, $response->resultsPerPage);
		$this->assertEquals(1, $response->numPages);
		$this->assertEquals(2, $response->total);

		$this->assertEquals("cats", $response->tags->{2});
		$this->assertEquals("dogs", $response->tags->{1});

		$this->assertValidMetaInfo($json);
	}

	function testTransformTagCountsPagination() {
		$tags = array();
		for ($i = 0; $i < 100; $i++) {
			$tags[$i] = md5($i);
		}

		// page 1
		$json = $this->object->transformTagCounts($tags, 1, 20, 100);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;

		$this->assertEquals(1, $response->page);
		$this->assertEquals(20, $response->resultsPerPage);
		$this->assertEquals(5, $response->numPages);
		$this->assertEquals(100, $response->total);

		// page 2
		$json = $this->object->transformTagCounts($tags, 2, 20, 100);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;

		$this->assertEquals(2, $response->page);
		$this->assertEquals(20, $response->resultsPerPage);
		$this->assertEquals(5, $response->numPages);
		$this->assertEquals(100, $response->total);

		$this->assertValidMetaInfo($json);
	}

	function testTransformVocabularyFrequencies() {
		$freqs = array();
		$json = $this->object->transformVocabularyFrequencies($freqs);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;

		$this->assertInternalType("array", $response->vocabulary);
		$this->assertValidMetaInfo($json);
	}

	function testTransformVocabularySuggestions() {
		$words = array('cats', 'dogs');
		$json = $this->object->transformVocabularySuggestions($words);
		$result = json_decode($json);
		$response = $result->dmBridgeResponse;

		$this->assertInternalType("array", $response->vocabulary);
		$this->assertEquals("cats", $response->vocabulary[0]);
		$this->assertValidMetaInfo($json);
	}

}
