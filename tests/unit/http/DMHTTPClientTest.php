<?php

class DMHTTPClientTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMHTTPClient
	 */
	protected $object;

	protected function setUp() {
		$req = new DMHTTPRequest();
		$this->object = new DMHTTPClient($req);
	}

	function testFollowingRedirects() {
		$this->object->setFollowRedirects(false);
		$this->assertFalse($this->object->isFollowingRedirects());
	}

	function testRequest() {
		$req = new DMHTTPRequest();
		$this->object->setRequest($req);
		$this->assertSame($req, $this->object->getRequest());
	}

	function testSendReturnsCorrectCharacterSet() {
		$this->object->getRequest()->setURI(
				new DMURI("http://digital.library.unlv.edu/"));
		$response = $this->object->send();
		$this->assertEquals("utf-8",
				$response->getRepresentation()->getCharacterSet());
	}

	function testSendReturnsCorrectResponseBody() {
		$this->object->getRequest()->setURI(
				new DMURI("http://digital.library.unlv.edu/"));
		$response = $this->object->send();
		$this->assertTrue(strlen($response->getRepresentation()->getBody()) > 0);
	}

	function testSendReturnsCorrectMediaType() {
		$this->object->getRequest()->setURI(
				new DMURI("http://digital.library.unlv.edu/"));
		$response = $this->object->send();
		$this->assertTrue($response->getRepresentation()->getMediaType()->equals(
				new DMMediaType("text", "html")));
	}

	function testSendReturnsCorrectStatusCode() {
		$this->object->getRequest()->setURI(
				new DMURI("http://digital.library.unlv.edu/"));
		$response = $this->object->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testSendReturnsCorrectStatusMessage() {
		$this->object->getRequest()->setURI(
				new DMURI("http://digital.library.unlv.edu/"));
		$response = $this->object->send();
		$this->assertTrue(strlen($response->getStatusMessage()) > 0);
	}

	function testSendReturnsResponseHeaders() {
		$this->object->getRequest()->setURI(
				new DMURI("http://digital.library.unlv.edu/"));
		$response = $this->object->send();
		$this->assertTrue(count($response->getHeaders()) > 0);
	}

	function testTimeout() {
		$timeout = 2042;
		$this->object->setTimeout($timeout);
		$this->assertEquals($timeout, $this->object->getTimeout());
	}

}
