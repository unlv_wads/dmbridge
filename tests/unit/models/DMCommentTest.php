<?php

/**
 * Test class for DMComment.
 * Generated by PHPUnit on 2010-10-25 at 09:04:32.
 */
class DMCommentTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMComment
	 */
	protected $object;

	protected function setUp() {
		$this->object = new DMComment();
	}

	protected function tearDown() {

	}

	public function testApprovedAccessors() {
		$this->assertEquals($this->object->isApproved(), false);

		$this->object->setApproved(true);
		$this->assertTrue($this->object->isApproved());
	}

	public function testEmailAccessors() {
		// don't test the whole gamut of valid/invalid emails here, as that will
		// be done in the test of DMMailer::isValidEmail()
		$valid_email = "fred@fox.com";
		$invalid_email = "bla@%";

		$this->object->setEmail($valid_email);
		$this->assertEquals($this->object->getEmail(), $valid_email);

		$this->setExpectedException("DMInvalidEmailException");
		$this->object->setEmail($invalid_email);
	}

	public function testIDAccessors() {
		$this->assertEquals($this->object->getID(), null);

		$this->object->setID(5);
		$this->assertEquals($this->object->getID(), 5);
	}

	public function testNameAccessors() {
		$valid_names = array();
		$valid_names[] = "Bill";
		foreach ($valid_names as $name) {
			$this->object->setName($name);
			$this->assertEquals($this->object->getName(), $name);
		}

		$invalid_names = array();
		$invalid_names[] = "a";
		$invalid_names[] = "St. Francis Copernicus Maximilian III, Esq., CPA, MLIS";
		foreach ($invalid_names as $name) {
			$this->setExpectedException("DMIllegalArgumentException");
			$this->object->setName($name);
		}
	}

	public function testObjectAccessors() {
		$col = new DMCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$this->object->setObject($obj);
		$this->assertEquals($this->object->getObject(), $obj);
	}

	public function testTimestampAccessors() {
		$date = new DMDateTime();
		$this->object->setTimestamp($date);
		$this->assertEquals($this->object->getTimestamp(), $date);
	}

	public function testGetURI() {
		// will be handled by the test of
		// DMInternalURI::getResourceURI()
	}

	public function testValueAccessors() {
		$valid_values = array();
		$valid_values[] = "First post.... oops wrong site";
		foreach ($valid_values as $value) {
			$this->object->setValue($value);
			$this->assertEquals($this->object->getValue(), $value);
		}

		$invalid_values = array();
		$invalid_values[] = "a";
		foreach ($invalid_values as $value) {
			$this->setExpectedException("DMIllegalArgumentException");
			$this->object->setValue($value);
		}
	}

}
