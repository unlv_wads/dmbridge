<?php

/**
 * Used in the tests
 */
class TestModelA {
	function poke() {
		return "cats";
	}
}

class TestModelB {
	function squeeze() {
		return "fruit";
	}
}

class DMModelTest extends PHPUnit_Framework_TestCase {

	function testAssociatingModelWorks() {
		$model_a = new TestModelA();
		$model_b = new TestModelB();
		$col = new DMCollection(TEST_ALIAS);
		$col->addAssociatedModel($model_a);
		$col->addAssociatedModel($model_b);
		$this->assertEquals("cats", $col->poke());
		$this->assertEquals("fruit", $col->squeeze());
	}

	function testDisassociatingModelWorks() {
		$model_a = new TestModelA();
		$col = new DMCollection(TEST_ALIAS);
		$col->addAssociatedModel($model_a);
		$col->disassociateModel($model_a);
		$this->assertEquals(0, count($col->getAssociatedModels()));
	}

	function testGetAssociatedModelsWorks() {
		$model_a = new TestModelA();
		$col = new DMCollection(TEST_ALIAS);
		$col->addAssociatedModel($model_a);
		$models = $col->getAssociatedModels();
		$this->assertSame($model_a, $models[0]);
		$this->assertEquals(1, count($models));
	}

}
