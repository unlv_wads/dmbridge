<?php

/**
 * Test class for DMShellCommand.
 */
class DMShellCommandTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMPDFFile
	 */
	protected $object;

	protected function setUp() {
		$this->object = new DMShellCommand("ls");
	}

	function testEquals() {
		$cmd = new DMShellCommand("ls");
		$this->assertTrue($cmd->equals($this->object));
		$cmd = new DMShellCommand("ps");
		$this->assertFalse($cmd->equals($this->object));
	}

	function testCommand() {
		$cmd = "asdlfjkas";
		$this->object->setCommand($cmd);
		$this->assertEquals($cmd, $this->object->getCommand());
	}

}

