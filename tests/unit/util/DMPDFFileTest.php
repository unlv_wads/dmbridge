<?php

/**
 * Test class for DMPDFFile.
 */
class DMPDFFileTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMPDFFile
	 */
	protected $object;

	protected function setUp() {
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, 2);
		$this->object = $obj->getFile();
	}

	function testGetNumPages() {
		$this->assertEquals(2, $this->object->getNumPages());
	}

}

