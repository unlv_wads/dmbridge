<?php

/**
 * Test class for DMFile.
 */
class DMFileTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMFile
	 */
	protected $object;

	protected function setUp() {
		$this->object = new DMFile(__FILE__);
	}

	/**
	 * Tests getUnitsForByteSize().
	 */
	function testGetIecUnitsForByteSize() {
		$this->assertEquals("bytes", DMFile::getUnitsForByteSize(5, "IEC"));
		$this->assertEquals("kibibytes",
				DMFile::getUnitsForByteSize(pow(1024, 2) - 5, "IEC"));
		$this->assertEquals("mebibytes",
				DMFile::getUnitsForByteSize(pow(1024, 3) - 5, "IEC"));
		$this->assertEquals("gibibytes",
				DMFile::getUnitsForByteSize(pow(1024, 4) - 5, "IEC"));
		$this->assertEquals("tebibytes",
				DMFile::getUnitsForByteSize(pow(1024, 5) - 5, "IEC"));
		$this->assertEquals("pebibytes",
				DMFile::getUnitsForByteSize(pow(1024, 6) - 5, "IEC"));
		$this->assertEquals("exbibytes",
				DMFile::getUnitsForByteSize(pow(1024, 7) - 5, "IEC"));
		$this->assertEquals("zebibytes",
				DMFile::getUnitsForByteSize(pow(1024, 8) - 5, "IEC"));
		$this->assertEquals("yobibytes",
				DMFile::getUnitsForByteSize(pow(1024, 9) - 5, "IEC"));
	}

	/**
	 * Tests getUnitsForByteSize().
	 */
	function testGetJedecUnitsForByteSize() {
		$this->assertEquals("bytes",
				DMFile::getUnitsForByteSize(5, "JEDEC"));
		$this->assertEquals("kilobytes",
				DMFile::getUnitsForByteSize(pow(1024, 2) - 5, "JEDEC"));
		$this->assertEquals("megabytes",
				DMFile::getUnitsForByteSize(pow(1024, 3) - 5, "JEDEC"));
		$this->assertEquals("gigabytes",
				DMFile::getUnitsForByteSize(pow(1024, 4) - 5, "JEDEC"));
	}

	/**
	 * Tests getUnitsForByteSize().
	 */
	function testGetSiUnitsForByteSize() {
		$this->assertEquals("bytes", DMFile::getUnitsForByteSize(5));
		$this->assertEquals("kilobytes",
				DMFile::getUnitsForByteSize(pow(1000, 2) - 5));
		$this->assertEquals("megabytes",
				DMFile::getUnitsForByteSize(pow(1000, 3) - 5));
		$this->assertEquals("gigabytes",
				DMFile::getUnitsForByteSize(pow(1000, 4) - 5));
		$this->assertEquals("terabytes",
				DMFile::getUnitsForByteSize(pow(1000, 5) - 5));
		$this->assertEquals("petabytes",
				DMFile::getUnitsForByteSize(pow(1000, 6) - 5));
		$this->assertEquals("exabytes",
				DMFile::getUnitsForByteSize(pow(1000, 7) - 5));
		$this->assertEquals("zettabytes",
				DMFile::getUnitsForByteSize(pow(1000, 8) - 5));
		$this->assertEquals("yottabytes",
				DMFile::getUnitsForByteSize(pow(1000, 9) - 5));
	}
	
	/**
	 * Tests getUnitSymbolForByteSize().
	 */
	function testGetIecUnitSymbolForByteSize() {
		$this->assertEquals("b", DMFile::getUnitSymbolForByteSize(5, "IEC"));
		$this->assertEquals("Ki",
				DMFile::getUnitSymbolForByteSize(pow(1024, 2) - 5, "IEC"));
		$this->assertEquals("Mi",
				DMFile::getUnitSymbolForByteSize(pow(1024, 3) - 5, "IEC"));
		$this->assertEquals("Gi",
				DMFile::getUnitSymbolForByteSize(pow(1024, 4) - 5, "IEC"));
		$this->assertEquals("Ti",
				DMFile::getUnitSymbolForByteSize(pow(1024, 5) - 5, "IEC"));
		$this->assertEquals("Pi",
				DMFile::getUnitSymbolForByteSize(pow(1024, 6) - 5, "IEC"));
		$this->assertEquals("Ei",
				DMFile::getUnitSymbolForByteSize(pow(1024, 7) - 5, "IEC"));
		$this->assertEquals("Zi",
				DMFile::getUnitSymbolForByteSize(pow(1024, 8) - 5, "IEC"));
		$this->assertEquals("Yi",
				DMFile::getUnitSymbolForByteSize(pow(1024, 9) - 5, "IEC"));
	}

	/**
	 * Tests getUnitSymbolForByteSize().
	 */
	function testGetJedecUnitSymbolForByteSize() {
		$this->assertEquals("b",
				DMFile::getUnitSymbolForByteSize(5, "JEDEC"));
		$this->assertEquals("K",
				DMFile::getUnitSymbolForByteSize(pow(1024, 2) - 5, "JEDEC"));
		$this->assertEquals("M",
				DMFile::getUnitSymbolForByteSize(pow(1024, 3) - 5, "JEDEC"));
		$this->assertEquals("G",
				DMFile::getUnitSymbolForByteSize(pow(1024, 4) - 5, "JEDEC"));
	}

	/**
	 * Tests getUnitSymbolForByteSize().
	 */
	function testGetSiUnitSymbolForByteSize() {
		$this->assertEquals("b", DMFile::getUnitSymbolForByteSize(5));
		$this->assertEquals("k",
				DMFile::getUnitSymbolForByteSize(pow(1000, 2) - 5));
		$this->assertEquals("M",
				DMFile::getUnitSymbolForByteSize(pow(1000, 3) - 5));
		$this->assertEquals("G",
				DMFile::getUnitSymbolForByteSize(pow(1000, 4) - 5));
		$this->assertEquals("T",
				DMFile::getUnitSymbolForByteSize(pow(1000, 5) - 5));
		$this->assertEquals("P",
				DMFile::getUnitSymbolForByteSize(pow(1000, 6) - 5));
		$this->assertEquals("E",
				DMFile::getUnitSymbolForByteSize(pow(1000, 7) - 5));
		$this->assertEquals("Z",
				DMFile::getUnitSymbolForByteSize(pow(1000, 8) - 5));
		$this->assertEquals("Y",
				DMFile::getUnitSymbolForByteSize(pow(1000, 9) - 5));
	}

	function testToString() {
		$this->assertEquals($this->object->__toString(),
				$this->object->getLastPathComponent());
	}

	function testEqualsWithEqualInstance() {
		$file = new DMFile(__FILE__);
		$this->assertTrue($this->object->equals($file));
	}

	function testEqualsWithInequalInstance() {
		$file = new DMFile("/cats");
		$this->assertFalse($this->object->equals($file));
	}

	function testGetExtensionOnFileWithExtension() {
		$this->assertEquals("php", $this->object->getExtension());
	}

	function testGetExtensionOnFileWithoutExtension() {
		$this->object->setPathname(dirname($this->object->getPathname()));
		$this->assertNull($this->object->getExtension());
	}

	function testGetHumanReadableSize() {
		// @todo Write testGetHumanReadableSize()
		$this->markTestIncomplete();
	}

	function testGetLastPathComponent() {
		$this->assertEquals("DMFileTest.php", $this->object->getLastPathComponent());
	}

	function getMediaType() {
		$this->assertEquals($this->object->getMediaType(),
			DMMediaType::getTypeForExtension($this->object->getExtension()));
	}

	function testGetNameWithoutExtension() {
		$names = array(
			"cats.jpg" => "cats",
			"bla.bla.bla.bla" => "bla.bla.bla",
			"dogs" => "dogs"
		);
		foreach ($names as $original => $expected) {
			$this->object->setPathname("/bla/bla/bla/" . $original);
			$this->assertEquals($expected, $this->object->getNameWithoutExtension());
		}
	}

	function testPathname() {
		$pathname = "/cats";
		$this->object->setPathname($pathname);
		$this->assertEquals($pathname, $this->object->getPathname());
	}

	function testGetSize() {
		$this->assertTrue($this->object->getSize() > 0);
	}

	function testGetSizeOnNonexistentFileThrowsException() {
		$this->object->setPathname("/asdf/adsf/asdf/adafdf");
		$this->setExpectedException("DMFileNotFoundException");
		$this->object->getSize();
	}

}

