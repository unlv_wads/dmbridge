<?php

/**
 * Test class for DMString.
 */
class DMStringTest extends PHPUnit_Framework_TestCase {

	private static function getNonUtf8String() {
		$pathname = dirname(__FILE__) . "/ISO8859-7.txt";
		$fh = fopen($pathname, "r");
		$data = fread($fh, filesize($pathname));
		fclose($fh);
		return $data;
	}

	function testCamelCaseToUnderScore() {
		$this->assertEquals("beep_boop_boop",
				DMString::camelCaseToUnderscore("beepBoopBoop"));
	}

	function testCleanWithCleanStringReturnsSameString() {
		$this->assertEquals("beep", DMString::clean("beep"));
	}

	function testCleanWithDirtyStringReturnsCleanString() {
		$data = self::getNonUtf8String();
		// @todo this is a little crude
		$this->assertNotEquals(DMString::clean($data), $data);
	}

	function testParanoidWithCleanStringReturnsSameString() {
		$this->assertEquals("beep123", DMString::paranoid("beep123"));
	}

	function testParanoidWithDirtyStringReturnsCleanString() {
		$this->assertEquals("beep123", DMString::paranoid("beep123é®"));
	}

	function testParanoidWithAllowListReturnsCorrectString() {
		$this->assertEquals("beep123/",
				DMString::paranoid("beep123/,...:", array("/")));
	}

	function testHighlightWithOneMatch() {
		$expected = 'cat <span class="dmHighlightedTerm">food</span>';
		$result = DMString::highlight("food", "cat food");
		$this->assertEquals($expected, $result);
	}

	function testHighlightWithTwoMatches() {
		$expected = 'cat <span class="dmHighlightedTerm">food</span> '
			. '<span class="dmHighlightedTerm">food</span>';
		$result = DMString::highlight("food", "cat food food");
		$this->assertEquals($expected, $result);
	}

	function testHighlightWithNoMatches() {
		$expected = 'cat food';
		$result = DMString::highlight("peas", "cat food");
		$this->assertEquals($expected, $result);
	}

	function testHyperlink() {
		$text = "http://localhost is my favorite site";
		$expected = '<a href="http://localhost">http://localhost</a> is my '
			. 'favorite site';
		$result = DMString::hyperlink($text);
		$this->assertEquals($expected, $result);
	}

	function testTruncate() {
		$str = "I like to eat eat apples and bananas";
		$expected = "I like to eat eat...";
		$result = DMString::truncate($str, 5);
		$this->assertEquals($expected, $result);
	}

	function testIsUTF8OnUTF8TextReturnsTrue() {
		$str = "asldfkjasldfkjsaf";
		$result = DMString::isUTF8($str);
		$this->assertEquals($str, $result);
	}

	function testIsUTF8OnNonUTF8TextReturnsFalse() {
		$data = self::getNonUtf8String();
		$this->assertFalse(DMString::isUTF8($data));
	}

	function testIsValidHostname() {
		// this is supported by its own tests and won't be tested here
	}

	function testIsValidIPAddress() {
		// this is supported by its own tests and won't be tested here
	}

	function testIsValidPathnameOnValidPathnameReturnsTrue() {
		$path = "/afd/adflweijr/afdljiwer/wlerj.txt";
		$this->assertTrue(DMString::isValidPathname($path));
	}

	function testIsValidPathnameOnInvalidPathnameReturnsFalse() {
		$path = "/55bla~~`()$@*&";
		$this->assertFalse(DMString::isValidPathname($path));
	}

	function testUnderscoreToCamelCase() {
		$expected = "peasAndCarrots";
		$result = DMString::underscoreToCamelCase("peas_and_carrots");
		$this->assertEquals($expected, $result);
	}

	function testWebsafe() {
		$expected = "I am an evil attackeralert(&quot;boo&quot;);";
		$text = 'I am an evil attacker<script type="text/javascript">alert("boo");</script>';
		$result = DMString::websafe($text);
		$this->assertEquals($expected, $result);
	}

	function testXmlentities() {
		$expected = "an XML tag looks like &lt;tag&gt;";
		$text = "an XML tag looks like <tag>";
		$result = DMString::xmlentities($text);
		$this->assertEquals($expected, $result);
	}

}
