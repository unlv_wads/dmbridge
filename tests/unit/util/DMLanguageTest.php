<?php

/**
 * Test class for DMLanguage.
 */
class DMLanguageTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMLanguage
	 */
	protected $object;

	protected function setUp() {
		$this->object = new DMLanguage();
	}

	public function testGetAll() {
		foreach (DMLanguage::getAll() as $lang) {
			$this->assertType("DMLanguage", $lang);
		}
	}

	public function test__toString() {
		$name = "klingon";
		$this->object->setNativeName($name);
		$this->assertEquals($name, $this->object->__toString());
	}

	public function testISO6391Code() {
		$value = "peanut";
		$this->object->setISO6391Code($value);
		$this->assertEquals($value, $this->object->getISO6391Code());
	}

	public function testISO6392Code() {
		$value = "peanut";
		$this->object->setISO6392Code($value);
		$this->assertEquals($value, $this->object->getISO6392Code());
	}

	public function testISO6393Code() {
		$value = "peanut";
		$this->object->setISO6393Code($value);
		$this->assertEquals($value, $this->object->getISO6393Code());
	}

	public function testNativeName() {
		$value = "peanut";
		$this->object->setNativeName($value);
		$this->assertEquals($value, $this->object->getNativeName());
	}

}

