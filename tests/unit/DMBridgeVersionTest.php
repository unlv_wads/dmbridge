<?php

class DMBridgeVersionTest extends PHPUnit_Framework_TestCase {

	private static function parseVersionFile(&$version, &$seq) {
		$file = dirname(__FILE__) . "/../../VERSION.txt";
		$data = file_get_contents($file);
		$tmp = explode("\n", $data);
		$version = (string) substr($tmp[0], 0, 20);
		$seq = (int) substr($tmp[1], 0, 3);
	}

	function testGetDmBridgeVersion() {
		$version = $seq = 0;
		self::parseVersionFile($version, $seq);
		$this->assertEquals($version, DMBridgeVersion::getDmBridgeVersion());
	}

	function testGetDmBridgeVersionSequence() {
		$version = $seq = 0;
		self::parseVersionFile($version, $seq);
		$this->assertEquals($seq, DMBridgeVersion::getDmBridgeVersionSequence());
	}

	function testGetCurrentDmBridgeVersionSequence() {
		$stub = $this->getMock('DMHTTPRequest');
		$stub->expects($this->any())
			->method('GET')
			->will($this->returnValue("10"));
		$this->assertEquals(10,
				DMBridgeVersion::getCurrentDmBridgeVersionSequence($stub));
	}

	function testGetHTTPAPIVersions() {
		$this->assertEquals(array(1), DMBridgeVersion::getHTTPAPIVersions());
	}

	function testGetLatestHTTPAPIVersion() {
		$this->assertEquals(1, DMBridgeVersion::getLatestHTTPAPIVersion());
	}

	function testIsNewerDmBridgeVersionAvailableWhenItIs() {
		$version = $seq = 0;
		self::parseVersionFile($version, $seq);

		$stub = $this->getMock('DMHTTPRequest');
		$stub->expects($this->any())
			->method('GET')
			->will($this->returnValue("500"));
		$this->assertTrue(DMBridgeVersion::isNewerDmBridgeVersionAvailable($stub));
	}

	function testIsNewerDmBridgeVersionAvailableWhenItIsnt() {
		$version = $seq = 0;
		self::parseVersionFile($version, $seq);

		// old version
		$stub = $this->getMock('DMHTTPRequest');
		$stub->expects($this->any())
			->method('GET')
			->will($this->returnValue("2"));
		$this->assertFalse(DMBridgeVersion::isNewerDmBridgeVersionAvailable($stub));

		// same version
		$stub = $this->getMock('DMHTTPRequest');
		$stub->expects($this->any())
			->method('GET')
			->will($this->returnValue($seq . ""));
		$this->assertFalse(DMBridgeVersion::isNewerDmBridgeVersionAvailable($stub));
	}

}

