<?php

/**
 * Test class for DMConfigIni.
 */
class DMConfigIniTest extends PHPUnit_Framework_TestCase {

	/**
	 * @return string
	 */
	private static function getIniPath() {
		return sys_get_temp_dir() . "/test.ini";
	}

	/**
	 * @param string pathname
	 * @param array options
	 */
	private static function writeIniFile($pathname, array $options) {
		$tmp = "";
		foreach ($options as $section => $values) {
			$tmp .= "[$section]\n";
			foreach ($values as $key => $val) {
				if (is_array($val)) {
					foreach($val as $k =>$v) {
						$tmp .= "{$key}[$k] = \"$v\"\n";
					}
				} else {
					$tmp .= "$key = \"$val\"\n";
				}
			}
			$tmp .= "\n";
		}
		file_put_contents($pathname, $tmp);
		unset($tmp);
	}

	/**
	 * @var DMConfigIni
	 */
	protected $object;

	protected function setUp() {
		$options = array(
			'dmBridgeConfig' => array(
				'bla' => 'cats',
				'truebool' => 'true',
				'falsebool' => 'false'
			)
		);
		self::writeIniFile(self::getIniPath(), $options);

		$this->object = DMConfigIni::getInstance(self::getIniPath());
	}

	protected function tearDown() {
		DMConfigIni::destroyInstance($this->object);
		$this->object = null;
		unlink(self::getIniPath());
	}

	function testGetBoolean() {
		$this->assertTrue($this->object->getBoolean("truebool"));
		$this->assertFalse($this->object->getBoolean("falsebool"));
	}

	function testSetBoolean() {
		$this->object->setBoolean("dogs", true);
		$this->assertTrue($this->object->getBoolean("dogs"));
		$this->object->setBoolean("dogs", false);
		$this->assertFalse($this->object->getBoolean("dogs"));
	}

	function testGetString() {
		$this->assertEquals("cats", $this->object->getString("bla"));
	}

	function testGetStringWithDefaultValue() {
		$this->assertEquals("cats", $this->object->getString("asdflkjasf", "cats"));
	}

	function testSetString() {
		$this->object->setString("dogs", "ketchup");
		$this->assertEquals("ketchup", $this->object->getString("dogs"));
	}

}
