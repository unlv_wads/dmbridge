<?php

class DMCdmAuthenticationServiceTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMCdmAuthenticationService
	 */
	private $object;
	
	protected function setUp() {
		$this->object = new DMCdmAuthenticationService();
	}

	function testAuthenticateWithValidUsername() {
		$this->assertTrue(
				$this->object->authenticate(new DMUser(TEST_USERNAME), TEST_PASSWORD));
	}

	function testAuthenticationWithInvalidUsername() {
		$this->assertFalse(
				$this->object->authenticate(new DMUser("lafjsf"), "alskjlwejrk"));
	}

}
