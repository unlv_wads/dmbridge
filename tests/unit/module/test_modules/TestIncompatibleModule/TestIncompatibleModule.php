<?php

class TestIncompatibleModule extends DMAbstractDMBridgeModule
implements DMBridgeModule {

	public function getName() {
		return "TestIncompatibleModule";
	}

	public function  getMinSupportedVersionSequence() {
		return 999999;
	}

	public function getVersion() {
		return "1.0";
	}

}

