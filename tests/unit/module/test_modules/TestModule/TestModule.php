<?php

class TestModule extends DMAbstractDMBridgeModule implements DMBridgeModule {

	public function  getMinSupportedVersionSequence() {
		return 11;
	}

	public function getName() {
		return "TestModule";
	}
	
	public function getVersion() {
		return "1.0";
	}

}

