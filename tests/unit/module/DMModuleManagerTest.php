<?php

/**
 * Test class for DMModuleManagerTest.
 */
class DMModuleManagerTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMModuleManager
	 */
	protected $object;

	private static function getConfigXmlPathname() {
		return dirname(__FILE__) . "/../../../data/config.default.xml";
	}

	protected function setUp() {
		$this->object = DMModuleManager::getInstance();
		$this->object->setModulesPathname(dirname(__FILE__) . "/test_modules");
		$config = DMConfigXML::getInstance(self::getConfigXmlPathname());
		$this->object->setConfigXML($config);
	}

	protected function tearDown() {
		DMConfigXML::destroyInstance(
				DMConfigXML::getInstance(self::getConfigXmlPathname()));
		DMModuleManager::destroyInstance($this->object);
		$this->object = null;
	}

	function testGetAllModules() {
		$all = $this->object->getAllModules();
		$this->assertEquals(2, count($all));
		foreach ($all as $module) {
			$this->assertInstanceOf("DMBridgeModule", $module);
		}
	}

	function testGetEnabledModules() {
		$module = $this->object->getModuleByName("TestModule");
		$this->object->setModuleEnabled($module, true);
		$this->assertEquals(1, count($this->object->getEnabledModules()));
		$this->object->setModuleEnabled($module, false);
		$this->assertEquals(0, count($this->object->getEnabledModules()));
	}

	function testGetModuleByName() {
		$module = $this->object->getModuleByName("TestModule");
		$this->assertInstanceOf("DMBridgeModule", $module);
		$module = $this->object->getModuleByName("cats");
		$this->assertNull($module);
	}

	function testIsModuleEnabled() {
		$module = $this->object->getModuleByName("TestModule");
		$this->object->setModuleEnabled($module, true);
		$this->assertTrue($this->object->isModuleEnabled($module));
		$this->object->setModuleEnabled($module, false);
		$this->assertFalse($this->object->isModuleEnabled($module));
	}

	function testSetModuleEnabledWithCompatibleModule() {
		$module = $this->object->getModuleByName("TestModule");
		$this->object->setModuleEnabled($module, true);
		$this->assertTrue($this->object->isModuleEnabled($module));
	}

	function testSetModuleEnabledWithIncompatibleModule() {
		$module = $this->object->getModuleByName("TestIncompatibleModule");
		$this->setExpectedException("DMModuleActivationException");
		$this->object->setModuleEnabled($module, true);
	}

	function testModulesPathname() {
		$pathname = "sea bass";
		$this->object->setModulesPathname($pathname);
		$this->assertEquals($pathname, $this->object->getModulesPathname());
	}

}
