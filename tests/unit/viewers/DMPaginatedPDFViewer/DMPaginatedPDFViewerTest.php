<?php

class DMPaginatedPDFViewerTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMPaginatedPDFViewer
	 */
	private $object;

	protected function setUp() {
		$col = new DMCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$type = new DMMediaType("wlerkjwr", "lwkerjwlrkj");
		$this->object = new DMPaginatedPDFViewer($obj, $type, 100, 100);
	}

	function testGetHTMLTag() {
		$this->object->getHTMLTag();
	}

	function testGetHTMLTagNoJS() {
		$this->object->getHTMLTagNoJS();
	}

	function testGetName() {
		$this->assertEquals("Paginated PDF Viewer", $this->object->getName());
	}

}

