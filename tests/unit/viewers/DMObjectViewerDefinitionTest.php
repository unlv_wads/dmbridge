<?php

class DMObjectViewerDefinitionTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMObjectViewerDefinition
	 */
	private $object;

	protected function setUp() {
		$type = new DMMediaType("oranges", "peanuts");
		$this->object = new DMObjectViewerDefinition($type);
	}

	function testConstructor() {
		$this->assertTrue($this->object->getMediaType()->equals(
				new DMMediaType("oranges", "peanuts")));
	}

	function testClass() {
		$this->object->setClass("cats");
		$this->assertEquals("cats", $this->object->getClass());
	}

	function testMaxFileSize() {
		$this->object->setMaxFileSize(253);
		$this->assertEquals(253, $this->object->getMaxFileSize());
	}

	function testHeight() {
		$this->object->setHeight(234);
		$this->assertEquals(234, $this->object->getHeight());
	}

	function testMediaType() {
		$type = new DMMediaType("alkjfalekjr", "leakrjal;erj");
		$this->object->setMediaType($type);
		$this->assertSame($type, $this->object->getMediaType());
	}

	function testWidth() {
		$this->object->setWidth(345);
		$this->assertEquals(345, $this->object->getWidth());
	}

}

