<?php

class DMGenericObjectViewerTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMGenericObjectViewer
	 */
	private $object;

	protected function setUp() {
		$col = new DMCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$type = new DMMediaType("wlerkjwr", "lwkerjwlrkj");
		$this->object = new DMGenericObjectViewer($obj, $type, 100, 100);
	}

	function testGetHTMLTag() {
		$this->object->getHTMLTag();
	}

	function testGetName() {
		$this->assertEquals("Generic Object Viewer", $this->object->getName());
	}

}

