<?php

class DMObjectViewerDelegateManagerTest extends PHPUnit_Framework_TestCase {

	function testGetNameForClass() {
		$expected = "Generic Image Viewer";
		$actual = DMObjectViewerDelegateManager::getNameForClass("DMGenericImageViewer");
		$this->assertEquals($expected, $actual);
	}

	function getAllViewerDelegateClassNames() {
		$names = DMObjectViewerDelegateManager::getAllViewerDelegateClassNames();
		$this->assertEquals(6, count($names));
	}

	function getAllViewerDelegateNames() {
		$names = DMObjectViewerDelegateManager::getAllViewerDelegateNames();
		$this->assertEquals(6, count($names));
	}

}

