<?php

class DMNullViewerTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMNullViewer
	 */
	private $object;

	protected function setUp() {
		$col = new DMCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$type = new DMMediaType("wlerkjwr", "lwkerjwlrkj");
		$this->object = new DMNullViewer($obj, $type, 100, 100);
	}

	function testGetHTMLTag() {
		$this->object->getHTMLTag();
	}

	function testGetHTMLTagNoJS() {
		$this->object->getHTMLTagNoJS();
	}

	function testMessage() {
		$msg = "cats";
		$this->object->setMessage($msg);
		$this->assertEquals($msg, $this->object->getMessage());
	}

	function testGetName() {
		$this->assertEquals("Null Viewer", $this->object->getName());
	}

}

