<?php

class DMQuickTimeViewerTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMQuickTimeViewer
	 */
	private $object;

	protected function setUp() {
		$col = new DMCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$type = new DMMediaType("wlerkjwr", "lwkerjwlrkj");
		$this->object = new DMQuickTimeViewer($obj, $type, 100, 100);
	}

	function testGetHTMLTag() {
		$this->object->getHTMLTag();
	}

	function testGetName() {
		$this->assertEquals("QuickTime Viewer", $this->object->getName());
	}

}

