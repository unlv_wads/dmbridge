<?php

class DMGenericPDFViewerTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMGenericPDFViewer
	 */
	private $object;

	protected function setUp() {
		$col = new DMCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$type = new DMMediaType("wlerkjwr", "lwkerjwlrkj");
		$this->object = new DMGenericPDFViewer($obj, $type, 100, 100);
	}

	function testGetHTMLTag() {
		$this->object->getHTMLTag();
	}

	function testGetName() {
		$this->assertEquals("Generic PDF Viewer", $this->object->getName());
	}

}

