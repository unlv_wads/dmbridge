<?php

class DMGenericImageViewerTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var DMGenericImageViewer
	 */
	private $object;

	protected function setUp() {
		$col = new DMCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$type = new DMMediaType("wlerkjwr", "lwkerjwlrkj");
		$this->object = new DMGenericImageViewer($obj, $type, 100, 100);
	}

	function testGetHTMLTag() {
		$this->object->getHTMLTag();
	}

	function testGetName() {
		$this->assertEquals("Generic Image Viewer", $this->object->getName());
	}

}

