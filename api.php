<?php

/**
 * The official dmBridge PHP API include file. If you are using the dmBridge
 * PHP API, include this file and no others.
 */

require_once(dirname(__FILE__) . "/system/init/autoload.php");

