<?php
$relroot = substr(dirname($_SERVER['PHP_SELF']), 0, strlen(dirname($_SERVER['PHP_SELF'])) - 7);
header("Content-Type: text/css");
?>

/*************************** help ********************************/
/*
a.help:before {
	content: url('<?php echo $relroot ?>/images/fugue/information.png');
}

div#help a:before {
	content: url('<?php echo $relroot ?>/images/fugue/information.png');
}
*/
p.help:before {
	content: url('<?php echo $relroot ?>/images/fugue/information.png');
}

/*************************** flash ********************************/

*:before {
    padding-right:0.4em;
}

p.dmSuccess:before {
	content: url('<?php echo $relroot ?>/images/fugue/tick_circle.png');
}

p.dmNeutral:before {
	content: url('<?php echo $relroot ?>/images/fugue/information.png');
}

p.dmFailure:before {
	content: url('<?php echo $relroot ?>/images/fugue/cross_circle.png');
}

p.dmAlert:before {
	content: url('<?php echo $relroot ?>/images/fugue/exclamation.png');
}

/**************************** header *******************************/

div#header h4.user:before {
	content: url('<?php echo $relroot ?>/images/fugue/user.png');
}

/****************************** menu ******************************/

div#leftMenu span.ModuleSection:before {
	content: url('<?php echo $relroot ?>/images/fugue/puzzle.png');
}

div#leftMenu span#BasicSetup:before {
	content: url('<?php echo $relroot ?>/images/fugue/application__pencil.png');
}

div#leftMenu span#DataStore:before {
	content: url('<?php echo $relroot ?>/images/fugue/database.png');
}

div#leftMenu span#Modules:before {
	content: url('<?php echo $relroot ?>/images/fugue/puzzle.png');
}

div#leftMenu span#TemplateSets:before {
	content: url('<?php echo $relroot ?>/images/fugue/folders_stack.png');
}

div#leftMenu span#Collections:before {
	content: url('<?php echo $relroot ?>/images/fugue/images_stack.png');
}

div#leftMenu span#Comments:before {
	content: url('<?php echo $relroot ?>/images/fugue/balloon.png');
}

div#leftMenu span#Ratings:before {
	content: url('<?php echo $relroot ?>/images/fugue/star.png');
}

div#leftMenu span#Tags:before {
	content: url('<?php echo $relroot ?>/images/fugue/tag.png');
}

div#leftMenu span#Feeds:before {
	content: url('<?php echo $relroot ?>/images/fugue/feed.png');
}

div#leftMenu span#CQR:before {
	content: url('<?php echo $relroot ?>/images/fugue/magnifier.png');
}

div#leftMenu span#Documentation:before {
	content: url('<?php echo $relroot ?>/images/fugue/books_stack.png');
}

/************************** main ********************************/

fieldset#news legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/newspaper.png');
}

fieldset#version legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/server-cast.png');
}

/************************ basic setup **************************/

fieldset#basicInstitution legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/bank__pencil.png');
}

/************************ collections **************************/

fieldset#collectionSearchCollection legend:before,
fieldset#collectionObjectViewCollection legend:before,
fieldset#collectionResultsViewCollection legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/images_stack.png');
}

fieldset#collectionViewObjectView legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/application_image.png');
}

fieldset#collectionViewResultsView legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/application_list.png');
}

fieldset#collectionViewGridView legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/application_list.png');
}

fieldset#collectionViewTileView legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/application_icon.png');
}

fieldset#collectionFacets legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/block.png');
}

fieldset#collectionSearchDateSearch legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/calendar__pencil.png');
}

fieldset#collectionReferenceURLRedirectionCollection legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/arrow_turn.png');
}

fieldset#collectionTemplateSetDefaultCollection legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/images_stack.png');
}

fieldset#collectionTemplateSetCollection legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/images_stack.png');
}

/************************ modules **************************/

fieldset#modulesAll legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/puzzle.png');
}

/************************ template sets **************************/

fieldset#templatesChoose legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/folders_stack.png');
}

fieldset#templatesConfiguration legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/folder__pencil.png');
}

fieldset#templatesAllowedCollections legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/lock__pencil.png');
}

fieldset#templateSearchViewTemplateSet legend:before,
fieldset#templateObjectViewTemplateSet legend:before,
fieldset#templateResultsViewTemplateSet legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/images_stack.png');
}

fieldset#templateViewObjectView legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/application_image.png');
}

fieldset#templateViewGridView legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/application_list.png');
}

fieldset#templateViewTileView legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/application_icon.png');
}

fieldset#templateSearchDateSearch legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/calendar__pencil.png');
}

/************************ comments **************************/

fieldset#commentsConfiguration legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/balloon__pencil.png');
}

fieldset#commentsModerate legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/balloon__exclamation.png');
}

fieldset#commentsApproved legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/balloons.png');
}

fieldset#commentsOverall legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/balloons.png');
}

fieldset#commentsPerCollection legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/balloons.png');
}

fieldset#commentsMostCommented legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/images_stack.png');
}

/************************* ratings **************************/

fieldset#ratingsOverall legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/star.png');
}

fieldset#ratingsPerCollection legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/star.png');
}

fieldset#ratingsHighestRated legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/plus_circle.png');
}

fieldset#ratingsLowestRated legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/minus_circle.png');
}

fieldset#ratingsMostRated legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/images_stack.png');
}

/************************ tags **************************/

fieldset#tagsConfiguration legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/tag__pencil.png');
}

fieldset#tagsModerate legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/tag__exclamation.png');
}

fieldset#tagsApproved legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/tags.png');
}

fieldset#tagsOverall legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/tags.png');
}

fieldset#tagsPerCollection legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/tags.png');
}

fieldset#tagsMostTagged legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/images_stack.png');
}

/************************ feeds **************************/

fieldset#feedProviderInformation legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/bank__pencil.png');
}

fieldset#feedTraditionalView legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/application_text.png');
}

/************************ feeds **************************/

fieldset#cqrConvert legend:before {
	content: url('<?php echo $relroot ?>/images/fugue/magnifier.png');
}

