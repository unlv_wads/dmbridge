<?php include_once(dirname(__FILE__) . '/../includes/head.html.php'); ?>

</head>

<?php include_once(dirname(__FILE__) . '/../includes/body.html.php') ?>

	<h1>Custom Queries and Results</h1>

	<div id="content">

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form method="get" action="">
			<fieldset id="cqrConvert">
				<legend>Convert</legend>
				<table class="form" cellspacing="0" cellpadding="0" border="0">
					<tr class="odd">
						<th>
							<label for="inputCQR">CONTENTdm&reg; CQR URL</label>
						</th>
						<td>
							<textarea id="inputCQR" rows="16"
								   style="width:100%">Paste a CQR URL from CONTENTdm&reg; here.</textarea>
						</td>
					</tr>
					<tr>
						<th>
							<label for="collection">Template Set</label>
						</th>
						<td>
							<select id="collection">
								<option value="/dmdefault">Default</option>
								<?php foreach (DMCollection::getAuthorized() as $col): ?>
									<option value="<?php echo DMString::websafe($col->getAlias()) ?>"
											><?php echo DMString::websafe($col->getName()) ?></option>
								<?php endforeach ?>
							</select>
							<p class="help">The CQR code will point to the template set used by this
								collection.</p>
						</td>
					</tr>
					<tr class="odd">
						<th>
							<label for="outputCQR">dmBridge CQR URL</label>
						</th>
						<td>
							<textarea id="outputCQR" rows="6"
								   style="width:100%"></textarea>
							<p><a id="outputLink" href="#">Test It</a></p>
						</td>
					</tr>
				</table>
			</fieldset>

			<input id="host" type="hidden" value="<?php echo $_SERVER['SERVER_NAME'] ?>">
			<input id="dmBridgeRoot" type="hidden" value="<?php echo DMConfigIni::getInstance()->getString("dmbridge.base_uri_path") ?>">
			<input id="convert" type="button" value="Convert">
		</form>
	</div> <!-- #content -->

	<script type="text/javascript"
		src="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/admin/scripts/libraries.js"></script>
	<script type="text/javascript"
		src="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/admin/scripts/convertCQR.js"></script>

	<?php include_once(dirname(__FILE__) . '/../includes/footer.html.php') ?>
