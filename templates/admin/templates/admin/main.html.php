<?php

$absroot = dirname(__FILE__);

?>

	<?php include_once($absroot . '/../includes/head.html.php') ?>

</head>

<?php include_once($absroot . '/../includes/body.html.php') ?>

	<h1>Welcome, <?php echo DMHTTPRequest::getCurrent()->getSession()->getUser()->getUsername() ?>!</h1>

	<div id="content">

		<form action="" method="get">
			<fieldset id="news">
				<legend>News</legend>
				<div class="dmNews">
					<?php echo $view->getHelper()->getHtmlNews(); ?>
				</div>
			</fieldset>
			<fieldset id="version">
				<legend>System Information</legend>
				<div class="dmNews">
					<?php echo $view->getHelper()->getHtmlVersionDetail(); ?>
				</div>
			</fieldset>
		</form>

	</div> <!-- #content -->

	<?php include_once($absroot . '/../includes/footer.html.php') ?>
