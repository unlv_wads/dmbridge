<?php

$absroot = dirname(__FILE__);

?>

	<?php include_once($absroot . '/../includes/head.html.php') ?>

</head>

<?php include_once($absroot . '/../includes/body.html.php') ?>

	<div id="content">
		<h2>Login</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form method="post" action="">
			<fieldset id="userLogin">
				<table class="form" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<th>
							<label>Username</label>
						</th>
						<td>
							<input type="text" name="username" maxlength="30"
								style="width:50%" />
						</td>
					</tr>
					<tr>
						<th>
							<label>Password</label>
						</th>
						<td>
							<input type="password" name="password" maxlength="30"
								style="width:50%" />
						</td>
					</tr>
				</table>
			</fieldset>

			<input type="hidden" name="method" value="post" />
			<input type="submit" value="Login" />
		</form>
	</div> <!-- #content -->

	<?php include_once($absroot . '/../includes/footer.html.php') ?>
