	<?php include_once(dirname(__FILE__) . "/../includes/head.html.php") ?>

	<script type="text/javascript"
		src="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/admin/scripts/deleteTemplate.js"></script>

	<script type="text/javascript">
	$(document).ready(function() {
		init();
	});
	</script>

</head>

<?php include_once(dirname(__FILE__) . "/../includes/body.html.php") ?>

	<h1>Template Sets</h1>

	<div id="content">
		<h2>Overview</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form method="post" action="">
			<fieldset id="dataStoreXML">
				<legend>Registered Template Sets</legend>

				<table class="columnar" cellpadding="0" cellspacing="2" border="0">
					<tr>
						<th></th>
						<th></th>
						<th>Name</th>
						<th>Folder name</th>
						<th>Default collection</th>
					</tr>
					<?php $i = 0; foreach (DMConfigXML::getInstance()->getAllTemplateSets() as $ts): ?>
						<tr <?php (!($i & 1)) ? print 'class="odd"' : print '' ?>>
							<td><a href="<?php echo DMInternalURI::getURIWithParams("admin/templates/" . $ts->getID() . "/edit") ?>">Edit</a></td>
							<td>
								<a class="deleteTemplate" href="#">Delete</a>
								<div class="deleteTemplate">
									<p>Are you sure you want to delete this template set?</p>
									<p>The template set itself will not be deleted - only
									the reference to it.</p>
									<input type="hidden" name="id" value="<?php echo $ts->getID() ?>"/>
									<input type="submit" value="Delete"/>
									<input type="button" class="cancelDelete" value="Cancel" />
								</div>
							</td>
							<td><?php echo DMString::websafe($ts->getName()) ?></td>
							<td><code>templates/<?php echo DMString::paranoid(str_replace(' ', '_', strtolower($ts->getName())), array('_')) ?></code></td>
							<td><?php echo $ts->getDefaultCollection()->getName() ?></td>
						</tr>
					<?php $i++; endforeach ?>
				</table>
				<p class="help">A template set's default collection will be used
				in results view when the URI does not contain a collection alias.</p>
			</fieldset>
		</form>

	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . "/../includes/footer.html.php") ?>
