	<?php include_once(dirname(__FILE__) . "/../includes/head.html.php") ?>

</head>

<?php include_once(dirname(__FILE__) . "/../includes/body.html.php") ?>

	<h1>Template Sets</h1>

	<div id="content">
		<h2><?php echo DMString::websafe($view->getVar("template_set")->getName()) ?></h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form method="post" action="">
			<fieldset id="templatesConfiguration">
				<legend>Basic Information</legend>

				<table class="form" cellpadding="0" cellspacing="0" border="0">
					<tr class="odd">
						<th>Name</th>
						<td>
							<?php echo $view->getVar('name') ?>
						</td>
					</tr>
					<tr class="odd">
						<th>Folder path</th>
						<td>
							<input type="hidden" id="dmBridgeRoot"
								   value="<?php echo realpath(dirname(__FILE__) . "/../../../../") ?>">
							<code><span id="templateSetFolderPath"></span></code>
						</td>
					</tr>
				</table>
			</fieldset>

			<fieldset id="templatesAllowedCollections">
				<legend>Allowed Collections</legend>
				<table class="form" cellpadding="0" cellspacing="0" border="0">
					<tr class="odd">
						<th>Collection</th>
						<td>
							<table class="nested" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<th style="width:1%">Allowed</th>
									<th style="width:1%">Default</th>
									<th>Collection</th>
								</tr>
								<?php $cols = $view->getVar("all_collections");
								for ($i = 0; $i < count($cols); $i++):
									$ac = $view->getVar("allowed_collections");
									$dc = $view->getVar("default_collection"); ?>
									<tr>
										<td style="text-align:center">
											<?php echo $ac[$i] ?>
										</td>
										<td style="text-align:center">
											<?php if ($i > 0) echo $dc[$i]; ?>
										</td>
										<td><?php echo DMString::websafe($cols[$i]->getName()) ?></td>
									</tr>
								<?php endfor ?>
							</table>
						</td>
					</tr>
				</table>
			</fieldset>

			<fieldset id="collectionViewResultsViewLimit">
				<legend>Results Per Page</legend>
				<table class="form" cellspacing="0" cellpadding="0">
					<tr class="odd">
						<th>
							<label for="results_per_page">Results per page</label>
						</th>
						<td>
							<?php echo $view->getVar("results_per_page"); ?>
							<p class="help">This can be overridden with the <code>rpp</code>
								query parameter, e.g. <code>?rpp=30</code>.</p>
						</td>
					</tr>
				</table>
			</fieldset>

			<fieldset id="collectionViewTileView">
				<legend>Tile View (Results View)</legend>
				<table class="form" cellpadding="0" cellspacing="0" border="0">
					<tr class="odd">
						<th>
							<label>Columns</label>
						</th>
						<td>
							<?php echo $view->getVar("tile_cols"); ?>
						</td>
					</tr>
				</table>
			</fieldset>

			<input type="submit" value="Update" />
		</form>

	</div> <!-- #content -->

	<script type="text/javascript"
		src="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/admin/scripts/editTemplateSet.js"></script>

	<?php include_once(dirname(__FILE__) . "/../includes/footer.html.php") ?>
