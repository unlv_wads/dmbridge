	<?php include_once(dirname(__FILE__) . '/../includes/head.html.php') ?>

</head>

<?php include_once(dirname(__FILE__) . '/../includes/body.html.php') ?>

	<h1>Modules</h1>

	<div id="content">

		<h2>View</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form method="post" action="">
			<fieldset id="modulesAll">
				<legend>All Modules</legend>
				<table class="columnar" cellspacing="2" cellpadding="0" border="0">
					<tr>
						<th></th>
						<th>Enabled</th>
						<th>Name</th>
						<th>Version</th>
					</tr>
					<?php $i = 0;
					foreach ($view->getVar("modules") as $m): ?>
						<tr <?php (!($i & 1)) ? print 'class="odd"' : print '' ?>>
							<td style="width:1px">
								<input type="checkbox" name="name[]"
									value="<?php echo DMString::websafe($m->getName()) ?>" />
							</td>
							<td style="width:1px; text-align:center">
								<?php if ($view->getVar("manager")->isModuleEnabled($m)): ?>
									<span class="positive">✓</span>
								<?php else: ?>
									<span class="negative">X</span>
								<?php endif ?>
							</td>
							<td>
								<?php echo DMString::websafe($m->getName()) ?>
							</td>
							<td><?php echo DMString::websafe($m->getVersion()) ?></td>
						</tr>
					<?php $i++; endforeach ?>
				</table>
			</fieldset>

			<input type="submit" name="action" value="Enable Selected"/>
			<input type="submit" name="action" value="Disable Selected"/>
		</form>
	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . '/../includes/footer.html.php') ?>
