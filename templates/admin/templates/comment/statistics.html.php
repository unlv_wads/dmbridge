	<?php include_once(dirname(__FILE__) . '/../includes/head.html.php') ?>

</head>

<?php include_once(dirname(__FILE__) . '/../includes/body.html.php') ?>

	<h1>Comments</h1>

	<div id="content">
		<h2>Statistics</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form action="" method="post">

			<fieldset id="commentsOverall">
				<legend>Overall</legend>
				<table class="form" cellspacing="0" cellpadding="0" border="0">
					<tr class="odd">
						<th>
							<label>Number of comments in all collections</label>
						</th>
						<td>
							<?php echo $view->getVar("num_comments") ?>
						</td>
					</tr>
					<tr>
						<th>
							<label>Number of unique objects commented on</label>
						</th>
						<td>
							<?php echo $view->getVar("num_objects_with_approved_comments") ?>
						</td>
					</tr>
				</table>
			</fieldset>

			<fieldset id="commentsPerCollection">
				<legend>Comments Per Collection</legend>
				<table class="columnar" cellspacing="2" cellpadding="0" border="0">
					<tr>
						<th></th>
						<th></th>
						<th colspan="2">Per Object</th>
					</tr>
					<tr>
						<th>Collection</th>
						<th>Total</th>
						<th>Mean</th>
						<th>Median</th>
					</tr>
					<?php $i = 0; foreach ($view->getVar("collections") as $c):
						$ds = DMDataStoreFactory::getDataStore();
						$cq = new DMCommentQuery($ds);
						$cq->addCollection($c);
						$cq->setApproved(1);
						$cq->getSearchResults(); ?>
						<tr <?php if (!($i & 1)): ?>class="odd"<?php endif ?>
								style="text-align:center">
							<td style="text-align:left">
								<?php echo DMString::websafe($c->getName()) ?>
							</td>
							<td><?php echo $cq->getNumResults() ?></td>
							<td><?php echo round($ds->getMeanNumCommentsPerObjectInCollection($c), 2) ?></td>
							<td><?php echo $ds->getMedianNumCommentsPerObjectInCollection($c) ?></td>
						</tr>
					<?php $i++; endforeach ?>
				</table>
			</fieldset>

			<fieldset id="commentsMostCommented">
				<legend>Most Commented Objects</legend>
				<table class="columnar" cellspacing="2" cellpadding="0" border="0">
					<tr>
						<th>Title</th>
						<th>Collection</th>
						<th>Total</th>
					</tr>
					<?php $i = 0; foreach ($view->getVar("most_commented_objects") as $o):
						$cq = new DMCommentQuery(DMDataStoreFactory::getDataStore());
						$cq->setApproved(1);
						$cq->addObject($o);
						$cq->getSearchResults(); ?>
						<tr <?php if (!($i & 1)): ?>class="odd"<?php endif ?>>
							<td>
								<a title="<?php echo DMString::websafe(($o->getMetadata('title')) ? $o->getMetadata('title')->getValue() : "(Unknown)") ?>"
									href="<?php echo DMString::websafe($o->getURI(DMBridgeComponent::TemplateEngine)) ?>">
									<?php echo DMString::websafe(($o->getMetadata('title'))
											? DMString::truncate($o->getMetadata('title')->getValue(), 6) : "(Unknown)") ?>
								</a>
							</td>
							<td><?php echo DMString::websafe($o->getCollection()->getName()) ?></td>
							<td><?php echo $cq->getNumResults() ?></td>
						</tr>
					<?php $i++; endforeach ?>
				</table>
			</fieldset>

		</form>
	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . '/../includes/footer.html.php') ?>
