	<?php include_once(dirname(__FILE__) . '/../includes/head.html.php') ?>

</head>

<?php include_once(dirname(__FILE__) . '/../includes/body.html.php') ?>

	<h1>Comments</h1>

	<div id="content">

		<h2>Moderate</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form action="" method="post">
			<?php if (DMConfigXML::getInstance()->isCommentModerationEnabled()): ?>
				<?php if (count($view->getVar("comments")) > 0): ?>
					<fieldset id="commentsModerate">
						<legend>Moderate Comments</legend>
						<?php echo $view->getHelper()->getHtmlEditableCommentsAsTable($view->getVar("comments")) ?>
					</fieldset>

					<input type="submit" name="moderate" value="Approve Selected" />
					<input type="submit" name="moderate" value="Reject Selected" />

				<?php else: ?>
					<fieldset id="commentsModerate">
						<legend>Moderate Comments</legend>
						<p>There are no unmoderated comments.</p>
					</fieldset>
				<?php endif ?>

			<?php else: ?>
				<p>Comment moderation is disabled. All comments will be
					automatically approved.</p>
			<?php endif ?>

		</form>
	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . '/../includes/footer.html.php') ?>
