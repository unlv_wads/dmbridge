	<?php include_once(dirname(__FILE__) . '/../includes/head.html.php') ?>

</head>

<?php include_once(dirname(__FILE__) . '/../includes/body.html.php') ?>

	<h1>Comments</h1>

	<div id="content">

		<h2>Search</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form action="" method="get">
			<fieldset id="commentsSearch">
				<table class="form" cellspacing="0" cellpadding="0" border="0">
					<tr class="odd">
						<td>Collection</td>
						<td>
							<select name="alias">
								<option value="/dmdefault">All Collections</option>
								<?php foreach (DMCollection::getAuthorized() as $col): ?>
									<option value="<?php echo $col->getAlias()?>"
											<?php if ($col->equals($view->getVar("collection"))): ?>
											selected="selected"<?php endif ?>><?php echo DMString::websafe($col->getName()) ?></option>
								<?php endforeach ?>
							</select>
						</td>
					</tr>
					<tr class="odd">
						<td>Search Term</td>
						<td>
							<input type="text" name="q" maxlength="50"
								value="<?php echo $view->getVar("term") ?>"/>
							<input type="submit" value="Search"/>
						</td>
					</tr>
				</table>
			</fieldset>
		</form>

		<?php if (count($view->getVar("results")) > 0): ?>
			<form method="post" action="">
				<fieldset id="commentsSearchResults">
					<legend>Results</legend>
					<table class="form" cellspacing="2" cellpadding="0" border="0">
						<?php echo $view->getHelper()->getHtmlEditableCommentsAsTable($view->getVar("results")) ?>
					</table>
				</fieldset>

				<?php if ($view->getVar("num_results") > 0): ?>
					<input type="submit" name="action" value="Update Selected" />
					<input type="submit" name="action" value="Delete Selected" />
				<?php endif ?>
			</form>
		<?php elseif ($view->getVar("term")): ?>
			<p>No results.</p>
		<?php endif ?>

	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . '/../includes/footer.html.php') ?>
