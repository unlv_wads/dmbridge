	<?php include_once(dirname(__FILE__) . '/../includes/head.html.php') ?>

	<script type="text/javascript"
		src="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/admin/scripts/editComments.js"></script>

	<script type="text/javascript">
	$(document).ready(function() {
		init();
		toggleOnChange();
	});
	</script>

</head>

<?php include_once(dirname(__FILE__) . '/../includes/body.html.php') ?>

	<h1>Comments</h1>

	<div id="content">

		<h2>Configure</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form action="" method="post">
			<fieldset id="commentsConfiguration">
				<legend>Configuration</legend>
				<table class="form" cellspacing="0" cellpadding="0" border="0">
					<tr class="odd">
						<th>
							<label for="moderation">Enable</label>
						</th>
						<td>
							<?php echo $view->getVar("enable") ?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="moderation">Moderation</label>
						</th>
						<td>
							<?php echo $view->getVar("moderation") ?>
							<?php if ($view->getVar("are_unapproved_comments")): ?>
								<p class="help">Disabling comment moderation will automatically
									approve all unmoderated comments. </p>
							<?php endif ?>
						</td>
					</tr>
					<tr class="odd">
						<th>
							<label for="notification">Email notification of new comments</label>
						</th>
						<td>
							<?php echo $view->getVar("notification") ?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="notification_email">Email notification address</label>
						</th>
						<td>
							<?php echo $view->getVar("notification_email") ?>
						</td>
					</tr>
				</table>
			</fieldset>

			<input type="submit" value="Change" />
		</form>
	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . '/../includes/footer.html.php') ?>
