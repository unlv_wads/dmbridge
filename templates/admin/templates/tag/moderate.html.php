	<?php include_once(dirname(__FILE__) . "/../includes/head.html.php") ?>

</head>

<?php include_once(dirname(__FILE__) . "/../includes/body.html.php") ?>

	<h1>Tags</h1>

	<div id="content">
		<h2>Moderate</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form action="" method="post">
			<?php if ($view->getVar("moderation_is_enabled")): ?>
				<?php if (count($view->getVar("unapproved_tags")) > 0): ?>
					<fieldset id="tagsModerate">
						<legend>Moderate Tags</legend>
						<?php foreach ($view->getVar("unapproved_tags") as $t): ?>
							<?php try {
								$skip = false;
								$t->getObject();
							} catch (DMUnavailableModelException $e) {
								$skip = true;
							} ?>

							<table class="comments" cellpadding="0" cellspacing="2" border="0">
								<?php if ($skip): ?>
									<caption>Tag ID <?php echo $t->getID() ?> associated with invalid object.</caption>
								<?php else: ?>
									<caption>
										<label>
											<input type="checkbox" name="id[]"
												value="<?php echo DMString::websafe($t->getID()) ?>" />
											<?php echo DMString::websafe($t->getTimestamp()->format('M d, Y @ h:i A')) ?>
										</label>
									</caption>
									<tr>
										<td rowspan="3">
											<img src="<?php echo $t->getObject()->getThumbnailURL() ?>"
												 style="width:50px" />
										</td>
										<th>Object:</th>
										<td><a href="<?php echo DMString::websafe($t->getObject()->getURI(DMBridgeComponent::TemplateEngine)) ?>">
											<?php echo DMString::websafe($t->getObject()->getMetadata('title')) ?></a>
										</td>
									</tr>
									<tr>
										<th>Collection:</th>
										<td><?php echo DMString::websafe($t->getObject()->getCollection()->getName()) ?></td>
									</tr>
									<tr>
										<th>Text:</th>
										<td><?php echo DMString::websafe($t->getValue()) ?></td>
									</tr>
								<?php endif ?>
							</table>
						<?php endforeach ?>
					</fieldset>

					<input type="submit" name="moderate" value="Approve Selected" />
					<input type="submit" name="moderate" value="Reject Selected" />

				<?php else: ?>
					<fieldset id="tagsModerate">
						<legend>Moderate Tags</legend>
						<p>There are no unmoderated tags.</p>
					</fieldset>
				<?php endif ?>

			<?php else: ?>
				<p>Tag moderation is disabled. All tags will be
					automatically approved.</p>
			<?php endif ?>
		</form>
	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . "/../includes/footer.html.php") ?>
