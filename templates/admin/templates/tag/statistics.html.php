	<?php include_once(dirname(__FILE__) . "/../includes/head.html.php") ?>

</head>

<?php include_once(dirname(__FILE__) . "/../includes/body.html.php") ?>

	<h1>Tags</h1>

	<div id="content">
		<h2>Statistics</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form action="" method="post">
			<fieldset id="tagsOverall">
				<legend>Overall</legend>
				<table class="form" cellspacing="0" cellpadding="0" border="0">
					<tr class="odd">
						<th>
							<label>Number of unique objects tagged</label>
						</th>
						<td>
							<?php echo $view->getVar("num_with_approved_tags") ?>
						</td>
					</tr>
				</table>
			</fieldset>

			<fieldset id="tagsPerCollection">
				<legend>Tags Per Collection</legend>
				<table class="columnar" cellspacing="2" cellpadding="0" border="0">
					<tr>
						<th></th>
						<th></th>
						<th colspan="2">Per Object</th>
					</tr>
					<tr>
						<th>Collection</th>
						<th>Total</th>
						<th>Mean</th>
						<th>Median</th>
					</tr>
					<?php $i = 0; foreach ($view->getVar("all_collections") as $c): ?>
						<tr <?php (!($i & 1)) ? print 'class="odd"' : print '' ?>
								style="text-align:center">
							<td style="text-align:left">
								<?php echo DMString::websafe($c->getName()) ?>
							</td>
							<td><?php $tq = new DMTagQuery(DMDataStoreFactory::getDataStore());
								$tq->addCollection($c);
								$tq->setApproved(1);
								$tq->getSearchResultsAsObjects();
								echo $tq->getNumResults() ?></td>
							<td><?php echo round(DMDataStoreFactory::getDataStore()->getMeanNumTagsPerObjectInCollection($c), 2) ?></td>
							<td><?php echo DMDataStoreFactory::getDataStore()->getMedianNumTagsPerObjectInCollection($c) ?></td>
						</tr>
					<?php $i++; endforeach ?>
				</table>
			</fieldset>

			<fieldset id="tagsMostTagged">
				<legend>Most Tagged Objects</legend>
				<table class="columnar" cellspacing="2" cellpadding="0" border="0">
					<tr>
						<th>Title</th>
						<th>Collection</th>
						<th>Total</th>
					</tr>
					<?php $i = 0; foreach ($view->getVar("most_tagged") as $o):
						$tq = new DMTagQuery(DMDataStoreFactory::getDataStore());
						$tq->addObject($o);
						$tq->setApproved(1);
						$tq->getSearchResultsAsObjects();
						?>
						<tr <?php (!($i & 1)) ? print 'class="odd"' : print '' ?>>
							<td>
								<a title="<?php echo DMString::websafe($o->getMetadata('title')->getValue()) ?>"
									href="<?php echo DMString::websafe($o->getURI(DMBridgeComponent::TemplateEngine)) ?>">
									<?php echo DMString::websafe(DMString::truncate($o->getMetadata('title')->getValue(), 6)) ?>
								</a>
							</td>
							<td><?php echo DMString::websafe($o->getCollection()->getName()) ?></td>
							<td><?php echo $tq->getNumResults() ?></td>
						</tr>
					<?php $i++; endforeach ?>
				</table>
			</fieldset>

		</form>
	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . "/../includes/footer.html.php") ?>
