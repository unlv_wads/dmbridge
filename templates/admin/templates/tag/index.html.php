	<?php include_once(dirname(__FILE__) . '/../includes/head.html.php') ?>

</head>

<?php include_once(dirname(__FILE__) . '/../includes/body.html.php') ?>

	<h1>Tags</h1>

	<div id="content">

		<h2>View</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<?php if (count($view->getVar("tags"))): ?>
			<form method="post" action="">
				<fieldset id="tagsApproved">
					<legend>All Approved Tags</legend>
					<table class="columnar" cellspacing="2" cellpadding="0" border="0">
						<tr>
							<th>Value</th>
							<th>Count</th>
						</tr>
						<?php $i = 0; foreach ($view->getVar("tags") as $value => $freq): ?>
							<tr <?php (!($i & 1)) ? print 'class="odd"' : print '' ?>>
								<td>
									<label>
									<input type="checkbox" name="values[]"
										value="<?php echo DMString::websafe($value) ?>" />
										<?php echo DMString::websafe($value) ?></label>
								</td>
								<td>
									<?php echo DMString::websafe($freq) ?>
								</td>
							</tr>
						<?php $i++; endforeach ?>
					</table>
				</fieldset>

				<?php if ($view->getVar("num_approved_tags") > 0): ?>
					<input type="submit" value="Delete Selected" />
				<?php endif ?>
			</form>
		<?php else: ?>
			<p>There are no approved tags.</p>
		<?php endif ?>

	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . '/../includes/footer.html.php') ?>
