	<?php include_once(dirname(__FILE__) . "/../includes/head.html.php") ?>

</head>

<?php include_once(dirname(__FILE__) . "/../includes/body.html.php") ?>

	<h1>Tags</h1>

	<div id="content">
		<h2>Configure</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form action="" method="post">
			<fieldset id="tagsConfiguration">
				<legend>Configuration</legend>
				<table class="form" cellspacing="0" cellpadding="0" border="0">
					<tr class="odd">
						<th>
							<label for="enable">Enable</label>
						</th>
						<td>
							<?php echo $view->getVar("enable") ?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="moderation">Moderation</label>
						</th>
						<td>
							<?php echo $view->getVar("moderation") ?>
						</td>
					</tr>
				</table>
			</fieldset>

			<?php
			if ($view->getVar("num_unapproved_tags")): ?>
				<p class="dmAlert">Disabling tag moderation will automatically
					approve the <?php echo $view->getVar("num_unapproved_tags") ?>
					unmoderated tag(s).</p>
			<?php endif ?>

			<input type="submit" value="Change" />
		</form>
	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . "/../includes/footer.html.php") ?>
