	<?php include_once(dirname(__FILE__) . "/../includes/head.html.php") ?>

</head>

<body>
	<div id="wrap">
		<div id="header">
			<table cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<h4>dmBridge Control Panel | <a href="/cgi-bin/admin/start.exe">CONTENTdm&reg; Administration</a></h4>
					</td>
				</tr>
			</table>
		</div> <!-- #wrap -->

		<div id="error">
			<h2>Fatal Error</h2>

			<?php echo $view->getHelper()->getHtmlException() ?>

		</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . "/../includes/footer.html.php") ?>
