<?php

$relroot = dirname($_SERVER['PHP_SELF']) . '/views';
$absroot = dirname(__FILE__);

?>

	<?php include_once($absroot . '/../includes/head.html.php') ?>

</head>

<?php include_once($absroot . '/../includes/body.html.php') ?>

	<h1>Ratings</h1>

	<div id="content">
		<h2>Statistics</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form action="" method="post">
			<fieldset id="ratingsOverall">
				<legend>Overall</legend>
				<table class="form" cellspacing="0" cellpadding="0" border="0">
					<tr class="odd">
						<th>
							<label>Number of unique objects rated</label>
						</th>
						<td>
							<?php echo DMDataStoreFactory::getDataStore()->getNumObjectsWithRatings() ?>
						</td>
					</tr>
				</table>
			</fieldset>

			<fieldset id="ratingsPerCollection">
				<legend>Ratings Per Collection</legend>
				<table class="columnar" cellspacing="2" cellpadding="0" border="0">
					<tr>
						<th></th>
						<th></th>
						<th colspan="2">Per Object</th>
					</tr>
					<tr>
						<th>Collection</th>
						<th>Total</th>
						<th>Mean</th>
						<th>Median</th>
					</tr>
					<?php $i = 0; foreach (DMCollection::getAuthorized() as $c): ?>
						<tr <?php (!($i & 1)) ? print 'class="odd"' : print '' ?>
								style="text-align:center">
							<td style="text-align:left">
								<?php echo DMString::websafe($c->getName()) ?>
							</td>
							<td><?php echo DMDataStoreFactory::getDataStore()->getNumRatingsInCollection($c) ?></td>
							<td><?php echo round(DMDataStoreFactory::getDataStore()->getMeanNumRatingsPerObjectInCollection($c), 2) ?></td>
							<td><?php echo round(DMDataStoreFactory::getDataStore()->getMedianNumRatingsPerObjectInCollection($c), 2) ?></td>
						</tr>
					<?php $i++; endforeach ?>
				</table>
			</fieldset>

			<fieldset id="ratingsHighestRated">
				<legend>Highest Rated Objects</legend>
				<table class="columnar" cellspacing="2" cellpadding="0" border="0">
					<tr>
						<th>Title</th>
						<th>Collection</th>
						<th>Rating</th>
					</tr>
					<?php $i = 0; foreach (DMDataStoreFactory::getDataStore()->getObjectsWithHighestRatings(20) as $o): ?>
						<tr <?php (!($i & 1)) ? print 'class="odd"' : print '' ?>>
							<td>
								<a title="<?php echo DMString::websafe($o->getMetadata('title')->getValue()) ?>"
									href="<?php echo DMString::websafe($o->getURI(DMBridgeComponent::TemplateEngine)) ?>">
									<?php echo DMString::websafe(DMString::truncate($o->getMetadata('title')->getValue(), 6)) ?>
								</a>
							</td>
							<td><?php echo DMString::websafe($o->getCollection()->getName()) ?></td>
							<td><?php echo $o->getRating() ?></td>
						</tr>
					<?php $i++; endforeach ?>
				</table>
			</fieldset>

			<fieldset id="ratingsLowestRated">
				<legend>Lowest Rated Objects</legend>
				<table class="columnar" cellspacing="2" cellpadding="0" border="0">
					<tr>
						<th>Title</th>
						<th>Collection</th>
						<th>Rating</th>
					</tr>
					<?php $i = 0; foreach (DMDataStoreFactory::getDataStore()->getObjectsWithLowestRatings(20) as $o): ?>
						<tr <?php (!($i & 1)) ? print 'class="odd"' : print '' ?>>
							<td>
								<a title="<?php echo DMString::websafe($o->getMetadata('title')->getValue()) ?>"
									href="<?php echo DMString::websafe($o->getURI(DMBridgeComponent::TemplateEngine)) ?>">
									<?php echo DMString::websafe(DMString::truncate($o->getMetadata('title')->getValue(), 6)) ?>
								</a>
							</td>
							<td><?php echo DMString::websafe($o->getCollection()->getName()) ?></td>
							<td><?php echo $o->getRating() ?></td>
						</tr>
					<?php $i++; endforeach ?>
				</table>
			</fieldset>

			<fieldset id="ratingsMostRated">
				<legend>Most Rated Objects</legend>
				<table class="columnar" cellspacing="2" cellpadding="0" border="0">
					<tr>
						<th>Title</th>
						<th>Collection</th>
						<th>Total</th>
					</tr>
					<?php $i = 0; foreach (DMDataStoreFactory::getDataStore()->getObjectsWithMostRatings(20) as $o): ?>
						<tr <?php (!($i & 1)) ? print 'class="odd"' : print '' ?>>
							<td>
								<a title="<?php echo DMString::websafe($o->getMetadata('title')->getValue()) ?>"
									href="<?php echo DMString::websafe($o->getURI(DMBridgeComponent::TemplateEngine)) ?>">
									<?php echo DMString::websafe(DMString::truncate($o->getMetadata('title')->getValue(), 6)) ?>
								</a>
							</td>
							<td><?php echo DMString::websafe($o->getCollection()->getName()) ?></td>
							<td><?php echo DMDataStoreFactory::getDataStore()->getNumRatingsForObject($o) ?></td>
						</tr>
					<?php $i++; endforeach ?>
				</table>
			</fieldset>
		</form>
	</div> <!-- #content -->

	<?php include_once($absroot . '/../includes/footer.html.php') ?>
