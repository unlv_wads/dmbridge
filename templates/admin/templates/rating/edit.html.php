	<?php include_once(dirname(__FILE__) . "/../includes/head.html.php") ?>

</head>

<?php include_once(dirname(__FILE__) . "/../includes/body.html.php") ?>

	<h1>Ratings</h1>

	<div id="content">
		<h2>Configure</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form action="" method="post">
			<fieldset id="ratingsConfiguration">
				<legend>Configuration</legend>
				<table class="form" cellspacing="0" cellpadding="0" border="0">
					<tr class="odd">
						<th>
							<label for="enable">Enable</label>
						</th>
						<td>
							<?php echo $view->getVar("enable") ?>
						</td>
					</tr>
				</table>
			</fieldset>

			<input type="submit" value="Change" />
		</form>
	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . "/../includes/footer.html.php") ?>
