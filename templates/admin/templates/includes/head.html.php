<?php

$relroot = dirname($_SERVER['PHP_SELF']) . "/templates/admin";

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="expires" content="FRI, 13 APR 1999 01:00:00 GMT">
		<meta name="robots" content="noindex, nofollow, noarchive">

		<title>dmBridge Control Panel</title>

		<link rel="stylesheet" type="text/css"
			href="<?php echo $relroot ?>/styles/main.css">
		<link rel="stylesheet" type="text/css"
			href="<?php echo $relroot ?>/styles/icons.css.php">

		<script type="text/javascript"
			src="<?php echo dirname($_SERVER['PHP_SELF']) ?>/includes/js/jquery-1.6.min.js"></script>
		<script type="text/javascript"
			src="<?php echo $relroot ?>/scripts/common.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			initMenuAccordion();
		});
		</script>
