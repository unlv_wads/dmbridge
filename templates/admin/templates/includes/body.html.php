<body>
	<div id="wrap">
		<div id="header">
			<table cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<h4><a href="<?php echo DMInternalURI::getURIWithParams("admin") ?>">dmBridge Control Panel</a>
							| <a href="<?php echo DMConfigIni::getInstance()->getString("contentdm.start_exe.path") ?>">CONTENTdm&reg; Administration</a></h4>
					</td>
					<td style="text-align:right">
						<?php if (DMHTTPRequest::getCurrent()->getSession()->getUser()): ?>
						<h4 class="user"><?php echo DMHTTPRequest::getCurrent()->getSession()->getUser()->getUsername() ?> |
							<a href="<?php echo DMInternalURI::getURIWithParams("admin/logout") ?>">Logout</a></h4>
						<?php endif ?>
					</td>
				</tr>
			</table>
		</div> <!-- #wrap -->

		<?php if (DMHTTPRequest::getCurrent()->getSession()->getUser()): ?>
			<div id="leftMenu">
				<?php echo $view->getHelper()->getHtmlMainMenu() ?>
			</div>
		<?php endif ?>
