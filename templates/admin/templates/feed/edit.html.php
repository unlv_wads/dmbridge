	<?php include_once(dirname(__FILE__) . "/../includes/head.html.php") ?>

</head>

<?php include_once(dirname(__FILE__) . "/../includes/body.html.php") ?>

	<h1>Feeds</h1>

	<div id="content">

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form method="post" action="">
			<fieldset id="feedProviderInformation">
				<legend>Provider Information</legend>
				<table class="form" cellpadding="0" cellspacing="0" border="0">
					<tr class="odd">
						<th>
							<label for="title">Title</label>
						</th>
						<td>
							<?php echo $view->getVar("title") ?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="subtitle">Subtitle</label>
						</th>
						<td>
							<?php echo $view->getVar("subtitle") ?>
						</td>
					</tr>
					<tr class="odd">
						<th>
							<label for="copyright">Copyright notice</label>
						</th>
						<td>
							<?php echo $view->getVar("copyright"); ?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="language">Language</label>
						</th>
						<td>
							<?php echo $view->getVar("language"); ?>
						</td>
					</tr>
					<tr class="odd">
						<th>
							<label for="managing_editor_name">Managing editor name</label>
						</th>
						<td>
							<?php echo $view->getVar("managing_editor_name") ?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="managing_editor_email">Managing editor email</label>
						</th>
						<td>
							<?php echo $view->getVar("managing_editor_email") ?>
						</td>
					</tr>
					<tr class="odd">
						<th>
							<label for="webmaster_name">Webmaster name</label>
						</th>
						<td>
							<?php echo $view->getVar("webmaster_name") ?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="webmaster_email">Webmaster email</label>
						</th>
						<td>
							<?php echo $view->getVar("webmaster_email") ?>
						</td>
					</tr>
				</table>
			</fieldset>

			<input type="submit" value="Change" />
		</form>
	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . "/../includes/footer.html.php") ?>
