<?php include_once(dirname(__FILE__) . '/../includes/head.html.php'); ?>

</head>

<?php include_once(dirname(__FILE__) . '/../includes/body.html.php') ?>

	<h1>Basic Setup</h1>

	<div id="content">

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form method="post" action="">
			<fieldset id="basicInstitution">
				<legend>Institution</legend>
				<table class="form" cellspacing="0" cellpadding="0" border="0">
					<tr class="odd">
						<th>
							<label for="institution_name">Name</label>
						</th>
						<td>
							<?php echo $view->getVar("institution_name"); ?>
							<p class="help">Output in Atom feeds, among (potentially) other places.</p>
						</td>
					</tr>
				</table>
			</fieldset>

			<input type="submit" value="Change" />
		</form>
	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . '/../includes/footer.html.php') ?>
