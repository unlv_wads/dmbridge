	<?php include_once(dirname(__FILE__) . "/../includes/head.html.php") ?>

</head>

<?php include_once(dirname(__FILE__) . "/../includes/body.html.php") ?>

	<h1>Collections</h1>

	<div id="content">
		<h2>Overview</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form method="post" action="">
			<fieldset id="allCollections">
				<legend>All Collections</legend>

				<table class="columnar" cellpadding="0" cellspacing="2" border="0">
					<tr>
						<th colspan="4">&nbsp;</th>
						<th colspan="4">Custom preferences</th>
					</tr>
					<tr>
						<th></th>
						<th>Name</th>
						<th>Alias</th>
						<th># Objects</th>
						<th>Template set</th>
						<th>Object view</th>
						<th>Results view</th>
						<th>Search view</th>
					</tr>
					<tr>
						<?php $default_col = DMCollectionFactory::getCollection("/dmdefault") ?>
						<td><a href="<?php echo DMInternalURI::getURIWithParams("admin/collections/dmdefault/edit") ?>">Edit</a></td>
						<td><?php echo $default_col->getName() ?> (inherited by other collections)</td>
						<td><?php echo $default_col->getAlias() ?></td>
						<td>-</td>
						<td><?php echo $default_col->getTemplateSet()->getName() ?></td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
					</tr>
					<?php $i = 0; foreach (DMCollection::getAuthorized() as $col): ?>
						<tr <?php (!($i & 1)) ? print 'class="odd"' : print '' ?>>
							<td><a href="<?php echo DMInternalURI::getURIWithParams("admin/collections" . $col->getAlias() . "/edit") ?>">Edit</a></td>
							<td><?php echo DMString::websafe($col->getName()) ?></td>
							<td><?php echo $col->getAlias() ?></td>
							<td><?php echo $col->getNumObjects() ?></td>
							<td><?php echo $col->isUsingDefaultTemplateSet() ? "Inherit" : '<span class="positive">&#10003;</span>' ?></td>
							<td><?php echo $col->hasCustomObjectViewPrefs() ? '<span class="positive">&#10003;</span>' : "Inherit" ?></td>
							<td><?php echo $col->hasCustomResultsViewPrefs() ? '<span class="positive">&#10003;</span>' : "Inherit" ?></td>
							<td><?php echo $col->hasCustomSearchViewPrefs() ? '<span class="positive">&#10003;</span>' : "Inherit" ?></td>
						</tr>
					<?php $i++; endforeach ?>
				</table>
				<p class="help">To add or remove a collection, you must use the
					<a href="<?php echo DMConfigIni::getInstance()->getString("contentdm.start_exe.path") ?>">CONTENTdm&reg; administration
						module</a>.</p>
			</fieldset>
		</form>

	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . "/../includes/footer.html.php") ?>
