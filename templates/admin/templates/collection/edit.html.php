	<?php include_once(dirname(__FILE__) . "/../includes/head.html.php") ?>

	<script type="text/javascript"
		src="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/admin/scripts/editCollection.js"></script>

	<script type="text/javascript">
	$(document).ready(function() {
		init();
	});
	</script>

</head>

<?php include_once(dirname(__FILE__) . "/../includes/body.html.php") ?>

	<h1>Collections</h1>

	<div id="content">
		<h2><?php echo DMString::websafe($view->getVar("collection")->getName()) ?></h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<p class="help">Any settings made here will apply to all collections,
			but can be overridden on a per-collection basis.</p>

		<form method="post" action="">
			<?php if (!$view->getVar("collection")->isDefault()): ?>
				<fieldset id="collectionOverviewPageURL">
					<legend>Basic Informtion</legend>
					<table class="form" cellspacing="0" cellpadding="0">
						<tr class="odd">
							<th>
								<label for="overview_page_url">Overview page URL</label>
							</th>
							<td>
								<?php echo $view->getVar("overview_page_url"); ?>
								<p class="help">Overview page URLs serve to
									direct visitors to a web page with more
									information about a particular collection.
									They are exposed only in the context of the
									HTTP API, and are optional.</p>
							</td>
						</tr>
						<tr>
							<th>
								<label for="image_url_512">Image URL</label>
							</th>
							<td>
								<?php echo $view->getVar("image_url_512"); ?>
								<p class="help">The URL of a representative
									image for the collection. This should be
									constrained to 512 pixels on its longest
									side, and optimized for fast downloading.</p>
							</td>
						</tr>
						<tr class="odd">
							<th>
								<label for="description">Description</label>
							</th>
							<td>
								<?php echo $view->getVar("description"); ?>
								<p class="help">A brief description of the
									collection, no more than a few paragraphs.</p>
							</td>
						</tr>
					</table>
				</fieldset>
			<?php endif ?>

			<h3>Template Set</h3>
			<?php if (!$view->getVar("collection")->isDefault()): ?>
				<fieldset id="collectionTemplateSetOverride">
					<table class="form" cellspacing="0" cellpadding="0" border="0">
						<tr class="odd">
							<th>Definitions</th>
							<td>
								<?php foreach ($view->getVar("inherit_template_set") as $field): ?>
									<label><?php echo $field; ?></label>
								<?php endforeach ?>
							</td>
						</tr>
					</table>
				</fieldset>
			<?php endif ?>

			<fieldset id="collectionTemplateSet">
				<table class="form" cellspacing="0" cellpadding="0">
					<tr class="odd">
						<th>
							<label for="template_set">Template set</label>
						</th>
						<td>
							<?php echo $view->getVar("template_set"); ?>
						</td>
					</tr>
				</table>
			</fieldset>

			<h3>Object View</h3>
			<?php if (!$view->getVar("collection")->isDefault()): ?>
				<fieldset id="collectionObjectViewOverride">
					<table class="form" cellspacing="0" cellpadding="0" border="0">
						<tr class="odd">
							<th>Definitions</th>
							<td>
								<?php foreach ($view->getVar("inherit_object_view") as $field): ?>
									<label><?php echo $field; ?></label>
								<?php endforeach ?>
							</td>
						</tr>
					</table>
				</fieldset>
			<?php endif ?>

			<fieldset id="collectionViewObjectView">
				<legend>Viewer Assignments</legend>
				
				<p class="help">Object files larger than the given max file size will
					be rendered with the File Link Viewer. (This only applies to files,
					not images.)</p>
				
				<table class="columnar" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<th style="width:1%">&nbsp;</th>
						<th style="width:40%">MIME Type</th>
						<th style="width:1%">Viewer</th>
						<th style="width:1%">Width</th>
						<th style="width:1%">Height</th>
						<th style="width:1%">Max File Size (KB)</th>
					</tr>
					<?php $vds = $view->getVar("collection")->getViewerDefinitions() ?>
					<?php for ($i = 0; $i < count($vds); $i++): ?>
						<tr <?php if (!($i & 1)): ?>class="odd"<?php endif ?>>
							<td>
								<input type="image" onclick="return false;"
									   class="removeViewerDefinitionButton"
									src="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/admin/images/fugue/minus_circle.png"/>
							</td>
							<td>
								<?php $f = $view->getVar("mime"); echo $f[$i]; ?>
							</td>
							<td>
								<?php $f = $view->getVar("class"); echo $f[$i]; ?>
							</td>
							<td>
								<?php $f = $view->getVar("width"); echo $f[$i]; ?>
							</td>
							<td>
								<?php $f = $view->getVar("height"); echo $f[$i]; ?>
							</td>
							<td>
								<?php $f = $view->getVar("max_file_size_k"); echo $f[$i]; ?>
							</td>
						</tr>
					<?php endfor ?>
				</table>
				<label id="addViewerDefinitionLabel"><input type="image" onclick="return false" id="addViewerDefinition"
					src="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/admin/images/fugue/plus_circle.png"/>Add
					Viewer Assignment</label>
			</fieldset>

			<h3>Results View</h3>
			<?php if (!$view->getVar("collection")->isDefault()): ?>
				<fieldset id="collectionResultsViewOverride">
					<table class="form" cellspacing="0" cellpadding="0" border="0">
						<tr class="odd">
							<th>Definitions</th>
							<td>
								<?php foreach ($view->getVar("inherit_results_view") as $field): ?>
									<label><?php echo $field; ?></label>
								<?php endforeach ?>
							</td>
						</tr>
					</table>
				</fieldset>
			<?php endif ?>

			<fieldset id="collectionViewResultsView">
				<legend>Fields</legend>
				<table class="columnar" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<th>Field</th>
						<th>Sortable</th>
						<th>Default Sort</th>
						<th>Max Words</th>
					</tr>
					<?php $fields = $view->getVar("collection")->getFields(); ?>
					<?php for ($i = 0; $i < count($fields); $i++): ?>
						<tr <?php (!($i & 1)) ? print 'class="odd"' : print '' ?>>
							<td><?php echo DMString::websafe($fields[$i]->getName()) ?></td>
							<td style="text-align:center">
								<?php $f = $view->getVar("field_nicks"); echo $f[$i]; ?>
								<?php $f = $view->getVar("field_nicks_sortable"); echo $f[$i]; ?>
							</td>
							<td style="text-align:center">
								<?php $f = $view->getVar("field_nicks_default_sort"); echo $f[$i]; ?>
							</td>
							<td style="text-align:center">
								<?php $f = $view->getVar("field_nicks_max_words"); echo $f[$i]; ?>
							</td>
						</tr>
					<?php endfor ?>
				</table>
			</fieldset>

			<fieldset id="collectionViewGridView">
				<legend>Grid &amp; List Views</legend>
				<table class="form" cellspacing="0" cellpadding="0" border="0">
					<?php $i = 0; foreach ($view->getVar("grid_fields") as $field): ?>
						<tr <?php (!($i & 1)) ? print 'class="odd"' : print '' ?>>
							<td>Position <?php echo $i+1 ?></td>
							<td>
								<?php echo $field; ?>
							</td>
						</tr>
					<?php $i++; endforeach ?>
				</table>
			</fieldset>

			<fieldset id="collectionFacets">
				<legend>Facets</legend>
				<table class="form" cellspacing="0" cellpadding="0" border="0">
					<tr class="odd">
						<td style="width:70%">Displayed Facets
							<p class="help">Facets can be displayed in the templates using the
								<code>$view->getHelper()->getHtmlFacetsAsUL()</code> method call.</p></td>
						<td>
							<ul>
							<?php foreach ($view->getVar("facet_fields") as $field): ?>
								<li><label><?php echo $field; ?></label></li>
							<?php endforeach ?>
							</ul>
						</td>
					</tr>
				</table>
			</fieldset>

			<h3>Search View</h3>
			<?php if (!$view->getVar("collection")->isDefault()): ?>
				<fieldset id="collectionSearchView">
					<table class="form" cellspacing="0" cellpadding="0">
						<tr class="odd">
							<th>Definitions</th>
							<td>
								<?php foreach ($view->getVar("inherit_search_view") as $field): ?>
									<label><?php echo $field; ?></label>
								<?php endforeach ?>
							</td>
						</tr>
					</table>
				</fieldset>
			<?php endif ?>

			<fieldset id="collectionSearchDateSearch">
				<legend>Date Search</legend>
				<table class="form" cellspacing="0" cellpadding="0">
					<tr class="odd">
						<th>
							<label for="begin_year">Begin Year</label>
						</th>
						<td>
							<?php echo $view->getVar("begin_year"); ?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="end_year">End Year</label>
						</th>
						<td>
							<?php echo $view->getVar("end_year"); ?>
						</td>
					</tr>
				</table>
			</fieldset>

			<input type="submit" value="Change" />
		</form>

	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . "/../includes/footer.html.php") ?>
