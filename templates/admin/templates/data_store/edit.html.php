	<?php include_once(dirname(__FILE__) . "/../includes/head.html.php") ?>

	<script type="text/javascript"
		src="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/admin/scripts/editDataStore.js"></script>

	<script type="text/javascript">
	$(document).ready(function() {
		init();
		toggleOnChange();
	});
	</script>

</head>

<?php include_once(dirname(__FILE__) . "/../includes/body.html.php") ?>

	<h1>Data Store</h1>

	<div id="content">

		<h2>Configure</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form method="post" action="">
			<fieldset id="dataStoreEngine">
				<legend>Storage Engine</legend>
				<table class="form" cellspacing="0" cellpadding="0" border="0">
					<tr class="odd">
						<th><label for="engine">Storage Engine</label></th>
						<td>
							<?php echo $view->getVar("engine"); ?>
							<p id="sqlitehelp" class="help">dmBridge will use the following
							SQLite database file:
							<code><?php
							echo DMConfigXML::getInstance()->getDataDir() ?>/dmbridge.sqlite</code>.
							If it does not exist, it will be created automatically.
							To change the location of this file, change
							<code>dmbridge.data_path</code> in config.ini.</p>
						</td>
					</tr>
				</table>
			</fieldset>

			<?php if (!$view->getVar("data_store_is_ok")): ?>
				<p class="dmFlash dmFailure dmDataStoreStatus"><?php echo $view->getVar("error_message") ?></p>
			<?php endif ?>


			<fieldset id="dataStoreConnectionSettings">
				<legend>Database Connection Settings</legend>
				<table class="form" cellspacing="0" cellpadding="0" border="0">
					<tr id="dbhost_row" class="odd">
						<th><label for="dbhost">Host</label></th>
						<td>
							<?php echo $view->getVar("dbhost") ?>
							<p class="help">Set to <code>localhost</code> or leave blank
							for local socket connection.</p>
						</td>
					</tr>
					<tr id="dbport_row">
						<th>
							<label for="dbport">Port (optional)</label>
						</th>
						<td>
							<?php echo $view->getVar("dbport") ?>
						</td>
					</tr>
					<tr id="dbname_row" class="odd">
						<th>
							<label for="dbname">Database name</label>
						</th>
						<td>
							<?php echo $view->getVar("dbname") ?>
						</td>
					</tr>
					<tr id="dbuser_row">
						<th>
							<label for="dbuser">Username</label>
						</th>
						<td>
							<?php echo $view->getVar("dbuser") ?>
						</td>
					</tr>
					<tr id="dbpass_row" class="odd">
						<th>
							<label for="dbpass">Password</label>
						</th>
						<td>
							<?php echo $view->getVar("dbpass") ?>
						</td>
					</tr>
				</table>
			</fieldset>
			<input type="submit" value="Change" />
		</form>

	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . "/../includes/footer.html.php") ?>
