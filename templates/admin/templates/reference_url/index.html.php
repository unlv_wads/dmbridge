	<?php include_once(dirname(__FILE__) . "/../includes/head.html.php") ?>

</head>

<?php include_once(dirname(__FILE__) . "/../includes/body.html.php") ?>

	<h1>Collections</h1>

	<div id="content">
		<h2>Reference URL Redirection</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form method="post" action="">
			<?php if (!$view->getVar("script_installed")): ?>
				<p class="dmFailure">It appears that the dmBridge
				reference URL rerouting script has
				not been installed. In order for any settings you make here to
				work, you must install it. See the user's manual for more
				information.</a></p>
			<?php endif ?>

			<fieldset id="collectionReferenceURLRedirectionCollection">
				<legend>Rules</legend>
				<table class="columnar" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<th>Collection</th>
						<th>Route to CONTENTdm&reg; templates</th>
						<th>Route to dmBridge templates</th>
					</tr>
					<?php $i = 1; foreach ($view->getVar("all_collections") as $c): ?>
						<tr <?php echo ($i & 1) ? 'class="odd"' : '' ?>>
							<td>
								<?php echo DMString::websafe($c->getName()) ?>
							</td>
							<td style="text-align:center">
								<input type="radio" name="set[<?php echo DMString::websafe($c->getAlias()) ?>]"
									<?php if (!$c->isRedirectingReferenceURLs()): ?>checked="checked"<?php endif ?>
									value="cdm"/>
							</td>
							<td style="text-align:center">
								<input type="radio" name="set[<?php echo DMString::websafe($c->getAlias()) ?>]"
									<?php if ($c->isRedirectingReferenceURLs()): ?>checked="checked"<?php endif ?>
									value="dmbridge"/>
							</td>
						</tr>
					<?php $i++; endforeach ?>
				</table>
			</fieldset>

			<input type="submit" value="Change" />
		</form>
	</div> <!-- #content -->

	<?php include_once(dirname(__FILE__) . "/../includes/footer.html.php") ?>
