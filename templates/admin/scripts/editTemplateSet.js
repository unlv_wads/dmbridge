/*
 * dmBridge: a data access framework for CONTENTdm(R)
 *
 * Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
 * Education, on behalf of the University of Nevada, Las Vegas
 */

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */

$(document).ready(function() {
	init();
	selectivelyDisableDefaultRadio();
});

function init() {
	// collection selection checkboxes
	$('#all_aliases').click(function() {
		if (this.checked) {
			$("input[name='aliases\\[\\]']").each(function() {
				if (this.getAttribute('id') != 'all_aliases') {
					this.disabled = true;
				}
			});
		} else {
			$("input[name='aliases\\[\\]']").each(function() {
				this.disabled = false;
			});
		}
	});

	if ($('#all_aliases').attr('checked')) {
		$("input[name='aliases\\[\\]']").each(function() {
			if (this.getAttribute('id') != 'all_aliases') {
				this.disabled = true;
			}
		});
	}

	$('input[type=checkbox]').click(function() {
		selectivelyDisableDefaultRadio()
	});
	
	updateFolderPath();
	addNameChangedEventHandler();
}

function selectivelyDisableDefaultRadio() {
	$("input[type=radio]").each(function() {
		this.disabled = true;
	});

	var i = 0;
	$("fieldset#templatesAllowedCollections input[type=checkbox]").each(function() {
		if (i == 0) {
			if (this.checked) {
				$("input[type=radio]").each(function() {
					this.disabled = false;
				});
				$("input[type=checkbox]").each(function() {
					this.disabled = true;
					this.checked = false;
				});
				$("input[type=checkbox]")[0].disabled = false;
				$("input[type=checkbox]")[0].checked = true;
			} else {
				$("input[type=radio]").each(function() {
					this.disabled = true;
				});
				$("input[type=checkbox]").each(function() {
					this.disabled = false;
				});
			}
		}
		if (this.checked && $("input[type=radio]")[i-1]) {
			$("input[type=radio]")[i-1].disabled = false;
		}
		i++;
	});
}

function updateFolderPath() {
	$("span#templateSetFolderPath").text($("#dmBridgeRoot").val() + "/templates/" +
		$("input#name").val().toLowerCase().split(" ").join("_"));
}

function addNameChangedEventHandler() {
	$("input#name").keyup(function() {
		updateFolderPath();
	});
}

