/*
 * dmBridge: a data access framework for CONTENTdm(R)
 *
 * Copyright © 2009 Board of Regents of the Nevada System of Higher
 * Education, on behalf of the University of Nevada, Las Vegas
 */

/**
 * @package dmBridge
 * @subpackage Control Panel
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @version $Id: deleteTemplate.js 2217 2010-01-22 05:43:22Z alexantd $
 * @license http://www.opensource.org/licenses/mit-license.php
 */

function init() {
	$('a.deleteTemplate').click(function() {
		$(this).next().fadeIn('fast');
	});
	$('input.cancelDelete').click(function() {
		$(this).parent().hide();
	});
}
