/*
 * dmBridge: a data access framework for CONTENTdm(R)
 *
 * Copyright © 2009 Board of Regents of the Nevada System of Higher
 * Education, on behalf of the University of Nevada, Las Vegas
 */

/**
 * @package dmBridge
 * @subpackage Control Panel
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @version $Id: editComments.js 1804 2009-10-20 18:21:22Z alexantd $
 * @license http://www.opensource.org/licenses/mit-license.php
 */

function init() {
	if ($('select#notification').val() < 1) {
		$('input#notification_email').attr('disabled', 'disabled');
	}
}

function toggleOnChange() {
	$('select#notification').change(function() {
		if ($('select#notification').val() == 0) {
			$('input#notification_email').attr('disabled', 'disabled');
		}
		else {
			$('input#notification_email').removeAttr('disabled');
		}
	});
}
