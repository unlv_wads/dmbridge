/*
 * dmBridge: a data access framework for CONTENTdm(R)
 *
 * Copyright © 2009 Board of Regents of the Nevada System of Higher
 * Education, on behalf of the University of Nevada, Las Vegas
 */

/**
 * @package dmBridge
 * @subpackage Control Panel
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @version $Id: editDataStore.js 2145 2009-12-30 02:36:35Z alexantd $
 * @license http://www.opensource.org/licenses/mit-license.php
 */

function init() {
	switch ($('select#engine').val()) {
		case 'pdo_sqlite':
			$('fieldset#dataStoreConnectionSettings').hide();
			break;
		case 'null':
			$('fieldset#dataStoreConnectionSettings').hide();
			$('p#sqlitehelp').hide();
			$('p.dmDataStoreStatus').hide();
			break;
		default:
			$('p#sqlitehelp').hide();
			break;
	}
}

function toggleOnChange() {
	$('select#engine').change(function() {
		switch ($('select#engine').val()) {
			case 'pdo_sqlite':
				$('fieldset#dataStoreConnectionSettings').hide();
				$('p#sqlitehelp').fadeIn(100);
				$('p.dmDataStoreStatus').fadeIn(100);
				break;
			case 'null':
				$('fieldset#dataStoreConnectionSettings').hide();
				$('p#sqlitehelp').hide();
				$('p.dmDataStoreStatus').hide();
				break;
			default:
				$('fieldset#dataStoreConnectionSettings').fadeIn(100);
				$('p#sqlitehelp').hide();
				$('p.dmDataStoreStatus').fadeIn(100);
				break;
		}
	});
}
