/*
 * dmBridge: a data access framework for CONTENTdm(R)
 *
 * Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
 * Education, on behalf of the University of Nevada, Las Vegas
 */

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */

function init() {
	toggleTemplateSetVisibility();
	toggleObjectViewVisibility();
	toggleResultsViewVisibility();
	toggleSearchViewVisibility();

	addTemplateSetListener();
	addObjectViewListener();
	addResultsViewListener();
	addSearchViewListener();

	addAddViewerDefinitionListener();
	addRemoveViewerDefinitionListener();
}

function addTemplateSetListener() {
	$("input[name='dmForm[inherit_template_set]']").change(function() {
		toggleTemplateSetVisibility();
	});
}

function addObjectViewListener() {
	$("input[name='dmForm[inherit_object_view]']").change(function() {
		toggleObjectViewVisibility();
	});
}

function addResultsViewListener() {
	$("input[name='dmForm[inherit_results_view]']").change(function() {
		toggleResultsViewVisibility();
	});
}

function addSearchViewListener() {
	$("input[name='dmForm[inherit_search_view]']").change(function() {
		toggleSearchViewVisibility();
	});
}

function addAddViewerDefinitionListener() {
	$("input#addViewerDefinition").click(function() {
		addViewerDefinition();
	});
}

function addRemoveViewerDefinitionListener() {
	$("input.removeViewerDefinitionButton").click(function() {
		removeViewerDefinition($(this).closest('tr').prevAll().length - 1);
	});
}

function addViewerDefinition() {
	var newRow = $("fieldset#collectionViewObjectView table tr:last").clone();
	newRow.find("input, select").each(function() {
		$(this).val("");
	});
	$("fieldset#collectionViewObjectView table").append(newRow);
	// need to refresh this event listener
	addRemoveViewerDefinitionListener();
}

function removeViewerDefinition($index) {
	var table = $("fieldset#collectionViewObjectView table");
	table.find("tr").eq($index+1).fadeOut(400, function(){
		$(this).remove();
	});
}

function toggleTemplateSetVisibility() {
	if ($("input[name='dmForm[inherit_template_set]']:checked").val() == 1) {
		$('fieldset#collectionTemplateSet').fadeOut(100);
	} else {
		$('fieldset#collectionTemplateSet').fadeIn(100);
	}
}

function toggleObjectViewVisibility() {
	if ($("input[name='dmForm[inherit_object_view]']:checked").val() == 1) {
		$('fieldset#collectionViewObjectView').fadeOut(100);
	} else {
		$('fieldset#collectionViewObjectView').fadeIn(100);
	}
}

function toggleResultsViewVisibility() {
	if ($("input[name='dmForm[inherit_results_view]']:checked").val() == 1) {
		$('fieldset#collectionViewResultsView').fadeOut(100);
		$('fieldset#collectionViewTileView').fadeOut(100);
		$('fieldset#collectionViewGridView').fadeOut(100);
		$('fieldset#collectionFacets').fadeOut(100);
	} else {
		$('fieldset#collectionViewResultsView').fadeIn(100);
		$('fieldset#collectionViewTileView').fadeIn(100);
		$('fieldset#collectionViewGridView').fadeIn(100);
		$('fieldset#collectionFacets').fadeIn(100);
	}
}

function toggleSearchViewVisibility() {
	if ($("input[name='dmForm[inherit_search_view]']:checked").val() == 1) {
		$('fieldset#collectionSearchDateSearch').fadeOut(100);
	} else {
		$('fieldset#collectionSearchDateSearch').fadeIn(100);
	}
}
