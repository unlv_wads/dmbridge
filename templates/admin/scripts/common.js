function initMenuAccordion() {
	$(".accordionButton").click(function(ev) {
		var el = $(ev.target).next(".accordionPanel");
		var els = $(".accordionPanel");
		for (i = 0; i < els.length; i++) {
			if ($(els[i]).css("display") == "block") {
				$(els[i]).slideToggle("fast");
			}
		}
		el.slideToggle("fast");
	});
}
