<!DOCTYPE html>

<html>

<head>

	<?php echo $view->getHelper()->getHtmlMetaTags() ?>
	<meta name="author" content="My Great Self">

	<title>Sample My Favorites Template</title>

	<?php echo $view->getHelper()->getHtmlStylesheetTags() ?>
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/basic/styles/sample.css">

	<?php echo $view->getHelper()->getHtmlHeadScriptTags() ?>

</head>

<body>

	<h1>Sample My Favorites Template</h1>

	<?php include(dirname(__FILE__) . '/../includes/menu.inc.php') ?>

	<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

	<p><?php echo count($view->getAccessibleFavorites()) ?> object(s)</p>

	<h4 class="viewLinks"><?php echo $view->getHelper()->getHtmlViewLinks() ?></h4>

	<?php if (count($view->getAccessibleFavorites()) > 0): ?>
		<?php echo $view->getHelper()->getHtmlResults() ?>
	<?php else: ?>
		<p>You have no favorites.</p>
	<?php endif ?>

	<?php echo $view->getHelper()->getHtmlBodyScriptTags() ?>

	<!-- <?php echo $view->getLoadTime() ?>s -->

</body>

</html>
