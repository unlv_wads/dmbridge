<!DOCTYPE html>

<html>

<head>

	<?php echo $view->getHelper()->getHtmlMetaTags() ?>
	<meta name="author" content="My Great Self">

	<title>Sample Results List Template</title>

	<link rel="alternate" type="application/atom+xml" title="Atom 1.0"
		href="<?php echo $view->getQuery()->getURI('atom') ?>">

	<?php echo $view->getHelper()->getHtmlStylesheetTags() ?>
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/basic/styles/sample.css">

	<?php echo $view->getHelper()->getHtmlHeadScriptTags() ?>

</head>

<body>

	<p>There were no results for your search.</p>

 	<?php if ($view->getQuery()->getSuggestion()): ?>
		<p>Did you mean <?php echo $view->getHelper()->getHtmlLinkedSuggestion() ?>?</p>
	<?php endif ?>

	<?php echo $view->getHelper()->getHtmlBodyScriptTags() ?>

</body>

</html>
