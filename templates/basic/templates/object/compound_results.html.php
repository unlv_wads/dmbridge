<!DOCTYPE html>

<html>

<head>

	<?php echo $view->getHelper()->getHtmlMetaTags() ?>
	<meta name="author" content="My Great Self">

	<title>Sample Compound Object Search Template</title>

	<link rel="alternate" type="application/atom+xml" title="Atom 1.0"
		href="<?php echo $view->getQuery()->getFeedURI() ?>">

	<?php echo $view->getHelper()->getHtmlStylesheetTags() ?>
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/basic/styles/sample.css">

	<?php echo $view->getHelper()->getHtmlHeadScriptTags() ?>

</head>

<body>

	<h1>Sample Compound Object Search Template</h1>

	<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

	<h4><a href="<?php echo DMString::websafe($view->getObject()->getURI()) ?>">Back to object</a></h4>

	<h3>Searching within
		&quot;<?php echo DMString::websafe($view->getObject()->getParent()->getMetadata('title')) ?>&quot;</h3>

	<p>
		<?php echo $view->getQuery()->getNumResults() ?> page(s) containing matching results for
		<?php echo $view->getHelper()->getHtmlSearchTerms() ?>
	</p>

	<?php echo $view->getHelper()->getHtmlPageLinks() ?>

	<?php echo $view->getHelper()->getHtmlResults() ?>

	<?php echo $view->getHelper()->getHtmlScriptTags() ?>

	<!-- <?php echo $view->getLoadTime() ?>s -->

	<?php echo $view->getHelper()->getHtmlBodyScriptTags() ?>

</body>

</html>
