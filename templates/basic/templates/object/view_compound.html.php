<!DOCTYPE html>

<html>

<head>

	<?php echo $view->getHelper()->getHtmlMetaTags() ?>
	<meta name="author" content="My Great Self">

	<?php echo $view->getHelper()->getHtmlMetadataAsMetaTags() ?>

	<title>Sample Compound Object View Template</title>

	<link rel="alternate" type="application/atom+xml" title="Atom 1.0"
		href="<?php echo $view->getObject()->getFeedURI() ?>">

	<?php echo $view->getHelper()->getHtmlStylesheetTags() ?>
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/basic/styles/sample.css">
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/system/viewers/DMMonocleViewer/styles/dmmonocle.css.php">

	<?php echo $view->getHelper()->getHtmlHeadScriptTags() ?>

</head>

<body>

	<h1>Sample Compound Object View Template</h1>

	<?php include(dirname(__FILE__) . '/../includes/menu.inc.php') ?>

	<?php echo $view->getHelper()->getHtmlRatingsSection(0, 5, 1) ?>

	<p><?php echo $view->getHelper()->getHtmlPreviousNextLinks() ?></p>

	<p><?php echo $view->getHelper()->getHtmlAddFavoriteButton() ?></p>

	<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

	<h3><?php echo $view->getObject()->getMetadata('title') ?></h3>

	<?php if ($view->getObject()->isChild()): ?>
		<p><a href="<?php echo $view->getObject()->getParent()->getURI() ?>">This is
			just one page of a compound object. Click here to see the parent
			object.</a></p>
	<?php endif ?>

	<div>
		<div><?php echo $view->getHelper()->getHtmlCompoundPageFlipLinks() ?></div>

		<div style="width:240px; height:300px; overflow:auto">
			<ol>
				<?php echo $view->getHelper()->getHtmlChildLinksAsList(true) ?>
			</ol>
		</div>

		<div><?php echo $view->getHelper()->getHtmlCompoundObjectSearchForm() ?></div>
		<?php echo $view->getHelper()->getHtmlViewer() ?>
	</div>

	<?php if ($view->getQuery()): ?>
		<div>
			<?php echo $view->getQuery()->getNumResults() ?> result(s) for
			&quot;<?php echo DMString::websafe($view->getQuery()->getPredicates(0)) ?>&quot;
			on the following pages:
			<?php echo $view->getHelper()->getHtmlCompoundObjectPageNumbersInSearchResultsAsString() ?>
		</div>
	<?php endif ?>


	<?php if ($view->getObject()->getFullText()): ?>
		<div>
			<?php echo $view->getHelper()->getHtmlHighlightedFullText() ?>
		</div>
	<?php endif ?>


	<?php echo $view->getHelper()->getHtmlMetadataAsDL() ?>

	<p>When linking to this object, please use the following URL:</p>
	<p><strong><?php echo DMString::websafe($view->getObject()->getReferenceURL()) ?></strong></p>

	<div>
		<h3>Tags</h3>
		<?php echo $view->getHelper()->getHtmlTaggingSection() ?>
	</div>

	<div>
		<h3>Comments</h3>
		<a href="<?php echo $view->getObject()->getCommentsFeedURL() ?>">(Subscribe
			to recent comments)</a>
		<?php echo $view->getHelper()->getHtmlCommentsSection() ?>
	</div>

	<?php echo $view->getHelper()->getHtmlBodyScriptTags() ?>

	<!-- <?php echo $view->getLoadTime() ?>s -->

</body>

</html>
