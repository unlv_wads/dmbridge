<!DOCTYPE html>

<html>

<head>

	<?php echo $view->getHelper()->getHtmlMetaTags() ?>
	<meta name="author" content="My Great Self">

	<title>Sample Faceted Results List Template</title>

	<link rel="alternate" type="application/atom+xml" title="Atom 1.0"
		href="<?php echo $view->getQuery()->getFeedURI() ?>">

	<?php echo $view->getHelper()->getHtmlStylesheetTags() ?>
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/basic/styles/sample.css">

	<?php echo $view->getHelper()->getHtmlHeadScriptTags() ?>

</head>

<body>

	<h1>Sample Faceted Results List Template</h1>

	<?php include(dirname(__FILE__) . '/../includes/menu.inc.php') ?>

	<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

	<h4 class="pageLinks"><?php echo $view->getHelper()->getHtmlPageLinks() ?></h4>

	<?php echo $view->getHelper()->getHtmlFacetsAsUL() ?>

	<p>
		<?php echo $view->getQuery()->getNumResults(); ?>
		<?php if ($view->getQuery()->areSearchTerms()): ?>
			result(s) for <?php echo $view->getHelper()->getHtmlSearchTerms() ?>
		<?php else: ?>
		  object(s)
		<?php endif ?>
	</p>

	<h4 class="viewLinks"><?php echo $view->getHelper()->getHtmlViewLinks() ?></h4>

	<?php echo $view->getHelper()->getHtmlSortLinksAsUL() ?>
	<?php echo $view->getHelper()->getHtmlResults() ?>

	<?php echo $view->getHelper()->getHtmlBodyScriptTags() ?>

 <!-- <?php echo $view->getLoadTime() ?>s -->

</body>

</html>
