<!DOCTYPE html>

<html>

<head>

	<?php echo $view->getHelper()->getHtmlMetaTags() ?>
	<meta name="author" content="My Great Self">

	<?php echo $view->getHelper()->getHtmlMetadataAsMetaTags() ?>

	<title>Sample Object View Template</title>

	<link rel="alternate" type="application/atom+xml" title="Atom 1.0"
		href="<?php echo $view->getObject()->getFeedURI() ?>">

	<?php echo $view->getHelper()->getHtmlStylesheetTags() ?>
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/basic/styles/sample.css">
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/basic/styles/UNLVSpatial.css">
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/system/viewers/DMMonocleViewer/styles/dmmonocle.css.php">

	<?php echo $view->getHelper()->getHtmlHeadScriptTags() ?>

</head>

<body>

	<h1>Sample Object View Template</h1>

	<p>
		<?php echo $view->getHelper()->getHtmlLoginPageLink() ?> |
		<a href="<?php echo $view->getSearchView()->getURI() ?>">Search</a> |
		<a href="<?php echo $view->getFavoritesView()->getURI() ?>">Favorites</a> |
		<?php echo $view->getHelper()->getHtmlPreviousNextLinks() ?>
	</p>

	<?php echo $view->getHelper()->getHtmlRatingsSection(0, 5, 1) ?>

	<?php echo $view->getHelper()->getHtmlAddFavoriteButton() ?>

	<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

	<h3><?php echo $view->getObject()->getMetadata('title') ?></h3>

	<div>
		<?php echo $view->getHelper()->getHtmlViewer() ?>
	</div>

	<?php if ($view->getObject()->getFullText()): ?>
		<div>
			<?php echo $view->getHelper()->getHtmlHighlightedFullText() ?>
		</div>
	<?php endif ?>

	<?php echo $view->getHelper()->getHtmlMetadataAsDL() ?>

	<p>When linking to this object, please use the following URL:</p>
	<p><strong><?php echo DMString::websafe($view->getObject()->getReferenceURL()) ?></strong></p>

	<div>
		<h3>Tags</h3>
		<?php echo $view->getHelper()->getHtmlTaggingSection() ?>
	</div>

	<div>
		<h3>Comments</h3>
		<a href="<?php echo $view->getObject()->getCommentsFeedURL() ?>">(Subscribe
			to recent comments)</a>
		<?php echo $view->getHelper()->getHtmlCommentsSection() ?>
	</div>

	<?php echo $view->getHelper()->getHtmlBodyScriptTags() ?>

	<!-- <?php echo $view->getLoadTime() ?>s -->

</body>

</html>
