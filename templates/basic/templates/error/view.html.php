<!DOCTYPE html>

<html>

<head>

	<?php echo $view->getHelper()->getHtmlMetaTags() ?>
	<meta name="author" content="My Great Self">

	<title>Sample dmBridge Error Template</title>

	<?php echo $view->getHelper()->getHtmlStylesheetTags() ?>
	<style type="text/css">
	body {
		font-family:"Trebuchet MS", Tahoma, Helvetica, sans-serif;
	}
	div.dmExceptionDebug {
		background-color:#f0f0f0;
		border:1px dotted #a0a0a0;
		padding:0.5em;
	}
	</style>

	<?php echo $view->getHelper()->getHtmlHeadScriptTags() ?>

</head>

<body>

	<h1>Sample dmBridge Error Template</h1>

	<?php echo $view->getHelper()->getHtmlException() ?>

	<?php echo $view->getHelper()->getHtmlBodyScriptTags() ?>

</body>
</html>
