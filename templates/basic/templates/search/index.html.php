<!DOCTYPE html>

<html>

<head>

	<?php echo $view->getHelper()->getHtmlMetaTags() ?>
	<meta name="author" content="My Great Self">

	<title>Sample Advanced Search Template</title>

	<?php echo $view->getHelper()->getHtmlStylesheetTags() ?>
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/basic/styles/sample.css">

	<style type="text/css">
	fieldset ol, fieldset ul {
		list-style-type:none;
		margin-left:0px;
		padding-left:0px;
	}
	</style>

	<?php echo $view->getHelper()->getHtmlHeadScriptTags() ?>

</head>

<body>

	<h1>Sample Advanced Search Template</h1>

	<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

	<p>The lists of collections in the search forms below are dependent on
	the collections allowed access by this template set in the Control Panel.</p>
	<ul>
		<li>If no collections are defined, all collections will be searchable.</li>
		<li>If more than one is defined, only those will be searchable.</li>
		<li>If only one is defined, it and only it will be searched and the
			collection checkboxes will not appear.</li>
	</ul>

	<h3>Field search</h3>
	<?php echo $view->getHelper()->getHtmlFieldSearch() ?>

	<h3>Date search</h3>
	<?php echo $view->getHelper()->getHtmlDateSearch() ?>

	<h3>Proximity search</h3>
	<?php echo $view->getHelper()->getHtmlProximitySearch() ?>

	<?php echo $view->getHelper()->getHtmlBodyScriptTags() ?>
	<!-- <?php echo $view->getLoadTime() ?>s -->

</body>

</html>
