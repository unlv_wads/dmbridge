<!DOCTYPE html>

<html>

<head>

	<?php echo $view->getHelper()->getHtmlMetaTags() ?>
	<meta name="author" content="My Great Self">

	<title>Sample Error Template</title>

	<?php echo $view->getHelper()->getHtmlStylesheetTags() ?>
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/clean_white/styles/common.css">

	<?php echo $view->getHelper()->getHtmlHeadScriptTags() ?>

</head>

<body>

<div id="wrap">

	<div id="header" class="content">
		<h1><a href="<?php echo DMInternalURI::getURIWithParams("objects") ?>">My Great Digital Collections</a></h1>
	</div>

	<div class="content">
		<h2>Sample error template</h2>
		<?php echo $view->getHelper()->getHtmlException() ?>
	</div>

	<div id="footer" class="content">
		<p>Copyright &copy; 2011 My Great Institution. All rights reserved.</p>
	</div>

</div> <!-- #wrap -->

<?php echo $view->getHelper()->getHtmlBodyScriptTags() ?>

</body>
</html>
