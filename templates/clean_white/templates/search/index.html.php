<!DOCTYPE html>

<html>

<head>

	<?php echo $view->getHelper()->getHtmlMetaTags() ?>
	<meta name="author" content="My Great Self">

	<title>Sample Advanced Search Template</title>

	<?php echo $view->getHelper()->getHtmlStylesheetTags() ?>
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/clean_white/styles/common.css">
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/clean_white/styles/search_view.css">

	<style type="text/css">
	fieldset ol, fieldset ul {
		list-style-type:none;
		margin-left:0px;
		padding-left:0px;
	}
	</style>

	<?php echo $view->getHelper()->getHtmlHeadScriptTags() ?>
	<script type="text/javascript">
	/* @todo This seems to interfere with the Google Map
	$(document).ready(function() {
		$('.collapsed').hide();
		$('.collapseTrigger').click(function() {
			$(this).next(".collapsible").slideToggle(300);
		});
	});*/
	</script>

</head>

<body>

<div id="wrap">

	<div id="header" class="content">
		<h1><a href="<?php echo DMInternalURI::getURIWithParams("objects") ?>">My Great Digital Collections</a></h1>
	</div>

	<table class="nav">
		<tr>
			<td>
				<a href="<?php echo DMInternalURI::getURIWithParams("objects") ?>">Back to results</a>
			</td>
			<td style="text-align:right">
				<a href="<?php echo $view->getFavoritesView()->getURI() ?>">My Favorites</a> |
				<?php echo $view->getHelper()->getHtmlLoginPageLink() ?>
			</td>
		</tr>
	</table>

	<div class="content">

		<h2>Sample advanced search template</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<ul>
			<li><a href="#searchByField">Search by field</a></li>
			<li><a href="#searchByDate">Search by date</a></li>
			<li><a href="#searchByTermProximity">Search by term proximity</a></li>
			<li><a href="#searchBySpatialArea">Search by spatial area</a></li>
		</ul>

		<div id="searchByField" class="objectBox">
			<h4 class="collapseTrigger">Search by field</h4>
			<div class="search content collapsible collapsed">
				<?php echo $view->getHelper()->getHtmlFieldSearch() ?>
			</div>
		</div>

		<div id="searchByDate" class="objectBox">
			<h4 class="collapseTrigger">Search by date</h4>
			<div class="search content collapsible collapsed">
				<?php echo $view->getHelper()->getHtmlDateSearch() ?>
			</div>
		</div>

		<div id="searchByTermProximity" class="objectBox">
			<h4 class="collapseTrigger">Search by term proximity</h4>
			<div class="search content collapsible collapsed">
				<?php echo $view->getHelper()->getHtmlProximitySearch() ?>
			</div>
		</div>

		<?php if (DMModuleManager::getInstance()->isModuleEnabled("UNLVSpatial")): ?>
			<div id="searchBySpatialArea" class="objectBox">
				<h4 class="collapseTrigger">Search by spatial area</h4>
				<div class="search content collapsible collapsed">
					<?php echo $view->getHelper()->getHtmlGoogleMapsSpatialSearch(35, -115) ?>
				</div>
			</div>
		<?php endif ?>

	</div> <!-- #content -->

	<div id="footer" class="content">
		<p>Copyright &copy; 2011 My Great Institution. All rights reserved.</p>
	</div>

</div> <!-- #wrap -->

<?php echo $view->getHelper()->getHtmlBodyScriptTags() ?>

<!-- <?php echo $view->getLoadTime() ?>s -->

</body>

</html>
