<!DOCTYPE html>

<html>

<head>

	<?php echo $view->getHelper()->getHtmlMetaTags() ?>
	<meta name="author" content="My Great Self">

	<title>Sample Compound Object Results Template</title>

	<link rel="alternate" type="application/atom+xml" title="Atom 1.0"
		href="<?php echo $view->getQuery()->getFeedURI() ?>">

	<?php echo $view->getHelper()->getHtmlStylesheetTags() ?>
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/clean_white/styles/common.css">
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/clean_white/styles/results_view.css">

	<?php echo $view->getHelper()->getHtmlHeadScriptTags() ?>

</head>

<body>

<div id="wrap">

	<div id="header" class="content">
		<h1><a href="<?php echo DMInternalURI::getURIWithParams("objects") ?>">My Great Digital Collections</a></h1>
	</div>

	<table class="nav">
		<tr>
			<td style="width:250px">
				<a href="<?php echo DMString::websafe($view->getObject()->getURI()) ?>">Back to object</a>
			</td>
			<td></td>
			<td style="width:250px; text-align:right">
				<a href="<?php echo $view->getFavoritesView()->getURI() ?>">My Favorites</a> |
				<a href="<?php echo $view->getSearchView()->getURI() ?>">Search</a> |
				<?php echo $view->getHelper()->getHtmlLoginPageLink() ?>
			</td>
		</tr>
	</table>

	<div class="content">

		<h2>Sample Compound Object Results Template</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<h3>Searching within
			&quot;<?php echo DMString::websafe($view->getObject()->getParent()->getMetadata("title")) ?>&quot;</h3>

		<p>
			<?php echo $view->getQuery()->getNumResults() ?> page(s) containing matching results for
			<?php echo $view->getHelper()->getHtmlSearchTerms() ?>
		</p>

		<?php echo $view->getHelper()->getHtmlResults() ?>

	</div>

	<table class="nav">
		<tr>
			<td style="width:250px">
				<?php echo $view->getHelper()->getHtmlViewLinks() ?>
			</td>
			<td></td>
			<td style="width:250px">
			</td>
		</tr>
	</table>

	<div id="footer" class="content">
		<p>Copyright &copy; 2011 My Great Institution. All rights reserved.</p>
	</div>

</div> <!-- #wrap -->

<?php echo $view->getHelper()->getHtmlBodyScriptTags() ?>

<!-- <?php echo $view->getLoadTime() ?>s -->

</body>

</html>
