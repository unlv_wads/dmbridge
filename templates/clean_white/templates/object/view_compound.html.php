<!DOCTYPE html>

<html>

<head>

	<?php echo $view->getHelper()->getHtmlMetaTags() ?>
	<meta name="author" content="My Great Self">

	<?php echo $view->getHelper()->getHtmlMetadataAsMetaTags() ?>

	<title>Sample Compound Object View Template</title>

	<link rel="alternate" type="application/atom+xml" title="Atom 1.0"
		href="<?php echo $view->getObject()->getFeedURI() ?>">

	<?php echo $view->getHelper()->getHtmlStylesheetTags() ?>
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/clean_white/styles/common.css">
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/clean_white/styles/object_view.css">
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/system/viewers/DMMonocleViewer/styles/dmmonocle.css.php">

	<?php echo $view->getHelper()->getHtmlHeadScriptTags() ?>
	<script type="text/javascript">
	$(document).ready(function() {
		$('.collapsed').hide();
		$('.collapseTrigger').click(function() {
			$(this).next(".collapsible").slideToggle(300);
		});
	});
	</script>

</head>

<body>

<div id="wrap">

	<div id="header" class="content">
		<h1><a href="<?php echo DMInternalURI::getURIWithParams("objects") ?>/">My Great Digital Collections</a></h1>
	</div>

	<table class="nav">
		<tr>
			<td>
				<?php echo $view->getHelper()->getHtmlPreviousNextLinks() ?>
			</td>
			<td>
				<?php echo $view->getHelper()->getHtmlCompoundPageFlipLinks() ?>
			</td>
			<td style="text-align:right">
				Citation URL:
				<span class="referenceURL"><?php echo DMString::websafe($view->getObject()->getReferenceURL()) ?></span> |
				<a href="<?php echo $view->getFavoritesView()->getURI() ?>">My Favorites</a> |
				<?php echo $view->getHelper()->getHtmlLoginPageLink() ?>
			</td>
		</tr>
	</table>

	<div id="rtcol">

		<div class="content">

			<div id="pages" class="objectBox">
				<h4 class="collapseTrigger">Pages</h4>
				<div class="content collapsible">
					<ol>
						<?php echo $view->getHelper()->getHtmlChildLinksAsList(true) ?>
					</ol>
				</div>
			</div>

			<div id="metadata" class="objectBox">
				<h4>Metadata</h4>
				<div class="content">
					<?php echo $view->getHelper()->getHtmlMetadataAsDL(null, null, true, true) ?>
				</div>
			</div> <!-- #metadata -->

			<div id="recentlyViewed" class="objectBox">
				<h4 class="collapseTrigger">Recently Viewed</h4>
				<div class="content collapsible">
					<?php echo $view->getHelper()->getHtmlRecentlyViewedObjectsAsList() ?>
					<div style="clear:both"></div>
				</div>
			</div>

			<div id="moreResults" class="objectBox">
				<h4 class="collapseTrigger">More Results</h4>
				<div class="content collapsible">
					<?php echo $view->getHelper()->getHtmlResultsAsUL(true) ?>
					<div style="clear:both"></div>
				</div>
			</div>

		</div> <!-- #content -->

	</div> <!-- #rtcol -->

	<div id="leftcol">

		<div class="content">

			<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

			<h2>
				<?php echo DMString::websafe($view->getObject()->getField('title')->getValue()) ?>
				<?php if ($view->getObject()->isChild()): ?>
					<a href="<?php echo DMString::websafe($view->getObject()->getParent()->getURI()) ?>">(parent object)</a>
				<?php endif ?>
			</h2>

			<div class="content" id="viewer">
				<?php echo $view->getHelper()->getHtmlViewer(520, 450) ?>

				<table class="objectConsole" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td>
							<?php echo $view->getHelper()->getHtmlAddFavoriteButton() ?>
						</td>
						<td style="text-align:right">
							<?php echo $view->getHelper()->getHtmlRatingsSection(0, 5, 1) ?>
						</td>
					</tr>
				</table>

				<?php if ($view->getObject()->getFullText()): ?>
					<div>
						<?php echo $view->getHelper()->getHtmlHighlightedFullText() ?>
					</div>
				<?php endif ?>

			</div> <!-- #viewer -->

			<div><?php echo $view->getHelper()->getHtmlCompoundObjectSearchForm() ?></div>

			<?php if ($view->getQuery()): ?>
				<div>
					<?php echo $view->getQuery()->getNumResults() ?> result(s) for
					&quot;<?php echo DMString::websafe($view->getQuery()->getSearchTerms(0)) ?>&quot;
					on the following pages:
					<?php echo $view->getHelper()->getCompoundObjectPageNumbersInSearchResultsAsString() ?>
				</div>
			<?php endif ?>

			<?php if (DMModuleManager::getInstance()->isModuleEnabled("UNLVSpatial")): ?>
				<?php if ($view->getHelper()->getSpatialObject()): ?>
					<div class="map objectBox">
						<h4 class="collapseTrigger">Location</h4>
						<div class="content collapsible">
							<?php echo $view->getHelper()->getHtmlGoogleMapsObject() ?>
						</div>
					</div>
				<?php endif ?>
			<?php endif ?>

			<div class="tags objectBox">
				<h4 class="collapseTrigger">Tags</h4>
				<div class="content collapsible">
					<?php echo $view->getHelper()->getHtmlTagsAsCloud() ?>
					<?php echo $view->getHelper()->getHtmlTaggingSection() ?>
				</div>
			</div>

			<div class="comments objectBox">
				<h4 class="collapseTrigger">Comments</h4>
				<div class="content collapsible">
					<?php echo $view->getHelper()->getHtmlCommentsSection() ?>
				</div>
			</div>

		</div> <!-- #content -->

	</div> <!-- #leftcol -->

	<div id="footer" class="content">
		<p>Copyright &copy; 2011 My Great Institution. All rights reserved.</p>
	</div>

</div> <!-- #wrap -->

	<?php echo $view->getHelper()->getHtmlBodyScriptTags() ?>

<!-- <?php echo $view->getLoadTime() ?>s -->

</body>

</html>
