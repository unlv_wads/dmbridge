<!DOCTYPE html>

<html>

<head>

	<?php echo $view->getHelper()->getHtmlMetaTags() ?>
	<meta name="author" content="My Great Self" />

	<title>Sample Results List Template</title>

	<link rel="alternate" type="application/atom+xml" title="Atom 1.0"
		href="<?php echo $view->getQuery()->getFeedURI() ?>">

	<?php echo $view->getHelper()->getHtmlStylesheetTags() ?>
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/clean_white/styles/common.css">
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/clean_white/styles/results_view.css">

	<?php echo $view->getHelper()->getHtmlHeadScriptTags() ?>

</head>

<body>

<div id="wrap">

	<div id="header" class="content">
		<h1><a href="<?php echo DMInternalURI::getURIWithParams("objects") ?>">My Great Digital Collections</a></h1>
	</div>

	<table class="nav">
		<tr>
			<td style="width:250px">
				<?php echo $view->getHelper()->getHtmlViewLinks() ?>
			</td>
			<td class="pageLinks" style="text-align:center">
				<?php echo $view->getHelper()->getHtmlPageLinks() ?>
			</td>
			<td style="width:250px; text-align:right">
				<a href="<?php echo $view->getFavoritesView()->getURI() ?>">My Favorites</a> |
				<a href="<?php echo $view->getSearchView()->getURI() ?>">Search</a> |
				<?php echo $view->getHelper()->getHtmlLoginPageLink() ?>
			</td>
		</tr>
	</table>

	<div class="content">

		<h2>Sample results view template</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<p>
			<?php echo number_format($view->getQuery()->getNumResults()) ?>
			<?php if (count($view->getQuery()->getPredicates())): ?>
				result(s) for <?php echo $view->getHelper()->getHtmlSearchTerms() ?>
			<?php else: ?>
			  object(s)
			<?php endif ?>
		</p>

		<?php if (DMModuleManager::getInstance()->isModuleEnabled("UNLVSpatial")): ?>
			<?php if (count($view->getHelper()->getSpatialObjects())): ?>
				<?php echo $view->getHelper()->getHtmlGoogleMapsObjectResults() ?>
			<?php endif ?>
		<?php endif ?>

		<?php echo $view->getHelper()->getHtmlSortLinksAsUL() ?>
		<?php echo $view->getHelper()->getHtmlResults() ?>

	</div> <!-- .content -->

	<table class="nav">
		<tr>
			<td style="width:250px">
			</td>
			<td class="pageLinks" style="text-align:center">
				<?php echo $view->getHelper()->getHtmlPageLinks() ?>
			</td>
			<td style="width:250px">
			</td>
		</tr>
	</table>

	<div id="footer" class="content">
		<p>Copyright &copy; 2011 My Great Institution. All rights reserved.</p>
	</div>

</div> <!-- #wrap -->

<?php echo $view->getHelper()->getHtmlBodyScriptTags() ?>

<!-- <?php echo $view->getLoadTime() ?>s -->

</body>

</html>
