<!DOCTYPE html>

<html>

<head>

	<?php echo $view->getHelper()->getHtmlMetaTags() ?>
	<meta name="author" content="My Great Self">

	<title>Sample My Favorites Template</title>

	<?php echo $view->getHelper()->getHtmlStylesheetTags() ?>
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/clean_white/styles/common.css">
	<link rel="stylesheet" type="text/css"
		href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/templates/clean_white/styles/results_view.css">

	<?php echo $view->getHelper()->getHtmlHeadScriptTags() ?>

</head>

<body>

<div id="wrap">

	<div id="header" class="content">
		<h1><a href="<?php echo DMInternalURI::getURIWithParams("objects") ?>">My Great Digital Collections</a></h1>
	</div>

	<table class="nav">
		<tr>
			<td style="width:250px">
				<?php echo $view->getHelper()->getHtmlViewLinks() ?>
			</td>
			<td class="pageLinks" style="text-align:center">
				<?php echo $view->getHelper()->getHtmlPageLinks() ?>
			</td>
			<td style="width:250px; text-align:right">
				<a href="<?php echo DMInternalURI::getURIWithParams("objects") ?>">Back To Results</a> |
				<a href="<?php echo $view->getSearchView()->getURI() ?>">Search</a> |
				<?php echo $view->getHelper()->getHtmlLoginPageLink() ?>
			</td>
		</tr>
	</table>

	<div class="content">

		<h1>Sample favorites view template</h1>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<p><?php echo $view->getQuery()->getNumResults() ?> object(s)</p>

		<?php echo $view->getHelper()->getHtmlResults() ?>

	</div>

	<table class="nav">
		<tr>
			<td style="width:250px">
				<a href="<?php echo DMInternalURI::getURIWithParams("objects") ?>">Back To Results</a>
			</td>
			<td class="pageLinks" style="text-align:center">
				<?php echo $view->getHelper()->getHtmlPageLinks() ?>
			</td>
			<td style="width:250px">
			</td>
		</tr>
	</table>

	<div id="footer" class="content">
		<p>Copyright &copy; 2011 My Great Institution. All rights reserved.</p>
	</div>

</div> <!-- #wrap -->

<?php echo $view->getHelper()->getHtmlBodyScriptTags() ?>

<!-- <?php echo $view->getLoadTime() ?>s -->

</body>

</html>
