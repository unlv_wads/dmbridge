<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html>

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="expires" content="FRI, 13 APR 1999 01:00:00 GMT" />
	<meta name="robots" content="noindex, nofollow, noarchive" />

	<title>Error</title>

	<style type="text/css">
	body {
		font-family:Helvetica, Arial, sans-serif;
		font-size:0.9em;
	}
	dt {
		font-weight:bold;
		line-height:2em;
	}
	</style>

</head>

<body>

<h1>Error</h1>

<?php echo $view->getHelper()->getHtmlException() ?>

</body>
</html>
