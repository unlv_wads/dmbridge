<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html>

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="expires" content="FRI, 13 APR 1999 01:00:00 GMT" />
	<meta name="robots" content="noindex, nofollow, noarchive" />

	<title>Welcome to dmBridge!</title>

	<style type="text/css">
	body {
		font-family:Helvetica, Arial, sans-serif;
		font-size:0.9em;
	}
	ul {
		font-size:1.3em;
		line-height:1.5em;
	}
	</style>

</head>

<body>

<h1>Welcome to dmBridge!</h1>

<ul>
	<li><a href="<?php echo DMInternalURI::getURIWithParams("objects") ?>">Web templates</a></li>
	<li><a href="<?php echo DMInternalURI::getURIWithParams("api/1") ?>">HTTP API</a></li>
	<li><a href="<?php echo DMInternalURI::getURIWithParams("admin") ?>">Control Panel</a></li>
</ul>

</body>
</html>
