<?php

/**
 * This is a sample template helper. See the user's manual for information
 * about how template helpers work and how to write your own.
 *
 * @author My Great Self <myself@great.edu>
 */
class SampleTemplateHelper extends DMCustomTemplateHelper {

	/**
	 * @return HTML paragraph
	 */
	public function squeak() {
		return '<p>I just wrote my first template helper!</p>';
	}

}
