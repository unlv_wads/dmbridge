This is the dmBridge extensions folder. The contents of this folder are as
follows:

helpers
    Used to store template helper classes.

modules
    Used to store dmBridge modules.

viewers
    Used to store custom dmBridge object viewers.

CustomAuthenticationService.php
    An example of a custom authentication service. See the inline
    documentation in the file for more information.

For more information about all of the above, refer to the user's manual.
