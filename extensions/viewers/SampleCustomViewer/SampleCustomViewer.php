<?php

/**
 * This is a sample custom viewer. See the user's manual for information
 * about how viewers work and how to write your own.
 *
 * @author My Great Self <myself@great.edu>
 */
class SampleCustomViewer extends DMAbstractViewer
implements DMObjectViewerDelegate {

	/**
	 * An example of how to use custom JavaScript and CSS for the viewer. These
	 * must NOT be output by getHTMLTag(). If the viewer does not require
	 * custom JS/CSS, it is safe to omit this method.
	 */
	public function viewerWillRender() {
		$this->getHelper()->addBodyScriptTag(
				dirname($_SERVER['PHP_SELF'])
				. "/extensions/viewers/SampleCustomViewer/myscript.js");
		$this->getHelper()->addStylesheetTag(
				dirname($_SERVER['PHP_SELF'])
				. "/extensions/viewers/SampleCustomViewer/mystyles.css");
	}

	/**
	 * Returns the HTML to render the viewer, minus any required script or
	 * style tags, which should instead be added in viewerWillRender().
	 *
	 * @return HTML anchor tag
	 */
	public function getHTMLTag() {
		return sprintf(
			'<a href="%s">As you can see, this simple example does the same
				thing as DMFileLinkViewer!</a>',
			DMString::websafe($this->getObject()->getFileURL()));
	}

	/**
	 * @return HTML anchor tag
	 */
	public function getHTMLTagNoJS() {
		return $this->getHTMLTag();
	}

	/**
	 * Whether the viewer is "bandwidth-safe," e.g. whether the content it
	 * loads can be safely viewed over a subjectively "slow" connection.
	 * 
	 * @return boolean True, always.
	 * @since 2.1
	 */
	public function isLowBandwidthCompatible() {
		return true;
	}

	/**
	 * @return The name of the object viewer.
	 */
	public function getName() {
		return "Sample Custom Viewer";
	}

}
