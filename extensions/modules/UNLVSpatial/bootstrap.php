<?php
#
# UNLVSpatial: a dmBridge module for spatial content
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * This file should include all the PHP files needed by the module, whether via
 * include()/require() or autoloading. If using an autoloader, make sure to
 * implement it in such a way that it doesn't conflict with dmBridge's own
 * autoloader. (See http://php.net/manual/en/function.spl-autoload-register.php)
 */

spl_autoload_register("UNLVSpatialAutoloader");

function UNLVSpatialAutoloader($class) {
	// All of our classes are prefixed with "UNLVSpatial" which is a handy way
	// of distinguishing them.
	if (substr($class, 0, 11) != "UNLVSpatial") {
		// We only want to autoload our own classes.
		return;
	}
	$prefix = dirname(__FILE__);
	$all_classes = array(
		'UNLVSpatial' => '',
		'UNLVSpatialAbstractSpatialObject' => 'models',
		'UNLVSpatialController' => 'controllers',
		'UNLVSpatialCPController' => 'controllers',
		'UNLVSpatialGenericDataStore' => 'system',
		'UNLVSpatialIndexer' => 'system',
		'UNLVSpatialJSONRepresentationV1' => 'output',
		'UNLVSpatialJSONPRepresentationV1' => 'output',
		'UNLVSpatialMySQLDataStore' => 'system',
		'UNLVSpatialObject' => 'models',
		'UNLVSpatialObjectFactory' => 'models',
		'UNLVSpatialPointObject' => 'models',
		'UNLVSpatialQuery' => 'models',
		'UNLVSpatialQueryTerm' => 'models',
		'UNLVSpatialRectObject' => 'models',
		'UNLVSpatialSQLiteDataStore' => 'system',
		'UNLVSpatialTemplateHelper' => 'template',
		'UNLVSpatialXMLRepresentationV1' => 'output'
	);

	if (array_key_exists($class, $all_classes)) {
		$path = $prefix . "/" . $all_classes[$class] . "/" . $class . ".php";
		if (include_once($path)) {
			return;
		}
	}
}
