<?php

$tpl_includes = dirname(__FILE__) . "/../../../../templates/admin/templates/includes";
include_once($tpl_includes . "/head.html.php") ?>

</head>

<?php include_once($tpl_includes . "/body.html.php") ?>

	<h1>Spatial Objects</h1>

	<div id="content">

		<h2>Index</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form method="post" action="">

			<p class="help">Indexing is only necessary for spatial searching. Changes
				made to object spatial boundaries in CONTENTdm&reg; will not be reflected
				in spatial search queries until the index is updated.</p>
			<p class="help">The spatial metadata in the chosen spatial coverage
				field must be encoded in <a href="http://dublincore.org/documents/dcmi-box/">DCMI
				Box</a> or <a href="http://dublincore.org/documents/dcmi-point/">DCMI
				Point</a> with <a href="http://dublincore.org/documents/dcmi-box/#sec3">DCSV
				syntax</a>, in decimal degrees.</p>

			<fieldset>
				<legend>Collections</legend>
				<table class="columnar" cellpadding="0" cellspacing="2" border="0">
					<tr>
						<th>Collection</th>
						<th>Num Objects</th>
						<th>Indexed Objects</th>
						<th>DCSV DCMI Box/Point coverage field</th>
					</tr>
					<?php $i = 0; foreach ($view->getVar("collections") as $c): ?>
						<tr <?php echo (!($i & 1)) ? 'class="odd"' : "" ?>>
							<td>
								<label>
									<input type="radio" name="alias"
									   value="<?php echo $c->getAlias() ?>"/>
									<?php echo DMString::websafe($c->getName()) ?>
								</label>
							</td>
							<td><?php echo number_format($c->getNumObjects()) ?></td>
							<td><?php echo number_format(UNLVSpatial::getDataStore()->getNumObjectsInCollection($c)) ?></td>
							<td>
								<select name="fields[<?php echo $c->getAlias() ?>]"
										style="width:100%">
									<?php foreach ($c->getFields() as $field): ?>
										<option value="<?php echo $field->getNick() ?>"
												<?php if ($field->getNick() == "covera"):
														echo 'selected="selected"';
												endif; ?>><?php echo $field->getName() ?></option>
									<?php endforeach ?>
								</select>
							</td>
						</tr>
					<?php $i++; endforeach ?>
				</table>
			</fieldset>

			<input type="submit" value="(Re-)Index Selected Collection"/>
		
			<?php if ($view->getVar("indexer")): ?>
				<h3>Results</h3>
				<?php echo $view->getHelper()->getHtmlIndexerSummary() ?>
			<?php else: ?>
				<p class="help">This may take a while for large numbers of objects.</p>
			<?php endif ?>

		</form>
	</div> <!-- #content -->

	<?php include_once($tpl_includes . "/footer.html.php") ?>
