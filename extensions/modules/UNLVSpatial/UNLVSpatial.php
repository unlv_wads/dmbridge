<?php
#
# UNLVSpatial: a dmBridge module for spatial content
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * The main module class. The purpose of this class is to interface the module
 * with dmBridge. As such, it needs to comply with the DMBridgeModule interface.
 * It also complies with a number of other sub-interfaces of DMBridgeModule,
 * to support more specific features, including functionality within the
 * Control Panel; the ability to persist data in the dmBridge data store; and
 * the ability to provide template helpers in the dmBridge template engine.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVSpatial extends DMAbstractDMBridgeModule
implements DMBridgeControlPanelModule, DMBridgeDataStoreModule,
		DMBridgeRepresentationSchemaModule, DMBridgeTemplateHelperModule {

	private $template_helper;

	/**
	 * @return DMDataStore
	 */
	public static function getDataStore() {
		$ds = DMDataStoreFactory::getDataStore();
		switch ($ds->getType()) {
		case DMDataStoreType::PDO_MySQL:
			$ds = UNLVSpatialMySQLDataStore::getInstance();
			break;
		case DMDataStoreType::PDO_SQLite:
			$ds = UNLVSpatialSQLiteDataStore::getInstance();
			break;
		}
		return $ds;
	}

	/**
	 * @return array Array of DMCPMenuSections
	 */
	public function getControlPanelMenuSections() {
		$sections = array();

		$section = new DMCPMenuSection("Spatial Objects");

		$item = new DMCPMenuItem(
				"Index",
				DMInternalURI::getURIWithParams("admin/spatial_objects"));
		$section->addMenuItem($item);

		$sections[] = $section;
		return $sections;
	}

	/**
	 * @return int
	 */
	public function getMinSupportedVersionSequence() {
		return 12;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return "UNLVSpatial";
	}

	/**
	 * @return string
	 */
	public function getPathToSchemas() {
		return $this->getAbsolutePathname() . "/schema";
	}

	/**
	 * Invoked when the module is first installed. Should return any and all
	 * SQL commands that are necessary to get the tables in a working state.
	 *
	 * @return array Array of SQL command strings to be executed, with one
	 * command per array element.
	 */
	public function getSetupSQLForMySQL() {
		return UNLVSpatialMySQLDataStore::getSetupSQL();
	}

	/**
	 * Invoked when the module is first installed. Should return any and all
	 * SQL commands that are necessary to get the tables in a working state.
	 *
	 * @return array Array of SQL command strings to be executed, with one
	 * command per array element.
	 */
	public function getSetupSQLForSQLite() {
		return UNLVSpatialSQLiteDataStore::getSetupSQL();
	}

	/**
	 * @return array An array of strings corresponding to the data stores
	 * supported by the module. See the DMDataStoreType class for a list of
	 * types.
	 */
	public function getSupportedDataStores() {
		return array(
			DMDataStoreType::PDO_MySQL,
			DMDataStoreType::PDO_SQLite
		);
	}

	/**
	 * @param DMAbstractView view
	 * @param DMSession session
	 * @return DMTemplateHelper
	 */
	public function getTemplateHelper(DMAbstractView $view,
			DMSession $session) {
		if (!$this->template_helper
				|| !$this->template_helper->getView() instanceof $view) {
			$this->template_helper = new UNLVSpatialTemplateHelper($view,
					$session);
		}
		return $this->template_helper;
	}

	/**
	 * Invoked when the module is upgraded. Should return any and all
	 * SQL commands necessary to upgrade the tables from any previous version
	 * of the module.
	 *
	 * @return array Array of SQL command strings to be executed, with one
	 * command per array element.
	 * @see getSupportedDataStores()
	 */
	public function getUpgradeSQLForMySQL() {
		return UNLVSpatialMySQLDataStore::getUpgradeSQL();
	}

	/**
	 * Invoked when the module is upgraded. Should return any and all
	 * SQL commands necessary to upgrade the tables from any previous version
	 * of the module.
	 *
	 * @return array Array of SQL command strings to be executed, with one
	 * command per array element
	 * @see getSupportedDataStores()
	 */
	public function getUpgradeSQLForSQLite() {
		return UNLVSpatialSQLiteDataStore::getUpgradeSQL();
	}

	/**
	 * @return string
	 */
	public function getVersion() {
		return "1.0";
	}

}

