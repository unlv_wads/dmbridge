<?php
#
# UNLVSpatial: a dmBridge module for spatial content
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Used in the context of the Control Panel. (routes.xml describes how its
 * methods are hooked up to URIs.)
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVSpatialCPController extends DMCPAdminController {

	public function reindex() {
		$this->preFlightCheck();

		if (DMHTTPRequest::getCurrent()->getMethod() == DMHTTPMethod::POST) {
			$req = DMHTTPRequest::getCurrent();
			$rep = $req->getRepresentation();
			$alias = DMCollection::getSanitizedAlias(
					$rep->getFormValue("alias"));
			$flash = null;
			try {
				$collection = DMCollectionFactory::getCollection($alias);
				$nick = $rep->getFormValue("fields");
				$field = $collection->getField($nick[$alias]);

				$indexer = new UNLVSpatialIndexer();
				$this->tpl_vars['indexer'] = $indexer;
				$indexer->reindex($collection, $field);
				$flash = new DMFlash(
					sprintf("Indexing complete. %d indexed; %d skipped.",
							count($indexer->getIndexedObjects()),
							count($indexer->getSkippedObjects())),
					true);
			} catch (DMUnavailableModelException $e) {
				$flash = new DMFlash("You must select a collection.", false);
			}
			DMHTTPRequest::getCurrent()->getSession()->setFlash($flash);
		}

		$this->tpl_vars['collections'] = DMCollection::getAuthorized();

		$this->renderModuleTemplate(
				dirname(__FILE__) . "/../cp_templates/reindex.html.php");
		die;
	}

}
