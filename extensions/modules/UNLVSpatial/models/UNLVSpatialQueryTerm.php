<?php
#
# UNLVSpatial: a dmBridge module for spatial content
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * A query term which is mostly query-by-example. Instantiate a
 * UNLVSpatialRectObject object with the desired boundary properties, then set
 * it with setSpatialRectObject(). The query term will use the properties of
 * that object in the query.
 * 
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVSpatialQueryTerm {

	private static $valid_modes = array("within", "intersect", "outside");

	/** @var UNLVSpatialRectObject */
	private $spatial_object_example;

	/** @var one of getValidModes() */
	private $matching_mode = "within";

	/**
	 * @return Array of valid modes, as strings
	 */
	public static function getValidModes() {
		return self::$valid_modes;
	}

	/**
	 * @return string
	 */
	public function getMatchingMode() {
		return $this->matching_mode;
	}

	/**
	 * @param string mode
	 * @throws DMIllegalArgumentException
	 */
	public function setMatchingMode($mode) {
		if (!in_array($mode, self::getValidModes())) {
			throw new DMIllegalArgumentException(
				sprintf("Matching mode must be one of the following: ",
					implode(', ', self::getValidModes())));
		}
		$this->matching_mode = $mode;
	}

	/**
	 * @return UNLVSpatialRectObject
	 */
	public function getExampleObject() {
		return $this->spatial_object_example;
	}

	/**
	 * @param UNLVSpatialRectObject obj
	 */
	public function setExampleObject(UNLVSpatialRectObject $obj) {
		$this->spatial_object_example = $obj;
	}

	/**
	 * @return Boolean
	 */
	public function isValid() {
		return $this->spatial_object_example->isValid();
	}

}
