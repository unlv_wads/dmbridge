<?php
#
# UNLVSpatial: a dmBridge module for spatial content
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>Encapsulates a "spatially enabled" rectangular DMObject. To "spatialize"
 * a given DMObject, call spatializeObject().</p>
 * 
 * <p>Note that this class neither extends nor composes DMObject. Instead, its
 * methods will be "added" to those in the DMObject class when the
 * <code>addAssociatedModel()</code> method is called on the DMObject
 * instance, with an instance of this class supplied as a parameter. (See the
 * user's manual for an explanation of how dmBridge module models work.)</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVSpatialRectObject extends UNLVSpatialAbstractSpatialObject {

	/** @var float */
	private $north_lat, $south_lat, $east_long, $west_long;


	/**
	 * @return float
	 */
	public function getSpatialCenterLat() {
		return ($this->getSpatialNorthLat() + $this->getSpatialSouthLat()) / 2;
	}

	/**
	 * @return float
	 */
	public function getSpatialCenterLong() {
		return ($this->getSpatialEastLong() + $this->getSpatialWestLong()) / 2;
	}

	/**
	 * @return float
	 */
	public function getSpatialEastLong() {
		return $this->east_long;
	}

	/**
	 * @param float float
	 */
	public function setSpatialEastLong($float) {
		$this->east_long = (float) $float;
	}

	/**
	 * @return float
	 */
	public function getSpatialNorthLat() {
		return $this->north_lat;
	}

	/**
	 * @param float float
	 */
	public function setSpatialNorthLat($float) {
		$this->north_lat = (float) $float;
	}

	/**
	 * @return float
	 */
	public function getSpatialSouthLat() {
		return $this->south_lat;
	}

	/**
	 * @param float float
	 */
	public function setSpatialSouthLat($float) {
		$this->south_lat = (float) $float;
	}

	/**
	 * @return string Either "metadata" (if parsed from object DC metadata);
	 * "local" (for indexed UNLVSpatial objects); or null.
	 */
	public function getSpatialDataSource() {
		return $this->data_source;
	}

	/**
	 * @param string source Either "metadata" (if parsed from object DC
	 * metadata); "local" (for indexed UNLVSpatial objects); or null.
	 */
	public function setSpatialDataSource($source) {
		$this->data_source = $source;
	}

	/**
	 * @return float
	 */
	public function getSpatialWestLong() {
		return $this->west_long;
	}

	/**
	 * @param float float
	 */
	public function setSpatialWestLong($float) {
		$this->west_long = (float) $float;
	}

	/**
	 * @return boolean
	 */
	public function isSpatiallyValid() {
		if ($this->getSpatialEastLong() > 180
				|| $this->getSpatialEastLong() < -180
				|| $this->getSpatialWestLong() > 180
				|| $this->getSpatialWestLong() < -180) {
			return false;
		}
		if ($this->getSpatialNorthLat() > 90
				|| $this->getSpatialNorthLat() < -90
				|| $this->getSpatialSouthLat() > 90
				|| $this->getSpatialSouthLat() < -90) {
			return false;
		}
		return true;
	}

}

