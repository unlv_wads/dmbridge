<?php
#
# UNLVSpatial: a dmBridge module for spatial content
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>Encapsulates a "spatially enabled" point DMObject. To "spatialize"
 * a given DMObject, call spatializeObject().</p>
 * 
 * <p>Note that this class neither extends nor composes DMObject. Instead, its
 * methods will be "added" to those in the DMObject class when the
 * <code>addAssociatedModel()</code> method is called on the DMObject
 * instance, with an instance of this class supplied as a parameter. (See the
 * user's manual for an explanation of how dmBridge module models work.)</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVSpatialPointObject extends UNLVSpatialAbstractSpatialObject {

	/** @var float */
	private $lat, $long;


	/**
	 * Alias of getSpatialLat().
	 * 
	 * @return float
	 */
	public function getSpatialCenterLat() {
		return $this->getSpatialLat();
	}

	/**
	 * Alias of getSpatialLong().
	 * 
	 * @return float
	 */
	public function getSpatialCenterLong() {
		return $this->getSpatialLong();
	}

	/**
	 * @return float
	 */
	public function getSpatialLat() {
		return $this->lat;
	}

	/**
	 * @param float float
	 */
	public function setSpatialLat($float) {
		$this->lat = (float) $float;
	}

	/**
	 * @return float
	 */
	public function getSpatialLong() {
		return $this->long;
	}

	/**
	 * @param float float
	 */
	public function setSpatialLong($float) {
		$this->long = (float) $float;
	}

	/**
	 * @return boolean
	 */
	public function isSpatiallyValid() {
		if ($this->getSpatialLong() > 180 || $this->getSpatialLong() < -180) {
			return false;
		}
		if ($this->getSpatialLat() > 90 || $this->getSpatialLat() < -90) {
			return false;
		}
		return true;
	}

}

