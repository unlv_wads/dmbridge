<?php
#
# UNLVSpatial: a dmBridge module for spatial content
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>Encapsulates a "spatially enabled" DMObject. To "spatialize" a given
 * DMObject, call spatializeObject().</p>
 * 
 * 
 * <p>Note that this class neither extends nor composes DMObject. Instead, its
 * methods will be "added" to those in the DMObject class when the
 * <code>addAssociatedModel()</code> method is called on the DMObject
 * instance, with an instance of this class supplied as a parameter. (See the
 * user's manual for an explanation of how dmBridge module models work.)</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class UNLVSpatialAbstractSpatialObject {

	/** @var string */
	private $data_source;


	/**
	 * @return string Either "metadata" (if parsed from object DC metadata);
	 * "local" (for indexed UNLVSpatial objects); or null.
	 */
	public function getSpatialDataSource() {
		return $this->data_source;
	}

	/**
	 * @param string source Either "metadata" (if parsed from object DC
	 * metadata); "local" (for indexed UNLVSpatial objects); or null.
	 */
	public function setSpatialDataSource($source) {
		$this->data_source = $source;
	}

	/**
	 * @return string
	 */
	public function getSpatialModelClassName() {
		$rc = new ReflectionClass($this);
		return $rc->getName();
	}

}

