<?php
#
# UNLVSpatial: a dmBridge module for spatial content
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>Interface to be implemented by all spatial object classes.</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
interface UNLVSpatialObject {

	/**
	 * @return float
	 */
	function getSpatialCenterLat();

	/**
	 * @return float
	 */
	function getSpatialCenterLong();

	/**
	 * @return string Either "metadata" (if parsed from object DC metadata);
	 * "local" (for indexed UNLVSpatial objects); or null.
	 */
	function getSpatialDataSource();

	/**
	 * @param string source Either "metadata" (if parsed from object DC
	 * metadata); "local" (for indexed UNLVSpatial objects); or null.
	 */
	function setSpatialDataSource($source);

	/**
	 * @return string
	 */
	function getSpatialModelClassName();

	/**
	 * @return boolean Whether the object's spatial coordinates are all valid.
	 */
	function isSpatiallyValid();

}

