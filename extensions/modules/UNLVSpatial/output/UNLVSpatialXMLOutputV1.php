<?php
#
# UNLVSpatial: a dmBridge module for spatial content
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVSpatialXMLRepresentationV1 extends DMXMLRepresentationV1 implements DMHTTPRepresentationTransformer {

	/**
	 * Overrides parent. The strategy here will be to get the output of the
	 * overridden parent and inject our own XML fragment into it.
	 *
	 * @param DMObject obj
	 * @return XML-encoded string
	 */
	public function transformObject(DMObject $obj) {
		// build the XML structure, but don't return it
		parent::transformObject($obj);

		// add dmObject/bounds elements
		$lat_n = $this->dxml->createElement("latN", $obj->getSpatialNorthLat());
		$lat_s = $this->dxml->createElement("latS", $obj->getSpatialSouthLat());
		$long_w = $this->dxml->createElement("longW", $obj->getSpatialWestLong());
		$long_e = $this->dxml->createElement("longE", $obj->getSpatialEastLong());
		$bounds = $this->dxml->createElement("bounds");
		$bounds->appendChild($lat_n);
		$bounds->appendChild($lat_s);
		$bounds->appendChild($long_w);
		$bounds->appendChild($long_e);
		$this->dxml->documentElement->getElementsByTagName('dmObject')->item(0)
				->appendChild($bounds);
		return $this->output();
	}


	/**
	 * @param UNLVSpatialQuery query
	 * @return XML-encoded string
	 */
	public function transformSpatialResults(UNLVSpatialQuery $query) {
		$uri = clone DMHTTPRequest::getCurrent()->getURI();
		$uri->unsetQuery();
		$uri->setParams("api/1/schema/xml/spatial_objects_list.xsd");
		$this->addSchemaAtURI($uri);

		// results
		$results = $this->dxml->createElement('results');
		$results->setAttribute('page', $query->getPage());
		$results->setAttribute('perPage', $query->getNumResultsPerPage());
		$results->setAttribute('numPages', $query->getNumPages());
		$results->setAttribute('total', $query->getNumResults());

		foreach ($query->getSearchResults() as $obj) {
			$objnode = $this->dxml->createElement('dmObject');
			$objnode->setAttribute('alias', $obj->getCollection()->getAlias());
			$objnode->setAttribute('pointer', $obj->getPtr());
			$objnode->setAttribute('xlink:type', 'simple');
			$objnode->setAttribute('xlink:href', $obj->getURI());

			$bounds = $this->dxml->createElement("bounds");
			$lat_n = $this->dxml->createElement("northLat", $obj->getSpatialNorthLat());
			$lat_s = $this->dxml->createElement("southLat", $obj->getSpatialSouthLat());
			$long_w = $this->dxml->createElement("westLong", $obj->getSpatialWestLong());
			$long_e = $this->dxml->createElement("eastLong", $obj->getSpatialEastLong());
			$bounds->appendChild($lat_n);
			$bounds->appendChild($lat_s);
			$bounds->appendChild($long_w);
			$bounds->appendChild($long_e);
			$objnode->appendChild($bounds);

			$results->appendChild($objnode);
		}
		$this->dxml->documentElement->appendChild($results);

		return $this->output();
	}

}

