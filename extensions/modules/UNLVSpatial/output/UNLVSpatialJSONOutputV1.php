<?php
#
# UNLVSpatial: a dmBridge module for spatial content
# Copyright © 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVSpatialJSONRepresentationV1 extends DMJSONRepresentationV1 implements DMHTTPRepresentationTransformer {

	/**
	 * @param DMObject obj
	 * @return string JSON-encoded string
	 */
	public function transformObject(DMObject $obj) {
		// build the JSON structure, but don't return it
		parent::transformObject($obj);

		$this->json_array['dmBridgeResponse']['dmObject']['bounds'] = array(
			'lat_n' => $obj->getSpatialNorthLat(),
			'lat_s' => $obj->getSpatialSouthLat(),
			'long_w' => $obj->getSpatialWestLong(),
			'long_e' => $obj->getSpatialEastLong()
		);

		return $this->output();
	}

	/**
	 * @param UNLVSpatialQuery q
	 * @return string JSON-encoded string
	 */
	public function transformSpatialResults(UNLVSpatialQuery $q) {
		// results
		$this->addKey('page', $q->getPage());
		$this->addKey('resultsPerPage', $q->getNumResultsPerPage());
		$this->addKey('numPages', $q->getNumPages());
		$this->addKey('total', $q->getNumResults());

		$data = array();
		foreach ($q->getSearchResults() as $obj) {
			$data[] = array(
				'alias' => $obj->getCollection()->getAlias(),
				'pointer' => $obj->getPtr(),
				'href' => $obj->getURI(),
				'bounds' => array(
					'lat_n' => $obj->getSpatialNorthLat(),
					'lat_s' => $obj->getSpatialSouthLat(),
					'long_w' => $obj->getSpatialWestLong(),
					'long_e' => $obj->getSpatialEastLong()
				),
			);
		}
		$this->addKey('results', $data);

		return $this->output();
	}

}
