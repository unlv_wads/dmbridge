<?php
#
# UNLVSpatial: a dmBridge module for spatial content
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVSpatialJSONPRepresentationV1 extends UNLVSpatialJSONRepresentationV1 implements DMHTTPRepresentationTransformer {

	private $callback = "dmBridgeRequest";


	public function __construct() {
		parent::__construct();
		$callback = DMHTTPRequest::getCurrent()->getURI()->getQueryValue("callback");
		$this->setMediaType(new DMMediaType("text", "javascript"));
		if ($callback) {
			$this->callback = substr($callback, 0, 30);
			$this->callback = DMString::paranoid($this->callback, array("_"));
		}
	}

	protected function output() {
		return sprintf("%s(%s)", $this->callback, parent::output());
	}

}
