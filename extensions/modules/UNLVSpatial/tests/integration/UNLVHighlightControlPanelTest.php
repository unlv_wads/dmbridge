<?php

require_once("DmBridgeSystemTest.php");

/**
 * dmBridge Control Panel test plan
 * (for PHPUnit)
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class ControlPanelTest extends DmBridgeSystemTest {

	/** @var DOMDocument */
	private $dxml;

	private static function getURI() {
		return TEST_URL;
	}

	protected function setUp() {
		parent::setUp();
		$this->dxml = new DOMDocument("1.0", "utf-8");
	}


	//////////////////////////////////////////////////////////////////////////
	// GET admin
	//////////////////////////////////////////////////////////////////////////

}
