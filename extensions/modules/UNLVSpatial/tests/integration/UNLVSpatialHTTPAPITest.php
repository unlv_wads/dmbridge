<?php

require_once "PHPUnit/Framework.php";

/**
 * UNLVSpatial HTTP API functional test suite
 * (for PHPUnit)
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVSpatialHTTPAPITest extends PHPUnit_Framework_TestCase {

	private static $has_run_once = false;

	/** @var DOMDocument */
	private $dxml;
	/** @var DMHTTPClient */
	private $http_client;

	private static function getAPIURI() {
		return TEST_URL;
	}

	private static function getXSDRoot() {
		return dirname(__FILE__) . "/../../../../includes/schema/1/xml";
	}

	public function __construct() {
		if (!self::$has_run_once) {
			if (!function_exists("json_last_error")) {
				echo "FYI: json_last_error() is not available; you need PHP 5.3 "
					. "for JSON schema validation.\n";
			}
			self::$has_run_once = true;
		}
	}

	protected function initializeDmBridgeWithSQLiteDataStore() {
		$config = DMConfigIni::getInstance();
		$config->setString("dmbridge.database.engine", "pdo_sqlite");
		$config->setString("dmbridge.database.mysql.host", TEST_DB_HOST);
		$config->setString("dmbridge.database.mysql.name", TEST_DB_NAME);
		$config->setString("dmbridge.database.mysql.password", TEST_DB_PASS);
		$config->setString("dmbridge.database.mysql.port", TEST_DB_PORT);
		$config->setString("dmbridge.database.mysql.username", TEST_DB_USER);
		$this->seedDataStore();
	}

	protected function seedDataStore() {
		$mm = DMModuleManager::getInstance();
		$module = $mm->getModuleByName("UNLVSpatial");
		$mm->setModuleEnabled($module, true);

		$ds = DMDataStoreFactory::getDataStore();
		$sql_stmts = array();
		if ($ds instanceof DMSQLiteDataStore) {
			$sql_stmts = $module->getSetupSQLForSQLite();
			if (file_exists($ds->getPath())) {
				unlink($ds->getPath());
				DMSQLiteDataStore::destroyInstance();
				$ds = DMDataStoreFactory::getDataStore();
			}
		} else if ($ds instanceof DMMySQLDataStore) {
			$sql_stmts = $module->getSetupSQLForMySQL();
		}

		foreach ($sql_stmts as $sql) {
			$ds->write($sql, array());
		}
		$ds->write("DELETE FROM spatial_object", array());

		for ($i = 1; $i < 8; $i++) {
			$sql = "INSERT INTO spatial_object(alias, ptr)
				VALUES (:alias, :ptr)";
			$params = array(
				':alias' => '/static',
				':ptr' => $i
			);
			$ds->write($sql, $params);
		}
	}

	protected function setUp() {
		$this->initializeDmBridgeWithSQLiteDataStore();
		$this->dxml = new DOMDocument("1.0", "utf-8");
		$this->http_client = new DMHTTPClient(new DMHTTPRequest());
	}

	protected function tearDown() {
		$this->dxml = null;
	}


	//////////////////////////////////////////////////////////////////////////
	// GET api/1/objects/random
	//////////////////////////////////////////////////////////////////////////

	function testRandomObjectJSONValidity() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/objects/random.json");
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$this->assertTrue(strlen($response) > 0);
		json_decode($response);
		if (function_exists('json_last_error')) {
			$this->assertFalse((bool) json_last_error());
		}
	}

	function testRandomObjectXMLValidity() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/objects/random.xml");
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$this->assertTrue($this->dxml->loadXML($response));
		$this->assertTrue(
			$this->dxml->schemaValidate(self::getXSDRoot() . '/object_view.xsd'));
	}

	function testAvailableRandomObjectReturns200() {
		$uri = new DMURI(self::getAPIURI() . "/api/1/objects/random.xml");
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$this->assertEquals(200, $response->getStatusCode());
	}

	function testUnavailableRandomObjectReturns204() {
		$ds = DMDataStoreFactory::getDataStore();
		$ds->write("DELETE FROM random_object", array());
		
		$uri = new DMURI(self::getAPIURI() . "/api/1/objects/random.xml");
		$this->http_client->getRequest()->setURI($uri);
		$response = $this->http_client->send();
		$this->assertEquals(204, $response->getStatusCode());
	}

}

