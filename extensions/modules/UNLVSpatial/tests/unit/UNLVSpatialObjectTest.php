<?php

require_once(dirname(__FILE__) . "/../bootstrap.php");

class UNLVSpatialObjectTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var UNLVSpatialObject
	 */
	protected $object;

	protected function setUp() {
		$this->object = new UNLVSpatialObject();
	}

	function testSpatializeObjectWithNoSpatialData() {
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		UNLVSpatialObject::spatializeObject($obj);
		$this->setExpectedException("DMNoSuchMethodException");
		$obj->getSpatialDataSource();
	}

	/**
	 * @todo Write testSpatializeObjectWithLocalSpatialData()
	 */
	function testSpatializeObjectWithLocalSpatialData() {
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		UNLVSpatialObject::spatializeObject($obj);
		$this->markTestIncomplete();
	}

	function testSpatializeObjectWithMetdataDCMIBoxSpatialData() {
		$col = DMCollectionFactory::getCollection(TEST_ALIAS);
		$obj = DMObjectFactory::getObject($col, TEST_PTR);
		$f = new DMDCElement("covera");
		$f->setValue("name=Western Australia; northlimit=-13.5; southlimit=-35.5; westlimit=112.5; eastlimit=129");
		$obj->addField($f);
		UNLVSpatialObject::spatializeObject($obj);
		$this->assertEquals(-13.5, $obj->getSpatialNorthLat());
		$this->assertEquals(-35.5, $obj->getSpatialSouthLat());
		$this->assertEquals(112.5, $obj->getSpatialWestLong());
		$this->assertEquals(129, $obj->getSpatialEastLong());
	}

	function testCenterLat() {
		$this->object->setSpatialNorthLat(40);
		$this->object->setSpatialSouthLat(20);
		$this->assertEquals(30, $this->object->getSpatialCenterLat());
	}

	function testCenterLong() {
		$this->object->setSpatialWestLong(-120);
		$this->object->setSpatialEastLong(-110);
		$this->assertEquals(-115, $this->object->getSpatialCenterLong());
	}

	function testEastLong() {
		$this->object->setSpatialEastLong(-150);
		$this->assertEquals(-150, $this->object->getSpatialEastLong());
	}

	function testNorthLat() {
		$this->object->setSpatialNorthLat(15);
		$this->assertEquals(15, $this->object->getSpatialNorthLat());
	}

	function testSouthLat() {
		$this->object->setSpatialSouthLat(40);
		$this->assertEquals(40, $this->object->getSpatialSouthLat());
	}

	function testSpatialDataSource() {
		$this->object->setSpatialDataSource("metadata");
		$this->assertEquals("metadata", $this->object->getSpatialDataSource());
	}

	function getSpatialWestLong() {
		$this->object->setSpatialWestLong(-15);
		$this->assertEquals(-15, $this->object->getSpatialWestLong());
	}

}
