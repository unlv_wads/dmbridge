<?php
#
# UNLVSpatial: a dmBridge module for spatial content
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

require_once(dirname(__FILE__) . "/../../../../tests/DMTestDataSeeder.php");

/**
 * Seeds the dmBridge data store with fixture data to support testing of the
 * UNLVSpatial module.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVSpatialTestDataSeeder extends DMTestDataSeeder {

	public function seed() {
		parent::seed();
		$ds = UNLVSpatial::getDataStore();

		$col = DMCollectionFactory::getCollection(TEST_DYNAMIC_ALIAS);

		for ($i = 1; $i <= 100; $i++) {
			$obj = DMObjectFactory::getObject($col, $i + 1000);
			if ($i < 50) {
				$sp_obj = new UNLVSpatialRectObject();
				$sp_obj->setSpatialEastLong(rand(0, 180));
				$sp_obj->setSpatialWestLong(rand(-180, 0));
				$sp_obj->setSpatialNorthLat(rand(0, 90));
				$sp_obj->setSpatialSouthLat(rand(-90, 0));
				$obj->addAssociatedModel($sp_obj);
			} else {
				$sp_obj = new UNLVSpatialPointObject();
				$sp_obj->setSpatialLong(rand(0, 180));
				$sp_obj->setSpatialLat(rand(-90, 0));
				$obj->addAssociatedModel($sp_obj);
			}
			$ds->updateObject($obj);
		}
	}

	public function wipe() {
		parent::wipe();
		$ds = DMDataStoreFactory::getDataStore();
		if (($ds instanceof DMSQLiteDataStore && file_exists($ds->getPath()))
				|| !($ds instanceof DMSQLiteDataStore)) {
			$ds->write("DELETE FROM spatial_object", array());
		}
	}

}
