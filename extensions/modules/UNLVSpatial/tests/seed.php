<?php

require_once("bootstrap.php");
require_once("UNLVSpatialTestDataSeeder.php");

$seeder = new UNLVSpatialTestDataSeeder();
$seeder->wipe();
$seeder->seed();

die("Done\n");

