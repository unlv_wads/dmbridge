<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class UNLVRandomDataStoreFactory {

	/**
	 * @return UNLVRandomDataStore
	 */
	public static function getDataStore() {
		$ds = DMDataStoreFactory::getDataStore();
		switch ($ds->getType()) {
			case DMDataStoreType::PDO_MySQL:
				$ds = UNLVRandomMySQLDataStore::getInstance();
				break;
			case DMDataStoreType::PDO_SQLite:
				$ds = UNLVRandomSQLiteDataStore::getInstance();
				break;
		}
		return $ds;
	}
}