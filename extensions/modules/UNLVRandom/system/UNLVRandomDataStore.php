<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
interface UNLVRandomDataStore {

	/**
	 * @param DMObject obj
	 */
	public function addRandomObject(DMObject $obj);

	/**
	 * @return DMObject
	 */
	function getRandomObject();

	/**
	 * @return boolean
	 */
	function isRandomObject(DMObject $obj);

	/**
	 * @return array of DMObjects
	 */
	function getRandomObjects();

}

