<?php
#
# UNLVRandom: a dmBridge module for random objects
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * A convenient place to access the random object.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class UNLVRandomObject {

	public static function getRandomObject() {
		$ds = self::getDataStore();
		return ($ds) ? $ds->getRandomObject() : null;
	}

	public static function getDataStore() {
		$ds = DMDataStoreFactory::getDataStore();
		switch ($ds->getType()) {
			case DMDataStoreType::PDO_MySQL:
				$ds = UNLVRandomMySQLDataStore::getInstance();
				break;
			case DMDataStoreType::PDO_SQLite:
				$ds = UNLVRandomSQLiteDataStore::getInstance();
				break;
		}
		return $ds;
	}

}

