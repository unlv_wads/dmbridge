<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVRandomAddForm extends DMAbstractForm {

	protected function init() {
		$f = new DMSelectField('alias');
		$f->addAttribute('id', 'alias');
		$f->addOption('', 'Choose a collection...');
		foreach (DMCollection::getAuthorized() as $c) {
			$f->addOption($c->getAlias(), DMString::truncate($c->getName(), 60));
		}
		$f->setLabel('Collection');
		$this->addField($f);

		$f = new DMTextField('ptr');
		$f->addAttribute('id', 'ptr');
		$f->addAttribute('size', '7');
		$f->setType(DMFormFieldType::INTEGER);
		$f->setMin(0);
		$f->setMax(1000000);
		$f->setLabel('Pointer');
		$this->addField($f);
	}

	protected function loadFromDataStore() {}

	public function doAction() {
		$col = DMCollectionFactory::getCollection(
			$this->getFieldByName('alias')->getValue());
		$ptr = $this->getFieldByName('ptr')->getValue();
		$object = DMObjectFactory::getObject($col, $ptr);
		$ds = UNLVRandomDataStoreFactory::getDataStore();

		// check to make sure the object doesn't already exist
		if ($ds->isRandomObject($object)) {
			throw new DMIllegalArgumentException(
				DMLocalizedString::getString("RANDOM_OBJECT_ALREADY_EXISTS",
						dirname(__FILE__) . "/../strings"));
		}
		// nope, so add it

		$ds->addRandomObject($object);
	}

}

