<?php
#
# UNLVRandom: a dmBridge module for random objects
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * This file should include all the PHP files needed by the module, whether via
 * include()/require() or autoloading. If using an autoloader, make sure to
 * implement it in such a way that it doesn't conflict with dmBridge's own
 * autoloader. (See http://php.net/manual/en/function.spl-autoload-register.php)
 */

spl_autoload_register("UNLVRandomAutoloader");

function UNLVRandomAutoloader($class) {
	// All of our classes are prefixed with "UNLVRandom" which is a handy,
	// though long, way of distinguishing them. We only want to autoload our own
	// classes.
	if (substr($class, 0, 10) != "UNLVRandom") {
		return;
	}
	$prefix = dirname(__FILE__);
	$all_classes = array(
		'UNLVRandom' => '',
		'UNLVRandomAddForm' => 'system',
		'UNLVRandomController' => 'controllers',
		'UNLVRandomCPController' => 'controllers',
		'UNLVRandomDataStore' => 'system',
		'UNLVRandomDataStoreFactory' => 'system',
		'UNLVRandomMySQLDataStore' => 'system',
		'UNLVRandomObject' => 'system',
		'UNLVRandomSQLiteDataStore' => 'system'
	);

	if (array_key_exists($class, $all_classes)) {
		$path = $prefix . "/" . $all_classes[$class] . "/" . $class . ".php";
		if (include_once($path)) {
			return;
		}
	}
}
