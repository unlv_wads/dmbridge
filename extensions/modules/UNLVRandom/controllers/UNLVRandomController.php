<?php
#
# UNLVRandom: a dmBridge module for random objects
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Used in the context of the HTTP API. (routes.xml describes how its methods
 * are hooked up to URIs.)
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVRandomController {

	public function random() {
		if (!$obj = UNLVRandomObject::getRandomObject()) {
			header("HTTP/1.1 204 No Content");
			die;
		}
		$output = DMHTTPResponseFactory::getTransformer();
		$output->outputHeaders();
		die($output->transformObject($obj));
	}

}
