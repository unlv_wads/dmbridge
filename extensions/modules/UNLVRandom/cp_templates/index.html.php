<?php

$tpl_includes = dirname(__FILE__) . "/../../../../templates/admin/templates/includes";
include_once($tpl_includes . "/head.html.php") ?>

</head>

<?php include_once($tpl_includes . "/body.html.php") ?>

	<h1>Random Object</h1>

	<div id="content">

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<?php if (count($view->getVar("random_objects")) > 0): ?>
			<form method="post" action="">
				<fieldset id="randomObjectPool">
					<legend>Pool</legend>
					<table class="columnar" cellpadding="0" cellspacing="2" border="0">
						<tr>
							<th></th>
							<th>Title</th>
							<th>Collection</th>
							<th>Pointer</th>
						</tr>
						<?php $i = 0; foreach ($view->getVar("random_objects") as $o): ?>
							<tr <?php (!($i & 1)) ? print 'class="odd"' : print '' ?>>
								<td>
									<input type="checkbox" name="objects[]"
										value="<?php echo $o->getCollection()->getAlias() ?>|<?php echo $o->getPtr() ?>" />
								</td>
								<td>
									<a title="<?php echo DMString::websafe($o->getMetadata('title')) ?>"
										href="<?php echo DMString::websafe($o->getURI(DMBridgeComponent::TemplateEngine)) ?>">
										<?php echo DMString::websafe(DMString::truncate($o->getMetadata('title'), 10)) ?></a>
								</td>
								<td>
									<?php echo DMString::websafe($o->getCollection()->getName()) ?>
								</td>
								<td>
									<?php echo DMString::websafe($o->getPtr()) ?>
								</td>
							</tr>
						<?php $i++; endforeach ?>
					</table>
				</fieldset>

				<input type="submit" value="Remove Checked From Pool" />
			</form>
		<?php else: ?>
			<p>There are currently no objects in the random object pool.</p>
		<?php endif ?>
	</div> <!-- #content -->

	<?php include_once($tpl_includes . "/footer.html.php") ?>
