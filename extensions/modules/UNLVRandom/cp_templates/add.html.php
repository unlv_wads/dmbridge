<?php

$tpl_includes = dirname(__FILE__) . "/../../../../templates/admin/templates/includes";
include_once($tpl_includes . "/head.html.php") ?>

</head>

<?php include_once($tpl_includes . "/body.html.php") ?>

	<h1>Random Object</h1>

	<div id="content">
		<h2>Add</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form method="post" action="">
			<fieldset id="randomObjectAdd">
				<legend>Add a Random Object</legend>
				<table class="form" cellpadding="0" cellspacing="0" border="0">
					<tr class="odd">
						<th>
							<label for="alias">Collection</label>
						</th>
						<td>
							<?php echo $view->getVar("alias") ?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="ptr">Pointer</label>
						</th>
						<td>
							<?php echo $view->getVar("ptr") ?>
						</td>
					</tr>
				</table>
			</fieldset>

			<input type="submit" value="Add" />
		</form>

	</div> <!-- #content -->

	<?php include_once($tpl_includes . "/footer.html.php") ?>
