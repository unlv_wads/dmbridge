<?php

/**
 * Welcome to the simplest possible dmBridge module!
 *
 * Even though it doesn't actually do anything, this is a real module that
 * really works! You can switch it on and off in the Control Panel and
 * imagine all the great things it will do when you finish it. For more
 * information about modules, see the user's manual.
 */

/**
 * @author Your Name Here
 */
class SampleModule extends DMAbstractDMBridgeModule
implements DMBridgeModule {

	/**
	 * @return string
	 */
	public function getName() {
		return "Sample Module";
	}

	/**
	 * @return int
	 */
	public function getMinSupportedVersionSequence() {
		return 11;
	}

	/**
	 * @return string
	 */
	public function getVersion() {
		return "1.0";
	}

}
