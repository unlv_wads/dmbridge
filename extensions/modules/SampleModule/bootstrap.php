<?php

/**
 * This file needs to make all of the code used by the module available
 * to the PHP runtime. Because this module is very simple, there is only
 * one require() statement. You could also use your own autoloader; see
 * some of the other bundled modules for an example of this.
 */

require_once("SampleModule.php");
