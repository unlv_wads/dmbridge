<?php

$tpl_includes = dirname(__FILE__) . "/../../../../templates/admin/templates/includes";
include_once($tpl_includes . "/head.html.php") ?>

</head>

<?php include_once($tpl_includes . "/body.html.php") ?>

	<h1>Highlighted Object</h1>

	<div id="content">

		<h2>Add</h2>

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<form method="post" action="">
			<fieldset id="highlightedObjectAdd">
				<legend>Object Information</legend>
				<table class="form" cellspacing="0" cellpadding="0" border="0">
					<tr class="odd">
						<th>
							<label for="alias">Collection</label>
						</th>
						<td>
							<?php echo $view->getVar("alias") ?>
						</td>
					</tr>
					<tr>
						<th>
							<label for="ptr">Pointer</label>
						</th>
						<td>
							<?php echo $view->getVar("ptr") ?>
						</td>
					</tr>
					<tr class="odd">
						<th>
							<label>Effective Date</label>
						</th>
						<td>
							<?php echo $view->getVar("year") ?>
							<?php echo $view->getVar("month") ?>
							<?php echo $view->getVar("day") ?>
						</td>
					</tr>
				</table>
			</fieldset>

			<input type="submit" value="Add" />
		</form>

	</div> <!-- #content -->

	<?php include_once($tpl_includes . "/footer.html.php") ?>
