<?php

$tpl_includes = dirname(__FILE__) . "/../../../../templates/admin/templates/includes";
include_once($tpl_includes . "/head.html.php") ?>

</head>

<?php include_once($tpl_includes . "/body.html.php") ?>

	<h1>Highlighted Object</h1>

	<div id="content">

		<?php echo $view->getHelper()->getHtmlFormattedFlash() ?>

		<?php if (count($view->getVar("highlighted_objects")) > 0): ?>
			<form method="post" action="">
				<fieldset id="highlightedObjectSchedule">
					<legend>Schedule</legend>
					<table class="columnar" cellspacing="2" cellpadding="0" border="0">
						<tr>
							<th></th>
							<th>Effective</th>
							<th>Title</th>
							<th>Alias</th>
							<th>Pointer</th>
						</tr>
						<?php
						$i = 0;
						$drawn = false;
						foreach ($view->getVar("highlighted_objects") as $obj):
							$class = (!($i & 1)) ? 'odd' : '';
							$class .= ($view->getVar("highlighted_object") && $view->getVar("highlighted_object")->equals($obj)) ? ' active' : ''; ?>
							<tr class="<?php echo $class ?>">
								<td>
									<input type="checkbox" name="dates"
										value="<?php echo $obj->getHighlightedDate()->format("Y-m-d") ?>" />
								</td>
								<td>
									<?php echo $obj->getHighlightedDate()->format("Y-m-d") ?>
								</td>
								<td>
									<a title="<?php echo DMString::websafe($obj->getMetadata("title")) ?>"
										href="<?php echo DMString::websafe($obj->getReferenceURL()) ?>">
										<?php echo DMString::websafe(DMString::truncate($obj->getMetadata("title"), 6)) ?>
									</a>
								</td>
								<td>
									<span title="<?php echo DMString::websafe($obj->getCollection()->getName()) ?>"
										  ><?php echo DMString::websafe($obj->getCollection()->getAlias()) ?></span>
								</td>
								<td>
									<?php echo DMString::websafe($obj->getPtr()) ?>
								</td>
							</tr>
						<?php $i++; endforeach ?>
					</table>
				</fieldset>

				<input type="submit" value="Un-Highlight Checked" />
			</form>
		<?php else: ?>
			<p>There are currently no highlighted objects scheduled.</p>
		<?php endif ?>
	</div> <!-- #content -->

	<?php include_once($tpl_includes . "/footer.html.php") ?>
