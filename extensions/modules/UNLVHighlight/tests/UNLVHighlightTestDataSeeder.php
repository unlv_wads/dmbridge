<?php
#
# UNLVHighlight: a dmBridge module for highlighting objects
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Seeds the dmBridge data store with fixture data to support testing of the
 * UNLVHighlight module.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVHighlightTestDataSeeder extends DMTestDataSeeder {

	public function seed() {
		parent::seed();
	}

}
