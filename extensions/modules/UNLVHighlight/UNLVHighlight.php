<?php
#
# UNLVHighlight: a dmBridge module for highlighting objects
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * The main module class. The purpose of this class is to interface the module
 * with dmBridge. As such, it needs to comply with the DMBridgeModule interface.
 * In this case, because it uses the dmBridge data store, it additionally needs
 * to comply with the DMBridgeDataStoreModule interface (which extends
 * DMBridgeModule). In getSupportedDataStores(), we tell dmBridge which data
 * stores the module supports.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVHighlight extends DMAbstractDMBridgeModule
implements DMBridgeControlPanelModule, DMBridgeDataStoreModule,
		DMBridgeRepresentationSchemaModule {

	/**
	 * @return array Array of DMCPMenuSections
	 */
	public function getControlPanelMenuSections() {
		$sections = array();
		$section = new DMCPMenuSection("Highlighted Object");
		$item = new DMCPMenuItem(
				"Add",
				DMInternalURI::getURIWithParams("admin/highlighted/add"));
		$section->addMenuItem($item);
		$item = new DMCPMenuItem(
				"Schedule",
				DMInternalURI::getURIWithParams("admin/highlighted"));
		$section->addMenuItem($item);
		$sections[] = $section;
		return $sections;
	}

	/**
	 * @return int
	 */
	public function getMinSupportedVersionSequence() {
		return 12;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return "UNLVHighlight";
	}

	/**
	 * @return string
	 */
	public function getPathToSchemas() {
		return $this->getAbsolutePathname() . "/schema";
	}

	/**
	 * Invoked when the module is first installed. Should return any and all
	 * SQL commands that are necessary to get the tables in a working state.
	 *
	 * @return array Array of SQL command strings to be executed, with one
	 * command per array element.
	 */
	public function getSetupSQLForMySQL() {
		return UNLVHighlightMySQLDataStore::getSetupSQL();
	}

	/**
	 * Invoked when the module is first installed. Should return any and all
	 * SQL commands that are necessary to get the tables in a working state.
	 *
	 * @return array Array of SQL command strings to be executed, with one
	 * command per array element.
	 */
	public function getSetupSQLForSQLite() {
		return UNLVHighlightSQLiteDataStore::getSetupSQL();
	}

	/**
	 * @return array An array of strings corresponding to the data stores
	 * supported by the module. See the DMDataStoreType class for a list of
	 * types.
	 */
	public function getSupportedDataStores() {
		return array(
			DMDataStoreType::PDO_MySQL,
			DMDataStoreType::PDO_SQLite
		);
	}

	/**
	 * Invoked when the module is upgraded. Should return any and all
	 * SQL commands necessary to upgrade the tables from any previous version
	 * of the module.
	 *
	 * @return array Array of SQL command strings to be executed, with one
	 * command per array element.
	 * @see getSupportedDataStores()
	 */
	function getUpgradeSQLForMySQL() {
		return UNLVHighlightMySQLDataStore::getUpgradeSQL();
	}

	/**
	 * Invoked when the module is upgraded. Should return any and all
	 * SQL commands necessary to upgrade the tables from any previous version
	 * of the module.
	 *
	 * @return array Array of SQL command strings to be executed, with one
	 * command per array element
	 * @see getSupportedDataStores()
	 */
	function getUpgradeSQLForSQLite() {
		return UNLVHighlightSQLiteDataStore::getUpgradeSQL();
	}

	/**
	 * @return string
	 */
	function getVersion() {
		return "1.0";
	}

}

