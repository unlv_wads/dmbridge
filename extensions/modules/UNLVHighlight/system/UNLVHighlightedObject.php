<?php
#
# UNLVHighlight: a dmBridge module for highlighting objects
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * A convenient place to access the random object.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVHighlightedObject {

	/** @var DMDateTime */
	private $highlighted_date;

	/**
	 * @return DMObject
	 */
	public static function getHighlightedObject() {
		$ds = self::getDataStore();
		return ($ds) ? $ds->getHighlightedObject() : null;
	}

	/**
	 * @return UNLVHighlightDataStore
	 */
	public static function getDataStore() {
		$ds = DMDataStoreFactory::getDataStore();
		switch ($ds->getType()) {
			case DMDataStoreType::PDO_MySQL:
				$ds = UNLVHighlightMySQLDataStore::getInstance();
				break;
			case DMDataStoreType::PDO_SQLite:
				$ds = UNLVHighlightSQLiteDataStore::getInstance();
				break;
		}
		return $ds;
	}

	/**
	 * @param DMDateTime date 
	 */
	public function __construct(DMDateTime $date) {
		$this->setHighlightedDate($date);
	}

	/**
	 * @return DMDateTime
	 */
	public function getHighlightedDate() {
		return $this->highlighted_date;
	}

	/**
	 * @param DMDateTime date
	 */
	public function setHighlightedDate(DMDateTime $date) {
		$this->highlighted_date = $date;
	}

}

