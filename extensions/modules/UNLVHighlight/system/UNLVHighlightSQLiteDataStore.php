<?php
#
# UNLVHighlight: a dmBridge module for highlighting objects
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVHighlightSQLiteDataStore extends DMSQLiteDataStore
implements UNLVHighlightDataStore {

	private static $instance;

	/**
	 * This does not work in SQLite.
	 *
	 * @return SQL command string
	 */
	public static function getHighlightedObjectSQL() {
		return "SELECT alias, ptr, datetime FROM highlighted_object
			WHERE datetime <= DATETIME('now')
			ORDER BY datetime DESC
			LIMIT 1";
	}

	/**
	 * @return SQL command string
	 */
	public static function getHighlightedObjectsSQL() {
		return "SELECT alias, ptr, datetime FROM highlighted_object
			ORDER BY datetime";
	}

	/**
	 * Wrapped by getSetupSQLForSQLite() in the main module class (UNLVRandom).
	 *
	 * @return array
	 */
	public static function getSetupSQL() {
		return array(
			'CREATE TABLE "highlighted_object" (
				"id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,
				"alias" VARCHAR NOT NULL,
				"ptr" INTEGER NOT NULL,
				"datetime" VARCHAR NOT NULL
			);'
		);
	}

	/**
	 * Wrapped by getUpgradeSQLForSQLite() in the main module class (UNLVRandom).
	 * 
	 * @return array
	 */
	public static function getUpgradeSQL() {
		return array(
			// currently empty because there is no previous version from which
			// to upgrade.
		);
	}

	/**
	 * @return self object
	 * @since 0.3
	 */
	public static function getInstance() {
		if (!self::$instance instanceof self) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	/**
	 * @param DMObject obj
	 * @param DMDateTime datetime
	 */
	public function addHighlightedObject(DMObject $obj, DMDateTime $datetime) {
		$sql = UNLVHighlightGenericDataStore::addHighlightedObjectSQL();
		$params = array(
			':alias' => $obj->getCollection()->getAlias(),
			':ptr' => $obj->getPtr(),
			':datetime' => $datetime->asMySQLDateTime()
		);
		$this->write($sql, $params);
	}

	/**
	 * @param DMDateTime date
	 */
	function deleteHighlightedObjectsOnDate(DMDateTime $date) {
		$sql = UNLVHighlightGenericDataStore::getDeleteObjectsSQL();
		$params = array(
			':datetime' => $date->asMySQLDateTime()
		);
		$this->write($sql, $params);
	}

	/**
	 * @return DMObject
	 */
	public function getHighlightedObject() {
		$sql = self::getHighlightedObjectSQL();
		$result = $this->read($sql, array());

		if (count($result)) {
			$collection = DMCollectionFactory::getCollection(
					$result[0]['alias']);
			$obj = DMObjectFactory::getObject($collection, $result[0]['ptr']);
			$h_obj = new UNLVHighlightedObject(new DMDateTime($result[0]['datetime']));
			$obj->addAssociatedModel($h_obj);
			return $obj;
		}
		return null;
	}

	/**
	 * @return array of DMObjects
	 */
	public function getHighlightedObjects() {
		$sql = self::getHighlightedObjectsSQL();
		$result = $this->read($sql, array());

		$objects = array();
		foreach ($result as $row) {
			try {
				$collection = DMCollectionFactory::getCollection($row['alias']);
				$obj = DMObjectFactory::getObject($collection, $row['ptr']);

				$h_obj = new UNLVHighlightedObject(new DMDateTime($row['datetime']));
				$obj->addAssociatedModel($h_obj);

				$objects[] = $obj;
			} catch (DMUnavailableModelException $e) {
				// nothing we can do; skip
			}
		}
		return $objects;
	}

}
