<?php
#
# UNLVHighlight: a dmBridge module for highlighting objects
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * A "generic" data store class that returns some generic, cross-DBMS-compatible
 * SQL commands.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class UNLVHighlightGenericDataStore extends DMMySQLDataStore {

	/**
	 * @return SQL command string
	 */
	public static function addHighlightedObjectSQL() {
		return "INSERT INTO highlighted_object(alias, ptr, datetime)
			VALUES(:alias, :ptr, :datetime);";
	}

	/**
	 * @return SQL command string
	 */
	public static function getDeleteObjectsSQL() {
		return "DELETE FROM highlighted_object WHERE datetime = :datetime;";
	}

}
