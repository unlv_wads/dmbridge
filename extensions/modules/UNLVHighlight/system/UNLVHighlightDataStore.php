<?php
#
# UNLVHighlight: a dmBridge module for highlighting objects
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
interface UNLVHighlightDataStore {
	
	/**
	 * @param DMObject obj
	 * @param DMDateTime datetime
	 */
	function addHighlightedObject(DMObject $obj, DMDateTime $datetime);

	/**
	 * @param DMDateTime date
	 */
	function deleteHighlightedObjectsOnDate(DMDateTime $date);

	/**
	 * @return DMObject
	 */
	function getHighlightedObject();

	/**
	 * @return array of DMObjects
	 */
	function getHighlightedObjects();

}

