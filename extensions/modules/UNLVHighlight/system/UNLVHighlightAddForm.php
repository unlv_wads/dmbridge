<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVHighlightAddForm extends DMAbstractForm {

	protected function init() {
		$f = new DMSelectField('alias');
		$f->addAttribute('id', 'alias');
		$f->addOption('', 'Choose a collection...');
		foreach (DMCollection::getAuthorized() as $c) {
			$f->addOption($c->getAlias(), DMString::truncate($c->getName(), 60));
		}
		$f->setLabel('Collection');
		$this->addField($f);

		$f = new DMTextField('ptr');
		$f->addAttribute('id', 'ptr');
		$f->addAttribute('size', '7');
		$f->setType(DMFormFieldType::INTEGER);
		$f->setMin(0);
		$f->setMax(1000000);
		$f->setLabel('Pointer');
		$this->addField($f);

		$f = new DMSelectField('year');
		$f->addAttribute('id', 'year');
		$f->setValue(date('Y'));
		$f->setLabel('Year');
		$min = date('Y');
		$max = date('Y', strtotime('+2 years'));
		$f->setMin($min);
		$f->setMax($max);
		foreach (range($min, $max) as $y) {
			$f->addOption($y, $y);
		}
		$this->addField($f);

		$f = new DMSelectField('month');
		$f->addAttribute('id', 'month');
		$f->setValue(date('n'));
		$f->setMin(1);
		$f->setMax(12);
		foreach (range(1, 12) as $m) {
			$f->addOption($m, date('F', mktime(0, 0, 0, $m, 2)));
		}
		$f->setLabel('Month');
		$this->addField($f);

		$f = new DMSelectField('day');
		$f->addAttribute('id', 'day');
		$f->setValue(date('j'));
		$f->setMin(1);
		$f->setMax(31);
		foreach (range(1, 31) as $d) {
			$f->addOption($d, $d);
		}
		$f->setLabel('Day');
		$this->addField($f);
	}

	protected function loadFromDataStore() {}

	public function doAction() {
		$col = DMCollectionFactory::getCollection(
				$this->getFieldByName('alias')->getValue());
		$object = DMObjectFactory::getObject($col,
				(int) $this->getFieldByName('ptr')->getValue());
		$date = new DMDateTime(
			sprintf("%d-%d-%d",
				$this->getFieldByName("year")->getValue(),
				$this->getFieldByName("month")->getValue(),
				$this->getFieldByName("day")->getValue()));

		$ds = UNLVHighlightDataStoreFactory::getDataStore();
		$ds->addHighlightedObject($object, $date);
	}

}
