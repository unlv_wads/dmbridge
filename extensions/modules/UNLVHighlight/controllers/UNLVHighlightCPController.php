<?php
#
# UNLVHighlight: a dmBridge module for highlighting objects
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVHighlightCPController extends DMCPAdminController {

	public function index() {
		$this->preFlightCheck();

		if ($this->getHTTPRequest()->getMethod() == DMHTTPMethod::POST) {
			$this->delete();
		}

		$ds = UNLVHighlightDataStoreFactory::getDataStore();
		$this->tpl_vars['highlighted_objects'] = $ds->getHighlightedObjects();
		$this->tpl_vars['highlighted_object'] = $ds->getHighlightedObject();

		$this->renderModuleTemplate(
					dirname(__FILE__) . "/../cp_templates/index.html.php");
		die;
	}

	public function add() {
		$this->preFlightCheck();

		$this->setForm(new UNLVHighlightAddForm());

		if (DMHTTPRequest::getCurrent()->getMethod() == DMHTTPMethod::POST) {
			$this->handleForm(DMLocalizedString::getString(
					"HIGHLIGHTED_OBJECT_ADDED",
					dirname(__FILE__) . "/../strings"));
		}

		$this->tpl_vars['alias'] = $this->getForm()->getFieldByName("alias")
				->getHTMLTag();
		$this->tpl_vars['ptr'] = $this->getForm()->getFieldByName("ptr")
				->getHTMLTag();
		$this->tpl_vars['year'] = $this->getForm()->getFieldByName("year")
				->getHTMLTag();
		$this->tpl_vars['month'] = $this->getForm()->getFieldByName("month")
				->getHTMLTag();
		$this->tpl_vars['day'] = $this->getForm()->getFieldByName("day")
				->getHTMLTag();
		
		$this->renderModuleTemplate(
					dirname(__FILE__) . "/../cp_templates/add.html.php");
		die;
	}

	private function delete() {
		$rep = $this->getHTTPRequest()->getRepresentation();
		$dates = $rep->getFormValues("dates");
		$ds = UNLVHighlightDataStoreFactory::getDataStore();
		try {
			foreach ($dates as $date) {
				$date = new DMDateTime($date);
				$ds->deleteHighlightedObjectsOnDate($date);
			}
			$this->getSession()->setFlash(new DMFlash(
					DMLocalizedString::getString(
							"HIGHLIGHTED_OBJECT_DELETED",
							dirname(__FILE__) . "/../strings"),
					true));
		} catch (DMIOException $e) {
			$this->getSession()->setFlash(
					new DMFlash($e->getMessage(), false));
		}
	}

}

