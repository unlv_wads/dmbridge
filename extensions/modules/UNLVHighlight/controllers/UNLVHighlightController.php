<?php
#
# UNLVHighlight: a dmBridge module for highlighting objects
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class UNLVHighlightController {

	/**
	 * Outputs the highlighted object
	 */
	public function highlighted() {
		if (!$obj = UNLVHighlightedObject::getHighlightedObject()) {
			header("HTTP/1.1 204 No Content");
			die;
		}
		$output = DMHTTPResponseFactory::getTransformer();
		$output->outputHeaders();
		die($output->transformObject($obj));
	}

	/**
	 * Outputs the highlighted object.
	 */
	public function highlightedFeed() {
		if (!$obj = DMObject::getHighlighted()) {
			header("HTTP/1.1 204 No Content");
			die;
		}
		$output = DMHTTPResponseFactory::getTransformer();
		// highlighted object is not accessible via the DMHTMLRepresentation
		if ($output instanceof DMHTMLRepresentation) {
			$output = DMAtomRepresentation::getInstance();
		}
		$output->outputHeaders();
		die($output->transformObject($obj));
	}

}
