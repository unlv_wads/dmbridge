<?php

/**
 * <p>By default, dmBridge performs authentication using its built-in
 * DMCdmAuthenticationService class, which authenticates against CONTENTdm's
 * HTTP Basic authentication component the same as the CONTENTdm(R)
 * administration module.</p>
 *
 * <p>It is also possible to tell dmBridge to use a different authentication
 * service. This file contains a skeleton of a class needed to do this.
 * To authenticate against some other service, like an LDAP server (for
 * example), simply complete the authenticate() method and set
 * "dmbridge.authentication_class" to the name of this class in config.ini.
 * (You are free to rename this class, but you must keep it in this same folder
 * and the class name must match the filename.)</p>
 *
 * @author Your Name Here
 */
class CustomAuthenticationService extends DMAbstractAuthenticationService
implements DMAuthenticationService {

	/**
	 * @param DMUser user
	 * @param string password
	 * @return boolean True if authentication was successful; false if not
	 */
	public function authenticate(DMUser $user, $password) {
		// Check whether $user->getUsername() & $password are valid. If so,
		// return true. Otherwise, return false.
	}

}
