<?php /* DO NOT ALTER THIS LINE -- dmBridge -- DO NOT ALTER THIS LINE */
/**
 * dmBridge replacement reference URL routing script
 *
 * To use, back up your existing /u/index.php file and copy this over it.
 */

// Set this to the absolute path to your dmBridge folder, relative
// to your web server's document root.
define('DMBRIDGE_ROOT', '/dm');

/****************** Do not change anything below this line ******************/

// Set $_SERVER['DOCUMENT_ROOT'] properly for IIS
if (!isset($_SERVER['DOCUMENT_ROOT'])) {
	if (isset($_SERVER['SCRIPT_FILENAME'])) {
		$_SERVER['DOCUMENT_ROOT'] = str_replace(
			'\\',
			'/',
			substr($_SERVER['SCRIPT_FILENAME'], 0, 0-strlen($_SERVER['PHP_SELF']))
		);
	}
	else if (isset($_SERVER['PATH_TRANSLATED'])) {
		$_SERVER['DOCUMENT_ROOT'] = str_replace(
			'\\', '/',
			substr(
				str_replace('\\\\', '\\', $_SERVER['PATH_TRANSLATED']),
				0,
				0-strlen($_SERVER['PHP_SELF'])
			)
		);
	}
}

require_once(
	sprintf('%s/%s/system/init/ref_url.php',
		$_SERVER['DOCUMENT_ROOT'],
		trim(DMBRIDGE_ROOT, '/')));

