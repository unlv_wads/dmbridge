--
-- dmBridge SQLite3 schema
--
-- Dumped with SQLite Manager
-- Tested with SQLite 3.6.16
--
-- Note: The vertical-bar statement separators are important!
--

-- ||| --

CREATE TABLE "comment" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "alias" VARCHAR(32) NOT NULL,
  "ptr" NUMERIC(7,0) NOT NULL,
  "name" VARCHAR(50) NOT NULL,
  "email" VARCHAR(50),
  "text" VARCHAR(5000) NOT NULL,
  "is_approved" NUMERIC(1,0) DEFAULT 0,
  "posted_at" DATETIME DEFAULT CURRENT_TIMESTAMP
);

-- ||| --

CREATE TABLE "rating" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "alias" VARCHAR(32) NOT NULL,
  "ptr" NUMERIC(7,0) NOT NULL,
  "value" NUMERIC(3,0) NOT NULL,
  "posted_at" DATETIME DEFAULT CURRENT_TIMESTAMP
);

-- ||| --

CREATE TABLE "tag" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "alias" VARCHAR(32) NOT NULL,
  "ptr" NUMERIC(7,0) NOT NULL,
  "value" VARCHAR(100) NOT NULL,
  "is_approved" NUMERIC(1,0) DEFAULT 0,
  "posted_at" DATETIME DEFAULT CURRENT_TIMESTAMP
);
