--
-- dmBridge MySQL schema
--
-- Dumped with phpMyAdmin 2.11.7
-- Tested with MySQL 5.1.31
--
-- Note: The vertical-bar statement separators are important!
--


-- ||| --

CREATE TABLE IF NOT EXISTS comment (
   id int(11) NOT NULL AUTO_INCREMENT,
   alias varchar(32) NOT NULL,
   ptr decimal(7,0) unsigned NOT NULL,
   email varchar(64),
   name varchar(64),
   text varchar(8192) NOT NULL,
   posted_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
   is_approved decimal(1,0) unsigned DEFAULT '0',
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ||| --

CREATE TABLE IF NOT EXISTS rating (
   id int(11) unsigned NOT NULL AUTO_INCREMENT,
   alias varchar(32) NOT NULL,
   ptr decimal(7,0) unsigned NOT NULL,
   value decimal(3,0) unsigned NOT NULL,
   posted_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ||| --

CREATE TABLE IF NOT EXISTS tag (
   id int(11) unsigned NOT NULL AUTO_INCREMENT,
   alias varchar(32) NOT NULL,
   ptr decimal(7,0) unsigned NOT NULL,
   value varchar(64) NOT NULL,
   posted_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
   is_approved decimal(1,0) unsigned DEFAULT '0',
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
