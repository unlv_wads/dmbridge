This folder contains various JavaScripts that can be shared across
template sets and whatever else. Refer to it thusly:

dirname($_SERVER['PHP_SELF']) . "/includes/js/myscript.js"

This folder should NOT be used for custom scripts. JavaScripts for modules
should reside in module folders.

