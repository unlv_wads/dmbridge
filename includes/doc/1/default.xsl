<?xml version="1.0" encoding="utf-8" ?>

<!--
dmBridge HTTP API XSL stylesheet
-->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xlink="http://www.w3.org/1999/xlink">

	<xsl:output method="html"/>
	<xsl:param name="dmBridgeURI"/>

	<xsl:template match="/dmBridgeHTTPAPI">
		<html>
			<head>
				<title>Digital Collections HTTP API Documentation</title>
			</head>
			<body>
				<div id="wrap">
					<h1>Digital Collections HTTP API Documentation</h1>

					<h3>Character encoding</h3>
					<p>The API returns all character data as UTF-8, and expects
					to receive it as UTF-8 as well.</p>

					<h3>Status codes</h3>
					<p>Clients should depend on the numeric HTTP status code only, not
					the result message. In addition to the documented status codes
					below, clients should be prepared to handle unexpected
					500-level errors in response to any request.</p>

					<h3>Content representations</h3>
					<p>This API service will output XML, JSON, or JSONP depending on the
					order of preference expressed by your HTTP client in the <code>Accept</code>
					header. This can be overridden by appending one of the extensions
					below to the URI.</p>

					<table cellspacing="2" cellpadding="0" border="0">
						<tr>
							<th> </th>
							<th>URI extension</th>
							<th>MIME type</th>
							<th>Notes</th>
						</tr>
						<tr>
							<th>XML</th>
							<td>.xml</td>
							<td>application/xml</td>
							<td>XML records are in the <code>http://digital.library.unlv.edu</code>
							namespace. They use <a href="http://www.w3.org/TR/xlink/">XLink</a>
							for cross-document hyperlinking and <a href="http://www.w3.org/XML/Schema">XML
							Schema</a> for validation.</td>
						</tr>
						<tr>
							<th>JSON</th>
							<td>.json</td>
							<td>application/json</td>
							<td></td>
						</tr>
						<tr>
							<th>JSONP</th>
							<td>.jsonp</td>
							<td>text/javascript</td>
							<td>JSONP wraps JSON output inside a JavaScript
							callback function. By default, this function is named
							<code>dmBridgeRequest</code>, but a custom function name
							can be specified with the <code>callback</code> query
							string parameter; e.g. <code>?callback=myFunction</code>.</td>
						</tr>
					</table>

					<h3>Other notes</h3>
					<p>Please be respectful of this server's limited processing and
					bandwidth resources, and employ caching where possible and
					appropriate.</p>

					<h3>API methods</h3>

					<div id="toc">
						<xsl:apply-templates select="category"/>
					</div>
					<xsl:apply-templates select="category/apiMethod"/>
				</div>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="category">
		<table cellpadding="0" cellspacing="2" border="0">
			<caption><xsl:value-of select="@name"/></caption>
			<xsl:for-each select="apiMethod">

				<tr>
					<xsl:if test="position() mod 2 > 0">
						<xsl:attribute name="class">odd</xsl:attribute>
					</xsl:if>
					<td style="width:40%">
						<xsl:element name="a">
							<xsl:attribute name="href">
								#<xsl:value-of select="@id"/>
							</xsl:attribute>
							<xsl:value-of select="title"/>
						</xsl:element>
					</td>
					<td>
						<xsl:value-of select="uri"/>
					</td>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>

	<xsl:template match="category/apiMethod">
		<div class="apiMethod">
			<xsl:element name="a">
				<xsl:attribute name="name">
					<xsl:value-of select="@id"/>
				</xsl:attribute>
			</xsl:element>

			<h2><xsl:value-of select="./title"/></h2>

			<p class="description"><xsl:value-of select="./description"/></p>

			<h3>HTTP method</h3>
			<p class="uri">
				<xsl:for-each select="uri">
					<xsl:value-of select="../httpMethod"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="."/>
					<br/>
				</xsl:for-each>
			</p>

			<h3>Query parameters</h3>
			<xsl:choose>
				<xsl:when test="parameters">
					<xsl:apply-templates select="parameters"/>
				</xsl:when>
				<xsl:otherwise>
					<p>None</p>
				</xsl:otherwise>
			</xsl:choose>

			<h3>Status codes</h3>
			<xsl:apply-templates select="statusCodes"/>

			<xsl:if test="example">
				<h3>Examples</h3>
				<ul>
					<xsl:apply-templates select="example"/>
				</ul>
			</xsl:if>
		</div>
	</xsl:template>

	<xsl:template match="parameters">
		<table cellspacing="3" cellpadding="0" border="0">
			<tr>
				<th>Key</th>
				<th>Required?</th>
				<th>Description</th>
				<th>Allowed Values</th>
				<th>Default Value</th>
			</tr>
			<xsl:for-each select="parameter">
				<tr>
					<xsl:if test="position() mod 2 > 0">
						<xsl:attribute name="class">odd</xsl:attribute>
					</xsl:if>
					<td><xsl:value-of select="@key"/></td>
					<td>
						<xsl:choose>
							<xsl:when test="@required = 1">Yes</xsl:when>
							<xsl:otherwise>No</xsl:otherwise>
						</xsl:choose>
					</td>
					<td><xsl:value-of select="description"/></td>
					<td>
						<xsl:if test="allowedValues">
							<xsl:apply-templates select="allowedValues"/>
						</xsl:if>
					</td>
					<td>
						<xsl:if test="defaultValue">
							<xsl:value-of select="defaultValue"/>
						</xsl:if>
					</td>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>

	<xsl:template match="allowedValues">
		<ul>
			<xsl:for-each select="value">
				<li>
					<xsl:if test="@value">
						<strong><xsl:value-of select="@value"/></strong>
						<xsl:text>: </xsl:text>
					</xsl:if>
					<xsl:value-of select="description"/>
				</li>
			</xsl:for-each>
		</ul>
	</xsl:template>

	<xsl:template match="statusCodes">
		<dl>
			<xsl:for-each select="code">
				<dt><xsl:value-of select="@code"/></dt>
				<dd><xsl:value-of select="condition"/></dd>
			</xsl:for-each>
		</dl>
	</xsl:template>

	<xsl:template match="example">
		<li>
			<xsl:element name="a">
				<xsl:attribute name="class">example</xsl:attribute>
				<xsl:attribute name="href">
					<xsl:value-of select="$dmBridgeURI"/>
					<xsl:value-of select="@params"/>
					<xsl:text>.</xsl:text>
					<xsl:value-of select="@format"/>
					<xsl:if test="@query">
						<xsl:text>?</xsl:text>
						<xsl:value-of select="@query"/>
					</xsl:if>
				</xsl:attribute>
				<xsl:value-of select="translate(@format,
                                'abcdefghijklmnopqrstuvwxyz',
                                'ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
			</xsl:element>
		</li>
	</xsl:template>

</xsl:stylesheet>
