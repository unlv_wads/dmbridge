<?xml version="1.0" encoding="utf-8" standalone="yes"?>

<dmBridgeHTTPAPI version="1">

	<category name="Objects">

		<apiMethod id="objects/[alias]">
			<uri>api/1/objects/[alias]</uri>
			<title>List all objects in a collection</title>
			<description>Retrieves all objects in a collection in a paginated list.
			The default number of results per page is 20.</description>
			<example format="xml" params="api/1/objects/hughes" />
			<example format="json" params="api/1/objects/hughes" />
			<parameters>
				<parameter key="page" required="0">
					<description>Page number</description>
					<defaultValue>1</defaultValue>
					<allowedValues>
						<value>
							<description>Positive integer</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="rpp" required="0">
					<description>Results per page</description>
					<defaultValue>50</defaultValue>
					<allowedValues>
						<value>
							<description>Integer between 10 and 100</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="sort" required="0">
					<description>Nickname of the field by which to sort</description>
				</parameter>
				<parameter key="order" required="0">
					<defaultValue>asc</defaultValue>
					<description>Sort order</description>
					<allowedValues>
						<value value="asc">
							<description>Ascending</description>
						</value>
						<value value="desc">
							<description>Descending</description>
						</value>
					</allowedValues>
				</parameter>
			</parameters>
            <httpMethod>GET</httpMethod>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="404 Not Found">
					<condition>The collection with specified [alias] either does not
					exist or is unpublished.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="objects/[alias]/highest_rated">
			<uri>api/1/objects/[alias]/highest_rated</uri>
			<title>List the highest rated objects in a collection</title>
			<example format="xml" params="api/1/objects/hughes/highest_rated" />
			<example format="json" params="api/1/objects/hughes/highest_rated" />
            <httpMethod>GET</httpMethod>
			<parameters>
				<parameter key="rpp" required="0">
					<defaultValue>50</defaultValue>
					<description>Results per page</description>
					<allowedValues>
						<value>
							<description>Integer between 10 and 100</description>
						</value>
					</allowedValues>
                </parameter>
			</parameters>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="501 Not Implemented">
					<condition>Ratings are not available from this service.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="objects/[alias]/[pointer]">
			<uri>api/1/objects/[alias]/[pointer]</uri>
			<title>View a specific object</title>
			<description>View an object identified by collection alias [alias]
			and pointer [pointer].</description>
			<example format="xml" params="api/1/objects/hughes/87" />
			<example format="json" params="api/1/objects/hughes/87" />
			<httpMethod>GET</httpMethod>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="404 Not Found">
					<condition>The object with specified [alias] and [pointer] either
					does not exist or is in an unpublished collection.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="search">
			<uri>api/1/search</uri>
			<title>Search for objects based on their metadata</title>
			<description>Up to four search terms can be used in a query.
			Each term is composed of a string (text to search for), a matching mode,
			and a metadata field in which to search.</description>
			<example format="xml" params="api/1/search" query="CISOROOT=/hughes&amp;CISOOP1=exact&amp;CISOFIELD1=title&amp;CISOBOX1=mobots"/>
			<example format="json" params="api/1/search" query="CISOROOT=/hughes&amp;CISOOP1=exact&amp;CISOFIELD1=title&amp;CISOBOX1=mobots"/>
			<parameters>
				<parameter key="q" required="0">
					<description>Simple string query; an alternative to the "CISO"
					parameters below. If this is supplied, the "CISO" parameters
					are not necessary.</description>
				</parameter>
				<parameter key="CISOOP[1-4]" required="1">
					<description>Matching mode</description>
					<allowedValues>
						<value value="all">
							<description>Match objects that contain all words in the
							search string</description>
						</value>
						<value value="exact">
							<description>Match objects that contain the exact search
							string</description>
						</value>
						<value value="any">
							<description>Match objects that contain any of the words
							in the search string</description>
						</value>
						<value value="none">
							<description>Match objects that do not contain any of the
							words in the search string</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="CISOBOX[1-4]" required="1">
					<description>Search string</description>
				</parameter>
				<parameter key="CISOFIELD[1-4]" required="1">
					<description>Nickname of the field in which to search</description>
					<allowedValues>
						<value>
							<description>A valid field nickname</description>
						</value>
						<value value="CISOSEARCHALL">
							<description>Search across all fields</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="CISOROOT" required="1">
					<description>A valid collection alias; multiple aliases
					separated by commas; or the word "all" to search across all
					collections</description>
				</parameter>
				<parameter key="page" required="0">
					<description>Page number</description>
					<defaultValue>1</defaultValue>
					<allowedValues>
						<value>
							<description>Positive integer</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="rpp" required="0">
					<description>Results per page</description>
					<defaultValue>50</defaultValue>
					<allowedValues>
						<value>
							<description>Positive integer between 10 and 100</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="sort" required="0">
					<description>Nickname of the field by which to sort</description>
				</parameter>
				<parameter key="order" required="0">
				    <defaultValue>asc</defaultValue>
					<description>Sort order</description>
					<allowedValues>
						<value value="asc">
							<description>Ascending</description>
						</value>
						<value value="desc">
							<description>Descending</description>
						</value>
                    </allowedValues>
                </parameter>
			</parameters>
			<httpMethod>GET</httpMethod>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="400 Bad Request">
					<condition>Parameters were supplied in an unexpected format.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="search_by_rating">
			<uri>api/1/search_by_rating</uri>
			<title>Search for objects by rating</title>
			<description>Searches for objects based on the specified rating thresholds.</description>
			<example format="xml" params="api/1/search_by_rating" query="collections=/hughes&amp;rating=0.5"/>
			<example format="json" params="api/1/search_by_rating" query="collections=/hughes&amp;rating=0.5"/>
			<parameters>
				<parameter key="page" required="0">
					<description>Page number</description>
					<defaultValue>1</defaultValue>
					<allowedValues>
						<value>
							<description>Positive integer</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="rpp" required="0">
					<description>Results per page</description>
					<defaultValue>50</defaultValue>
					<allowedValues>
						<value>
							<description>Positive integer between 10 and 100</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="min_rating" required="1">
					<description>Minimum rating threshold</description>
					<defaultValue></defaultValue>
					<allowedValues>
						<value>
							<description>Positive decimal between 0 and 1.</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="max_rating" required="1">
					<description>Maximum rating threshold</description>
					<defaultValue></defaultValue>
					<allowedValues>
						<value>
							<description>Positive decimal between 0 and 1.</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="collections" required="0">
					<description>Collections</description>
					<allowedValues>
						<value>
							<description>A valid collection alias, or multiple aliases
							separated by commas, to which to limit the search.</description>
						</value>
					</allowedValues>
				</parameter>
			</parameters>
			<httpMethod>GET</httpMethod>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="400 Bad Request">
					<condition>Parameters were supplied in an unexpected format.</condition>
				</code>
				<code code="501 Not Implemented">
					<condition>Ratings are not available from this service.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="objects/[alias]/[pointer]/bitstream">
			<uri>api/1/objects/[alias]/[pointer]/bitstream</uri>
			<title>Retrieve an object's bitstream</title>
			<description>Retrieves an object's bitstream (file).</description>
			<example format="jpeg" params="api/1/objects/hughes/87/bitstream" />
			<httpMethod>GET</httpMethod>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="404 Not Found">
					<condition>The object with specified [alias] and [pointer] either
					does not exist or is in an unpublished collection.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="objects/[alias]/[pointer]/excerpt">
			<uri>api/1/objects/[alias]/[pointer]/excerpt</uri>
			<title>Retrieve an excerpt of an object's bitstream</title>
			<description>Retrieves an excerpt of an object's bitstream (file).
			Currently only works with PDF objects.</description>
			<example format="jpeg" params="api/1/objects/hughes/87/excerpt" />
			<httpMethod>GET</httpMethod>
			<parameters>
				<parameter key="page" required="1">
					<description>Page number, for PDF objects only</description>
				</parameter>
				<parameter key="format" required="0">
					<description>Output file format, for PDF objects only.</description>
					<allowedValues>
						<value>
							<description>pdf</description>
						</value>
					</allowedValues>
				</parameter>
			</parameters>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="400 Bad Request">
					<condition>Parameters were supplied in an unexpected format.</condition>
				</code>
				<code code="404 Not Found">
					<condition>The object with specified [alias] and [pointer] either
					does not exist or is in an unpublished collection; or there is
					no excerpt available for it.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="objects/[alias]/[pointer]/search">
			<uri>api/1/objects/[alias]/[pointer]/search</uri>
			<title>Search within the full text of an object</title>
			<description></description>
			<example format="xml" params="api/1/objects/snv/4838/search" query="term=papers"/>
			<example format="json" params="api/1/objects/snv/4838/search" query="term=papers"/>
			<parameters>
				<parameter key="term" required="1">
					<description>Search string</description>
				</parameter>
			</parameters>
			<httpMethod>GET</httpMethod>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="400 Bad Request">
					<condition>Parameters were supplied in an unexpected format.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="objects/[alias]/[pointer]/rating">
			<uri>api/1/objects/[alias]/[pointer]/rating</uri>
			<title>View an object's rating</title>
			<description>Returns an object's mean rating along with the number of
			times it has been rated. Note that this information is also available in
			the object's representation (<a href="#objects/[alias]">objects/[alias]</a>).</description>
			<example format="xml" params="api/1/objects/hughes/87/rating" />
			<example format="json" params="api/1/objects/hughes/87/rating" />
			<httpMethod>GET</httpMethod>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="404 Not Found">
					<condition>The object with specified [alias] and [pointer] either
					does not exist or is in an unpublished collection.</condition>
				</code>
				<code code="501 Not Implemented">
					<condition>Ratings are not available from this service.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="objects/[alias]/[pointer]/rating">
			<uri>api/1/objects/[alias]/[pointer]/rating</uri>
			<title>Rate an object</title>
			<description>Rates an object on a numeric scale. Note: The client should
			check (using the <a href="#status">status</a> method)
			whether the server supports object ratings before soliciting them from the user.</description>
            <httpMethod>POST</httpMethod>
			<parameters>
				<parameter key="value" required="1">
					<description>Rating value</description>
					<allowedValues>
						<value>
							<description>0-100</description>
						</value>
					</allowedValues>
				</parameter>
			</parameters>
			<statusCodes>
				<code code="204 No Content">
					<condition>The request succeeded.</condition>
				</code>
				<code code="400 Bad Request">
					<condition>Invalid parameters were supplied.</condition>
				</code>
				<code code="403 Forbidden">
					<condition>New ratings are not currently being accepted.</condition>
   				</code>
                <code code="404 Not Found">
					<condition>The object with specified [alias] and [pointer] either
					does not exist or is in an unpublished collection.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="objects/[alias]/[pointer]/thumbnail">
			<uri>api/1/objects/[alias]/[pointer]/thumbnail</uri>
			<title>View an object's thumbnail image</title>
			<description>Returns an object's thumbnail image.</description>
			<example format="jpeg" params="api/1/objects/hughes/87/thumbnail" />
			<httpMethod>GET</httpMethod>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="404 Not Found">
					<condition>The object with specified [alias] and [pointer] either
					does not exist or is in an unpublished collection.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

	</category>

	<category name="Collections">

		<apiMethod id="collections">
			<uri>api/1/collections</uri>
			<title>List all collections</title>
			<description>Lists all public collections. Note that this does
				<strong>not</strong> list any objects within the collections. To
				list objects within a single collection, use
				<a href="#objects/[alias]">objects/[alias]</a>. There is currently no way to
				list all objects within all collections.</description>
			<example format="xml" params="api/1/collections" />
			<example format="json" params="api/1/collections" />
			<httpMethod>GET</httpMethod>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="collections/[alias]">
			<uri>api/1/collections/[alias]</uri>
			<title>View a specific collection</title>
			<description>Returns the collection with alias [alias]. Note that this
				does <strong>not</strong> list any objects within the collection;
				for that, use <a href="#objects/[alias]">objects/[alias]</a>.</description>
			<example format="xml" params="api/1/collections/hughes" />
			<example format="json" params="api/1/collections/hughes" />
			<httpMethod>GET</httpMethod>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="404 Not Found">
					<condition>The collection with specified [alias] either
					does not exist or is unpublished.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

	</category>

	<category name="Comments">

		<apiMethod id="comments">
			<uri>api/1/comments</uri>
			<title>Search for comments</title>
			<description>Returns a paginated list of all approved comments in the
				system matching the given criteria, sorted in descending order by posting date.</description>
			<example format="xml" params="api/1/comments" />
			<example format="json" params="api/1/comments" />
			<httpMethod>GET</httpMethod>
			<parameters>
				<parameter key="page" required="0">
					<description>Page number</description>
					<defaultValue>1</defaultValue>
					<allowedValues>
						<value>
							<description>Positive integer</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="rpp" required="0">
					<description>Results per page</description>
					<defaultValue>50</defaultValue>
					<allowedValues>
						<value>
							<description>Positive integer between 10 and 100</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="term" required="0">
					<description>Search term</description>
					<allowedValues>
						<value>
							<description>A term to search for within the comment body. Only
							comments containing this term will be retrieved.</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="collections" required="0">
					<description>Collections</description>
					<allowedValues>
						<value>
							<description>A valid collection alias, or multiple aliases
							separated by commas, to which to limit the search.</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="objects" required="0">
					<description>Objects</description>
					<allowedValues>
						<value>
							<description>A comma-separated list of object alias/pointer pairs,
							which themselves are separated by slashes; e.g "alias1/ptr1,alias2/ptr2".
							Only comments on these objects will be retrieved.</description>
						</value>
					</allowedValues>
				</parameter>
			</parameters>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="501 Not Implemented">
					<condition>Comments are not available from this service.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="objects/[alias]/[pointer]/comments">
			<uri>api/1/objects/[alias]/[pointer]/comments</uri>
			<title>List all comments on an object</title>
			<description>Lists all approved comments for an object.</description>
			<example format="xml" params="api/1/objects/hughes/87/comments" />
			<example format="json" params="api/1/objects/hughes/87/comments" />
			<httpMethod>GET</httpMethod>
			<parameters>
				<parameter key="page" required="0">
					<description>Page number</description>
					<defaultValue>1</defaultValue>
					<allowedValues>
						<value>
							<description>Positive integer</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="rpp" required="0">
					<description>Results per page</description>
					<defaultValue>50</defaultValue>
					<allowedValues>
						<value>
							<description>Integer between 10 and 100</description>
						</value>
					</allowedValues>
				</parameter>
			</parameters>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="404 Not Found">
					<condition>The object with specified [alias] and [pointer] either
					does not exist or is in an unpublished collection.</condition>
				</code>
				<code code="501 Not Implemented">
					<condition>Comments are not available from this service.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="collections/[alias]/[pointer]/comments">
			<uri>api/1/objects/[alias]/[pointer]/comments</uri>
			<title>Post a new comment</title>
			<description>Posts a new comment. Note: The client should check (using the <a href="#status">status</a> method)
			whether commenting is available before soliciting comments from the user.</description>
			<httpMethod>POST</httpMethod>
			<parameters>
				<parameter key="text" required="1">
					<description>Comment body</description>
				</parameter>
				<parameter key="name" required="1">
					<description>Name or nickname of the person posting the
					comment</description>
				</parameter>
				<parameter key="email" required="0">
					<description>Email address of the person posting the
					comment</description>
					<allowedValues>
						<value>
							<description>Valid e-mail address</description>
						</value>
					</allowedValues>
				</parameter>
			</parameters>
			<statusCodes>
				<code code="201 Created">
					<condition>The request succeeded and the comment has been posted. The URI of the comment is
						returned in the Location header.</condition>
				</code>
				<code code="202 Accepted">
					<condition>The request succeeded and the comment may or may not eventually be posted.</condition>
				</code>
				<code code="400 Bad Request">
					<condition>Invalid parameters were supplied.</condition>
				</code>
				<code code="403 Forbidden">
    				<condition>New comments are not currently being accepted.</condition>
				</code>
				<code code="404 Not Found">
					<condition>The object with specified [alias] and [pointer] either
					does not exist or is in an unpublished collection.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="comments/[id]">
			<uri>api/1/comments/[id]</uri>
			<title>View a specific comment</title>
			<description>Returns the comment with id [id]. Only approved comments
				are available.</description>
			<example format="xml" params="api/1/comments/1" />
			<example format="json" params="api/1/comments/1" />
			<httpMethod>GET</httpMethod>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="404 Not Found">
					<condition>The comment with id [id] either does not exist or
					has not been approved.</condition>
				</code>
				<code code="501 Not Implemented">
					<condition>Comments are not available from this service.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

	</category>

	<category name="Tags">

		<apiMethod id="tags">
			<uri>api/1/tags</uri>
			<title>Search for tags</title>
			<description>Returns a paginated list of all tags in the
				system matching the given criteria, grouped by frequency.</description>
			<example format="xml" params="api/1/comments" />
			<example format="json" params="api/1/comments" />
			<httpMethod>GET</httpMethod>
			<parameters>
				<parameter key="page" required="0">
					<description>Page number</description>
					<defaultValue>1</defaultValue>
					<allowedValues>
						<value>
							<description>Positive integer</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="rpp" required="0">
					<description>Results per page</description>
					<defaultValue>50</defaultValue>
					<allowedValues>
						<value>
							<description>Positive integer between 10 and 100</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="sort" required="0">
					<description>Results sorting method</description>
					<defaultValue>null</defaultValue>
					<allowedValues>
						<value value="(null)">
							<description>Sort descending by frequency</description>
						</value>
						<value value="random">
							<description>Randomize the results</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="term" required="0">
					<description>Search term</description>
					<allowedValues>
						<value>
							<description>A term to search for within the tag. Only
							tag containing this term will be retrieved.</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="collections" required="0">
					<description>Collections</description>
					<allowedValues>
						<value>
							<description>A valid collection alias, or multiple aliases
							separated by commas, to which to limit the search.</description>
						</value>
					</allowedValues>
				</parameter>
				<parameter key="objects" required="0">
					<description>Objects</description>
					<allowedValues>
						<value>
							<description>A comma-separated list of object alias/pointer pairs,
							which themselves are separated by slashes; e.g "alias1/ptr1,alias2/ptr2".
							Only tags on these objects will be retrieved.</description>
						</value>
					</allowedValues>
				</parameter>
			</parameters>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="501 Not Implemented">
					<condition>Tags are not available from this service.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="objects/[alias]/[pointer]/tags">
			<uri>api/1/objects/[alias]/[pointer]/tags</uri>
			<title>List all tags for an object</title>
			<description>Lists all approved tags for an object.</description>
			<example format="xml" params="api/1/objects/hughes/87/tags" />
			<example format="json" params="api/1/objects/hughes/87/tags" />
			<httpMethod>GET</httpMethod>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="404 Not Found">
					<condition>The object with specified [alias] and [pointer] either
					does not exist or is in an unpublished collection.</condition>
				</code>
				<code code="501 Not Implemented">
					<condition>Tags are not available from this service.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="objects/[alias]/[pointer]/tags">
			<uri>api/1/objects/[alias]/[pointer]/tags</uri>
			<title>Post a new tag</title>
			<description>Posts a new tag. Note: The client should check (using the <a href="#status">status</a> method)
			whether tagging is available before soliciting tags from the user.</description>
            <httpMethod>POST</httpMethod>
			<parameters>
				<parameter key="value" required="1">
					<description>Tag text</description>
					<allowedValues>
						<value>
							<description>String between 2 and 50 characters long</description>
						</value>
					</allowedValues>
				</parameter>
			</parameters>
			<statusCodes>
				<code code="201 Created">
					<condition>The request succeeded and the tag has been posted. The URI of the tag is
						returned in the Location header.</condition>
				</code>
				<code code="202 Accepted">
					<condition>The request succeeded and the tag may or may not eventually be posted.</condition>
				</code>
				<code code="400 Bad Request">
					<condition>Invalid parameters were supplied.</condition>
				</code>
				<code code="403 Forbidden">
					<condition>New tags are not currently being accepted.</condition>
				</code>
                <code code="404 Not Found">
					<condition>The object with specified [alias] and [pointer] either
					does not exist or is in an unpublished collection.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="tags/[id]">
			<uri>api/1/tags/[id]</uri>
			<title>View a specific tag</title>
			<description>Returns the tag with id [id]. Only approved tags
				are available.</description>
			<example format="xml" params="api/1/tags/4" />
			<example format="json" params="api/1/tags/4" />
			<httpMethod>GET</httpMethod>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="404 Not Found">
					<condition>The tag with id [id] either does not exist or
					has not been approved.</condition>
				</code>
				<code code="501 Not Implemented">
					<condition>Tags are not available from this service.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

	</category>

	<category name="Vocabulary">

		<apiMethod id="vocabulary/[alias]/[field]">
			<uri>api/1/vocabulary/[alias]/[field]</uri>
			<uri>api/1/vocabulary/[alias]/[field1,field2]</uri>
			<title>View vocabulary term frequencies within one or multiple fields of a collection</title>
			<description>Returns counts of vocabulary terms contained within field [field]
				of collection with alias [alias].</description>
			<example format="xml" params="api/1/vocabulary/hughes/subjec" />
			<example format="json" params="api/1/vocabulary/hughes/subjec" />
			<httpMethod>GET</httpMethod>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="404 Not Found">
					<condition>Either [field] is not available within collection
					[alias]; or the collection with specified [alias]
					does not exist or is unpublished.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

		<apiMethod id="vocabulary/suggest">
			<uri>api/1/vocabulary/suggest</uri>
			<title>Suggest vocabulary terms that match a given string (search suggestions)</title>
			<description>Returns the vocabulary contained within field [field]
				of collection with alias [alias].</description>
			<example format="xml" params="api/1/vocabulary/suggest" query="CISOFIELD1=title&amp;CISOROOT=/hughes&amp;CISOBOX1=mobots"/>
			<example format="json" params="api/1/vocabulary/suggest" query="CISOFIELD1=title&amp;CISOROOT=/hughes&amp;CISOBOX1=mobots"/>
			<httpMethod>GET</httpMethod>
			<parameters>
				<parameter key="CISOFIELD[1..*]" required="1">
					<description>Nick of the field in which to search; use multiple
					occurrences of this key (with unique numeric suffixes) to search
					multiple fields</description>
				</parameter>
				<parameter key="CISOROOT" required="1">
					<description>Alias of a specific collection in which to search</description>
				</parameter>
				<parameter key="CISOBOX1" required="1">
					<description>String for which suggestions should be provided</description>
				</parameter>
			</parameters>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
				<code code="400 Bad Request">
					<condition>Invalid parameters were supplied.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>

	</category>

    <category name="API Information">
		<apiMethod id="status">
			<uri>api/1/status</uri>
			<title>Retrieve API status information</title>
			<description>Returns various information about the current state of the API service.</description>
			<example format="xml" params="api/1/status" />
			<example format="json" params="api/1/status" />
			<httpMethod>GET</httpMethod>
			<statusCodes>
				<code code="200 OK">
					<condition>The request succeeded.</condition>
				</code>
			</statusCodes>
			<firstAPIVersion>1</firstAPIVersion>
		</apiMethod>
    </category>

</dmBridgeHTTPAPI>
