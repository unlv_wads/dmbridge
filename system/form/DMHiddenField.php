<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMHiddenField extends DMAbstractFormField implements DMFormField {

	/**
	 * @return HTML input element enclosed in a div tag
	 * @since 0.4
	 */
	public function getHTMLTag() {
		$dxml = new DMDOMDocument("1.0", "utf-8");
		$dxml->loadXML("<div/>");

		$class = "dmFormField";
		if (!$this->isValid() && $this->isUserData()) {
			$msg = $this->getValidationMessage();
			$dxml->documentElement->appendChild($dxml->createTextNode($msg));
			$dxml->documentElement->appendChild($dxml->createElement('br'));
			$class .= " dmInvalid";
		}
		$dxml->documentElement->setAttribute("class", $class);

		$input = $dxml->createElement("input");
		$input->setAttribute("type", "hidden");
		$input->setAttribute("name", $this->getHTMLName());
		$input->setAttribute("value", DMString::websafe($this->getValue()));
		$dxml->documentElement->appendChild($input);

		return $dxml->saveHTML($dxml->documentElement);
	}

}

