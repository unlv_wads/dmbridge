<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Encapsulates an HTML &lt;input type="radio"&gt; element.
 * 
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMRadioField extends DMAbstractFormField implements DMFormField {

	/** @var boolean */
	private $is_checked = false;


	/**
	 * @param name
	 * @since 0.4
	 */
	public function __construct($name) {
		parent::__construct($name);
		$this->setRequired(false);
	}

	/**
	 * @return boolean
	 */
	public function isChecked() {
		return $this->is_checked;
	}

	/**
	 * @param bool
	 * @since 0.4
	 */
	public function setChecked($bool) {
		$this->is_checked = (bool) $bool;
	}

	/**
	 * @return HTML input element
	 * @since 0.4
	 */
	public function getHTMLTag() {
		$dxml = new DMDOMDocument("1.0", "utf-8");
		$dxml->loadXML("<div/>");

		$class = "dmFormField";
		if (!$this->isValid() && $this->isUserData()) {
			$msg = $this->getValidationMessage();
			$dxml->documentElement->appendChild($dxml->createTextNode($msg));
			$class .= " dmInvalid";
		}
		$dxml->documentElement->setAttribute("class", $class);

		$input = $dxml->createElement("input");
		$input->setAttribute("type", "radio");
		$input->setAttribute("name", $this->getHTMLName());
		$input->setAttribute("value", DMString::websafe($this->getValue()));
		if ($this->isChecked()) {
			$input->setAttribute("checked", "checked");
		}
		if (!$this->isEnabled()) {
			$input->setAttribute("disabled", "disabled");
		}
		foreach ($this->getAttributes() as $k => $v) {
			$input->setAttribute($k, $v);
		}
		$dxml->documentElement->appendChild($input);
		$dxml->documentElement->appendChild(
			$dxml->createTextNode($this->getLabel())
		);

		return $dxml->saveHTML($dxml->documentElement);
	}

}

