<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
interface DMFormField {

	/**
	 * @param string name The HTML name of the field.
	 */
	function __construct($name);

	/**
	 * @param string k The HTML attribute key
	 * @param string v The HTML attribute value
	 */
	function addAttribute($k, $v);

	/**
	 * @return string The field as an HTML tag.
	 */
	function getHTMLTag();

	/**
	 * @return The HTML label of the field.
	 */
	function getLabel();

	/**
	 * @param string str The HTML label of the field.
	 */
	function setLabel($str);

	/**
	 * @param int int The maximum length of the value of the field.
	 */
	function setMaxLength($int);

	/**
	 * @param int int The minimum length of the value of the field.
	 */
	function setMinLength($int);

	/**
	 * @return string The HTML name of the field.
	 */
	function getName();

	/**
	 * @param bool bool Whether the field is required to be filled in.
	 */
	function setRequired($bool);

	/**
	 * @param int type DMFormFieldType constant
	 */
	function setType($type);

	/**
	 * @return boolean Whether the field's value is valid according to the
	 * current constraints on the field.
	 */
	function isValid();

	/**
	 * @return string The value of the field.
	 */
	function getValue();

	/**
	 * @param string value The value of the field.
	 */
	function setValue($value);

}

