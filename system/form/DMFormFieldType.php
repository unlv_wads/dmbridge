<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * An enum-like class.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMFormFieldType {

	const INTEGER = 0;
	const STRING = 1;
	const EMAIL = 2;
	const URL = 3;

}

