<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Encapsulates an HTML &lt;input type="text"&gt; field.
 * 
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMTextField extends DMAbstractFormField implements DMFormField {

	/** @var int */
	private $size;

	/**
	 * @return int
	 */
	public final function getSize() {
		return $this->size;
	}

	/**
	 * @param int
	 */
	public final function setSize($int) {
		$this->size = (int) $int;
	}

	/**
	 * @return HTML input element enclosed in a div tag
	 * @since 0.4
	 */
	public function getHTMLTag() {
		$dxml = new DMDOMDocument("1.0", "utf-8");
		$dxml->loadXML("<div/>");

		$class = "dmFormField";
		if (!$this->isValid() && $this->isUserData()) {
			$msg = $this->getValidationMessage();
			$dxml->documentElement->appendChild($dxml->createTextNode($msg));
			$dxml->documentElement->appendChild($dxml->createElement('br'));
			$class .= " dmInvalid";
		}
		$dxml->documentElement->setAttribute("class", $class);

		$input = $dxml->createElement("input");
		$input->setAttribute("type", "text");
		$input->setAttribute("name", $this->getHTMLName());
		$input->setAttribute("value", $this->getValue());

		if ($this->getSize()) {
			$input->setAttribute("size", $this->getSize());
		}
		if ($this->getMaxLength()) {
			$input->setAttribute("maxlength", $this->getMaxLength());
		}
		else if ($this->getMax()) {
			$input->setAttribute("maxlength", strlen($this->getMax()));
		}
		if (!$this->isEnabled()) {
			$input->setAttribute("disabled", "disabled");
		}
		foreach ($this->getAttributes() as $k => $v) {
			$input->setAttribute($k, $v);
		}
		$dxml->documentElement->appendChild($input);

		return $dxml->saveHTML($dxml->documentElement);
	}

}

