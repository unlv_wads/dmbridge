<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Encapsulates an HTML &lt;input type="password"&gt; element.
 * 
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMPasswordField extends DMAbstractFormField implements DMFormField {

	/**
	 * @return HTML input element
	 * @since 0.4
	 */
	public function getHTMLTag() {
		$dxml = new DMDOMDocument("1.0", "utf-8");
		$dxml->loadXML("<div/>");

		$class = "dmFormField";
		if (!$this->isValid() && $this->isUserData()) {
			$msg = $this->getValidationMessage();
			$dxml->documentElement->appendChild($dxml->createTextNode($msg));
			$class .= " dmInvalid";
		}
		$dxml->documentElement->setAttribute("class", $class);

		$input = $dxml->createElement("input");
		$input->setAttribute("type", "password");
		$input->setAttribute("name", $this->getHTMLName());
		if ($this->getMaxLength()) {
			$input->setAttribute("maxlength", $this->getMaxLength());
		}
		if (!$this->isEnabled()) {
			$input->setAttribute("disabled", "disabled");
		}
		foreach ($this->getAttributes() as $k => $v) {
			$input->setAttribute($k, $v);
		}
		$dxml->documentElement->appendChild($input);
		return $dxml->saveHTML($dxml->documentElement);
	}

}

