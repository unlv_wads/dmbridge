<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Encapsulates an HTML &lt;textarea&gt; element.
 * 
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMTextareaField extends DMAbstractFormField implements DMFormField {

	/** @var int */
	private $rows;
	/** @var int */
	private $columns;


	/**
	 * @return int
	 * @since 0.4
	 */
	public function getNumColumns() {
		return $this->columns;
	}

	/**
	 * @param int
	 * @since 0.4
	 */
	public function setNumColumns($int) {
		$this->columns = (int) $int;
	}

	/**
	 * @return int
	 * @since 0.4
	 */
	public function getNumRows() {
		return $this->rows;
	}

	/**
	 * @param int
	 * @since 0.4
	 */
	public function setNumRows($int) {
		$this->rows = (int) $int;
	}

	/**
	 * @return HTML textarea element enclosed in a div tag
	 * @since 0.4
	 */
	public function getHTMLTag() {
		$dxml = new DMDOMDocument("1.0", "utf-8");
		$dxml->loadXML("<div/>");

		$class = "dmFormField";
		if (!$this->isValid() && $this->isUserData()) {
			$msg = $this->getValidationMessage();
			$dxml->documentElement->appendChild($dxml->createTextNode($msg));
			$class .= " dmInvalid";
		}
		$dxml->documentElement->setAttribute("class", $class);

		$ta = $dxml->createElement("textarea");
		$ta->setAttribute("name", $this->getHTMLName());
		$ta->setAttribute("rows", $this->getNumRows());
		$ta->setAttribute("cols", $this->getNumColumns());
		$ta->nodeValue = DMString::websafe($this->getValue());
		if (!$this->isEnabled()) {
			$ta->setAttribute("disabled", "disabled");
		}
		foreach ($this->getAttributes() as $k => $v) {
			$ta->setAttribute($k, $v);
		}
		$dxml->documentElement->appendChild($ta);
		return $dxml->saveHTML($dxml->documentElement);
	}

}

