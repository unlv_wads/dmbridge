<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>Abstract superclass from which all template helper classes inherit.</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMAbstractTemplateHelper implements DMTemplateHelper {

	/**
	 * @var DMSession
	 */
	private $session;

	/**
	 * @var DMAbstractView subclass
	 */
	private $view;

	/**
	 * @param DMAbstractView view
	 */
	public function __construct(DMAbstractView $view, DMSession $session) {
		$this->setView($view);
		$this->setSession($session);
		$this->helperWillRender();
	}

	/**
	 * Default implementation that does nothing. See the documentation in
	 * DMTemplateHelper for more information.
	 */
	public function helperWillRender() {
	}

	/**
	 * @return DMSession
	 */
	public function getSession() {
		return $this->session;
	}

	/**
	 * @param DMUser user
	 */
	public function setSession(DMSession $session) {
		$this->session = $session;
	}

	/**
	 * @return DMAbstractView
	 */
	public function getView() {
		return $this->view;
	}

	/**
	 * @param DMAbstractView view
	 */
	public function setView(DMAbstractView $view) {
		$this->view = $view;
	}

}

