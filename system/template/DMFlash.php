<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Encapsulates a temporary status message that is generated in response to
 * some state-changing action on the server. It consists of a value (a text
 * string) and a status, which can be one of positive/success
 * negative/failure, or neutral/null (the default; see setStatus()).
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMFlash {

	private $value, $status;

	/**
	 * @param string value
	 * @param Boolean|null status
	 * @since 0.1
	 */
	public final function __construct($value, $status = null) {
		$this->setValue($value);
		$this->setStatus($status);
	}

	/**
	 * @return string
	 * @since 0.1
	 */
	public final function __toString() {
		return (string) $this->getValue();
	}

	/**
	 * @return True for positive/sucess, false for negative/failure, or null
	 * for neutral/null. You must use the === operator to check the return
	 * value.
	 * @since 0.1
	 */
	public final function getStatus() {
		return $this->status;
	}

	/**
	 * @param Boolean|null status True for sucess, false for failure, or null
	 * for neutral (will affect the CSS class given to the tag rendered by
	 * DMGenericTemplateHelper::drawFormattedFlash()).
	 * @since 0.1
	 */
	public final function setStatus($status) {
		if ($status === null) {
			$this->status = null;
			return;
		}
		$this->status = ($status);
	}

	/**
	 * @return string
	 * @since 0.1
	 */
	public final function getValue() {
		return $this->value;
	}

	/**
	 * @param string value
	 * @since 0.1
	 */
	public final function setValue($value) {
		$this->value = $value;
	}

}

