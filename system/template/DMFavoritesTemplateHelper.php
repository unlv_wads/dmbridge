<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>A class consisting of helper methods to assist in creating web page
 * templates. These methods are specific to favorites view and are neither
 * guaranteed nor expected to work in any other view.</p>
 *
 * <p>This class was called FavoritesDraw in dmBridge 1.x.</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMFavoritesTemplateHelper extends DMResultsTemplateHelper
implements DMTemplateHelper {

	/**
	 * @return DOMDocument
	 * @Override
	 */
	protected function getResultsForm() {
		$alias = $this->getView()->getCollection()->getAlias();
		$alias = ($alias == "/dmdefault") ? "" : $alias;
		$tpl_set = $this->getView()->getTemplate()->getTemplateSet();

		$dxml = new DMDOMDocument("1.0", "utf-8");
		$dxml->loadXML("<form/>");
		$dxml->documentElement->setAttribute("class", "dmResults");
		$table = $dxml->createElement("table");
		$dxml->documentElement->appendChild($table);

		$dxml->documentElement->setAttribute("action",
				DMInternalURI::getURIWithParams(
						sprintf("objects%s/favorites", $alias)));
		$dxml->documentElement->setAttribute("method", "post");

		$input_div = $dxml->createElement("div");

		if (count($this->getSession()->getAccessibleFavorites($tpl_set))) {
			$input = $dxml->createElement("input");
			$input->setAttribute("type", "hidden");
			$input->setAttribute("name", "params");
			$input->setAttribute("value",
					sprintf("objects%s/favorites", $alias));
			$input_div->appendChild($input);

			$input = $dxml->createElement("input");
			$input->setAttribute("type", "hidden");
			$input->setAttribute("name", "method");
			$input->setAttribute("value", "DELETE");
			$input_div->appendChild($input);

			$input = $dxml->createElement("input");
			$input->setAttribute("type", "submit");
			$input->setAttribute("class", "dmRemoveFavorites");
			$input->setAttribute("value", "Remove Checked From Favorites");
			$input_div->appendChild($input);
		}

		$dxml->documentElement->appendChild($input_div);
		return $dxml;
	}

}

