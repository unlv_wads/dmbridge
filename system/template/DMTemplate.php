<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Encapsulates an HTML template file within a template set.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMTemplate extends DMModel {

	/** @var string */
	private $absolute_pathname;
	/** @var string */
	private $relative_pathname;
	/** @var DMTemplateSet */
	private $template_set;

	/**
	 * @param DMTemplateSet ts 
	 * @param string relative_pathname The pathname of the template, relative
	 * to the root of the template set folder
	 */
	public function __construct(DMTemplateSet $ts, $relative_pathname) {
		$this->setTemplateSet($ts);
		$this->setRelativePathname($relative_pathname);
	}

	/**
	 * @return string The absolute pathname of the template
	 */
	public function getAbsolutePathname() {
		return $this->absolute_pathname;
	}

	/**
	 * @param string pathname
	 */
	public function setAbsolutePathname($pathname) {
		$this->absolute_pathname = $pathname;
	}

	/**
	 * @return string The pathname of the template, relative to the root of the
	 * template set folder
	 */
	public function getRelativePathname() {
		return $this->relative_pathname;
	}

	/**
	 * @param string pathname
	 */
	public function setRelativePathname($pathname) {
		$this->relative_pathname = $pathname;
		$this->absolute_pathname = $_SERVER['DOCUMENT_ROOT']
				. $this->getTemplateSet()->getPath() . $pathname;
	}

	/**
	 * @return DMTemplateSet
	 */
	public function getTemplateSet() {
		return $this->template_set;
	}

	/**
	 * @param DMTemplateSet ts 
	 */
	public function setTemplateSet(DMTemplateSet $ts) {
		$this->template_set = $ts;
	}

}
