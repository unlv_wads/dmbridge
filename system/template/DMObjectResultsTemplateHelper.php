<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>A class consisting of helper methods to assist in creating web page
 * templates. This class should be used in the context of a compound object
 * search results page. These methods are specific to object results view and
 * are neither guaranteed nor expected to work in any other view.</p>
 *
 * <p>This class was called ObjectResultsDraw in dmBridge 1.x.</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMObjectResultsTemplateHelper extends DMResultsTemplateHelper
implements DMTemplateHelper {

	/**
	 * @param string view Currently does nothing
	 * @return string HTML table element
	 * @since 0.2
	 * @Override
	 */
	public function getHtmlResults($view = "grid") {
		$uri = DMHTTPRequest::getCurrent()->getURI();
		$term = DMString::websafe(trim($uri->getQueryValue("term")));
		$idx = 1;
		$rows = "";
		foreach ($this->getView()->getQuery()->getSearchResults() as $obj) {
			$rows .= sprintf(
				'<tr%s>
					<td class="dmIndex">%d</td>
					<td class="dmThumbnail"><a href="%s"><img src="%s"></a></td>
					<td class="dmTitle"><a href="%s">%s</a></td>
					<td class="dmFullText">%s</td>
				</tr>',
				($idx & 1) ? ' class="odd"' : '',
				$idx,
				DMString::websafe($obj->getURI()),
				DMString::websafe($obj->getThumbURL()),
				DMString::websafe($obj->getURI()),
				DMString::websafe($obj->getMetadata('title')),
				DMString::highlight($term, DMString::websafe($obj->getFullText()))
			);
			$idx++;
		}
		return '<table class="dmGridResults" cellpadding="0" cellspacing="0"
			border="0">' . $rows . '</table>';
	}

}

