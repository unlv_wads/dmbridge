<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Interface to be implemented by all template helper classes, whether
 * built-in or provided by modules.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
interface DMTemplateHelper {

	/**
	 * Delegate method, called before any other methods in the helper class are
	 * called. Any scripts, stylesheets, etc. used by any of the helper methods
	 * should be added here.
	 */
	function helperWillRender();

	/**
	 * @param DMAbstractView view
	 */
	function setView(DMAbstractView $view);

}

