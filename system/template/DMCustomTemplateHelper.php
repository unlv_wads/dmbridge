<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>Abstract superclass from which all custom template helpers inherit.</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMCustomTemplateHelper extends DMAbstractTemplateHelper
implements DMTemplateHelper {

	/**
	 * Resolves calls to built-in template helper methods from custom ones.
	 *
	 * @param string method Method name
	 * @param array args Method arguments
	 * @throws DMNoSuchMethodException
	 */
	public function  __call($method, $args) {
		if (method_exists($this->getView()->getHelper(), $method)) {
			return call_user_func_array(
					array($this->getView()->getHelper(), $method), $args);
		}
		throw new DMNoSuchMethodException($method);
	}

}

