<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>A class consisting of helper methods to assist in creating web page
 * templates. The methods in this class are specific to system error view.</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMSystemErrorTemplateHelper extends DMGenericTemplateHelper
implements DMTemplateHelper {

	/**
	 * @return string HTML paragraph and, if debug mode is enabled, div of class
	 * dmExceptionDebug containing a stack trace
	 * @since 0.2
	 */
	public function getHtmlException() {
		$e = $this->getView()->getException();
		$msg = sprintf("<p>%s</p>", DMString::websafe($e->getMessage()));
		$debug_msg = "";
		if (DMConfigIni::getInstance()->getString("dmbridge.debug_mode")) {
			$debug_msg = sprintf('
				<div class="dmExceptionDebug">
					<dl>
						<dt>Type:</dt>
						<dd>%s</dd>

						<dt>Stack trace:</dt>
						<dd><pre>%s</pre></dd>

						<dt>dmBridge version:</dt>
						<dd>%s</dd>
					</dl>
					<p><strong>Debug mode is ON.</strong> To hide the above
					information from public view, turn it off in
					config.ini.</p>
				</div>',
				get_class($e),
				wordwrap(DMString::websafe($e->getTraceAsString()), 100),
				DMString::websafe(DMConfigXML::getInstance()->getVersion())
			);
		}
		return $msg . $debug_msg;
	}

}
