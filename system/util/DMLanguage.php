<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMLanguage {

	/** @var array Array of DMLanguages, lazy-loaded by getAll() */
	private static $languages = array();

	/** @var string */
	private $iso6391code;
	/** @var string */
	private $iso6392code;
	/** @var string */
	private $iso6393code;
	/** @var string */
	private $native_name;

	/**
	 * @return array of DMLanguage objects
	 */
	public static function getAll() {
		if (!count(self::$languages)) {
			$pathname = dirname(__FILE__) . "/../../includes/languages.xml";
			$langs_xml = new DOMDocument("1.0", "utf-8");
			$langs_xml->load($pathname);

			foreach ($langs_xml->getElementsByTagName("language") as $langNode) {
				$lang = new DMLanguage();
				$lang->setNativeName(
					$langNode->getElementsByTagName("nativeName")->item(0)->nodeValue);
				$lang->setISO6391Code(
						$langNode->getElementsByTagName("ISO639-1")->item(0)->nodeValue);
				$lang->setISO6392Code(
						$langNode->getElementsByTagName("ISO639-2")->item(0)->nodeValue);
				$lang->setISO6393Code(
						$langNode->getElementsByTagName("ISO639-3")->item(0)->nodeValue);
				self::$languages[] = $lang;
			}
		}
		return self::$languages;
	}

	/**
	 * @return string The native name of the language.
	 */
	public function __toString() {
		return $this->getNativeName();
	}

	/**
	 * @return string
	 */
	public function getISO6391Code() {
		return $this->iso6391code;
	}

	/**
	 * @param string code
	 */
	public function setISO6391Code($code) {
		$this->iso6391code = $code;
	}

	/**
	 * @return string
	 */
	public function getISO6392Code() {
		return $this->iso6392code;
	}

	/**
	 * @param string code
	 */
	public function setISO6392Code($code) {
		$this->iso6392code = $code;
	}

	/**
	 * @return string
	 */
	public function getISO6393Code() {
		return $this->iso6393code;
	}

	/**
	 * @param string code
	 */
	public function setISO6393Code($code) {
		$this->iso6393code = $code;
	}

	/**
	 * @return string
	 */
	public function getNativeName() {
		return $this->native_name;
	}

	/**
	 * @param string name
	 */
	public function setNativeName($name) {
		$this->native_name = $name;
	}

}
