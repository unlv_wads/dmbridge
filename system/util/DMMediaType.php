<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Encapsulates an RFC 2046 Internet media type, a.k.a. MIME type.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMMediaType {

	/** @var DOMDocument */
	private static $dxml;

	/** @var string */
	private $name;

	/** @var string */
	private $subtype;

	/** @var string */
	private $type;
	
	/**
	 * @return array
	 */
	public static function getKnownExtensions() {
		if (!self::$dxml) {
			self::$dxml = new DOMDocument("1.0", "utf-8");
			self::$dxml->load(
					dirname(__FILE__) . "/../../includes/MediaTypes.xml");
		}
		$dxp = new DOMXPath(self::$dxml);
		$result = $dxp->query("//extension");
		$extensions = array();
		foreach ($result as $node) {
			$extensions[] = $node->nodeValue;
		}
		return $extensions;
	}

	/**
	 * @param string ext
	 * @return DMMediaType A best guess at the media type for the given
	 * extension. If the guess is unsuccessful, returns null.
	 */
	public static function getTypeForExtension($ext) {
		if (!self::$dxml) {
			self::$dxml = new DOMDocument("1.0", "utf-8");
			self::$dxml->load(
					dirname(__FILE__) . "/../../includes/MediaTypes.xml");
		}
		$dxp = new DOMXPath(self::$dxml);
		$result = $dxp->query(
			sprintf("//type[extensions/extension = '%s'][1]", $ext));
		if ($result->length > 0) {
			$type = new DMMediaType(
				$result->item(0)->getAttribute("type"),
				$result->item(0)->getAttribute("subtype"));
			$type->setName(
				$result->item(0)->getElementsByTagName("name")->item(0)->nodeValue);
			return $type;
		}
		return null;
	}

	/**
	 * @param string str
	 * @return DMMediaType
	 */
	public static function getTypeForString($str) {
		$tmp = explode("/", $str);
		if (count($tmp) == 2) {
			return new DMMediaType($tmp[0], $tmp[1]);
		}
		return null;
	}

	/**
	 * @param string type
	 * @param string subtype
	 */
	public function __construct($type, $subtype) {
		$this->setType($type);
		$this->setSubtype($subtype);
	}

	/**
	 * @return string String representation of the media type.
	 */
	public function __toString() {
		return $this->getType() . "/" . $this->getSubtype();
	}

	/**
	 * @param object object
	 * @return boolean
	 */
	public function equals($object) {
		if (!$object instanceof DMMediaType) {
			return false;
		}
		return ($this->getType() == $object->getType()
				&& $this->getSubtype() == $object->getSubtype());
	}

	/**
	 * @return string
	 */
	public function getName() {
		if (!$this->name) {
			if (!self::$dxml) {
				self::$dxml = new DOMDocument("1.0", "utf-8");
				self::$dxml->load(
						dirname(__FILE__) . "/../../includes/MediaTypes.xml");
			}
			$dxp = new DOMXPath(self::$dxml);
			$result = $dxp->query(
				sprintf("//type[@type = '%s' and @subtype = '%s']/name",
						$this->getType(),
						$this->getSubtype()));
			if ($result->length > 0) {
				$this->name = $result->item(0)->nodeValue;
			}
		}
		return $this->name;
	}

	/**
	 * @param string name
	 */
	public function setName($name) {
		$this->name = (string) $name;
	}

	/**
	 * @return string
	 */
	public function getSubtype() {
		return $this->subtype;
	}

	/**
	 * @param string subtype
	 */
	public function setSubtype($subtype) {
		$this->subtype = $subtype;
	}

	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @param string type
	 */
	public function setType($type) {
		$this->type = $type;
	}

}
