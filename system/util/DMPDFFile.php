<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMPDFFile extends DMFile {

	/**
	 * Uses Ghostscript to get the page count of the PDF.
	 * 
	 * @return int
	 */
	public function getNumPages() {
		$cmd = sprintf('%s -dNODISPLAY -q -c "(%s) (r) file runpdfbegin pdfpagecount = quit"',
				DMConfigIni::getInstance()->getString("dmbridge.ghostscript_path"),
				$this->getPathname());
		$stdout = shell_exec($cmd);
		return (int) trim($stdout);
	}

}
