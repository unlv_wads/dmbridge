<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
final class DMShellCommand {

	/**
	 * @var string
	 */
	private $command;

	/**
	 * @param string command
	 */
	public function __construct($command) {
		$this->setCommand($command);
	}

	/**
	 * @param mixed obj
	 * @return boolean
	 */
	public function equals($obj) {
		if (!$obj instanceof DMShellCommand) {
			return false;
		}
		return ($obj->getCommand() == $this->getCommand());
	}

	/**
	 * @return string
	 */
	public function getCommand() {
		return $this->command;
	}

	/**
	 * @param string command
	 */
	public function setCommand($command) {
		$this->command = (string) $command;
	}

}
