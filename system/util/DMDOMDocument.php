<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Overrides the saveHTML() method to prevent it from outputting enclosing
 * <code>html</code> and <code>body</code> tags.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMDOMDocument extends DOMDocument {

	/**
	 * @Override
	 * @return string HTML string
	 */
	public function saveHTML() {
		return preg_replace('/^<!DOCTYPE.+?>/', '',
				str_replace(array('<html>', '</html>', '<body>', '</body>'),
						array('', '', '', ''),
						parent::saveHTML()));
	}

}
