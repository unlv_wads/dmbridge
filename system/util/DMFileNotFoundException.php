<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMFileNotFoundException extends DMException {

	/**
	 * @param string msg
	 * @param int code
	 */
	public function __construct($msg = null, $code = 5) {
		$msg = DMLocalizedString::getString("FILE_NOT_FOUND") . $msg;
		parent::__construct($msg, $code);
		$this->setHTTPResponseCode(404);
	}

}
