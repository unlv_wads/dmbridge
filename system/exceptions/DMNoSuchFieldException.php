<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMNoSuchFieldException extends DMException {

	/**
	 * @param string field
	 * @param int code
	 */
	public function __construct($field, $code = 5) {
		parent::__construct(
				sprintf(DMLocalizedString::getString("NO_SUCH_FIELD"), $field),
				$code);
		$this->setHTTPResponseCode(500);
	}

}
