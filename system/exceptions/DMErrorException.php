<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMErrorException extends ErrorException implements DMLoggable {

	/** @var boolean */
	private $has_been_logged = false;

	/** @var unsigned int */
	private $http_response_code = 500;

	/** @var DMDateTime */
	private $timestamp;


	/**
	 * @param string msg
	 * @param int code
	 * @since 0.1
	 */
	public function __construct($msg = null, $code = 3) {
		ini_set('xdebug.remote_mode', 'jit');
		parent::__construct($msg, $code);
		$this->setTimestamp(new DMDateTime());
	}

	/**
	 * @return bool
	 * @since 0.1
	 */
	public final function hasBeenLogged() {
		return $this->has_been_logged;
	}

	/**
	 * @param bool bool
	 */
	public final function setHasBeenLogged($bool) {
		$this->has_been_logged = ($bool);
	}

	/**
	 * @return int
	 */
	public final function getHTTPResponseCode() {
		return $this->http_response_code;
	}

	/**
	 * Wraps getMessage() in order to comply with DMLoggable.
	 *
	 * @return string
	 */
	public final function getLogMessage() {
		return $this->getMessage();
	}

	/**
	 * Wraps getCode().
	 * 
	 * @return int
	 */
	public final function getLogPriority() {
		return $this->getCode();
	}

	/**
	 * Wraps self::setCode().
	 * 
	 * @param int
	 */
	public function setLogPriority($int) {
		$this->setCode($int);
	}

	/**
	 * @return DMDateTime
	 * @since 0.1
	 */
	public final function getTimestamp() {
		return $this->timestamp;
	}

	private function setTimestamp(DMDateTime $dt) {
		$this->timestamp = $dt;
	}

}
