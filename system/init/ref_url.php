<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Replacement reference URL routing script for use with dmBridge.
 * Selectively routes collections to either CONTENTdm(R) or dmBridge templates
 * based on defined redirects in the dmBridge Control Panel. Unlike the
 * routing script provided with CONTENTdm(R), this script uses HTTP redirects
 * instead of JavaScript redirects.
 *
 * To use, copy /dm/u/index.php to /u/index.php on the CONTENTdm(R) server.
 * Do not modify this file.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */

include_once(dirname(__FILE__) . "/autoload.php");

if (empty($_SERVER['QUERY_STRING'])) {
	header("HTTP/1.1 400 Bad Request");
	die("No item specified.");
}

$tmp = explode(",", $_SERVER['QUERY_STRING']);
$alias = substr($tmp[0], 0, 80);
if (count($tmp) < 2) {
	header("HTTP/1.1 400 Bad Request");
	die("Bad request.");
}
$ptr = abs((int) substr($tmp[1], 0, 7));

// check for existence of the object
$col = $obj = null;
try {
	$col = DMCollectionFactory::getCollection(
			DMCollection::getSanitizedAlias($alias));
	$obj = DMObjectFactory::getObject($col, $ptr);
} catch (DMUnavailableModelException $e) {
	header("HTTP/1.1 404 Not Found");
	die("Invalid item.");
}

$uri = ($col->isRedirectingReferenceURLs())
		? $obj->getURI() : $obj->getCdmURI();

header("Location: " . $uri); // HTTP 302 redirect

die;
