<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMInvalidRouteException extends DMException {

	private $route;

	/**
	 * @param DMRoute route
	 * @param int code
	 * @since 0.1
	 */
	public function __construct(DMRoute $route = null, $code = 5) {
		parent::__construct(DMLocalizedString::getString("INVALID_ROUTE"), $code);
		$this->route = $route;
		$this->setHTTPResponseCode(404);
	}

}
