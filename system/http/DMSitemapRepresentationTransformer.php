<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMSitemapRepresentationTransformer extends DMXMLRepresentationTransformer {

	public function __construct() {
		parent::__construct();
	}

	/**
	 * @param DMObjectQuery q
	 * @since 0.9
	 * @Override
	 */
	public function transformResults(DMObjectQuery $q) {
		$this->dxml->loadXML("<urlset/>");
		$doc = $this->dxml->documentElement;
		$doc->setAttribute("xmlns",
				"http://www.sitemaps.org/schemas/sitemap/0.9");
		foreach ($q->getSearchResults() as $obj) {
			$url = $this->dxml->createElement("url");
			$loc = $this->dxml->createElement("loc",
					DMString::xmlentities($obj->getURI()));
			$url->appendChild($loc);
			$doc->appendChild($url);
		}
		return $this->output();
	}

	/**
	 * @Override
	 */
	protected function output() {
		return $this->dxml->saveXML($this->dxml->documentElement);
	}

}
