<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMJSONRepresentationTransformer
extends DMAbstractRepresentationTransformer {

	/**
	 * @var array Array of key-value pairs to be transformed into a JSON string
	 * by one of the transformX() methods.
	 */
	protected $json_array = array();

	/**
	 * @since 0.4
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * @return int
	 */
	public function getVersion() {
		return $this->version;
	}

}
