<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMJSONPRepresentationTransformerV1
extends DMJSONRepresentationTransformerV1
implements DMHTTPAPIRepresentationTransformer {

	private $callback = "dmBridgeRequest";


	public function __construct() {
		parent::__construct();
		$callback = DMHTTPRequest::getCurrent()->getURI()->getQueryValue("callback");
		if ($callback) {
			$this->callback = substr($callback, 0, 100);
			$this->callback = DMString::paranoid($this->callback, array("_", ".", ","));
		}
	}

	protected function output() {
		return sprintf("%s(%s)", $this->callback, parent::output());
	}

}
