<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMHTTPRepresentation {

	/** @var mixed */
	private $body;
	/** @var string character_set */
	private $character_set;
	/** @var array */
	private $form_data = array();
	/** @var DMMediaType */
	private $media_type;


	public function __construct() {
		$this->setCharacterSet("utf-8");
		$this->setMediaType(new DMMediaType("text", "plain"));
	}

	/**
	 * @return string String representation of the representation body.
	 */
	public function __toString() {
		return (string) $this->getBody();
	}

	/**
	 * Returns the body as its original type.
	 * 
	 * @return mixed
	 */
	public function getBody() {
		return $this->body;
	}

	/**
	 * Sets the body. To set a file on disk as the body, supply a DMFile. To
	 * set a shell command as the body, supply a DMShellCommand.
	 * 
	 * @param mixed body
	 */
	public function setBody($body) {
		$this->body = $body;
	}

	/**
	 * @return string
	 */
	public function getCharacterSet() {
		return $this->character_set;
	}

	/**
	 * @param string character_set
	 */
	public function setCharacterSet($character_set) {
		$this->character_set = $character_set;
	}

	/**
	 * Adds form data (encoding it automatically) and sets the media type to
	 * <code>application/x-www-form-urlencoded</code>. Multiple identical keys
	 * can be added.
	 *
	 * @param string key
	 * @param string value
	 * @see setFormData()
	 */
	public function addFormData($key, $value) {
		$this->setMediaType(
				new DMMediaType("application", "x-www-form-urlencoded"));
		$this->form_data[] = array(
			'key' => $key,
			'value' => $value
		);
	}

	/**
	 * @return Array of arrays with "key" and "value" keys
	 * @see getFormValue()
	 * @see getFormValues()
	 * @see getFormDataAsString()
	 */
	public function getFormData() {
		return $this->form_data;
	}

	/**
	 * Adds form data (encoding it automatically) and sets the media type to
	 * <code>application/x-www-form-urlencoded</code>. Existing keys matching
	 * $key will be deleted and then $key and $value will be added as a a new
	 * key-value pair.
	 *
	 * @param string key
	 * @param string value
	 * @see addFormData()
	 */
	public function setFormData($key, $value) {
		$count = count($this->form_data);
		for ($i = 0; $i < $count; $i++) {
			if ($this->form_data[$i]['key'] == $key) {
				unset($this->form_data[$i]);
			}
		}
		$this->addFormData($key, $value);
	}

	/**
	 * @return string URL-encoded form data
	 * @see getFormData()
	 * @see getFormValue()
	 * @see getFormValues()
	 */
	public function getFormDataAsString() {
		$str = array();
		foreach ($this->form_data as $data) {
			$str[] = urlencode($data['key']) . "=" . urlencode($data['value']);
		}
		return implode("&", $str);
	}

	/**
	 * @param string key
	 * @return string The first form value matching $key.
	 * @see getFormValues()
	 * @see getFormData()
	 * @see getFormDataAsString()
	 */
	public function getFormValue($key) {
		foreach ($this->form_data as $data) {
			if ($data['key'] == $key) {
				return $data['value'];
			}
		}
		return null;
	}

	/**
	 * @param string key
	 * @return array Array of all form values matching $key.
	 * @see getFormValue()
	 * @see getFormData()
	 * @see getFormDataAsString()
	 */
	public function getFormValues($key) {
		$values = array();
		foreach ($this->form_data as $data) {
			if ($data['key'] == $key) {
				$values[] = $data['value'];
			}
		}
		return $values;
	}

	/**
	 * Unsets all form data.
	 */
	public function unsetFormData() {
		$this->form_data = array();
	}

	/**
	 * @return DMMediaType
	 * @since 0.3
	 */
	public function getMediaType() {
		return $this->media_type;
	}

	/**
	 * @param DMMediaType type
	 * @since 0.3
	 */
	public function setMediaType(DMMediaType $type) {
		$this->media_type = $type;
	}

}
