<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Abstract class from which requests and responses inherit.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMHTTPMessage {

	/** @var array */
	protected $headers = array();
	/** @var string */
	protected $http_version = "1.1";
	/** @var DMHTTPRepresentation */
	protected $representation;


	/**
	 * @param string key
	 * @param string value
	 */
	public function addHeader($key, $value) {
		$this->headers[] = array(
			'key' => (string) $key,
			'value' => (string) $value
		);
	}

	/**
	 * @return array Array of arrays with 'key' and 'value' keys.
	 */
	public function getHeaders() {
		return $this->headers;
	}

	/**
	 * Erases all headers.
	 */
	public function unsetHeaders() {
		$this->headers = array();
	}

	/**
	 * @return string
	 */
	public function getHTTPVersion() {
		return $this->http_version;
	}

	/**
	 * @param string version
	 */
	public function setHTTPVersion($version) {
		$this->http_version = $version;
	}

	/**
	 * @return DMHTTPRepresentation
	 */
	public function getRepresentation() {
		return $this->representation;
	}

	/**
	 * @param DMHTTPRepresentation rep
	 */
	public function setRepresentation(DMHTTPRepresentation $rep) {
		$this->representation = $rep;
	}

}
