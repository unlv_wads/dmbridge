<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * An enum-like class encapsulating the available methods in HTTP 1.0/1.1.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMHTTPMethod {

	const GET = 1;
	const PUT = 2;
	const POST = 3;
	const DELETE = 4;
	const TRACE = 5;
	const HEAD = 6;
	const OPTIONS = 7;

}
