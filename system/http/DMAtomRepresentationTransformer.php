<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMAtomRepresentationTransformer extends DMFeedRepresentationTransformer {

	private function assembleHead(DMObject $obj = null) {
		$date_updated = new DMDateTime();
		// not ideal but should happen rarely
		$date_updated = $date_updated->asRFC3339();
		if ($obj) {
			$date_updated = $obj->getDateUpdated()->asRFC3339();
		}

		$this->dxml->loadXML('<feed/>');
		$root = $this->dxml->documentElement;
		$root->setAttribute('xmlns', 'http://www.w3.org/2005/Atom');
		$atomlink = $this->dxml->createElement("link");
		$atomlink->setAttribute("rel", "self");
		$atomlink->setAttribute("href", DMHTTPRequest::getCurrent()->getURI());
		$root->appendChild($atomlink);
		// id
		$root->appendChild($this->dxml->createElement("id",
				DMHTTPRequest::getCurrent()->getURI()->getAbsoluteHostURI()));
		// title
		$root->appendChild(
			$this->dxml->createElement(
				'title', DMString::xmlentities($this->getTitle())));
		// subtitle
		$root->appendChild(
			$this->dxml->createElement(
				'subtitle', DMString::xmlentities($this->getSubtitle())));
		// link
		$link = $root->appendChild($this->dxml->createElement('link'));
		$link->setAttribute('href', DMHTTPRequest::getCurrent()->getURI());
		// updated
		$root->appendChild($this->dxml->createElement('updated', $date_updated));
		// author
		$au = $root->appendChild($this->dxml->createElement('author'));
		// author/name
		$au->appendChild($this->dxml->createElement('name',
			DMString::xmlentities(
				DMConfigXML::getInstance()->getInstitutionName())));
		// author/email
		$au->appendChild($this->dxml->createElement('email',
			DMString::xmlentities(
				DMConfigXML::getInstance()->getFeedManagingEditorEmail())));
		// rights
		$root->appendChild($this->dxml->createElement('rights',
			DMString::xmlentities(
				DMConfigXML::getInstance()->getFeedCopyright())));
		// generator
		$root->appendChild($this->dxml->createElement('generator',
				'dmBridge ' . DMConfigXML::getInstance()->getVersion()));
	}


	/**
	 * @param DMBridge obj
	 * @return XML string
	 * @since 0.3
	 */
	public function transformObject(DMObject $obj) {
		$this->assembleHead($obj);
		$root = $this->dxml->documentElement;
		// entry
		$onode = $root->appendChild($this->dxml->createElement('entry'));
		// entry/title
		$onode->appendChild($this->dxml->createElement(
			'title', DMString::xmlentities($obj->getMetadata('title')->getValue())));
		// entry/author
		if ($obj->getMetadata('creato')) { // may be null
			$name = $onode->appendChild($this->dxml->createElement("author"));
			$name->appendChild($this->dxml->createElement(
				"name", DMString::xmlentities($obj->getMetadata('creato')->getValue())));
		}
		// entry/link
		$link = $onode->appendChild($this->dxml->createElement('link'));
		$link->setAttribute('href',
				$obj->getURI(DMBridgeComponent::TemplateEngine));
		// entry/id
		$onode->appendChild(
			$this->dxml->createElement('id', $obj->getReferenceURL()));
		// entry/published
		$onode->appendChild($this->dxml->createElement('published',
			$obj->getDateCreated()->asRFC3339()));
		// entry/updated
		$onode->appendChild($this->dxml->createElement('updated',
			$obj->getDateUpdated()->asRFC3339()));
		// entry/summary
		$desc = ($obj->getMetadata('descri') instanceof DMDCElement) ?
			DMString::xmlentities($obj->getMetadata('descri')->getValue()) : null;
		$desc = sprintf('<img src="%s" /><p>%s</p>',
			$obj->getThumbURL(), $desc);
		$cdata = $this->dxml->createCDATASection($desc);
		$node = $onode->appendChild(
			$this->dxml->createElement('summary'));
		$node->setAttribute('type', 'html');
		$node->appendChild($cdata);

		return $this->output();
	}

	/**
	 * @param DMObject obj
	 * @return XML string
	 * @since 0.3
	 */
	public function transformObjectComments(DMObject $obj) {
		$this->assembleHead($obj);
		$root = $this->dxml->documentElement;
		$cq = new DMCommentQuery(DMDataStoreFactory::getDataStore());
		$cq->setNumResultsPerPage(100);
		$cq->setApproved(1);
		$cq->addObject($obj);
		$comments = $cq->getSearchResults();

		foreach ($comments as $comment) {
			// entry
			$onode = $root->appendChild($this->dxml->createElement('entry'));
			// entry/title
			$onode->appendChild($this->dxml->createElement(
					"title", $comment->getName() . " "
					. $comment->getTimestamp()->format("Y-m-d")));
			// entry/author
			if ($obj->getMetadata('creato')) { // may be null
				$name = $onode->appendChild($this->dxml->createElement("author"));
				$name->appendChild($this->dxml->createElement(
					"name", DMString::xmlentities($comment->getName())));
			}
			// entry/link
			$link = $onode->appendChild($this->dxml->createElement('link'));
			$link->setAttribute('href',
					$obj->getURI(DMBridgeComponent::TemplateEngine));
			// entry/id
			$onode->appendChild(
				$this->dxml->createElement('id', $obj->getReferenceURL()));
			// entry/published
			$onode->appendChild($this->dxml->createElement('published',
				$comment->getTimestamp()->asRFC3339()));
			// entry/updated
			$onode->appendChild($this->dxml->createElement('updated',
				$comment->getTimestamp()->asRFC3339()));
			// entry/content
			$onode->appendChild($this->dxml->createElement('content',
				$comment->getValue()));
			$root->appendChild($onode);
		}

		if (count($comments)) {
			// set "updated" element to the date of the latest comment
			$root->getElementsByTagName("updated")->item(0)->nodeValue
					= $comments[0]->getTimestamp()->asRFC3339();
		}

		return $this->output();
	}

	/**
	 * @param DMObjectQuery query
	 * @return XML string
	 * @since 0.3
	 */
	public function transformResults(DMObjectQuery $query) {
		$objects = $query->getSearchResults();
		$this->assembleHead(count($objects) ? $objects[0] : null);

		$root = $this->dxml->documentElement;
		foreach ($objects as $o) {
			$onode = $root->appendChild($this->dxml->createElement('entry'));
			$onode->appendChild($this->dxml->createElement(
				'title', DMString::xmlentities($o->getMetadata('title')->getValue())));
			$link = $onode->appendChild($this->dxml->createElement('link'));
			$link->setAttribute('href',
					$o->getURI(DMBridgeComponent::TemplateEngine, "html"));
			$onode->appendChild(
				$this->dxml->createElement('id', $o->getReferenceURL()));
			$onode->appendChild($this->dxml->createElement('updated',
				$o->getDateUpdated()->asRFC3339()));
			$desc = ($o->getMetadata('descri') instanceof DMDCElement) ?
				DMString::xmlentities($o->getMetadata('descri')->getValue())
						: null;
			$desc = sprintf('<img src="%s" /><p>%s</p>',
				$o->getThumbURL(), $desc);
			$cdata = $this->dxml->createCDATASection($desc);
			$node = $onode->appendChild(
				$this->dxml->createElement('summary'));
			$node->setAttribute('type', 'html');
			$node->appendChild($cdata);
		}
		return $this->output();
	}

}

