<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMFeedRepresentationTransformer
extends DMAbstractRepresentationTransformer {

	/** @var string */
	private $subtitle, $title;
	/** @var DOMDocument */
	protected $dxml;


	/**
	 * @since 0.1
	 */
	public function __construct() {
		parent::__construct();
		$this->dxml = new DOMDocument();
		$this->dxml->preserveWhiteSpace = false;
	}

	/**
	 * @return XML string
	 * @since 0.2
	 */
	public function output() {
		$this->dxml->formatOutput = true;
		return $this->dxml->saveXML();
	}

	/**
	 * @return string
	 * @since 0.1
	 */
	protected final function getSubtitle() {
		if (!$this->subtitle) {
			$this->subtitle = DMConfigXML::getInstance()->getFeedSubtitle();
		}
		return $this->subtitle;
	}

	/**
	 * @return string
	 * @since 0.1
	 */
	protected final function getTitle() {
		if (!$this->title) {
			$this->title = DMConfigXML::getInstance()->getFeedTitle();
		}
		return $this->title;
	}

}

