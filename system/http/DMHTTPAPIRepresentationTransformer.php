<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Interface to be implemented by all "representation transformers," which are
 * classes that transform input into a given HTTP representation.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
interface DMHTTPAPIRepresentationTransformer {

	/**
	 * @return An empty representation; for example, a pair of empty XML tags,
	 * or perhaps no content at all.
	 */
	function nullResponse();

	/**
	 * @param DMCollection col The DMCollection to transform
	 * @return string HTTP representation
	 */
	function transformCollection(DMCollection $col);

	/**
	 * @param array collections Array of DMCollections
	 * @return string HTTP representation
	 */
	function transformCollections(array $collections);

	/**
	 * @param DMComment comment
	 * @return string HTTP representation
	 */
	function transformComment(DMComment $comment);

	/**
	 * @param array comments Array of DMComments
	 * @param int page
	 * @param int rpp Results per page
	 * @param int total
	 * @return string HTTP representation
	 */
	function transformComments(array $comments, $page, $rpp, $total);

	/**
	 * @param Exception e
	 * @return string HTTP representation
	 */
	function transformException(Exception $e);

	/**
	 * @param DMObject obj The DMObject to transform
	 * @return string HTTP representation
	 */
	function transformObject(DMObject $obj);

	/**
	 * @param DMObject obj The object whose comments should be transformed
	 * @param int page
	 * @param int rpp
	 * @return string HTTP representation
	 */
	function transformObjectComments(DMObject $obj, $page, $rpp);

	/**
	 * @param DMObject obj The object whose tags should be transformed
	 * @return string HTTP representation
	 */
	function transformObjectRating(DMObject $obj);

	/**
	 * @param DMObject obj
	 * @return string HTTP representation
	 */
	function transformObjectTags(DMObject $obj);

	/**
	 * @param DMObjectQuery query A DMObjectQuery initialized to retrieve a
	 * result set
	 * @return string HTTP representation
	 */
	function transformResults(DMObjectQuery $query);

	/**
	 * @return HTTP API status information
	 */
	function transformStatus();

	/**
	 * @param DMTag tag The DMTag to be transformed
	 * @return string HTTP representation
	 */
	function transformTag(DMTag $tag);

	/**
	 * @param array tags An array of DMTags to be transformed
	 * @param int page
	 * @param int rpp Results per page
	 * @param int total
	 * @return string HTTP representation
	 */
	function transformTagCounts(array $tags, $page, $rpp, $total);

	/**
	 * @param array freqs Hash of term/count pairs
	 * @return string HTTP representation
	 */
	function transformVocabularyFrequencies(array $freqs);

	/**
	 * @param array words Indexed array of strings
	 * @return string HTTP representation
	 */
	function transformVocabularySuggestions(array $words);

}
