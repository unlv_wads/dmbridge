<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Represents a user comment on an object. A DMComment object must be
 * attached to a DMObject in order to be saved to the data store; this is
 * done with DMObject::addComment().
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMComment extends DMAbstractSocialEntity implements DMURIAddressable,
		DMSocialEntity {

	/** @var string */
	private $author;
	/** @var string */
	private $author_email;

	/**
	 * @return string The email address supplied by the author of the comment.
	 * @since 0.1
	 */
	public function getEmail() {
		return $this->author_email;
	}

	/**
	 * @param string email
	 * @throws DMInvalidEmailException
	 */
	public function setEmail($email) {
		if ($email && !DMMailer::isValidEmail($email)) {
			throw new DMInvalidEmailException($email);
		}
		$this->author_email = $email;
	}

	/**
	 * @return string The name supplied by the poster of the comment
	 */
	public function getName() {
		return $this->author;
	}

	/**
	 * @param string str
	 * @throws DMIllegalArgumentException
	 */
	public function setName($str) {
		if (strlen($str) < 2) {
			throw new DMIllegalArgumentException(
				sprintf(
					DMLocalizedString::getString("STRING_TOO_SHORT"),
					"Name", 2));
		} else if (strlen($str) > 50) {
			throw new DMIllegalArgumentException(
				sprintf(
					DMLocalizedString::getString("STRING_TOO_LONG"),
					"Name", 50));
		}
		$this->author = $str;
	}

	/**
	 * @return DMInternalURI The URI of the comment in the HTTP API.
	 */
	public function getURI() {
		return DMInternalURI::getResourceURI(
				DMBridgeComponent::HTTPAPI,
				"comments/" . $this->getID());
	}

}
