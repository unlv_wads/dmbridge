<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Represents a tag that a user has given to an object.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMTag extends DMAbstractSocialEntity implements DMURIAddressable,
		DMSocialEntity {

	const MIN_LENGTH = 1;
	const MAX_LENGTH = 50;

	/**
	 * @return DMInternalURI
	 * @since 0.1
	 */
	public function getURI() {
		return DMInternalURI::getResourceURI(
				DMBridgeComponent::HTTPAPI,
				'tags/' . $this->getID());
	}

	/**
	 * @param string str
	 * @throws DMIllegalArgumentException
	 * @Override
	 */
	public function setValue($str) {
		if (strlen($str) < 1) {
			throw new DMIllegalArgumentException(
				sprintf(DMLocalizedString::getString('TAG_VALUE_TOO_SHORT'),
					self::MIN_LENGTH));
		} else if (strlen($str) > self::MAX_LENGTH) {
			throw new DMIllegalArgumentException(
				sprintf(DMLocalizedString::getString('TAG_VALUE_TOO_LONG'),
					self::MAX_LENGTH));
		}
		$this->value = (string) $str;
	}

}
