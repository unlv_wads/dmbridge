<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
interface DMSocialEntity {

	/**
	 * @return boolean
	 */
	function isApproved();

	/**
	 * @param boolean bool
	 */
	function setApproved($bool);

	/**
	 * @return int
	 */
	function getID();
	
	/**
	 * @param int id
	 */
	function setID($id);

	/**
	 * @return DMDateTime
	 */
	function getTimestamp();

	/**
	 * @param DMDateTime ts
	 */
	function setTimestamp(DMDateTime $ts);

	/**
	 * @return DMObject
	 */
	function getObject();

	/**
	 * @param DMObject obj
	 */
	function setObject(DMObject $obj);

	/**
	 * @return string
	 */
	function getValue();

	/**
	 * @param string str
	 * @throws DMIllegalArgumentException
	 */
	function setValue($str);
}
