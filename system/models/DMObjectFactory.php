<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMObjectFactory {

	/**
	 * @var Used to cache instantiated objects in memory. Structured as
	 * "aliaspointer => object".
	 */
	private static $object_cache = array();

	/**
	 * @param DMCollection c
	 * @param int ptr
	 * @return DMObject A shared DMObject instance.
	 * @throws DMUnavailableModelException
	 * @since 0.9
	 */
	public static function getObject(DMCollection $c, $ptr) {
		$key = $c->getAlias() . $ptr;

		foreach (self::$object_cache as $cache_key => $cache_value) {
			if ($cache_key == $key) {
				return $cache_value;
			}
		}

		if (!DMObject::exists($c, $ptr)) {
			throw new DMUnavailableModelException(
					DMLocalizedString::getString("INVALID_OBJECT"));
		}
		$obj = new DMObject($c, $ptr, DMDataStoreFactory::getDataStore(),
				DMConfigIni::getInstance());
		self::$object_cache[$key] = $obj;
		return self::$object_cache[$key];
	}

}
