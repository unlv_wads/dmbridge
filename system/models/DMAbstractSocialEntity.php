<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Base class from which social data classes inherit.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMAbstractSocialEntity extends DMModel {

	/** @var int */
	private $id;
	/** @var bool */
	private $is_approved = false;
	/** @var DMObject */
	private $object;
	/** @var DMDateTime */
	private $timestamp;
	/** @var string */
	protected $value;

	/**
	 * If supplied and is a valid comment ID from the data store,
	 * automatically populates the entity's properties from the data store.
	 * Otherwise instantiates an empty entity object.
	 *
	 * @throws DMUnavailableModelException
	 * @since 0.1
	 */
	public function __construct() {
		$this->setTimestamp(new DMDateTime());
	}

	/**
	 * @return string
	 */
	public function __toString() {
		return (string) $this->getValue();
	}

	/**
	 * @return bool
	 */
	public final function isApproved() {
		return $this->is_approved;
	}

	/**
	 * @param Boolean bool
	 */
	public final function setApproved($bool) {
		$this->is_approved = ($bool);
	}

	/**
	 * @return int
	 */
	public final function getID() {
		return $this->id;
	}

	/**
	 * @param int id
	 */
	public final function setID($id) {
		$this->id = (int) $id;
	}

	/**
	 * @return DMObject The DMObject with which the DMComment is associated, or
	 * null if there is no working data store
	 */
	public function getObject() {
		return $this->object;
	}

	/**
	 * Associates a DMObject with the entity <strong>in code only</strong>. To
	 * persist the relationship, use DMObject::addComment(), DMObject::addTag(),
	 * etc.
	 *
	 * @param DMObject object
	 */
	public function setObject(DMObject $object) {
		$this->object = $object;
	}

	/**
	 * @return dmDateTime The date/time that the entity was posted.
	 */
	public final function getTimestamp() {
		return $this->timestamp;
	}

	/**
	 * @param dmDateTime datetime
	 */
	public final function setTimestamp(DMDateTime $datetime) {
		$this->timestamp = $datetime;
	}

	/**
	 * @return string The tag's text
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * @param string str
	 * @throws DMUserInputException
	 */
	public function setValue($str) {
		if (strlen($str) < 2) {
			throw new DMIllegalArgumentException(
				sprintf(DMLocalizedString::getString('STRING_TOO_SHORT'), 'Value', 2));
		}
		$this->value = $str;
	}

}
