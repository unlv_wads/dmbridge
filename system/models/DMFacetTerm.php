<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Encapsulates a CONTENTdm(R) facet term, which has field nick, label, and
 * value properties.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMFacetTerm {

	/** @var int */
	private $count = 0;
	/** @var DMDCElement */
	private $field;
	/** @var DMURI */
	private $uri;

	/**
	 * @param DMDCElement field
	 * @param int count
	 */
	public function __construct(DMDCElement $field, $count = 0) {
		$this->setField($field);
		$this->setCount($count);
	}

	/**
	 * @return The name of the facet field. Equivalent to calling
	 * getField()->getName(). The return value of this method
	 * should not be relied upon.
	 * @see getName()
	 */
	public function __toString() {
		return (string) $this->getField()->getName();
	}

	/**
	 * @return int
	 */
	public function getCount() {
		return $this->count;
	}

	/**
	 * @param int int
	 */
	public function setCount($int) {
		$this->count = abs((int) $int);
	}

	/**
	 * @return DMDCElement
	 */
	public function getField() {
		return $this->field;
	}

	/**
	 * @param DMDCElement f
	 */
	public function setField(DMDCElement $f) {
		$this->field = $f;
	}

	/**
	 * @return DMURI
	 */
	public function getURI() {
		return $this->uri;
	}

	/**
	 * @param DMURI uri
	 */
	public function setURI(DMURI $uri) {
		$this->uri = $uri;
	}

}
