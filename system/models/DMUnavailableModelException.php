<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMUnavailableModelException extends DMException {

	/**
	 * @param string msg
	 * @param int code
	 */
	public function __construct($msg, $code = 4) {
		parent::__construct($msg, $code);
		$this->setHTTPResponseCode(404);
	}

}
