<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMCollectionFactory {

	/**
	 * @var Used to cache instantiated objects in memory. Structured as
	 * "alias => object".
	 */
	private static $object_cache = array();


	/**
	 * @param string alias
	 * @return DMCollection A shared DMCollection instance.
	 * @throws DMUnavailableModelException
	 * @since 2.0
	 */
	public static function getCollection($alias) {
		foreach (self::$object_cache as $cache_key => $cache_value) {
			if ($cache_key == $alias) {
				return $cache_value;
			}
		}

		if ($alias != "/dmdefault" && !DMCollection::exists($alias)) {
			throw new DMUnavailableModelException(
					DMLocalizedString::getString("INVALID_COLLECTION"));
		}
		self::$object_cache[$alias] = new DMCollection($alias);
		return self::$object_cache[$alias];
	}

}
