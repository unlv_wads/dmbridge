<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Encapsulates a query for objects from a user's favorites.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMFavoriteQuery extends DMAbstractQuery implements DMQuery {

	/** @var DMFavoriteQuery */
	private static $current;

	/** @var DMSession */
	private $session;

	/** @var DMTemplateSet */
	private $template_set;

	/**
	 * @return DMObjectQuery
	 * @deprecated
	 */
	public static function getCurrent() {
		return self::$current;
	}

	/**
	 * @param DMObjectQuery oq
	 * @deprecated
	 */
	public static function setCurrent(DMFavoriteQuery $oq) {
		self::$current = $oq;
	}

	/**
	 * @param DMSession session
	 * @param DMTemplateSet ts
	 */
	public function __construct(DMSession $session, DMTemplateSet $ts = null) {
		$this->session = $session;
		$this->template_set = $ts;
		$this->setNumResults(count($this->session->getAllFavorites()));
	}

	/**
	 * @return Array of DMObjects
	 * @throws DMInternalErrorException
	 */
	public function getSearchResults() {
		$results = ($this->template_set)
				? $this->session->getAccessibleFavorites($this->template_set)
				: $this->session->getAllFavorites();
		return array_slice($results,
				($this->getPage() - 1) * $this->getNumResultsPerPage(),
				$this->getNumResultsPerPage());
	}

}
