<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Encapsulates a query for comments.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMCommentQuery extends DMAbstractSocialDataQuery
implements DMDataStoreQuery, DMURIAddressable {

	/**
	 * @return Array of DMComment objects
	 */
	public function getSearchResults() {
		$this->setNumResults(
				$this->getDataStore()->getCommentsForQuery($this, true));
		return $this->getDataStore()->getCommentsForQuery($this, false);
	}

	/**
	 * @return DMInternalURI URI of the query
	 */
	public function getURI() {
		$terms = $this->getPredicates();
		$collections = $this->getCollections();
		$objects = $this->getObjects();
		$query = array();
		if (count($terms)) {
			$query['term'] = $terms[0];
		}
		if (count($collections)) {
			$aliases = array();
			foreach ($collections as $col) {
				$aliases[] = $col->getAlias();
			}
			$query['collections'] = implode(",", $aliases);
		} else if (count($objects)) {
			$encoded_objects = array();
			foreach ($objects as $obj) {
				$encoded_objects[] = $obj->getCollection()->getAlias() . "/"
						. $obj->getPtr();
			}
			$query['objects'] = implode(",", $encoded_objects);
		}
		return DMInternalURI::getURIWithParams("comments/search", $query);
	}

}
