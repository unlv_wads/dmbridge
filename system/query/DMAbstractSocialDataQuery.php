<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * An abstract query from which all social data queries inherit.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMAbstractSocialDataQuery extends DMAbstractDataStoreQuery {

	/** @var int 1 = approved; 0 = approved and unapproved; -1 unapproved */
	private $approved = 1;

	/**
	 * @return int 1 = only approved; 0 = approved and unapproved; -1 only
	 * unapproved
	 */
	public final function getApproved() {
		return $this->approved;
	}

	/**
	 * @param int int 1 = only approved; 0 = approved and unapproved; -1 only
	 * unapproved
	 */
	public final function setApproved($int) {
		$this->approved = (int) $int;
	}

}
