<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Abstract superclass inherited by queries that query a data store.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMAbstractDataStoreQuery extends DMAbstractQuery {

	/** @var DMDataStore */
	private $data_store;


	/**
	 * @param DMDataStore data_store The data store against which the query is
	 * to be exeucted.
	 */
	public function __construct(DMDataStore $data_store) {
		$this->data_store = $data_store;
	}

	/**
	 * @return DMDataStore
	 */
	public final function getDataStore() {
		return $this->data_store;
	}

	/**
	 * @param DMDataStore ddata_store
	 */
	public final function setDataStore(DMDataStore $data_store) {
		$this->data_store = $data_store;
	}
}
