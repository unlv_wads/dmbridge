<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * An object encapsulation of the dmQuery() $searchstring
 * parameter (see the CONTENTdm(R) API documentation).
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMQueryPredicate {

	private static $valid_modes = array('all', 'exact', 'any', 'none');

	/** @var DMDCElement */
	private $field;
	/** @var boolean */
	private $is_date = false;
	/** @var boolean */
	private $is_proximity = false;
	/** @var string */
	private $mode;
	/** @var string */
	private $string;


	/**
	 * @return array of valid modes, as strings
	 */
	public static final function getValidModes() {
		return self::$valid_modes;
	}

	/**
	 * @return string
	 */
	public function __toString() {
		return (string) $this->getString();
	}

	/**
	 * @return Boolean
	 * @since 0.1
	 */
	public final function isBrowse() {
		if ($this->getField() == "any" && $this->getMode() == "any"
				&& strlen($this->getString()) < 1) {
			return true;
		}
		return false;
	}

	/**
	 * @return Boolean
	 * @since 0.3
	 */
	public final function isDate() {
		return $this->is_date;
	}

	/**
	 * @param Boolean bool
	 * @since 0.3
	 */
	public final function setDate($bool) {
		$this->is_date = ($bool);
	}

	/**
	 * @return DMDCElement
	 */
	public final function getField() {
		return $this->field;
	}

	/**
	 * @param string field
	 */
	public final function setField(DMDCElement $field) {
		$this->field = $field;
	}

	/**
	 * @return string
	 */
	public final function getMode() {
		return $this->mode;
	}

	/**
	 * @param string mode
	 * @throws DMIllegalArgumentException
	 * @since 0.1
	 */
	public final function setMode($mode) {
		if (!in_array($mode, self::getValidModes())) {
			throw new DMIllegalArgumentException(
				sprintf(DMLocalizedString::getString("INVALID_MODE"),
					implode(', ', self::getValidModes())));
		}
		$this->mode = $mode;
	}

	/**
	 * @return Boolean
	 * @since 0.3
	 */
	public final function isProximity() {
		return $this->is_proximity;
	}

	/**
	 * @param Boolean bool
	 * @since 0.3
	 */
	public final function setProximity($bool) {
		$this->is_proximity = ($bool);
	}

	/**
	 * @return string
	 * @since 0.1
	 */
	public final function getString() {
		return $this->string;
	}

	/**
	 * @param string string
	 * @since 0.1
	 */
	public final function setString($string) {
		$this->string = trim($string);
	}

	/**
	 * @return Boolean
	 * @since 0.1
	 */
	public final function isValid() {
		if (!in_array($this->getMode(), self::getValidModes())) {
			return false;
		}
		if (!$this->getField()) {
			return false;
		}
		if (strlen($this->getString()) < 1) {
			return false;
		}
		return true;
	}

}
