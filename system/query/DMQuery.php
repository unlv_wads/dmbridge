<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * A generic query interface that all queries should implement. A query may
 * query CONTENTdm(R), the dmBridge data store, or something else. An abstract
 * implementation of this interface is provided in DMAbstractQuery.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
interface DMQuery {

	/**
	 * @return int The number of pages of results. Available only after the
	 * query has finished executing.
	 */
	function getNumPages();

	/**
	 * @return int The number of results.
	 */
	function getNumResults();

	/**
	 * @param int int The number of results to be retrieved per page.
	 */
	function setNumResultsPerPage($int);

	/**
	 * @return int
	 */
	function getPage();

	/**
	 * @param int page
	 */
	function setPage($page);

	/**
	 * Executes the query and returns the result set.
	 *
	 * @return array Array of unspecified types
	 */
	function getSearchResults();

	/**
	 * @return int The starting index of the current page of the result set,
	 * derived from the current page and the number of results per page.
	 */
	function getStart();

}
