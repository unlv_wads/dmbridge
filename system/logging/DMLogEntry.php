<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Generic log entry class.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */

class DMLogEntry implements DMLoggable {

	/** @var string */
	private $message;
	/** @var int */
	private $priority;

	/**
	 * @param string message
	 * @param int priority
	 */
	public function __construct($message = null, $priority = 3) {
		$this->setLogMessage($message);
		$this->setLogPriority($priority);
	}

	/**
	 * @return string
	 */
	public function getLogMessage() {
		return $this->message;
	}

	/**
	 * @param string message 
	 */
	public function setLogMessage($message) {
		$this->message = $message;
	}

	/**
	 * @return int
	 */
	public function getLogPriority() {
		return $this->priority;
	}

	/**
	 * @param int priority 
	 */
	public function setLogPriority($priority) {
		$this->priority = abs($priority);
	}
}
