<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * An interface to be implemented by anything that is going to get logged to
 * the system log.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */

interface DMLoggable {

	/**
	 * @return string
	 */
	function getLogMessage();

	/**
	 * @return int
	 */
	function getLogPriority();

}
