<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMEmailMessage implements DMMailable {

	/**
	 * @var string
	 */
	private $from_name, $from_email, $recipient_email, $subject, $body;

	/**
	 * @return string
	 * @since 0.1
	 */
	public final function getEmailBody() {
		return $this->body;
	}

	/**
	 * @param string text
	 * @since 0.1
	 */
	public final function setEmailBody($text) {
		$this->body = $text;
	}

	/**
	 * @return string
	 * @since 0.1
	 */
	public final function getEmailFromEmail() {
		return $this->from_email;
	}

	/**
	 * @param string email
	 * @since 0.1
	 */
	public final function setEmailFromEmail($email) {
		$this->from_email = $email;
	}

	/**
	 * @return string
	 * @since 0.1
	 */
	public final function getEmailFromName() {
		return $this->from_name;
	}

	/**
	 * @param string str
	 * @since 0.1
	 */
	public final function setEmailFromName($str) {
		$this->from_name = $str;
	}

	/**
	 * @return string
	 * @since 0.1
	 */
	public final function getEmailRecipientEmail() {
		return $this->recipient_email;
	}

	/**
	 * @param string email
	 * @since 0.1
	 */
	public final function setEmailRecipientEmail($email) {
		$this->recipient_email = $email;
	}

	/**
	 * @return string
	 * @since 0.1
	 */
	public final function getEmailSenderName() {
		return $this->sender_name;
	}

	/**
	 * @param string name
	 * @since 0.1
	 */
	public final function setEmailSenderName($name) {
		if (!empty($name)) {
			$this->sender_name = DMString::websafe(substr($name, 0, 100));
		}
		else {
			$this->sender_name = "Anonymous";
		}
	}

	/**
	 * @return string
	 * @since 0.1
	 */
	public final function getEmailSubject() {
		return $this->subject;
	}

	/**
	 * @param string str
	 * @since 0.1
	 */
	public final function setEmailSubject($str) {
		$this->subject = $str;
	}

}
