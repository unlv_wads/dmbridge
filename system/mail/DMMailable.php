<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
interface DMMailable {

	/**
	 * @return string Unformatted email body
	 */
	function getEmailBody();

	/**
	 * @return string Email of the sender
	 */
	function getEmailFromEmail();

	/**
	 * @return string
	 */
	function getEmailFromName();

	/**
	 * @return string Email of the recipient
	 */
	function getEmailRecipientEmail();

	/**
	 * @return string Email subject line
	 */
	function getEmailSubject();

}
