<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMInvalidEmailException extends DMIllegalArgumentException {

	/**
	 * @param string email
	 * @param int code
	 * @since 0.1
	 */
	public function __construct($email, $code = 3) {
		$msg = sprintf(DMLocalizedString::getString('INVALID_EMAIL_ADDRESS'), $email);
		parent::__construct($msg, $code);
		$this->setHTTPResponseCode(400);
	}

}
