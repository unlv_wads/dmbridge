<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMCPTagController extends DMCPAdminController {

	public function configure() {
		$this->preFlightCheck();

		$form = new DMCPTagSettingsForm();
		$this->setForm($form);

		if (DMHTTPRequest::getCurrent()->getMethod() == DMHTTPMethod::POST) {
			$this->handleForm();
		}

		$ds = DMDataStoreFactory::getDataStore();
		$tq = new DMTagQuery($ds);
		$tq->setApproved(-1);
		$tq->getSearchResultsAsObjects();

		$this->tpl_vars['enable']
				= $form->getFieldByName("enable")->getHTMLTag();
		$this->tpl_vars['moderation']
				= $form->getFieldByName("moderation")->getHTMLTag();
		$this->tpl_vars['num_unapproved_tags'] = $tq->getNumResults();

		$this->renderTemplate("/templates/tag/edit.html.php");
		die;
	}

	public function moderate() {
		$this->preFlightCheck();

		if (DMHTTPRequest::getCurrent()->getMethod() == DMHTTPMethod::POST) {
			$rep = DMHTTPRequest::getCurrent()->getRepresentation();
			if (is_array($rep->getFormValue("id"))) {
				$tags = array();
				foreach ($rep->getFormValue("id") as $id) {
					$tags[] = DMDataStoreFactory::getDataStore()->loadTag($id);
				}
				try {
					switch (strtolower(
							substr($rep->getFormValue("moderate"), 0, 6))) {
					case "approv":
						$this->approveTags($tags);
						break;
					case "reject":
						$this->deleteTags($tags);
						break;
					}
				} catch (DMDataStoreException $e) {
					DMHTTPRequest::getCurrent()->getSession()->setFlash(
							new DMFlash($e->getMessage(), false));
					return;
				}
				DMHTTPRequest::reload();
				die;
			} else {
				DMHTTPRequest::getCurrent()->getSession()->setFlash(
						new DMFlash(
								DMLocalizedString::getString("NO_TAG_SELECTED"),
								false));
			}
		}

		$this->tpl_vars['moderation_is_enabled']
				= DMConfigXML::getInstance()->isTagModerationEnabled();
		try {
			$tq = new DMTagQuery(DMDataStoreFactory::getDataStore());
			$tq->setApproved(-1);
			$this->tpl_vars['unapproved_tags'] = $tq->getSearchResultsAsObjects();
		} catch (DMException $e) {
			$this->tpl_vars['unapproved_tags'] = array();
		}

		$this->renderTemplate("/templates/tag/moderate.html.php");
		die;
	}

	public function statistics() {
		$this->preFlightCheck();

		$this->tpl_vars['all_collections'] = DMCollection::getAuthorized();
		$this->tpl_vars['most_tagged'] = DMDataStoreFactory::getDataStore()
				->getObjectsWithMostTags(20);
		$this->tpl_vars['num_with_approved_tags']
				= DMDataStoreFactory::getDataStore()
				->getNumObjectsWithApprovedTags();

		$this->renderTemplate("/templates/tag/statistics.html.php");
		die;
	}

	public function update() {
		$this->preFlightCheck();
		$this->index();
		$this->handleForm();
	}

	private function approveTag(DMTag $t) {
		$t->setApproved(true);
		$result = DMDataStoreFactory::getDataStore()->saveTag($t);
		if ($result) {
			DMHTTPRequest::getCurrent()->getSession()->setFlash(
					new DMFlash(DMLocalizedString::getString("TAG_APPROVED"),
							true));
		}
	}

	private function approveTags() {
		$rep = DMHTTPRequest::getCurrent()->getRepresentation();
		if (!is_array($rep->getFormValue("id"))) {
			return false;
		}
		foreach ($rep->getFormValue("id") as $id) {
			if ($id <> round($id) or $id < 0) {
				continue;
			}
			$t = DMDataStoreFactory::getDataStore()->loadTag((int) $id);
			$this->approveTag($t);
		}
	}

	private function deleteTag(DMTag $t) {
		$result = DMDataStoreFactory::getDataStore()->deleteTag($t);
		if ($result) {
			DMHTTPRequest::getCurrent()->getSession()->setFlash(new DMFlash(
					DMLocalizedString::getString("TAG_DELETED"), true));
		}
	}

	private function deleteTags() {
		$rep = DMHTTPRequest::getCurrent()->getRepresentation();
		if (!is_array($rep->getFormValue("id"))) {
			return false;
		}
		foreach ($rep->getFormValue("id") as $id) {
			if ($id <> round($id) or $id < 0) {
				continue;
			}
			$t = DMDataStoreFactory::getDataStore()->loadTag((int) $id);
			$this->deleteTag($t);
		}
	}

	public function index() {
		$this->preFlightCheck();

		$ds = DMDataStoreFactory::getDataStore();

		if (DMHTTPRequest::getCurrent()->getMethod() == DMHTTPMethod::POST) {
			$flash = new DMFlash(
					DMLocalizedString::getString("NO_TAG_SELECTED"), false);
			$rep = DMHTTPRequest::getCurrent()->getRepresentation();
			if (is_array($rep->getFormValue("values"))) {
				foreach ($rep->getFormValue("values") as $value) {
					DMDataStoreFactory::getDataStore()->deleteAllTagsWithValue($value);
				}
				$flash = new DMFlash(
					DMLocalizedString::getString("TAG_DELETED"), true);
			}
			DMHTTPRequest::getCurrent()->getSession()->setFlash($flash);
			DMHTTPRequest::reload();
			die;
		}

		$tq = new DMTagQuery($ds);
		$tq->setSortByFrequency(true);
		// @todo add pagination
		$tq->setNumResultsPerPage(100000);
		$this->tpl_vars['tags'] = $tq->getSearchResultsAsCounts();
		$this->tpl_vars['num_approved_tags']
				= $tq->getNumResults();

		$this->renderTemplate("/templates/tag/index.html.php");
		die;
	}

}
