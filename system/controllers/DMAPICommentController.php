<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMAPICommentController extends DMAbstractController {

	/**
	 * Outputs all approved comments.
	 *
	 * @param string aliasptr Comma-separated alias/pointer pair
	 */
	public function index() {
		$uri = DMHTTPRequest::getCurrent()->getURI();
		$page = abs((int) substr($uri->getQueryValue("page"), 0, 7));
		$page = ($page < 1) ? 1 : $page;

		$rpp = abs((int) substr($uri->getQueryValue("rpp"), 0, 4));
		$rpp = ($rpp > 100) ? 100 : $rpp;
		$rpp = ($rpp == 1) ? 50 : ($rpp < 10) ? 10 : $rpp;

		$term = substr($uri->getQueryValue("term"), 0, 100);
		$collections = explode(",", $uri->getQueryValue("collections"));
		$objects = explode(",", $uri->getQueryValue("objects"));

		$ds = DMDataStoreFactory::getDataStore();
		$cq = new DMCommentQuery($ds);
		$cq->setApproved(1);
		$cq->setPage($page);
		$cq->setNumResultsPerPage($rpp);
		if ($term) {
			$predicate = new DMQueryPredicate();
			$predicate->setString($term);
			$cq->setPredicates(array($predicate));
		}
		foreach ($collections as $alias) {
			if ($alias) {
				$col = DMCollectionFactory::getCollection(
						DMCollection::getSanitizedAlias($alias));
				$cq->addCollection($col);
			}
		}
		foreach ($objects as $aliasptr) {
			$split = explode("/", $aliasptr);
			if (count($split) == 3 && $split[1] && $split[2]) {
				$col = DMCollectionFactory::getCollection(
						DMCollection::getSanitizedAlias($split[1]));
				$obj = DMObjectFactory::getObject($col, (int) $split[2]);
				$cq->addObject($obj);
			}
		}

		$results = $cq->getSearchResults();
		$total = $cq->getNumResults();

		$response = new DMHTTPResponse();
		$rep = DMHTTPResponseFactory::getRepresentation();
		$trans = DMHTTPResponseFactory::getTransformer();
		$rep->setBody($trans->transformComments($results, $page, $rpp, $total));
		$response->setRepresentation($rep);
		$response->send();
		die;
	}

	/**
	 * @param int id
	 */
	public function view($id) {
		$ds = DMDataStoreFactory::getDataStore();
		$comment = $ds->loadComment($id);
		if (!$comment->isApproved()) {
			throw new DMUnavailableModelException("Access denied.", 401);
		}

		$response = new DMHTTPResponse();
		$rep = DMHTTPResponseFactory::getRepresentation();
		$trans = DMHTTPResponseFactory::getTransformer();
		$rep->setBody($trans->transformComment($comment));
		$response->setRepresentation($rep);
		$response->send();
		die;
	}

}
