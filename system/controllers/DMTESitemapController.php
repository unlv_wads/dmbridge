<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Controller for XML Sitemaps in the template engine.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMTESitemapController extends DMAbstractController {

	/**
	 * Serves a sitewide sitemap index, which links to every results view
	 * browse page. Separate sitemaps on each of these pages, in turn, link to
	 * individual object records. There is no way to "loop through" all objects
	 * here without it being super slow, which means tiered sitemap indices
	 * will be necessary.
	 *
	 * @return void
	 */
	public function view() {
		$dxml = new DOMDocument("1.0", "utf-8");
		$dxml->loadXML("<urlset/>");
		$doc = $dxml->documentElement;
		$doc->setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");

		foreach (DMCollection::getAuthorized() as $col) {
			$num_pages = ceil($col->getNumObjects() / 50);
			for ($i = 1; $i <= $num_pages; $i++) {
				// build xml elements
				$node = $dxml->createElement("url");
				$loc = $dxml->createElement("loc",
						DMInternalURI::getURIWithParams(
							"objects" . $col->getAlias(),
							array("page" => $i),
							"sitemap"));
				$node->appendChild($loc);
				$doc->appendChild($node);
			}
		}
		header("Content-Type: application/xml");
		die($dxml->saveXML());
	}

}
