<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Controller for the search form in the template engine. Querying and search
 * results gathering is done in DMTEObjectController.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMTESearchController extends DMAbstractController {

	public function index($alias) {
		$alias = (strlen($alias)) ? $alias : "/dmdefault";
		$col = DMCollectionFactory::getCollection(
				DMCollection::getSanitizedAlias($alias));

		$ts = $this->getTemplateSet();
		if (!$ts) {
			throw new DMUnavailableModelException(
					DMLocalizedString::getString("INVALID_TPL_SET"));
		}
		$allowed_collections = $ts->getAuthorizedCollections();
		$col = (count($allowed_collections) == 1)
			? $allowed_collections[0] : $col;

		// make sure we aren't viewing something we shouldn't be
		try {
			if (!$this->getTemplateSet()->isAuthorizedToViewCollection($col)) {
				throw new DMSecurityException(
					DMLocalizedString::getString("ACCESS_DENIED_TO_COL"));
			}
		} catch (DMSecurityException $e) {
			require_once($this->getTemplateSet()->getErrorTemplate()
					->getAbsolutePathname());
			die;
		}

		// entering this view should erase any browse/search results we had
		// generated previously
		$this->getSession()->unsetQuery();
		$this->getSession()->unsetResultsView();

		// Render compound or single template depending on what the object is
		$path = "/templates/search/index.html.php";
		$tpl = $this->getTemplateSet()->getTemplateAtPathname($path);
		global $view;
		$view = new DMSearchView($tpl, $this->getSession(),
				$col);
	}

}

