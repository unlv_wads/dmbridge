<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMCPFeedController extends DMCPAdminController {

	public function index() {
		$this->preFlightCheck();

		$form = new DMCPFeedSettingsForm();
		$this->setForm($form);

		if (DMHTTPRequest::getCurrent()->getMethod() == DMHTTPMethod::POST) {
			$this->handleForm();
		}

		$this->tpl_vars['copyright']
				= $form->getFieldByName('copyright')->getHTMLTag();
		$this->tpl_vars['title']
				= $form->getFieldByName('title')->getHTMLTag();
		$this->tpl_vars['subtitle']
				= $form->getFieldByName('subtitle')->getHTMLTag();
		$this->tpl_vars['language']
				= $form->getFieldByName('language')->getHTMLTag();
		$this->tpl_vars['managing_editor_name']
				= $form->getFieldByName('managing_editor_name')->getHTMLTag();
		$this->tpl_vars['managing_editor_email']
				= $form->getFieldByName('managing_editor_email')->getHTMLTag();
		$this->tpl_vars['webmaster_email']
				= $form->getFieldByName('webmaster_email')->getHTMLTag();
		$this->tpl_vars['webmaster_name']
				= $form->getFieldByName('webmaster_name')->getHTMLTag();

		$this->renderTemplate("/templates/feed/edit.html.php");
		die;
	}


	public function update() {
		$this->preFlightCheck();
		$this->index();
		$this->handleForm();
	}

}

