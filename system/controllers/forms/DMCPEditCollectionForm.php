<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMCPEditCollectionForm extends DMAbstractForm {

	const NUM_RESULTS_FIELDS = 6;

	/** @var DMCollection */
	private $collection;

	/**
	 * @param DMCollection col
	 */
	public function __construct(DMCollection $col) {
		$this->collection = $col;
		parent::__construct();
	}

	protected function init() {
		// overview page URL
		$f = new DMTextField("overview_page_url");
		$f->addAttribute("style", "width:100%");
		$f->setType(DMFormFieldType::URL);
		$f->setRequired(false);
		$f->setLabel("Overview Page URL");
		$this->addField($f);

		// image URL
		$f = new DMTextField("image_url_512");
		$f->addAttribute("style", "width:100%");
		$f->setType(DMFormFieldType::URL);
		$f->setRequired(false);
		$f->setLabel("Image URL");
		$this->addField($f);

		// overview page
		$f = new DMTextareaField("description");
		$f->addAttribute("style", "width:100%");
		$f->setNumRows(5);
		$f->setMaxLength(2000);
		$f->setRequired(false);
		$f->setLabel("Description");
		$this->addField($f);

		// template set
		$f = new DMSelectField("template_set");
		$f->setLabel("Template set");
		foreach (DMConfigXML::getInstance()->getAllTemplateSets() as $ts) {
			$f->addOption($ts->getID(), $ts->getName());
		}
		$this->addField($f);

		// inheritances
		$f = new DMRadioField("inherit_template_set");
		$f->setValue(1);
		$f->setLabel("Inherit from Default Collection");
		$this->addField($f);

		$f = new DMRadioField("inherit_template_set");
		$f->setValue(0);
		$f->setLabel("Override");
		$this->addField($f);

		$f = new DMRadioField("inherit_object_view");
		$f->setValue(1);
		$f->setLabel("Inherit from Default Collection");
		$this->addField($f);

		$f = new DMRadioField("inherit_object_view");
		$f->setValue(0);
		$f->setLabel("Override");
		$this->addField($f);

		$f = new DMRadioField("inherit_results_view");
		$f->setValue(1);
		$f->setLabel("Inherit from Default Collection");
		$this->addField($f);

		$f = new DMRadioField("inherit_results_view");
		$f->setValue(0);
		$f->setLabel("Override");
		$this->addField($f);

		$f = new DMRadioField("inherit_search_view");
		$f->setValue(1);
		$f->setLabel("Inherit from Default Collection");
		$this->addField($f);

		$f = new DMRadioField("inherit_search_view");
		$f->setValue(0);
		$f->setLabel("Override");
		$this->addField($f);

		// object viewers
		$i = 0;
		foreach ($this->collection->getViewerDefinitions() as $def) {
			$f = new DMTextField("mime");
			$f->setMaxLength(100);
			$f->addAttribute("style", "width:100%");
			$this->addField($f);

			$f = new DMSelectField("class");
			foreach (DMObjectViewerDelegateManager::getAllViewerDelegateClassNames() as $class) {
				$f->addOption($class,
						DMObjectViewerDelegateManager::getNameForClass($class));
			}
			$this->addField($f);

			$f = new DMTextField("width");
			$f->setSize(5);
			$f->setMaxLength(6);
			$this->addField($f);

			$f = new DMTextField("height");
			$f->setSize(4);
			$f->setMaxLength(6);
			$this->addField($f);

			$f = new DMTextField("max_file_size_k");
			$f->setSize(7);
			$f->setMaxLength(7);
			$this->addField($f);

			$i++;
		}

		// results view
		foreach ($this->collection->getFields() as $field) {
			$f = new DMHiddenField("field_nicks");
			$f->setValue($field->getNick());
			$this->addField($f);

			$f = new DMCheckboxField("field_nicks_sortable");
			$f->setValue($field->getNick());
			$this->addField($f);

			$f = new DMRadioField("field_nicks_default_sort");
			$f->setValue($field->getNick());
			$this->addField($f);

			$f = new DMTextField("field_nicks_max_words");
			$f->setType(DMFormFieldType::INTEGER);
			$f->setSize(2);
			$f->setRequired(false);
			$f->setMin(0);
			$f->setMax(2000);
			$this->addField($f);
		}

		// grid view
		for ($i = 0; $i < self::NUM_RESULTS_FIELDS; $i++) {
			$f = new DMSelectField("grid_fields");
			$f->setRequired(false);
			$f->addOption("", DMLocalizedString::getString("DO_NOT_DISPLAY"));
			$f->addOption("index", "Index");
			$f->addOption("thumb", "Thumbnail");
			foreach ($this->collection->getFields() as $field) {
				$f->addOption($field->getNick(), $field->getName());
			}
			$this->addField($f);
		}

		// facets
		foreach ($this->collection->getFields() as $field) {
			$f = new DMCheckboxField("facet_fields");
			$f->setValue($field->getNick());
			$f->setLabel($field->getName());
			$this->addField($f);
		}

		// search view
		$f = new DMTextField("begin_year");
		$f->setLabel("Begin Year");
		$f->setSize(4);
		$f->setMaxLength(4);
		$f->setMin(-10000);
		$f->setMax(date('Y'));
		$f->setType(DMFormFieldType::INTEGER);
		$this->addField($f);

		$f = new DMTextField("end_year");
		$f->setLabel("End Year");
		$f->setSize(4);
		$f->setMaxLength(4);
		$f->setMin(-10000);
		$f->setMax(date('Y'));
		$f->setType(DMFormFieldType::INTEGER);
		$this->addField($f);
	}

	protected function loadFromDataStore() {
		// overview page URL
		$this->getFieldByName("overview_page_url")->setValue(
				(string) $this->collection->getOverviewURI());
		// image URL
		$this->getFieldByName("image_url_512")->setValue(
				(string) $this->collection->getImage512URI());
		// description
		$this->getFieldByName("description")->setValue(
				(string) $this->collection->getDescription());

		// template set
		if ($this->collection->getTemplateSet()) {
			$this->getFieldByName("template_set")->setValue(
					$this->collection->getTemplateSet()->getID());
		}

		// inheritances
		$this->checkRadioWithNameAndValue("inherit_template_set",
				$this->collection->isUsingDefaultTemplateSet() ? 1 : 0);
		$this->checkRadioWithNameAndValue("inherit_object_view",
				$this->collection->hasCustomObjectViewPrefs() ? 0 : 1);
		$this->checkRadioWithNameAndValue("inherit_results_view",
				$this->collection->hasCustomResultsViewPrefs() ? 0 : 1);
		$this->checkRadioWithNameAndValue("inherit_search_view",
				$this->collection->hasCustomSearchViewPrefs() ? 0 : 1);

		// object viewers
		$mime_fields = $this->getFieldsByName("mime");
		$class_fields = $this->getFieldsByName("class");
		$width_fields = $this->getFieldsByName("width");
		$height_fields = $this->getFieldsByName("height");
		$size_fields = $this->getFieldsByName("max_file_size_k");
		$defs = $this->collection->getViewerDefinitions();
		$count = count($defs);
		for ($i = 0; $i < $count; $i++) {
			$mime_fields[$i]->setValue((string) $defs[$i]->getMediaType());
			$class_fields[$i]->setValue($defs[$i]->getClass());
			$width_fields[$i]->setValue($defs[$i]->getWidth());
			$height_fields[$i]->setValue($defs[$i]->getHeight());
			$size_fields[$i]->setValue($defs[$i]->getMaxFileSize());
		}

		// results view
		$col_fields = $this->collection->getFields();
		$sortable_fields = $this->getFieldsByName("field_nicks_sortable");
		$default_sort_fields = $this->getFieldsByName("field_nicks_default_sort");
		$max_words_fields = $this->getFieldsByName("field_nicks_max_words");
		$count = count($col_fields);
		for ($i = 0; $i < $count; $i++) {
			$sortable_fields[$i]->setChecked($col_fields[$i]->isSortable());
			if ($col_fields[$i]->isDefaultSort()) {
				foreach ($default_sort_fields as $f) {
					if ($f->getValue() == $col_fields[$i]->getNick()) {
						$f->setChecked(true);
						break;
					}
				}
			}
			$max_words_fields[$i]->setValue($col_fields[$i]->getMaxWords());
		}

		// grid view
		$col_fields = $this->collection->getGridViewFields();
		$i = 0;
		foreach ($this->getFieldsByName("grid_fields") as $field) {
			if ($i < count($col_fields)) {
				$field->setValue($col_fields[$i]->getNick());
			}
			$i++;
		}

		// facets
		foreach ($this->getFieldsByName("facet_fields") as $field) {
			foreach ($this->collection->getFacets() as $facet) {
				if ($field->getValue() == $facet->getField()->getNick()) {
					$field->setChecked(true);
					break;
				}
			}
		}

		// search view
		$f = $this->getFieldByName("begin_year");
		$f->setValue($this->collection->getDateSearchBeginYear());

		$f = $this->getFieldByName("end_year");
		$f->setValue($this->collection->getDateSearchEndYear());
	}

	public function doAction() {
		// overview page URL
		$uri = $this->getFieldByName("overview_page_url")->getValue();
		if (strlen($uri)) {
			$this->collection->setOverviewURI(new DMURI($uri));
		}
		// image URL
		$uri = $this->getFieldByName("image_url_512")->getValue();
		if (strlen($uri)) {
			$this->collection->setImage512URI(new DMURI($uri));
		}
		// description
		$desc = $this->getFieldByName("description")->getValue();
		$this->collection->setDescription($desc);

		// template set
		$this->collection->setTemplateSetID(
				$this->getFieldByName("template_set")->getValue());

		// inheritances
		$inherit_template_set = $this->getFieldByName("inherit_template_set")->isChecked();
		$this->collection->setUsingDefaultTemplateSet($inherit_template_set);
		$inherit_object_view = $this->getFieldByName("inherit_object_view")->isChecked();
		$this->collection->setHasCustomObjectViewPrefs(!$inherit_object_view);
		$inherit_results_view = $this->getFieldByName("inherit_results_view")->isChecked();
		$this->collection->setHasCustomResultsViewPrefs(!$inherit_results_view);
		$inherit_search_view = $this->getFieldByName("inherit_search_view")->isChecked();
		$this->collection->setHasCustomSearchViewPrefs(!$inherit_search_view);

		// object viewers
		$this->collection->unsetViewerDefinitions();
		if (!$inherit_object_view) {
			$mime_fields = $this->getFieldsByName("mime");
			$class_fields = $this->getFieldsByName("class");
			$width_fields = $this->getFieldsByName("width");
			$height_fields = $this->getFieldsByName("height");
			$size_fields = $this->getFieldsByName("max_file_size_k");
			$count = count($mime_fields);
			for ($i = 0; $i < $count; $i++) {
				$def = new DMObjectViewerDefinition(
						DMMediaType::getTypeForString($mime_fields[$i]->getValue()));
				$def->setClass($class_fields[$i]->getValue());
				$def->setWidth($width_fields[$i]->getValue());
				$def->setHeight($height_fields[$i]->getValue());
				$def->setMaxFileSize($size_fields[$i]->getValue());
				$this->collection->addViewerDefinition($def);
			}
		}

		// results view
		$this->collection->unsetFields();
		$this->collection->unsetGridViewFields();
		$this->collection->unsetFacets();
		if (!$inherit_results_view) {
			$field_nicks = $this->getFieldsByName("field_nicks");
			$sortables = $this->getFieldsByName("field_nicks_sortable");
			$default_sorts = $this->getFieldsByName("field_nicks_default_sort");
			$max_words = $this->getFieldsByName("field_nicks_max_words");
			$count = count($sortables);
			for ($i = 0; $i < $count; $i++) {
				$field = new DMDCElement($field_nicks[$i]->getValue());
				$field->setSortable($sortables[$i]->isChecked());
				$field->setDefaultSort($default_sorts[$i]->isChecked());
				$field->setMaxWords($max_words[$i]->getValue());
				$this->collection->addField($field);
			}

			// grid view
			foreach ($this->getFieldsByName("grid_fields") as $field) {
				if ($field->getValue()) {
					$elem = new DMDCElement($field->getValue());
					$this->collection->addGridViewField($elem);
				}
			}

			// facets
			foreach ($this->getFieldsByName("facet_fields") as $field) {
				if ($field->isChecked()) {
					$term = new DMFacetTerm(new DMDCElement($field->getValue()));
					$this->collection->addFacet($term);
				}
			}
		}

		// search view
		if ($inherit_search_view) {
			$this->collection->unsetDateSearchRange();
		} else {
			$this->collection->setDateSearchRange(
					$this->getFieldByName("begin_year")->getValue(),
					$this->getFieldByName("end_year")->getValue());
		}

		DMConfigXML::getInstance()->saveCollection($this->collection);
	}

}
