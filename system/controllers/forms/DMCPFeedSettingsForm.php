<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMCPFeedSettingsForm extends DMAbstractForm {

	protected function init() {
		$f = new DMTextField('copyright');
		$f->addAttribute('id', 'copyright');
		$f->addAttribute('style', 'width:98%');
		$f->setMinLength(10);
		$f->setMaxLength(150);
		$f->setLabel('Copyright notice');
		$this->addField($f);

		$f = new DMSelectField('language');
		$f->addAttribute('id', 'language');
		foreach (DMLanguage::getAll() as $l) {
			$f->addOption(
				DMString::websafe($l->getISO6391Code()),
				DMString::websafe($l->getNativeName()));
		}
		$f->setLabel('Language');
		$this->addField($f);

		$f = new DMTextField('managing_editor_name');
		$f->addAttribute('id', 'managing_editor_name');
		$f->addAttribute('style', 'width:50%');
		$f->setMinLength(2);
		$f->setMaxLength(100);
		$f->setLabel('Managing editor name');
		$this->addField($f);

		$f = new DMTextField('managing_editor_email');
		$f->addAttribute('id', 'managing_editor_email');
		$f->addAttribute('style', 'width:50%');
		$f->setType(DMFormFieldType::EMAIL);
		$f->setLabel('Managing editor email address');
		$this->addField($f);

		$f = new DMTextField('webmaster_name');
		$f->addAttribute('id', 'webmaster_name');
		$f->addAttribute('style', 'width:50%');
		$f->setMinLength(2);
		$f->setMaxLength(100);
		$f->setLabel('Webmaster name');
		$this->addField($f);

		$f = new DMTextField('webmaster_email');
		$f->addAttribute('id', 'webmaster_email');
		$f->addAttribute('style', 'width:50%');
		$f->setType(DMFormFieldType::EMAIL);
		$f->setLabel('Webmaster email address');
		$this->addField($f);

		$f = new DMTextField('title');
		$f->addAttribute('id', 'title');
		$f->addAttribute('style', 'width:98%');
		$f->setMinLength(2);
		$f->setMaxLength(100);
		$f->setLabel('Feed title');
		$this->addField($f);

		$f = new DMTextField('subtitle');
		$f->addAttribute('id', 'subtitle');
		$f->addAttribute('style', 'width:98%');
		$f->setMinLength(2);
		$f->setMaxLength(100);
		$f->setLabel('Feed subtitle');
		$this->addField($f);
	}

	protected function loadFromDataStore() {
		$this->getFieldByName('copyright')->setValue(
			DMConfigXML::getInstance()->getFeedCopyright());
		$this->getFieldByName('language')->setValue(
			DMConfigXML::getInstance()->getFeedLanguage());
		$this->getFieldByName('managing_editor_name')->setValue(
			DMConfigXML::getInstance()->getFeedManagingEditorName());
		$this->getFieldByName('managing_editor_email')->setValue(
			DMConfigXML::getInstance()->getFeedManagingEditorEmail());
		$this->getFieldByName('webmaster_name')->setValue(
			DMConfigXML::getInstance()->getFeedWebmasterName());
		$this->getFieldByName('webmaster_email')->setValue(
			DMConfigXML::getInstance()->getFeedWebmasterEmail());
		$this->getFieldByName('title')->setValue(
			DMConfigXML::getInstance()->getFeedTitle());
		$this->getFieldByName('subtitle')->setValue(
			DMConfigXML::getInstance()->getFeedSubtitle());
	}

	public function doAction() {
		DMConfigXML::getInstance()->setFeedCopyright(
			$this->getFieldByName('copyright')->getValue());
		DMConfigXML::getInstance()->setFeedLanguage(
			$this->getFieldByName('language')->getValue());
		DMConfigXML::getInstance()->setFeedManagingEditorName(
			$this->getFieldByName('managing_editor_name')->getValue());
		DMConfigXML::getInstance()->setFeedManagingEditorEmail(
			$this->getFieldByName('managing_editor_email')->getValue());
		DMConfigXML::getInstance()->setFeedWebmasterName(
			$this->getFieldByName('webmaster_name')->getValue());
		DMConfigXML::getInstance()->setFeedWebmasterEmail(
			$this->getFieldByName('webmaster_email')->getValue());
		DMConfigXML::getInstance()->setFeedTitle(
			$this->getFieldByName('title')->getValue());
		DMConfigXML::getInstance()->setFeedSubtitle(
			$this->getFieldByName('subtitle')->getValue());
		DMConfigXML::getInstance()->save();
	}

}
