<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMCPTagSettingsForm extends DMAbstractForm {

	protected function init() {
		$f = new DMSelectField('enable');
		$f->addAttribute('id', 'enable');
		$f->addOption(1, 'Enable Tagging');
		$f->addOption(0, 'Disable Tagging');
		$f->setLabel('Enable');
		$this->addField($f);

		$f = new DMSelectField('moderation');
		$f->addAttribute('id', 'moderation');
		$f->addOption(1, 'Enabled');
		$f->addOption(0, 'Disabled');
		$f->setLabel('Moderation');
		$this->addField($f);
	}

	protected function loadFromDataStore() {
		$this->getFieldByName('enable')->setValue(
			(int) DMConfigXML::getInstance()->isTaggingEnabled());
		$this->getFieldByName('moderation')->setValue(
			(int) DMConfigXML::getInstance()->isTagModerationEnabled());
	}

	public function doAction() {
		$bool = (bool) $this->getFieldByName('moderation')->getValue();
		if (!$bool) {
			// approve all unapproved tags
			$ds = DMDataStoreFactory::getDataStore();
			$tq = new DMTagQuery($ds);
			$tq->setApproved(-1);
			foreach ($tq->getSearchResultsAsObjects() as $t) {
				$t->setApproved(true);
				$ds->saveTag($t);
			}
		}
		DMConfigXML::getInstance()->setTaggingEnabled(
				(bool) $this->getFieldByName('enable')->getValue());
		DMConfigXML::getInstance()->setTagModerationEnabled($bool);
		DMConfigXML::getInstance()->save();
	}

}
