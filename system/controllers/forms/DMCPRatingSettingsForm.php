<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMCPRatingSettingsForm extends DMAbstractForm {

	protected function init() {
		$f = new DMSelectField('enable');
		$f->addAttribute('id', 'enable');
		$f->addOption(1, 'Enable Rating');
		$f->addOption(0, 'Disable Rating');
		$f->setLabel('Enable');
		$this->addField($f);
	}

	protected function loadFromDataStore() {
		$this->getFieldByName('enable')->setValue(
			(int) DMConfigXML::getInstance()->isRatingEnabled());
	}

	public function doAction() {
		DMConfigXML::getInstance()->setRatingEnabled(
				(bool) $this->getFieldByName('enable')->getValue());
		DMConfigXML::getInstance()->save();
	}

}
