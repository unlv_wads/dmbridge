<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMCPTemplateSetForm extends DMAbstractForm {

	/** @var DMTemplateSet */
	private $template_set;

	/**
	 * @param DMTemplateSet ts
	 */
	public function __construct(DMTemplateSet $ts) {
		$this->template_set = $ts;
		parent::__construct();
	}

	protected function init() {
		$f = new DMTextField("name");
		$f->addAttribute("id", "name");
		$f->addAttribute("size", 30);
		$f->setMinLength(2);
		$f->setMaxLength(50);
		$f->setLabel("Name");
		$this->addField($f);

		$f = new DMRadioField("default_collection");
		$f->setValue("/dmdefault");
		$f->setEnabled(false);
		$this->addField($f);

		$f = new DMCheckboxField("allowed_collections");
		$f->addAttribute("id", "all_aliases");
		$f->setValue("/dmdefault");
		$this->addField($f);

		foreach (DMCollection::getAuthorized() as $c) {
			$f = new DMRadioField("default_collection");
			$f->setValue($c->getAlias());
			$this->addField($f);

			$f = new DMCheckboxField("allowed_collections");
			$f->setValue($c->getAlias());
			$this->addField($f);
		}

		// results per page
		$f = new DMTextField("results_per_page");
		$f->setValue(20);
		$f->setType(DMFormFieldType::INTEGER);
		$f->setMin(10);
		$f->setMax(100);
		$f->setSize(3);
		$this->addField($f);

		// tile view
		$f = new DMSelectField("tile_cols");
		for ($i = 2; $i < 9; $i++) {
			$f->addOption($i);
		}
		$f->setValue(3);
		$this->addField($f);
	}

	protected function loadFromDataStore() {
		if (!$this->template_set->getID()) {
			return;
		}
		$this->getFieldByName("name")->setValue($this->template_set->getName());
		foreach ($this->getFieldsByName("default_collection") as $f) {
			if ($f->getValue()
					== $this->template_set->getDefaultCollection()->getAlias()) {
				$f->setChecked(true);
			}
		}
		foreach ($this->getFieldsByName("allowed_collections") as $f) {
			foreach ($this->template_set->getAuthorizedCollections() as $c) {
				if ($c->getAlias() == $f->getValue()) {
					$f->setChecked(true);
					break;
				}
			}
		}

		// results per page
		$this->getFieldByName("results_per_page")->setValue(
				$this->template_set->getNumResultsPerPage());

		// tile view
		$f = $this->getFieldByName("tile_cols");
		$f->setValue($this->template_set->getNumTileViewColumns());
	}

	public function doAction() {
		$this->template_set->setName(
				$this->getFieldByName("name")->getValue());
		try {
			$collection = DMCollectionFactory::getCollection(
					$this->getFieldByName("default_collection")->getValue());
			$this->template_set->setDefaultCollection($collection);
		} catch (DMUnavailableModelException $e) {
			// do nothing
		}

		$this->template_set->unsetAuthorizedCollections();
		foreach ($this->getFieldsByName("allowed_collections") as $f) {
			foreach (DMCollection::getAuthorized() as $c) {
				if ($f->getValue() == $c->getAlias()) {
					if ($f->isChecked()) {
						$this->template_set->addAuthorizedCollection($c);
					}
				}
			}
			if ($f->getValue() == "/dmdefault") {
				$this->template_set->unsetAuthorizedCollections();
			}
		}

		// results per page
		$this->template_set->setNumResultsPerPage(
				$this->getFieldByName("results_per_page")->getValue());

		// tile view
		$this->template_set->setNumTileViewColumns(
				$this->getFieldByName("tile_cols")->getValue());

		DMConfigXML::getInstance()->saveTemplateSet($this->template_set);
	}

}
