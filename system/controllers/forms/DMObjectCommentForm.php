<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMObjectCommentForm extends DMAbstractForm {

	protected function init() {
		$f = new DMTextField('name');
		$f->addAttribute('id', 'dmCommentName');
		$f->setMinLength(2);
		$f->setMaxLength(50);
		$f->setLabel('Name');
		$this->addField($f);

		$f = new DMTextField('email');
		$f->addAttribute('id', 'dmCommentEmail');
		$f->setMinLength(0);
		$f->setMaxLength(100);
		$f->setType(DMFormFieldType::EMAIL);
		$f->setLabel('Email');
		$this->addField($f);

		$f = new DMTextareaField('text');
		$f->addAttribute('id', 'dmCommentText');
		$f->setNumRows(5);
		$f->setNumColumns(5);
		$f->setMinLength(5);
		$f->setMaxLength(10000);
		$f->setLabel('Comment text');
		$this->addField($f);

		$f = new DMHiddenField('fauxcomment');
		$f->setMaxLength(0);
		$f->setRequired(false);
		$this->addField($f);

		$f = new DMHiddenField('answer');
		$f->setType(DMFormFieldType::INTEGER);
		$f->setMin(0);
		$f->setMax(20);
		$f->setValue(array_sum(DMAbstractForm::getCaptchaOperands()));
		$this->addField($f);

		$f = new DMTextField('response');
		$f->addAttribute('id', 'dmResponse');
		$f->setSize(2);
		$f->setMaxLength(2);
		$f->setLabel('Math response');
		$f->setStaticValidationMessage(
			DMLocalizedString::getString('INCORRECT_MATH_RESPONSE'));
		$this->addField($f);

		$f = new DMHiddenField('alias');
		$f->setMinLength(2);
		$f->setMaxLength(30);
		$f->setLabel('Alias');
		$this->addField($f);

		$f = new DMHiddenField('ptr');
		$f->setType(DMFormFieldType::INTEGER);
		$f->setMin(0);
		$f->setMax(9999999);
		$f->setLabel('Pointer');
		$this->addField($f);
	}

	protected function loadFromPOSTData(array $data) {
		parent::loadFromPOSTData($data);
		$this->getFieldByName('response')->addAllowedValue(
			$this->getFieldByName('answer')->getValue());
		$this->getFieldByName('answer')->setValue(
			array_sum(DMAbstractForm::getCaptchaOperands()));
	}


	/**
	 * @throws DMDataStoreException
	 * @throws DMIllegalArgumentException
	 * @throws DMPDOException
	 */
	public function doAction() {
		$col = DMCollectionFactory::getCollection(
				$this->getFieldByName('alias')->getValue());
		// the DMObject that has been commented on
		$obj = DMObjectFactory::getObject($col,
				$this->getFieldByName('ptr')->getValue());

		$c = new DMComment();
		$c->setEmail($this->getFieldByName('email')->getValue());
		$c->setName($this->getFieldByName('name')->getValue());
		$c->setValue($this->getFieldByName('text')->getValue());

		$obj->addComment($c);
		if (DMConfigXML::getInstance()->isCommentNotificationEnabled()) {
			$mailer = new DMMailer();
			$mailer->sendNotificationOfNewComment($c);
		}
		DMHTTPRequest::getCurrent()->getSession()->addHasCommented($obj);
	}

}
