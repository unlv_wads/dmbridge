<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMTagForm extends DMAbstractForm {

	protected function init() {
		$f = new DMTextField('value');
		$f->addAttribute('id', 'dmTagValue');
		$f->setMinLength(2);
		$f->setMaxLength(30);
		$f->setLabel('Tag value');
		$this->addField($f);

		$f = new DMHiddenField('fauxtag');
		$f->setMaxLength(0);
		$f->setRequired(false);
		$this->addField($f);

		$f = new DMHiddenField('alias');
		$f->setMinLength(2);
		$f->setMaxLength(30);
		$f->setLabel('Alias');
		$this->addField($f);

		$f = new DMHiddenField('ptr');
		$f->setType(DMFormFieldType::INTEGER);
		$f->setMin(0);
		$f->setMax(9999999);
		$f->setLabel('Pointer');
		$this->addField($f);
	}

	/**
	 * @throws DMIllegalArgumentException
	 */
	public function doAction() {
		$col = DMCollectionFactory::getCollection(
				$this->getFieldByName('alias')->getValue());
		$obj = DMObjectFactory::getObject($col,
				$this->getFieldByName('ptr')->getValue());

		$tag = new DMTag();
		$tag->setValue($this->getFieldByName('value')->getValue());

		DMDataStoreFactory::getDataStore()->saveTag($tag);
	}

}
