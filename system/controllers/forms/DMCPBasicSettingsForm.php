<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMCPBasicSettingsForm extends DMAbstractForm {

	protected function init() {
		$f = new DMTextField('institution_name');
		$f->addAttribute('id', 'institution_name');
		$f->addAttribute('style', 'width:98%');
		$f->setMinLength(2);
		$f->setMaxLength(100);
		$f->setLabel('Institution name');
		$this->addField($f);
	}

	protected function loadFromDataStore() {
		$this->getFieldByName('institution_name')->setValue(
			DMConfigXML::getInstance()->getInstitutionName());
	}

	public function doAction() {
		DMConfigXML::getInstance()->setInstitutionName(
			$this->getFieldByName('institution_name')->getValue());
		DMConfigXML::getInstance()->save();
	}

}

