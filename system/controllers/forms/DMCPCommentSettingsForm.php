<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMCPCommentSettingsForm extends DMAbstractForm {

	protected function init() {
		$f = new DMSelectField('enable');
		$f->addAttribute('id', 'enable');
		$f->addOption(1, 'Enable Commenting');
		$f->addOption(0, 'Disable Commenting');
		$f->setLabel('Enable');
		$this->addField($f);

		$f = new DMSelectField('moderation');
		$f->addAttribute('id', 'moderation');
		$f->addOption(1, 'Enabled');
		$f->addOption(0, 'Disabled');
		$f->setLabel('Moderation');
		$f->setRequired(false);
		$this->addField($f);

		$f = new DMSelectField('notification');
		$f->addAttribute('id', 'notification');
		$f->addOption(1, 'Enabled');
		$f->addOption(0, 'Disabled');
		$f->setLabel('Notification');
		$f->setRequired(false);
		$this->addField($f);

		$f = new DMTextField('notification_email');
		$f->addAttribute('id', 'notification_email');
		$f->addAttribute('size', 30);
		$f->setType(DMFormFieldType::EMAIL);
		$f->setRequired(false);
		$f->setLabel('Notification email');
		$this->addField($f);
	}

	protected function loadFromDataStore() {
		$enabled = DMConfigXML::getInstance()->isCommentingEnabled();
		$this->getFieldByName('enable')->setValue((int) $enabled);

		$this->getFieldByName('moderation')->setValue(
			(int) DMConfigXML::getInstance()->isCommentModerationEnabled());
		$this->getFieldByName("moderation")->setEnabled($enabled);

		$this->getFieldByName('notification')->setValue(
			(int) DMConfigXML::getInstance()->isCommentNotificationEnabled());
		$this->getFieldByName("notification")->setEnabled($enabled);

		$this->getFieldByName('notification_email')->setValue(
			DMConfigXML::getInstance()->getCommentNotificationEmail());
		$this->getFieldByName("notification_email")->setEnabled($enabled);
	}

	/**
	 * @Override
	 */
	public function validate() {
		$this->getFieldByName("notification_email")->setRequired(
				$this->getFieldByName('notification')->getValue());
		parent::validate();
	}

	public function doAction() {
		$bool = (bool) $this->getFieldByName('moderation')->getValue();
		DMConfigXML::getInstance()->setCommentModerationEnabled($bool);
		if (!$bool) {
			// approve all unapproved comments
			$ds = DMDataStoreFactory::getDataStore();
			if ($ds) {
				$cq = new DMCommentQuery($ds);
				$cq->setApproved(-1);
				foreach ($cq->getSearchResults() as $c) {
					$c->setApproved(true);
					DMDataStoreFactory::getDataStore()->saveComment($c);
				}
			}
		}
		DMConfigXML::getInstance()->setCommentingEnabled(
			(bool) $this->getFieldByName('enable')->getValue());
		DMConfigXML::getInstance()->setCommentNotificationEnabled(
			(bool) $this->getFieldByName('notification')->getValue());
		DMConfigXML::getInstance()->setCommentNotificationEmail(
			$this->getFieldByName('notification_email')->getValue());
		DMConfigXML::getInstance()->save();
	}

}
