<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMAPICollectionController extends DMAbstractController {

	public function index() {
		$response = new DMHTTPResponse();
		$rep = DMHTTPResponseFactory::getRepresentation();
		$trans = DMHTTPResponseFactory::getTransformer();
		$rep->setBody($trans->transformCollections(DMCollection::getAuthorized()));
		$response->setRepresentation($rep);
		$response->send();
		die;
	}

	/**
	 * @param string alias
	 */
	public function view($alias) {
		$alias = DMCollection::getSanitizedAlias($alias);
		if (!DMCollection::exists($alias)) {
			throw new DMUnavailableModelException("Invalid collection.");
		}

		$collection = DMCollectionFactory::getCollection($alias);

		$response = new DMHTTPResponse();
		$rep = DMHTTPResponseFactory::getRepresentation();
		$trans = DMHTTPResponseFactory::getTransformer();
		$rep->setBody($trans->transformCollection($collection));
		$response->setRepresentation($rep);
		$response->send();
		die;
	}

}
