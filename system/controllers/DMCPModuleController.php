<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMCPModuleController extends DMCPAdminController {

	public function index() {
		$this->preFlightCheck();

		$mm = DMModuleManager::getInstance();

		if (DMHTTPRequest::getCurrent()->getMethod() == DMHTTPMethod::POST) {
			$rep = DMHTTPRequest::getCurrent()->getRepresentation();
			if (is_array($rep->getFormValue("name"))) {
				$enabled = (substr($rep->getFormValue("action"), 0, 6) == "Enable");
				try {
					foreach ($rep->getFormValue("name") as $name) {
						$module = $mm->getModuleByName($name);
						$mm->setModuleEnabled($module, $enabled);
					}
				} catch (DMModuleActivationException $e) {
					DMHTTPRequest::getCurrent()->getSession()->setFlash(
							new DMFlash($e->getMessage(), false));
					DMHTTPRequest::reload();
					die;
				}

				DMConfigXML::getInstance()->save();
				$msg_key = $enabled ? "MODULES_ENABLED" : "MODULES_DISABLED";
				DMHTTPRequest::getCurrent()->getSession()->setFlash(
						new DMFlash(
								DMLocalizedString::getString($msg_key), true));
				DMHTTPRequest::reload();
				die;
			}
		}

		$this->tpl_vars['manager'] = $mm;
		$this->tpl_vars['modules'] = $mm->getAllModules();

		$this->renderTemplate("/templates/module/index.html.php");
		die;
	}

}
