<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Controller for "favorites view" in the template engine.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMTEFavoriteController extends DMAbstractController {

	public function create() {
		$rep = $this->getHTTPRequest()->getRepresentation();
		try {
			if (is_array($rep->getFormValue("data"))) {
				$this->createMultiple();
			} else {
				$this->createSingle();
			}
			$this->getSession()->setFlash(new DMFlash(
					DMLocalizedString::getString("OBJ_ADDED_TO_FAVORITES"),
					true));
		} catch (DMException $e) {
			header("HTTP/1.1 400 Bad Request");
			$this->getSession()->setFlash(new DMFlash(
					DMLocalizedString::getString("NONEXISTENT_OBJECT"),
					false));
		}
/* @todo Get rid of this?
		$params = ($rep->getFormValue("params"))
				? $rep->getFormValue("params") : "";
		DMHTTPRequest::redirectToParams($params);
		die; */
	}

	private function createSingle() {
		$rep = $this->getHTTPRequest()->getRepresentation();
		$obj = DMObjectFactory::getObject(
			DMCollectionFactory::getCollection(
					DMCollection::getSanitizedAlias(
							$rep->getFormValue("alias"))),
				(int) $rep->getFormValue("ptr"));
		$this->getSession()->addFavorite($obj);
	}

	private function createMultiple() {
		$rep = $this->getHTTPRequest()->getRepresentation();
		foreach ($rep->getFormValue("data") as $data) {
			$tmp = explode(DMAbstractForm::ALIAS_PTR_SEPARATOR, $data);
			$alias = DMCollection::getSanitizedAlias($tmp[0]);
			$ptr = (int) $tmp[1];
			$obj = DMObjectFactory::getObject(
				DMCollectionFactory::getCollection($alias), $ptr);
			$this->getSession()->addFavorite($obj);
		}
	}

	public function delete() {
		$rep = $this->getHTTPRequest()->getRepresentation();
		$data = $rep->getFormValue("data");
		$objects = array();
		for ($i = 0; $i < count($data); $i++) {
			$tmp = explode('|', $data[$i]);
			$objects[] = DMObjectFactory::getObject(
						DMCollectionFactory::getCollection(
								DMCollection::getSanitizedAlias($tmp[0])),
						(int) $tmp[1]);
		}
		$this->getSession()->deleteFavorites($objects);

		$this->getSession()->setFlash(new DMFlash(
					DMLocalizedString::getString("OBJ_REMOVED_FROM_FAVORITES"),
					true));
/* @todo Get rid of this?
		$params = ($rep->getFormValue("params"))
				? $rep->getFormValue("params") : "";
		DMHTTPRequest::redirectToParams($params);
		die; */
	}

	public function index($alias) {
		if ($this->getHTTPRequest()->getMethod() == DMHTTPMethod::POST) {
			$rep = $this->getHTTPRequest()->getRepresentation();
			if ($rep->getFormValue("method") == "DELETE") {
				$this->delete();
			} else {
				$this->create();
			}
		}
		$this->listAll($alias);
	}

	private function listAll($alias) {
		$this->getSession()->unsetQuery();

		$alias = DMCollection::getSanitizedAlias($alias);
		if (strlen($alias) < 2) {
			$alias = "/dmdefault";
		}
		$collection = DMCollectionFactory::getCollection($alias);
		if (!$collection) {
			throw new DMUnavailableModelException(
					DMLocalizedString::getString("NONEXISTENT_COLLECTION"));
		}

		// restrict favorites from unauthorized collections from appearing
		$objects = array();
		foreach ($this->getSession()->getAccessibleFavorites($this->getTemplateSet()) as $obj) {
			$objects[] = $obj;
		}

		$path = "/templates/favorite/index.html.php";
		$tpl = $this->getTemplateSet()->getTemplateAtPathname($path);
		global $view;
		$view = new DMFavoritesView($tpl, $this->getSession(),
				$collection,
				new DMFavoriteQuery($this->getSession(), $this->getTemplateSet()));

		// store results view in session to facilitate "back to results" link
		$this->getSession()->setResultsView($view);
	}

}

