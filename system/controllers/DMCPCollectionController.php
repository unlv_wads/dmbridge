<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMCPCollectionController extends DMCPAdminController {

	public function edit($alias) {
		$this->preFlightCheck();

		$alias = DMCollection::getSanitizedAlias($alias);
		$collection = DMCollectionFactory::getCollection($alias);

		$form = new DMCPEditCollectionForm($collection);
		$this->setForm($form);

		if (DMHTTPRequest::getCurrent()->getMethod() == DMHTTPMethod::POST) {
			$this->handleForm(DMLocalizedString::getString("COLLECTION_UPDATED"),
					"admin/collections");
		}

		try {
			$this->tpl_vars['collection'] = $collection;
			// overview page
			$this->tpl_vars['overview_page_url'] = $form
					->getFieldByName("overview_page_url")->getHTMLTag();
			// image url
			$this->tpl_vars['image_url_512'] = $form
					->getFieldByName("image_url_512")->getHTMLTag();
			// description
			$this->tpl_vars['description'] = $form
					->getFieldByName("description")->getHTMLTag();

			// template set
			$this->tpl_vars['template_set'] = $form
					->getFieldByName("template_set")->getHTMLTag();

			// inheritances
			foreach ($form->getFieldsByName("inherit_template_set") as $field) {
				$this->tpl_vars['inherit_template_set'][] = $field->getHTMLTag();
			}
			foreach ($form->getFieldsByName("inherit_object_view") as $field) {
				$this->tpl_vars['inherit_object_view'][] = $field->getHTMLTag();
			}
			foreach ($form->getFieldsByName("inherit_results_view") as $field) {
				$this->tpl_vars['inherit_results_view'][] = $field->getHTMLTag();
			}
			foreach ($form->getFieldsByName("inherit_search_view") as $field) {
				$this->tpl_vars['inherit_search_view'][] = $field->getHTMLTag();
			}

			// object viewers
			foreach ($form->getFieldsByName("mime") as $field) {
				$this->tpl_vars['mime'][] = $field->getHTMLTag();
			}
			foreach ($form->getFieldsByName("class") as $field) {
				$this->tpl_vars['class'][] = $field->getHTMLTag();
			}
			foreach ($form->getFieldsByName("width") as $field) {
				$this->tpl_vars['width'][] = $field->getHTMLTag();
			}
			foreach ($form->getFieldsByName("height") as $field) {
				$this->tpl_vars['height'][] = $field->getHTMLTag();
			}
			foreach ($form->getFieldsByName("max_file_size_k") as $field) {
				$this->tpl_vars['max_file_size_k'][] = $field->getHTMLTag();
			}

			// results view
			foreach ($form->getFieldsByName("field_nicks") as $field) {
				$this->tpl_vars['field_nicks'][] = $field->getHTMLTag();
			}
			foreach ($form->getFieldsByName("field_nicks_sortable") as $field) {
				$this->tpl_vars['field_nicks_sortable'][] = $field->getHTMLTag();
			}
			foreach ($form->getFieldsByName("field_nicks_default_sort") as $field) {
				$this->tpl_vars['field_nicks_default_sort'][] = $field->getHTMLTag();
			}
			foreach ($form->getFieldsByName("field_nicks_max_words") as $field) {
				$this->tpl_vars['field_nicks_max_words'][] = $field->getHTMLTag();
			}

			// grid/list view
			foreach ($form->getFieldsByName("grid_fields") as $field) {
				$this->tpl_vars['grid_fields'][] = $field->getHTMLTag();
			}

			// facets
			foreach ($form->getFieldsByName("facet_fields") as $field) {
				$this->tpl_vars['facet_fields'][] = $field->getHTMLTag();
			}

			// search view
			$this->tpl_vars['begin_year'] = $form
					->getFieldByName("begin_year")->getHTMLTag();
			$this->tpl_vars['end_year'] = $form
					->getFieldByName("end_year")->getHTMLTag();
		} catch (DMException $e) {
			DMHTTPRequest::getCurrent()->getSession()->setFlash(
					new DMFlash($e->getMessage(), false));
		}

		$this->renderTemplate("/templates/collection/edit.html.php");
		die;
	}

	public function index() {
		$this->preFlightCheck();

		if (DMHTTPRequest::getCurrent()->getMethod() == DMHTTPMethod::POST) {
			$this->delete();
		}

		$this->renderTemplate("/templates/collection/index.html.php");
		die;
	}

}
