<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMCPRatingController extends DMCPAdminController {

	public function configure() {
		$this->preFlightCheck();

		$form = new DMCPRatingSettingsForm();
		$this->setForm($form);

		if (DMHTTPRequest::getCurrent()->getMethod() == DMHTTPMethod::POST) {
			$this->handleForm();
		}

		$this->tpl_vars['enable']
				= $form->getFieldByName("enable")->getHTMLTag();

		$this->renderTemplate("/templates/rating/edit.html.php");
		die;
	}

	public function statistics() {
		$this->preFlightCheck();

		$this->renderTemplate("/templates/rating/statistics.html.php");
		die;
	}

}

