<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMCPReferenceURLController extends DMCPAdminController {

	public function index() {
		$this->preFlightCheck();

		if (DMHTTPRequest::getCurrent()->getMethod() == DMHTTPMethod::POST) {
			$sets = DMHTTPRequest::getCurrent()->getRepresentation()->getFormValue("set");
			$flash = null;
			if (is_array($sets)) {
				foreach ($sets as $alias => $dest) {
					$alias = DMCollection::getSanitizedAlias($alias);
					$col = DMCollectionFactory::getCollection($alias);
					$col->setRedirectingReferenceURLs(($dest == "dmbridge"));
					DMConfigXML::getInstance()->saveCollection($col);
				}
				$flash = new DMFlash(
					DMLocalizedString::getString("PREFS_UPDATED"), true);
			}
			DMHTTPRequest::getCurrent()->getSession()->setFlash($flash);
			DMHTTPRequest::reload();
			die;
		}

		// check whether the replacement reference URL routing script is
		// installed
		$installed = false;
		if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/u/index.php")) {
			$tmp = @file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/u/index.php");
			$tmp = explode("\n", $tmp);
			$installed = (strpos($tmp[0], "dmBridge") !== false);
		}

		$this->tpl_vars['script_installed'] = $installed;
		$this->tpl_vars['all_collections'] = DMCollection::getAuthorized();

		$this->renderTemplate("/templates/reference_url/index.html.php");
		die;
	}

}

