<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * A generic controller superclass from which all other controllers inherit.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMIntroController {

	public function index() {
		include_once(dirname(__FILE__) . "/../../templates/system/intro.html.php");
		die;
	}

}
