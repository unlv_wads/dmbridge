<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * A menu item is a single textual hyperlink within a menu section,
 * represented by DMCPMenuSection.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMCPMenuItem {

	/** string */
	private $title;
	/** DMURI */
	private $uri;

	/**
	 * @param string title The title of the menu item.
	 * @param DMURI uri The URI to which the menu item is to be hyperlinked.
	 */
	public function __construct($title, DMURI $uri) {
		$this->setTitle($title);
		$this->setURI($uri);
	}

	/**
	 * @return string The title of the menu item.
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param string title The title of the menu item.
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * @return DMURI The URI to which the menu item is hyperlinking.
	 */
	public function getURI() {
		return $this->uri;
	}

	/**
	 * @param DMURI uri The URI to which the menu item is hyperlinking.
	 */
	public function setURI(DMURI $uri) {
		$this->uri = $uri;
	}

}
