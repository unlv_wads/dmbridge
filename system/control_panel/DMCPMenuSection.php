<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * A menu section contains one or more menu items, represented by DMCPMenuItem.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMCPMenuSection {

	/** boolean */
	private $is_from_module;
	/** array of DMCPMenuItems */
	private $menu_items = array();
	/** string */
	private $title;

	/**
	 * @param string title
	 */
	public function __construct($title) {
		$this->title = $title;
	}

	/**
	 * @return boolean Whether the section is being provided by a module.
	 */
	public function isFromModule() {
		return $this->is_from_module;
	}

	/**
	 * @param boolean bool Whether the section is being provided by a module.
	 * This will be set automatically as necessary and there is no reason to
	 * invoke it manually.
	 */
	public function setFromModule($bool) {
		$this->is_from_module = ($bool);
	}

	/**
	 * @param DMCPMenuItem item
	 */
	public function addMenuItem(DMCPMenuItem $item) {
		$this->menu_items[] = $item;
	}

	/**
	 * @return string The URI to which the menu item is hyperlinked.
	 */
	public function getMenuItems() {
		return $this->menu_items;
	}

	/**
	 * @return string The title of the menu item.
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param string title The title of the menu item.
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

}
