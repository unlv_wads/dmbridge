<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Represents a news item on the dmBridge blog.
 *
 * @see DMUpdateManager::getNewsItems()
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMNewsItem {

	private $author, $date, $title, $body, $url;


	/**
	 * @return string
	 */
	public function getAuthor() {
		return $this->author;
	}

	/**
	 * @param str
	 */
	public function setAuthor($str) {
		$this->author = $str;
	}

	/**
	 *	@return string
	 */
	public function getBody() {
		return $this->body;
	}

	/**
	 * @param str
	 */
	public function setBody($str) {
		$this->body = $str;
	}

	/**
	 * @return DMDateTime
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * @param DMDateTime dt
	 */
	public function setDate(DMDateTime $dt) {
		$this->date = $dt;
	}

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param str
	 */
	public function setTitle($str) {
		$this->title = $str;
	}

	/**
	 *	@return string
	 */
	public function getURL() {
		return $this->url;
	}

	/**
	 * @param str
	 */
	public function setURL($str) {
		$this->url = $str;
	}

}
