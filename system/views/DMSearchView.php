<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMSearchView extends DMTemplateEngineView implements DMURIAddressable {

	/**
	 * @param DMTemplate tpl
	 * @param DMSession session
	 * @param DMCollection col
	 */
	public function __construct(DMTemplate $tpl, DMSession $session,
			DMCollection $col = null) {
		parent::__construct($tpl, $session, $col);
		$this->setHelper(new DMSearchTemplateHelper($this, $session));
	}

	/**
	 * @return int
	 */
	public function getDateSearchBeginYear() {
		return $this->getCollection()->getDateSearchBeginYear();
	}

	/**
	 * @return int
	 */
	public function getDateSearchEndYear() {
		return $this->getCollection()->getDateSearchEndYear();
	}

	/**
	 * @return DMInternalURI The URI of the view.
	 */
	public function getURI() {
		$params = "objects/search";
		if ($this->getCollection() && !$this->getCollection()->isDefault()) {
			$params = "objects" . $this->getCollection()->getAlias()
					. "/search";
		}
		return DMInternalURI::getURIWithParams($params);
	}

}
