<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMControlPanelView extends DMInternalView implements DMURIAddressable {

	/**
	 * @var DMObjectQuery
	 */
	private $query;

	private $template_vars = array();

	/**
	 * @param DMTemplate tpl
	 * @param DMSession session
	 */
	public function __construct(DMTemplate $tpl, DMSession $session) {
		parent::__construct($tpl, $session);
		$this->setHelper(new DMControlPanelTemplateHelper($this, $session));
	}

	/**
	 * @return DMObjectQuery
	 */
	public function getQuery() {
		return $this->query;
	}

	/**
	 * @param DMQuery query
	 */
	public function setQuery(DMQuery $query) {
		$this->query = $query;
	}

	/**
	 * @param string key
	 * @param mixed value
	 */
	public function addTemplateVar($key, $value) {
		$this->template_vars[$key] = $value;
	}

	/**
	 * @param mixed var
	 * @return mixed
	 */
	public function getTemplateVar($var) {
		return array_key_exists($var, $this->template_vars)
				? $this->template_vars[$var] : null;
	}

	/**
	 * @param array vars Associative array of key-value pairs
	 */
	public function setTemplateVars(array $vars) {
		$this->template_vars = $vars;
	}

	/**
	 * @return DMInternalURI The URI of the view.
	 */
	public function getURI() {
		return DMInternalURI::getURIWithParams("admin");
	}

	/**
	 * Alias of getTemplateVar().
	 *
	 * @param mixed var
	 */
	public function getVar($var) {
		return $this->getTemplateVar($var);
	}

}

