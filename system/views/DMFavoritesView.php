<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMFavoritesView extends DMResultsView implements DMURIAddressable {

	/**
	 * @param DMTemplate tpl
	 * @param DMSession session
	 * @param DMCollection col
	 * @param DMQuery query
	 */
	public function __construct(DMTemplate $tpl, DMSession $session,
			DMCollection $col = null, DMQuery $query = null) {
		parent::__construct($tpl, $session, $col, $query);
		$this->setHelper(new DMFavoritesTemplateHelper($this, $session));
	}

	/**
	 * @return array Array of DMObjects
	 */
	public function getAccessibleFavorites() {
		return $this->getSession()->getAccessibleFavorites(
				$this->getTemplate()->getTemplateSet());
	}

	/**
	 * @return array Array of DMObjects
	 */
	public function getAllFavorites() {
		return $this->getSession()->getAllFavorites();
	}

	/**
	 * @return DMInternalURI The URI of the view.
	 * @Override
	 */
	public function getURI() {
		$params = "objects/favorites";
		if ($this->getCollection() && !$this->getCollection()->isDefault()) {
			$params = "objects" . $this->getCollection()->getAlias()
					. "/favorites";
		}
		return DMInternalURI::getURIWithParams($params);
	}

}

