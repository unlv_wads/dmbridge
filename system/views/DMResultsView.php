<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Encapsulates "results view" within the HTML templates.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMResultsView extends DMTemplateEngineView implements DMURIAddressable {

	/**
	 * @var DMQuery
	 */
	private $query;

	/**
	 * @param DMTemplate tpl
	 * @param DMSession session
	 * @param DMCollection col
	 * @param DMQuery query
	 */
	public function __construct(DMTemplate $tpl, DMSession $session,
			DMCollection $col, DMQuery $query = null) {
		parent::__construct($tpl, $session, $col);
		$this->setHelper(new DMResultsTemplateHelper($this, $session));
		$this->query = $query;
	}

	/**
	 * @return DMQuery
	 */
	public function getQuery() {
		return $this->query;
	}

	/**
	 * @return array Array of DMDCElement objects
	 */
	public function getFields() {
		return $this->getCollection()->getResultsViewFields();
	}

	/**
	 * @return int
	 */
	public function getNumTileViewColumns() {
		return $this->getTemplate()->getTemplateSet()->getNumTileViewColumns();
	}

	/**
	 * @return DMInternalURI The URI of the view.
	 */
	public function getURI() {
		return $this->getQuery()->getURI();
	}

}

