<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Encapsulates "compound object results view" within the HTML templates.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMObjectResultsView extends DMTemplateEngineView
implements DMURIAddressable {

	/**
	 * @var DMQuery
	 */
	private $query;

	/**
	 * @param DMTemplate tpl
	 * @param DMSession session
	 * @param DMObject obj
	 * @param DMQuery query
	 */
	public function __construct(DMTemplate $tpl, DMSession $session,
			DMObject $obj, DMQuery $query) {
		parent::__construct($tpl, $session, $obj->getCollection());
		$this->setHelper(new DMObjectResultsTemplateHelper($this, $session));
		$this->query = $query;
	}

	/**
	 * @return DMQuery
	 */
	public function getQuery() {
		return $this->query;
	}

	/**
	 * @return DMInternalURI The URI of the view.
	 */
	public function getURI() {
		$params = "objects" . $this->getCollection()->getAlias() . "/search";
		return DMInternalURI::getURIWithParams($params);
	}

}

