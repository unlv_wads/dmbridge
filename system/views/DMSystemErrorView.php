<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Encapsulates "error view" within the HTML templates.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMSystemErrorView extends DMInternalView {

	/**
	 * @var Exception
	 */
	private $exception;

	/**
	 * @param DMTemplate tpl
	 * @param DMSession session
	 * @param Exception ex
	 */
	public function __construct(DMTemplate $tpl, DMSession $session,
			Exception $ex) {
		parent::__construct($tpl, $session);
		$this->setHelper(new DMSystemErrorTemplateHelper($this, $session));
		$this->exception = $ex;
	}

	/**
	 * @return DMCollection The default collection
	 */
	public function getCollection() {
		return DMCollectionFactory::getCollection("/dmdefault");
	}

	/**
	 * @return Exception
	 */
	public final function getException() {
		return $this->exception;
	}

	/**
	 * @return null
	 */
	public function getQuery() {
		return null;
	}

}
