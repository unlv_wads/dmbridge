<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Encapsulates a view <strong>not</strong> within the HTML templates, as in
 * the case of e.g. accessing template helper methods via the PHP API, outside
 * the template engine.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMExternalView extends DMAbstractView {

	/**
	 * @var DMCollection
	 */
	private $collection;

	/**
	 * @param DMCollection col
	 */
	public function __construct(DMCollection $col) {
		$this->setCollection($col);
		$this->setHelper(new DMGenericTemplateHelper($this, new DMSession()));
	}

	/**
	 * @return DMCollection
	 */
	public function getCollection() {
		return $this->collection;
	}

	/**
	 * @param DMCollection $col
	 */
	public function setCollection(DMCollection $col) {
		$this->collection = $col;
	}

	/**
	 * @return null Always returns null.
	 */
	public function getTemplate() {
		return null;
	}

}
