<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>Encapsulates a view that is internal to the dmBridge system, such as
 * within the template engine or control panel. This is in contrast to an
 * external view, which is an abstract view used in the context of custom
 * software that interfaces with the dmBridge PHP API.</p>
 * 
 * <p>The concrete DMInternalView instance can always be accessed within HTML
 * templates via the <code>$view</code> variable.</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMInternalView extends DMAbstractView {

	/**
	 * @var DMTemplate
	 */
	private $template;

	/**
	 * @param DMTemplate template
	 * @param DMUser user
	 */
	public function __construct(DMTemplate $template, DMSession $session) {
		parent::__construct($session);
		$this->setTemplate($template);
		$this->setHelper(new DMGenericTemplateHelper($this, $session));
	}

	/**
	 * Include()s the template file. <strong>Not for public use.</strong>
	 */
	public function render() {
		include_once($this->getTemplate()->getAbsolutePathname());
	}

	/**
	 * Returns <strong>and erases</strong> the flash message from the user's
	 * session. Do not use this method to check for the presence of a flash
	 * message; use <code>isFlash()</code> instead.
	 *
	 * @return string The HTML-escaped flash message
	 * @see isFlash()
	 * @since 0.1
	 */
	public function getFlash() {
		$flash = $this->getSession()->getFlash();
		if (!$flash instanceof DMFlash) {
			return false;
		}
		$msg = $flash->getValue();
		$this->getSession()->unsetFlash();
		return DMString::websafe($msg);
	}

	/**
	 * @return Boolean
	 * @see getFlash()
	 * @since 0.1
	 */
	public function isFlash() {
		return ($this->getSession()->getFlash() instanceof DMFlash);
	}

	/**
	 * @return DMTemplate
	 * @since 2.0
	 */
	public function getTemplate() {
		return $this->template;
	}

	/**
	 * @param DMTemplate template
	 */
	public function setTemplate(DMTemplate $template) {
		$this->template = $template;
	}

}
