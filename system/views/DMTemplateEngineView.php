<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMTemplateEngineView extends DMInternalView {

	/**
	 * @var DMCollection
	 */
	private $collection;

	/**
	 * @param DMTemplate tpl
	 * @param DMSession session
	 * @param DMCollection col
	 */
	public function __construct(DMTemplate $tpl, DMSession $session,
			DMCollection $col) {
		parent::__construct($tpl, $session);
		$this->collection = $col;
	}

	/**
	 * @return DMCollection
	 */
	public function getCollection() {
		return $this->collection;
	}

	/**
	 * @return DMFavoritesView A new DMFavoritesView with the template and
	 * collection of this instance.
	 */
	public function getFavoritesView() {
		return new DMFavoritesView($this->getTemplate(), $this->getSession(),
				$this->getCollection());
	}


	/**
	 * @return DMLoginView A new DMLoginView with the template and
	 * collection of this instance.
	 */
	public function getLoginView() {
		return new DMLoginView($this->getTemplate(), $this->getSession(),
				$this->getCollection());
	}

	/**
	 * @return DMInternalURI URI of logout resource
	 * @since 0.1
	 */
	public function getLogoutURI() {
		$params = "objects/logout";
		if ($this->getCollection() && !$this->getCollection()->isDefault()) {
			$params = "objects" . $this->getCollection()->getAlias()
					. "/logout";
		}
		return DMInternalURI::getURIWithParams($params);
	}

	/**
	 * @param int limit
	 * @return array Array of DMObjects
	 */
	public function getRecentlyViewedObjects($limit) {
		return $this->getSession()->getRecentlyViewedObjects($limit);
	}

	/**
	 * @return DMSearchView A new DMSearchView with the template and
	 * collection of this instance.
	 */
	public function getSearchView() {
		return new DMSearchView($this->getTemplate(), $this->getSession(),
				$this->getCollection());
	}

}
