<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Encapsulates "object view" within the HTML templates.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMObjectView extends DMTemplateEngineView implements DMURIAddressable {

	/**
	 * @var DMObjectCommentForm
	 */
	private $comment_form;

	/**
	 * @var DMObject
	 */
	private $object;

	/**
	 * @var DMObjectQuery
	 */
	private $query;

	/**
	 * @var DMTagForm
	 */
	private $tag_form;

	/**
	 * @param DMTemplate tpl
	 * @param DMSession session
	 * @param DMObject obj
	 * @param DMObjectQuery query
	 * @throws DMClassNotFoundException
	 */
	public function __construct(DMTemplate $tpl, DMSession $session,
			DMObject $obj, DMObjectQuery $query = null) {
		parent::__construct($tpl, $session, $obj->getCollection());
		$this->setHelper(new DMObjectTemplateHelper($this, $session));
		$this->setCommentForm(new DMObjectCommentForm());
		$this->setTagForm(new DMTagForm());
		$this->object = $obj;
		$this->query = $query;
		$viewer = ($obj->isCompound() && $obj->getChild(0) instanceof DMObject)
				? $obj->getChild(0)->getViewer() : $obj->getViewer();
		$viewer->setHelper($this->getHelper());
		$viewer->viewerWillRender();
	}

	/**
	 * @return DMObjectCommentForm
	 */
	public function getCommentForm() {
		return $this->comment_form;
	}

	/**
	 * @param DMObjectCommentForm form
	 */
	public function setCommentForm(DMObjectCommentForm $form) {
		$this->comment_form = $form;
	}

	/**
	 * @return DMObject
	 */
	public function getObject() {
		return $this->object;
	}

	/**
	 * @param DMObject obj
	 */
	public function setObject(DMObject $obj) {
		$this->object = $obj;
	}

	/**
	 * @return DMObjectQuery
	 */
	public function getQuery() {
		return $this->query;
	}

	/**
	 * @param DMObjectQuery query
	 */
	public function setQuery(DMObjectQuery $query) {
		$this->query = $query;
	}

	/**
	 * @return string
	 * @since 2.0
	 * @see getViewForNextObject()
	 * @see getViewForPreviousObject()
	 */
	public function getResultsView() {
		return $this->getSession()->getResultsView();
	}

	/**
	 * @return DMTagForm
	 */
	public function getTagForm() {
		return $this->tag_form;
	}

	/**
	 * @param DMTagForm form
	 */
	public function setTagForm(DMTagForm $form) {
		$this->tag_form = $form;
	}

	/**
	 * @return DMInternalURI The URI of the view (which will be the same as the
	 * URI of the object represented by the view).
	 */
	public function getURI() {
		return $this->getObject()->getURI();
	}

	/**
	 * Returns the DMObjectView instance corresponding to the next object in
	 * the results, or null if there is no next object.
	 *
	 * @return DMObjectView
	 * @see getViewForPreviousObject()
	 * @since 2.0
	 */
	public function getViewForNextObject() {
		if (!$this->getQuery()) {
			return null;
		}
		$all = $this->getQuery()->getSearchResults();
		$count = count($all);
		for ($i = 0; $i < $count; $i++) {
			if ($all[$i]->equals($this->object)) {
				if (array_key_exists($i + 1, $all)) {
					return new DMObjectView($this->getTemplate(),
							$this->getSession(), $all[$i + 1],
							$this->getQuery());
				}
			}
		}
		return null;
	}

	/**
	 * Returns the DMObjectView instance corresponding to the next object in
	 * the results, or null if there is no next object.
	 *
	 * @return DMObjectView
	 * @see getViewForNextObject()
	 * @since 2.0
	 */
	public function getViewForPreviousObject() {
		if (!$this->getQuery()) {
			return null;
		}
		$all = $this->getQuery()->getSearchResults();
		$count = count($all);
		for ($i = 0; $i < $count; $i++) {
			if ($all[$i]->equals($this->object)) {
				if (array_key_exists($i - 1, $all)) {
					return new DMObjectView($this->getTemplate(),
							$this->getSession(), $all[$i - 1],
							$this->getQuery());
				}
			}
		}
		return null;
	}

	/**
	 * @param DMMediaType type
	 * @return DMObjectViewerDefinition
	 * @since 2.0
	 */
	public function getViewerDefinitionForMediaType($type) {
		return $this->object->getCollection()
						->getViewerDefinitionForMediaType($type);
	}

}

