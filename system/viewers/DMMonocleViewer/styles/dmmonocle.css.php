<?php
/*
 * dmBridge: a data access framework for CONTENTdm(R)
 *
 * Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
 * Education, on behalf of the University of Nevada, Las Vegas
 */

$root_uri = dirname($_SERVER['PHP_SELF']) . "/../";

header("Content-Type: text/css"); ?>

/* This is the main viewer area. Set it's width and height 
to reflect the size you would like your viewer to be. */
div#dmMonocle {
	background-color: #EEE;
	
	/* These properties are necessary for dmMonocle functionality 
	- do not edit unless necessary */
	position:relative;
	overflow:hidden;
	margin: 0 auto;
	text-align: center;
	background-repeat:no-repeat;
	background-position:center center;
}
/* Positions the Thumbnail div. In future version, more flexibility will be given with the thumbnail. As is, it is rendered at the top-left corner. */
#dmThumbnail {
	border-right: 3px solid #FFF;
	border-bottom: 3px solid #FFF;
	
	/* Necessary Styles */
	position: absolute;
	z-index: 15;
}
/* This is where the program dumps and stiches together the tiles so they can be dragged. Most styles are generated automatically by dmMonocle, but if you want to use special cursors, this is the place to put them! */
#dmMainImage {	
	z-index: 1;
	cursor: url(<?php echo $root_uri ?>/images/cursors/openhand.cur), url(<?php echo $root_uri ?>/images/cursors/openhand.cur), move;
}
/* IE was not accepting clicks directly on the thumbnail image, had to add
   a div in between the Thumbnail and Navigator with a transparent BG
   for IE to click on. Please ensure the path to the background-image is 
   correct */
.dmClickNavLayer {
	z-index: 15;
	position: absolute;
	left: 0;
	top: 0;
	background-image: url(<?php echo $root_uri ?>/images/bg_ie.gif);
	}
/* Navigator styles (the little thing inside the thumbnail you can drag around
   to move the image). Feel free to change the color, give it a background, change the opacity, whatev! */
.dmNavigator {	
	background-color: #900;
	border: 1px solid #300;
	cursor: url(<?php echo $root_uri ?>/images/cursors/openhand.cur), url(<?php echo $root_uri ?>/images/cursors/openhand.cur), move;
	opacity: .4;
	filter: alpha(opacity = 40); /* IE does not recognize CSS's native opacity property */
	
	/* Do Not modify */
	position: absolute;
	z-index: 50;
	top: 0;
	left: 0;
	}

/* A special selector for when either the Nav or Main Image are being
   dragged. Currently used to alter the cursor, to go from an open to closed
   hand */
.dmDragging {
	cursor: url(<?php echo $root_uri ?>/images/cursors/closedhand.cur), url(<?php echo $root_uri ?>/images/cursors/closedhand.cur), move !important;
}
/* A special selector for when the nav or main image aren't draggable,
   such as when the image is fit to the viewer size */
.dmNoDrag {
	cursor: default !important;
}
/* The style for the loading tiles */
.dmImgTileLoading {
	background-image: url(<?php echo $root_uri ?>/images/loading.gif);
}


/************************
  =MENU
  
  By default, dmMonocle does not ouptut graphical icons for menu items, 
  but rather it outputs text links. The link styles can be overridden
  with the following as to display graphical icons.
  
***********************/
#dmMonocleMenu {
	background-color: #CCC;
	height: 30px;
	margin: 0 auto;
	padding: 0 0 3px 0;
	}
#dmMonocleMenu div {
	text-indent: -1000em;
	margin-right: 15px;
	background-repeat: no-repeat;
	cursor: pointer;
	margin-top: 9px;
	padding-top: 0;
	float: left;
	}
#dmMonocleHideNavigator {
	width: 16px;
	height: 16px;
	background-image: url(<?php echo $root_uri ?>/images/menu/toggleNav.gif);
	margin-left: 15px;
	}
#dmMonocleFitWindow {
	padding-left: 15px;
	border-left: 1px solid #999;
	background-position: right;
	width: 16px;
	height: 16px;
	background-image: url(<?php echo $root_uri ?>/images/menu/fitWindow.gif);
	}
#dmMonocleFitWidth {
	width: 16px;
	height: 16px;
	background-image: url(<?php echo $root_uri ?>/images/menu/fitWidth.gif);
	}
#dmMonocleMaxRes {
	width: 16px;
	height: 16px;
	background-image: url(<?php echo $root_uri ?>/images/menu/maxRes.gif);
	}
#dmMonocleRotateCounterclockwise {
	width: 16px;
	height: 16px;
	background-image: url(<?php echo $root_uri ?>/images/menu/rotateCounterClockwise.gif);
	padding-left: 15px;
	border-left: 1px solid #999;
	background-position: right;
	}
#dmMonocleRotateClockwise {
	width: 16px;
	height: 16px;
	background-image: url(<?php echo $root_uri ?>/images/menu/rotateClockwise.gif);
	}
#dmMonocleZoomOut {
	width: 16px;
	height: 16px;
	background-image: url(<?php echo $root_uri ?>/images/menu/zoomOut.gif);
	padding-left: 15px;
	border-left: 1px solid #999;
	background-position: right;
	}
#dmMonocleZoomIn {
	width: 16px;
	height: 16px;
	background-image: url(<?php echo $root_uri ?>/images/menu/zoomIn.gif);
	}
	
#dmMonocleDownloadButton {
	width: 16px;
	height: 16px;
	background-image: url(<?php echo $root_uri ?>/images/menu/print.gif);
	padding-left: 15px;
	border-left: 1px solid #999;
	background-position: right;	
	}	

/* Slider */

/* dmMonocle uses the jQuery UI Slider. These styles were
   created from scratch, but more styles can be easily
   generated using the jQueryUI theme roller (http://jqueryui.com/themeroller/)
*/
#dmMonocleMenu div#dmZoomLevelGague {
	text-indent: 0;
	margin-top: 7px;
	width: 100px;
}
#dmMonocleMenu .ui-slider { 
	position: relative; 
	text-align: left; 
	}
#dmMonocleMenu .ui-slider .ui-slider-handle { 
	outline: none; 
	position: absolute; 
	margin-top: 9px; 
	z-index: 2; 
	width: 12px; 
	height: 12px; 
	background-image: url(<?php echo $root_uri ?>/images/menu/sliderHandle.gif);
	background-repeat: no-repeat; 
	background-position: 0 0; 
	overflow: hidden;
	}
#dmMonocleMenu .ui-slider .ui-slider-handle:hover, #dmMonocleMenu .ui-slider .ui-slider-handle:active { 
	background-position: 0 -12px; 
	cursor: default; 
}
#dmMonocleMenu .ui-slider-horizontal { 
	height: 20px; 
	background-image: url(<?php echo $root_uri ?>/images/menu/sliderBG.gif);
	background-repeat: repeat-x; 
	background-position: left center;
	}
#dmMonocleMenu .ui-slider-horizontal .ui-slider-handle { 
	top: -5px; 
	margin-left: -5px; 
	}
