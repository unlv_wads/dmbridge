<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>The dmMonocle image viewer: a zoomable, pannable alternative to the
 * DMGenericImageViewer.</p>
 *
 * <p>The output of dmGenericImageViewer will be embedded inside a
 * &lt;noscript&gt; tag for the benefit of image search engines and visitors
 * who don't have JavaScript enabled.</p>
 *
 * @see DMGenericImageViewer
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 * @see http://digital.library.unlv.edu/software/dmmonocle
 */
class DMMonocleViewer extends DMAbstractViewer
implements DMObjectViewerDelegate {

	function viewerWillRender() {
		$root_uri = dirname($_SERVER['PHP_SELF'])
			. "/system/viewers/DMMonocleViewer";

		$this->getHelper()->addHeadScriptTag(
				dirname($_SERVER['PHP_SELF']) . "/includes/js/jquery-1.3.2.min.js");
		$this->getHelper()->addBodyScriptTag(
				$root_uri . "/scripts/jquery.event.drag-1.5.min.js");
		$this->getHelper()->addBodyScriptTag(
				$root_uri . "/scripts/jquery-ui-1.7.1.custom.min.js");
		$this->getHelper()->addBodyScriptTag(
				$root_uri . "/scripts/dmmonocle.js");
	}

	/**
	 * @return HTML tag
	 */
	public function getHTMLTag() {
		/* an alternative "viewer" for users who do not have javascript
		enabled; will be embedded in a <noscript> tag below. */
		$alt_viewer = new DMGenericImageViewer();
		$alt_viewer->setObject($this->getObject());
		$alt_viewer->setMediaType($this->getMediaType());
		$alt_viewer->setWidth($this->getWidth());
		$alt_viewer->setHeight($this->getHeight());

		return sprintf(
			'<div id="dmMonocle" style="width:%s; height:%s">
				<noscript>%s</noscript>
			</div>
			<script type="text/javascript">
			$(window).ready(function() {
				dmMonocle(%d, %d, %d, "%s", "%s");
			});
			</script>',
			$this->getWidth(),
			$this->getHeight(),
			$alt_viewer->getHTMLTag(),
			$this->getObject()->getWidth(),
			$this->getObject()->getHeight(),
			$this->getObject()->getPtr(),
			$this->getObject()->getCollection()->getAlias(),
			DMConfigIni::getInstance()->getString("contentdm.getimage_exe.path")
		);
	}

	/**
	 * @return HTML tag
	 */
	public function getHTMLTagNoJS() {
		// dmMonocle requires javascript; sorry
		return $this->getHtmlTag();
	}

	/**
	 * Whether the viewer is "bandwidth-safe," e.g. whether the content it
	 * loads can be safely viewed over a subjectively "slow" connection.
	 * 
	 * @return boolean True, always.
	 */
	public function isLowBandwidthCompatible() {
		return true;
	}

	/**
	 * @return The name of the object viewer.
	 */
	public function getName() {
		return "dmMonocle Image Viewer";
	}

}

