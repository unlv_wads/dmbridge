<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Defines the association between a media type and a dmBridge object viewer
 * class.
 * 
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
final class DMObjectViewerDefinition {

	/** @var string */
	private $class;
	/** @var int */
	private $max_file_size;
	/** @var DMMediaType */
	private $media_type;
	/** @var string */
	private $width;
	/** @var int */
	private $height;


	/**
	 * @param DMMediaType media_type
	 */
	public function __construct(DMMediaType $media_type) {
		$this->setMediaType($media_type);
	}

	/**
	 * @return string
	 */
	public function getClass() {
		return $this->class;
	}

	/**
	 * @param string str
	 */
	public function setClass($str) {
		$this->class = $str;
	}

	/**
	 * @return int
	 */
	public function getHeight() {
		return $this->height;
	}

	/**
	 * @param int int
	 */
	public function setHeight($int) {
		$this->height = (int) $int;
	}

	/**
	 * @return int Size in bytes
	 */
	public function getMaxFileSize() {
		return $this->max_file_size;
	}

	/**
	 * @param int int Size in bytes
	 */
	public function setMaxFileSize($int) {
		$this->max_file_size = (int) $int;
	}

	/**
	 * @return string
	 */
	public function getMediaType() {
		return $this->media_type;
	}

	/**
	 * @param DMMediaType media_type
	 */
	public function setMediaType(DMMediaType $media_type) {
		$this->media_type = $media_type;
	}

	/**
	 * @return string
	 */
	public function getWidth() {
		return $this->width;
	}

	/**
	 * @param string str
	 */
	public function setWidth($str) {
		$this->width = $str;
	}

}
