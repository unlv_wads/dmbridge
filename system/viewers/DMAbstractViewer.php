<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * An abstract object viewer class that all object viewer classes, whether
 * built-in or custom-written, should extend.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMAbstractViewer {

	/** @var DMGenericTemplateHelper */
	private $helper;
	/** @var DMObject */
	private $object;
	/** @var string */
	private $media_type;
	/** @var string */
	private $width;
	/** @var string */
	private $height;


	/**
	 * Does nothing.
	 */
	public function __construct() {
	}

	/**
	 * Default implementation that does nothing. See DMObjectViewerDelegate
	 * for documentation of this method.
	 *
	 * @return void
	 */
	public function viewerWillRender() {
	}

	/**
	 * @return DMGenericTemplateHelper
	 */
	protected function getHelper() {
		return $this->helper;
	}

	/**
	 * @param DMGenericTemplateHelper helper
	 */
	public function setHelper(DMGenericTemplateHelper $helper) {
		$this->helper = $helper;
	}

	/**
	 * @return string
	 */
	protected function getMediaType() {
		return $this->media_type;
	}

	/**
	 * @param DMMediaType media_type
	 */
	public function setMediaType(DMMediaType $media_type) {
		$this->media_type = $media_type;
	}

	/**
	 * @return DMObject
	 */
	protected function getObject() {
		return $this->object;
	}

	/**
	 * @param DMObject obj
	 */
	public function setObject(DMObject $obj) {
		$this->object = $obj;
	}

	/**
	 * @return string Width, including CSS units
	 */
	protected function getWidth() {
		$units = (round($this->width) == $this->width) ? "px" : "";
		return $this->width . $units;
	}

	/**
	 * @param string width Width, including CSS units
	 */
	public function setWidth($width) {
		$this->width = $width;
	}

	/**
	 * @return string Height, including CSS units
	 */
	protected function getHeight() {
		$units = (round($this->height) == $this->height) ? "px" : "";
		return $this->height . $units;
	}

	/**
	 * @param string height Height, including CSS units
	 */
	public function setHeight($height) {
		$this->height = $height;
	}

}
