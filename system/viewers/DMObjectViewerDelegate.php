<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * An object viewer interface that all object viewer classes, whether built-in
 * or custom, are required to implement.
 *
 * @see DMGenericObjectViewer
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
interface DMObjectViewerDelegate {

	/**
	 * Delegate method, called before the viewer is rendered. Necessary
	 * scripts, stylesheets, etc. should be added here.
	 */
	function viewerWillRender();

	/**
	 * Whether the viewer is "bandwidth-safe," e.g. whether the content it
	 * loads can be safely viewed over a subjectively "slow" connection.
	 * 
	 * @return boolean
	 * @since 2.1
	 */
	function isLowBandwidthCompatible();

	/**
	 * @return string HTML tag that renders the viewer
	 */
	function getHTMLTag();

	/**
	 * @return string HTML tag that renders the viewer
	 */
	function getHTMLTagNoJS();

	/**
	 * @param DMMediaType media_type Valid media (MIME) type
	 */
	function setMediaType(DMMediaType $media_type);

	/**
	 * @return The name of the viewer
	 */
	function getName();

	/**
	 * @param DMObject obj The object to "view"
	 */
	function setObject(DMObject $obj);

	/**
	 * @param string height HTML height, as a CSS-compatible string
	 */
	function setHeight($height);

	/**
	 * @param string width HTML width, as a CSS-compatible string
	 */
	function setWidth($width);

}
