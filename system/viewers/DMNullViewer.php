<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * A special viewer that is used automatically when there is an error using
 * a normal viewer, such as in a file-not-found circumstance.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMNullViewer extends DMAbstractViewer implements DMObjectViewerDelegate {

	/**
	 * @var string
	 */
	private $message;

	/**
	 * @return HTML tag
	 */
	public function getHTMLTag() {
		return sprintf(
			'<div id="dmNullViewer" style="width:%s; height:%s">
				<p>%s</p>
			</div>',
			$this->getWidth(), $this->getHeight(),
			DMString::websafe($this->getMessage()));
	}

	/**
	 * @return HTML tag
	 */
	public function getHTMLTagNoJS() {
		return $this->getHTMLTag();
	}

	/**
	 * @return string
	 */
	public function getMessage() {
		return $this->message;
	}

	/**
	 * @param string msg
	 */
	public function setMessage($msg) {
		$this->message = (string) $msg;
	}

	/**
	 * @return The name of the object viewer.
	 */
	public function getName() {
		return "Null Viewer";
	}

}
