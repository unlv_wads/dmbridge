<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Outputs an anchor (&lt;a&gt;) tag to download an object's file. Useful for
 * file formats that can't be displayed in a web browser.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMFileLinkViewer extends DMAbstractViewer
implements DMObjectViewerDelegate {

	/**
	 * @return HTML tag
	 */
	public function getHTMLTag() {
		$filesize = $this->getObject()->getFile()->getSize();
		return sprintf(
			'<div id="dmFileLinkViewer" style="width:%s; height:%s">
				<a href="%s">%s <span class="dmSize">(%s)</span></a>
			</div>',
			$this->getWidth(), $this->getHeight(),
			DMString::websafe($this->getObject()->getFileURL()),
			$this->getMediaType()->getName(),
			$this->getObject()->getFile()->getHumanReadableSize());
	}

	/**
	 * @return HTML tag
	 */
	public function getHTMLTagNoJS() {
		return $this->getHTMLTag();
	}

	/**
	 * Whether the viewer is "bandwidth-safe," e.g. whether the content it
	 * loads can be safely viewed over a subjectively "slow" connection.
	 * 
	 * @return boolean True, always.
	 */
	public function isLowBandwidthCompatible() {
		return true;
	}

	/**
	 * @return The name of the object viewer.
	 */
	public function getName() {
		return "File Link Viewer";
	}

}
