<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Outputs an image (&lt;img&gt;) tag with the URL of the object's image
 * embedded in the <code>src</code> attribute. In other words, renders the
 * object's image as a plain old web image. A minimalist alternative to
 * DMMonocleViewer.
 *
 * @see DMMonocleViewer
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMGenericImageViewer extends DMAbstractViewer
		implements DMObjectViewerDelegate {

	/**
	 * @return HTML anchor tag
	 */
	public function getHTMLTag() {
		$alt = "Object image";
		if ($this->getObject()->getField("title") instanceof DMDCElement) {
			$alt = $this->getObject()->getField("title")->getValue();
		}

		return sprintf(
			'<img src="%s" class="dmGenericImageViewer" alt="%s" />',
			DMString::websafe(
				$this->getObject()->getImageURL(
					$this->getWidth(), $this->getHeight())),
			DMString::websafe($alt));
	}

	/**
	 * @return HTML tag
	 */
	public function getHTMLTagNoJS() {
		return $this->getHTMLTag();
	}

	/**
	 * Whether the viewer is "bandwidth-safe," e.g. whether the content it
	 * loads can be safely viewed over a subjectively "slow" connection.
	 * 
	 * @return boolean True, always.
	 */
	public function isLowBandwidthCompatible() {
		return true;
	}

	/**
	 * @return The name of the object viewer.
	 */
	public function getName() {
		return "Generic Image Viewer";
	}

}
