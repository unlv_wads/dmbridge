<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>Outputs an &lt;embed&gt; tag inside a &lt;div&gt; of class
 * <code>dmPDFViewer</code>. If the web browser has the appropriate plug-in
 * installed (e.g. Adobe Acrobat), the PDF will be rendered inline with the
 * web page. If not, will display a hyperlink to download the PDF.</p>
 *
 * <p>This viewer requires Ghostscript to be installed on the server.</p>
 * 
 * <p>The &lt;embed&gt; tag is not HTML-compliant, but avoids compatibility
 * problems caused by the &lt;object&gt; tag.</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMPaginatedPDFViewer extends DMAbstractViewer
implements DMObjectViewerDelegate {

	function viewerWillRender() {
		$root_uri = dirname($_SERVER['PHP_SELF'])
			. "/system/viewers/DMPaginatedPDFViewer";
		
		$this->getHelper()->addStylesheetTag(
				$root_uri . "/styles/DMPaginatedPDFViewer.css");
		$this->getHelper()->addBodyScriptTag(
				"http://yui.yahooapis.com/3.3.0/build/yui/yui-min.js");
		$this->getHelper()->addBodyScriptTag(
				$root_uri . "/scripts/DMPaginatedPDFViewer.js");
	}

	/**
	 * @return HTML div tag
	 * @since 0.1
	 */
	public function getHTMLTag() {
		$num_pages = ($this->getObject()->getFile() instanceof DMPDFFile)
				? $this->getObject()->getFile()->getNumPages() : 0;
		return sprintf('<div class="DMPaginatedPDFViewer" style="width:%s; height:%s">
				<input type="hidden" name="fullPdfUri" value="%s"/>
				<input type="hidden" name="pdfGeneratorUri" value="%s"/>
				<input type="hidden" name="viewerBaseUri" value="%s/system/viewers/DMPaginatedPDFViewer"/>
				<input type="hidden" name="numPages" value="%d"/>
			</div>',
			$this->getWidth(),
			$this->getHeight(),
			$this->getObject()->getFileURL(),
			DMInternalURI::getURIWithParams("api/1/objects"
					. $this->getObject()->getCollection()->getAlias()
					. "/" . $this->getObject()->getPtr() . "/excerpt"),
			dirname($_SERVER['PHP_SELF']),
			$num_pages);
	}

	/**
	 * @return HTML tag
	 */
	public function getHTMLTagNoJS() {
		return $this->getHTMLTag();
	}

	/**
	 * Whether the viewer is "bandwidth-safe," e.g. whether the content it
	 * loads can be safely viewed over a subjectively "slow" connection.
	 * 
	 * @return boolean True, always.
	 */
	public function isLowBandwidthCompatible() {
		return true;
	}

	/**
	 * @return The name of the object viewer.
	 */
	public function getName() {
		return "Paginated PDF Viewer";
	}

}
