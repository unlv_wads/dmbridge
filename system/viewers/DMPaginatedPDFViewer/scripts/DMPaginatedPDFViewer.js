//
// dmBridge: a data access framework for CONTENTdm(R)
//
// Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
// Education, on behalf of the University of Nevada, Las Vegas
//

YUI().use("event-key", "node", function(Y) {
	Y.on("domready", function(e) {
		var viewerNode = Y.one("div.DMPaginatedPDFViewer");
		var viewer = new DMPaginatedPDFViewer(this, viewerNode);
		viewer.init();
	});
});

/**
 * DMPaginatedPDFViewer support script
 * Written to work with YUI 3.3.0.
 * 
 * @module DMPaginatedPDFViewer
 * @namespace edu.unlv.library.digital.dmbridge
 * @param Y YUI 3 instance
 * @param Y.Node viewerNode
 * @constructor
 * @requires event-key, node
 * @author Alex Dolski <alex.dolski@unlv.edu>
 */
function DMPaginatedPDFViewer(Y, viewerNode) {

	/**
	 * @property numPages
	 * @type int
	 */
	this.numPages = Y.one("input[name=numPages]").get("value");

	/**
	 * @property page
	 * @type int
	 * @default 1
	 */
	this.page = 1;

	/**
	 * @property baseUri
	 * @type string
	 * @private
	 */
	var baseUri = Y.one("input[name=viewerBaseUri]").get("value");

	/**
	 * @property fullPdfUri
	 * @type string
	 * @private
	 */
	var fullPdfUri = Y.one("input[name=fullPdfUri]").get("value");

	/**
	 * @property thisObj
	 * @type DMPaginatedPDFViewer
	 * @private
	 */
	var thisObj = this;

	/**
	 * @method goToPage
	 * @param int p
	 * @throws DMOutOfBoundsException
	 */
	this.goToPage = function(p) {
		p = parseInt(p);
		if (p > 0 && p <= this.numPages) {
			this.page = p;
			this.refresh();
			viewerNode.one("input#DMPaginatedPDFViewerTextField").set("value", this.page);
		} else {
			throw "DMOutOfBoundsException";
		}

		this.refreshPageButtons();
	}

	/**
	 * @method nextPage
	 * @throws DMOutOfBoundsException
	 */
	this.nextPage = function() {
		this.goToPage(this.page + 1);
	};

	/**
	 * @method getPDFPageURI
	 * @return string
	 */
	this.getPageURI = function() {
		var pdfGeneratorUri = Y.one("input[name=pdfGeneratorUri]").get("value");
		return pdfGeneratorUri + "?page=" + this.page;
	};

	/**
	 * @method previousPage
	 * @throws DMOutOfBoundsException
	 */
	this.previousPage = function() {
		this.goToPage(this.page - 1);
	};

	/**
	 * @method init
	 */
	this.init = function() {
		viewerNode.append(
			'<div id="DMPaginatedPDFViewerToolbar">'
				+ '<input type="image" src="' + baseUri + '/images/arrow-180.png" id="DMPaginatedPDFViewerPreviousPage" class="disabled" title="Previous Page" disabled="disabled">'
				+ '<span class="dmToolbarGroup">'
					+ '<input type="text" size="4" maxlength="4" id="DMPaginatedPDFViewerTextField" title="Page" value="1">/ ' + this.numPages
				+ '</span>'
				+ '<input type="image" src="' + baseUri + '/images/arrow.png" id="DMPaginatedPDFViewerNextPage" title="Next Page">'
				+ '<span class="dmToolbarSeparator"></span>'
				//+ '<input type="image" src="' + baseUri + '/images/printer.png" id="DMPaginatedPDFViewerPrint" title="Print">'
				+ '<input type="image" src="' + baseUri + '/images/document-pdf-text.png" id="DMPaginatedPDFViewerDownload" title="Download complete PDF">'
			+ '</div>');
		viewerNode.append(getEmbedTag());
		viewerNode.one("input#DMPaginatedPDFViewerPreviousPage").on("click", function(e) {
			thisObj.previousPage();
		});
		viewerNode.one("input#DMPaginatedPDFViewerTextField").on("key", function(e) {
			try {
				thisObj.goToPage(parseInt(e.target.get("value")));
			} catch (ex) {
				if (ex == "DMOutOfBoundsException") {
					alert("Requested page is out of bounds.");
				} else {
					throw ex;
				}
			}
		}, "down:13");
		viewerNode.one("input#DMPaginatedPDFViewerNextPage").on("click", function(e) {
			thisObj.nextPage();
		});/*
		viewerNode.one("input#DMPaginatedPDFViewerPrint").on("click", function(e) {
			this.print();
		});*/
		viewerNode.one("input#DMPaginatedPDFViewerDownload").on("click", function(e) {
			window.location = fullPdfUri;
		});
		
		this.refreshPageButtons();
	};

	/**
	 * @method refresh
	 */
	this.refresh = function() {
		viewerNode.one("object").replace(getEmbedTag());
		this.refreshPageButtons();
	};

	/**
	 * @method refreshPageButtons
	 * @protected
	 */
	this.refreshPageButtons = function() {
		var prevPageNode = viewerNode.one("input#DMPaginatedPDFViewerPreviousPage");
		var nextPageNode = viewerNode.one("input#DMPaginatedPDFViewerNextPage");
		if (this.page == 1) {
			prevPageNode.set("disabled", true);
			prevPageNode.addClass("disabled");
		} else {
			prevPageNode.set("disabled", false);
			prevPageNode.removeClass("disabled");
		}
		if (this.page == this.numPages) {
			nextPageNode.set("disabled", true);
			nextPageNode.addClass("disabled");
		} else {
			nextPageNode.set("disabled", false);
			nextPageNode.removeClass("disabled");
		}	
	};

	/**
	 * @method getEmbedTag
	 * @return Y.Node
	 * @private
	 */
	function getEmbedTag() {
		var pageUri = thisObj.getPageURI();
		var height = parseInt(viewerNode.getComputedStyle("height"))
			- parseInt(Y.one("div#DMPaginatedPDFViewerToolbar").getComputedStyle("height"));
		return Y.Node.create(
			'<object data="' + pageUri + '" type="application/pdf" width="100%" height="' + height + '">'
				+ '<a href="' + fullPdfUri + '">Download this PDF</a>'
			+ '</object>');
	};

}
