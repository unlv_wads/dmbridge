<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Outputs an &lt;object&gt; tag suitable for embedding QuickTime audio/video
 * in a web page.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMQuickTimeViewer extends DMAbstractViewer
implements DMObjectViewerDelegate {

	/**
	 * @return HTML tag
	 */
	public function getHTMLTag() {
		return sprintf(
			'<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B"
			codebase="http://www.apple.com/qtactivex/qtplugin.cab"
			width="%s" height="%d">
				<param name="src" value="%s" />
				<param name="controller" value="true" />
				<param name="autoplay" value="false" />
				<object type="%s" data="%s">
					<param name="controller" value="true" />
					<param name="autoplay" value="false" />
					<a href="%s">%s</a>
				</object>
			</object>',
			$this->getWidth(), $this->getHeight(),
			DMString::websafe($this->getObject()->getFileURL()),
			$this->getMediaType(),
			DMString::websafe($this->getObject()->getFileURL()),
			DMString::websafe($this->getObject()->getFileURL()),
			DMString::websafe($this->getObject()->getFilename())
		);
	}

	/**
	 * @return HTML tag
	 */
	public function getHTMLTagNoJS() {
		return $this->getHTMLTag();
	}

	/**
	 * Whether the viewer is "bandwidth-safe," e.g. whether the content it
	 * loads can be safely viewed over a subjectively "slow" connection.
	 * 
	 * @return boolean False, always.
	 */
	public function isLowBandwidthCompatible() {
		return false;
	}

	/**
	 * @return The name of the object viewer.
	 */
	public function getName() {
		return "QuickTime Viewer";
	}

}
