<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>The dmMonocle image viewer: a zoomable, pannable alternative to the
 * DMGenericImageViewer.</p>
 *
 * <p>The output of dmGenericImageViewer will be embedded inside a
 * &lt;noscript&gt; tag for the benefit of image search engines and visitors
 * who don't have JavaScript enabled.</p>
 *
 * @see DMGenericImageViewer
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 * @see http://digital.library.unlv.edu/software/dmmonocle
 */
class DMMonocle2Viewer extends DMAbstractViewer
implements DMObjectViewerDelegate {

	function viewerWillRender() {
		$root_uri = dirname($_SERVER['PHP_SELF'])
			. "/system/viewers/DMMonocle2Viewer";

		$this->getHelper()->addStylesheetTag(
				$root_uri . "/styles/dmmonocle.css");
		$this->getHelper()->addBodyScriptTag(
				"http://yui.yahooapis.com/3.3.0/build/yui/yui-min.js");
		$this->getHelper()->addBodyScriptTag(
				$root_uri . "/scripts/dmmonocle.js");
		$this->getHelper()->addBodyScriptCode(
			sprintf('YUI().use("node", "anim", "event-custom", "event-key", "slider", function(Y) {
					Y.on("domready", function(e) {
						var monocle = new DMMonocle(
							Y,
							{
								alias: "%s",
								ptr: %d,
								fullSize: {
									width: %d,
									height: %d
								}
							},
							"%s",
							"%s");
						monocle.init();
					});
				});',
			$this->getObject()->getCollection()->getAlias(),
			$this->getObject()->getPtr(),
			$this->getObject()->getWidth(),
			$this->getObject()->getHeight(),
			dirname($_SERVER['PHP_SELF']) . "/system/viewers/DMMonocle2Viewer",
			DMConfigIni::getInstance()->getString("contentdm.getimage_exe.path")));
	}

	/**
	 * @return HTML tag
	 */
	public function getHTMLTag() {
		/* an alternative "viewer" for users who do not have javascript
		enabled; will be embedded in a <noscript> tag below. */
		$alt_viewer = new DMGenericImageViewer();
		$alt_viewer->setObject($this->getObject());
		$alt_viewer->setMediaType($this->getMediaType());
		$alt_viewer->setWidth($this->getWidth());
		$alt_viewer->setHeight($this->getHeight());
		return sprintf(
			'<div id="dmMonocle" style="width:%s; height:%s">
				<noscript>%s</noscript>
			</div>',
			$this->getWidth(),
			$this->getHeight(),
			$alt_viewer->getHTMLTag());
	}

	/**
	 * @return HTML tag
	 */
	public function getHTMLTagNoJS() {
		// dmMonocle requires javascript; sorry
		return $this->getHtmlTag();
	}

	/**
	 * Whether the viewer is "bandwidth-safe," e.g. whether the content it
	 * loads can be safely viewed over a subjectively "slow" connection.
	 * 
	 * @return boolean True, always.
	 */
	public function isLowBandwidthCompatible() {
		return true;
	}

	/**
	 * @return The name of the object viewer.
	 */
	public function getName() {
		return "dmMonocle 2 Image Viewer";
	}

}

