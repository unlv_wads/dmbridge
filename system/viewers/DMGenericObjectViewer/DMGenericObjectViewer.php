<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * A general-purpose object viewer that outputs the URL of the file of the
 * current in an &lt;object&gt; tag. Should be used as a fallback in cases
 * where there is not a more appropriate viewer to handle a given file format.
 * Most web browsers, if they can't find a plug-in to deal with the MIME type,
 * will display the &lt;object&gt; tag as a link to download the file.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMGenericObjectViewer extends DMAbstractViewer
implements DMObjectViewerDelegate {

	/**
	 * @return HTML anchor tag
	 */
	public function getHTMLTag() {
		// width & height may be strings (%s)
		return sprintf('<object type="%s" data="%s" width="%s" height="%s">
				<param name="src" value="%s" />
				<a href="%s">%s</a>
			</object>',
			$this->getMediaType(),
			DMString::websafe($this->getObject()->getFileURL()),
			$this->getWidth(), $this->getHeight(),
			DMString::websafe($this->getObject()->getFileURL()),
			DMString::websafe($this->getObject()->getFileURL()),
			DMString::websafe($this->getObject()->getFilename())
		);
	}

	/**
	 * @return HTML tag
	 */
	public function getHTMLTagNoJS() {
		return $this->getHTMLTag();
	}

	/**
	 * Whether the viewer is "bandwidth-safe," e.g. whether the content it
	 * loads can be safely viewed over a subjectively "slow" connection.
	 * 
	 * @return boolean False, always.
	 */
	public function isLowBandwidthCompatible() {
		return false;
	}

	/**
	 * @return The name of the object viewer.
	 */
	public function getName() {
		return "Generic Object Viewer";
	}

}
