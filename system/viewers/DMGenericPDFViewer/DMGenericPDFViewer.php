<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>Outputs an &lt;embed&gt; tag inside a &lt;div&gt; of class
 * <code>dmPDFViewer</code>. If the web browser has the appropriate plug-in
 * installed (e.g. Adobe Acrobat), the PDF will be rendered inline with the
 * web page. If not, will display a hyperlink to download the PDF.</p>
 *
 * <p>The &lt;embed&gt; tag is not HTML-compliant, but avoids compatibility
 * problems caused by the &lt;object&gt; tag.</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMGenericPDFViewer extends DMAbstractViewer
implements DMObjectViewerDelegate {

	/** @var DMObjectQuery */
	private $query;


	/**
	 * @return HTML div tag
	 * @since 0.1
	 */
	public function getHTMLTag() {
		$terms = "";
		if ($this->getQuery() instanceof DMObjectQuery) {
			$terms = array();
			foreach ($this->getQuery()->getPredicates() as $t) {
				$terms[] = $t->getString();
			}
			$terms = DMString::websafe("#search=" . implode(" ", $terms));
		}

		// width & height may be strings (%)
		return sprintf('<div class="dmPDFViewer">
				<embed src="%s%s" type="%s" width="%s" height="%s">
					<a href="%s">Download this PDF</a>
				</embed>
			</div>',
			$this->getObject()->getFileURL(),
			$terms,
			$this->getMediaType(),
			$this->getWidth(),
			$this->getHeight(),
			$this->getObject()->getFileURL());
	}

	/**
	 * @return HTML tag
	 */
	public function getHTMLTagNoJS() {
		return $this->getHTMLTag();
	}

	/**
	 * Whether the viewer is "bandwidth-safe," e.g. whether the content it
	 * loads can be safely viewed over a subjectively "slow" connection.
	 * 
	 * @return boolean False, always.
	 */
	public function isLowBandwidthCompatible() {
		return false;
	}

	/**
	 * @return The name of the object viewer.
	 */
	public function getName() {
		return "Generic PDF Viewer";
	}

	/**
	 * @return DMObjectQuery
	 */
	protected function getQuery() {
		return $this->query;
	}

	/**
	 * @param DMObjectQuery query
	 */
	public function setQuery(DMObjectQuery $query) {
		$this->query = $query;
	}

}
