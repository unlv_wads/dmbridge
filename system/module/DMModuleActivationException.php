<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Manages all configuration operations involving the config.xml file.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMModuleActivationException extends DMException {

	/**
	 * @param string msg
	 * @param int code
	 */
	public function __construct($msg = null, $code = 5) {
		if (is_null($msg)) {
			$msg = DMLocalizedString::getString("MODULE_ACTIVATION_FAILED");
		}
		parent::__construct($msg, $code);
	}

}
