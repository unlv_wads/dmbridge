<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>Interface to be implemented by dmBridge modules that add their own
 * section to the dmBridge Control Panel.</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
interface DMBridgeControlPanelModule extends DMBridgeModule {

	/**
	 * @return array Array of DMCPMenuSections (normally just one), each
	 * containing one or more DMCPMenuItems. The menu sections returned by this
	 * method will be added to the Control Panel's main menu.
	 * @see DMCPMenuSection
	 */
	function getControlPanelMenuSections();

}

