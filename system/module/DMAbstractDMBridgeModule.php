<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * An abstract main module class from which all dmBridge modules should
 * inherit.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMAbstractDMBridgeModule {

	const BOOTSTRAP_FILENAME = "bootstrap.php";

	/**
	 * Currently does not do anything, but should be called anyway for the
	 * sake of future compatibility.
	 */
	public function __construct() {
	}

	/**
	 * @return string
	 */
	public function getAbsolutePathname() {
		$reflector = new ReflectionObject($this);
		return dirname($reflector->getFilename());
	}

	/**
	 * @return DOMDocument or null
	 */
	public function getHTTPAPIDocumentation() {
		$dxml = new DOMDocument("1.0", "utf-8");
		$pathname = $this->getAbsolutePathname() . "/http_api_doc.xml";
		if (file_exists($pathname)) {
			$dxml->load($pathname);
		}
		return $dxml;
	}

}
