<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * Base interface for dmBridge modules.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 * @see DMBridgeDataStoreModule
 */
interface DMBridgeModule {

	/**
	 * @return string The absolute pathname of the module's root folder.
	 * Typically this will be the return value of
	 * <code>dirname(__FILE__)</code>.
	 */
	function getAbsolutePathname();

	/**
	 * @return A DOMNode containing an XML representation of the module's
	 * HTTP API documentation, or null if the module does not have any.
	 */
	function getHTTPAPIDocumentation();

	/**
	 * @return string The name of the module.
	 */
	function getName();

	/**
	 * Every version of dmBridge has both a version, which is an arbitrary
	 * string, and a version sequence, which is an integer that increments by
	 * one with each new version. The current version sequence can be accessed
	 * via <code>DMBridgeVersion::getDmBridgeVersionSequence()</code>.
	 *
	 * @return int The minimum dmBridge version sequence that the module
	 * supports.
	 * @see DMBridgeVersion::getDmBridgeVersionSequence()
	 */
	function getMinSupportedVersionSequence();

	/**
	 * @return string The version of the module.
	 */
	function getVersion();

}

