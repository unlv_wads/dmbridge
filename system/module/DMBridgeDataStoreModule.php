<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>Interface to be implemented by dmBridge modules that use the dmBridge
 * data store.</p>
 *
 * <p>Modules should add a prefix to their table names in order to keep them
 * obviously separate from dmBridge tables.</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 * @see DMBridgeModule
 */
interface DMBridgeDataStoreModule extends DMBridgeModule {

	/**
	 * Invoked when the module is activated for the first time. Should return
	 * any and all SQL commands necessary to get the tables in a working state.
	 *
	 * @return array Array of SQL command strings to be executed, with one
	 * command per array element.
	 * @see getSupportedDataStores()
	 */
	function getSetupSQLForMySQL();

	/**
	 * Invoked when the module is activated for the first time. Should return
	 * any and all SQL commands necessary to get the tables in a working state.
	 *
	 * @return array Array of SQL command strings to be executed, with one
	 * command per array element
	 * @see getSupportedDataStores()
	 */
	function getSetupSQLForSQLite();

	/**
	 * @return array An array of integers corresponding to the data stores
	 * supported by the module. See the DMDataStoreType class for a list of
	 * types.
	 */
	function getSupportedDataStores();

	/**
	 * Invoked when the module is upgraded. Should return any and all
	 * SQL commands necessary to upgrade the tables from any previous version
	 * of the module.
	 *
	 * @return array Array of SQL command strings to be executed, with one
	 * command per array element.
	 * @see getSupportedDataStores()
	 */
	function getUpgradeSQLForMySQL();

	/**
	 * Invoked when the module is upgraded. Should return any and all
	 * SQL commands necessary to upgrade the tables from any previous version
	 * of the module.
	 *
	 * @return array Array of SQL command strings to be executed, with one
	 * command per array element
	 * @see getSupportedDataStores()
	 */
	function getUpgradeSQLForSQLite();

}

