<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>Interface to be implemented by dmBridge modules that provide template
 * helpers.</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
interface DMBridgeTemplateHelperModule {

	/**
	 * @param DMAbstractView view
	 * @param DMSession session
	 * @return DMTemplateHelper
	 */
	function getTemplateHelper(DMAbstractView $view, DMSession $session);

}
