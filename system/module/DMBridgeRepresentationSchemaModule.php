<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>Interface to be implemented by dmBridge modules that use XML schemas to
 * validate their XML representations in the HTTP API.</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
interface DMBridgeRepresentationSchemaModule extends DMBridgeModule {

	/**
	 * @return string Absolute pathname of the folder containing the XML
	 * schemas used by the module. The schemas contained within this folder
	 * will be available at
	 * http://[hostname]/[dmbridge root folder]/schema.
	 */
	function getPathToSchemas();

}

