<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
interface DMAuthenticationService {

	/**
	 * @param DMUser user
	 * @param string password
	 * @return boolean True for successful authentication, false for not
	 */
	function authenticate(DMUser $user, $password);

	/**
	 * @param DMUser user
	 * @return void
	 */
	function logout(DMUser $user);

	/**
	 * Should send all necessary cookies using setcookie() or setrawcookie().
	 * If there are no cookies to be sent, should do nothing.
	 */
	function sendCookies();

}
