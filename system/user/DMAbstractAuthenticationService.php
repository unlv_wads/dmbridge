<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMAbstractAuthenticationService {

	/**
	 * @param DMUser user
	 * @return void
	 */
	public function logout(DMUser $user) {
		return DMHTTPRequest::getCurrent()->getSession()->unsetUser();
	}

	/**
	 * Empty.
	 */
	public function sendCookies() {
	}

}
