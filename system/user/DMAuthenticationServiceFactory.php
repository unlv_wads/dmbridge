<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMAuthenticationServiceFactory {

	/**
	 * @var DMAuthenticationService
	 */
	private static $service;

	/**
	 * @return DMAuthenticationService
	 * @throws DMClassNotFoundException
	 */
	public static function getService() {
		if (!self::$service) {
			$custom_class = DMConfigIni::getInstance()->getString(
					"dmbridge.authentication_class");
			if ($custom_class) {
				$path = sprintf("%s/../../extensions/%s.php",
						dirname(__FILE__), $custom_class);
				if (!file_exists($path)) {
					throw new DMClassNotFoundException($path);
				}
				require_once($path);
				self::$service = new $custom_class;
			} else {
				self::$service = new DMCdmAuthenticationService();
			}
		}
		return self::$service;
	}

}
