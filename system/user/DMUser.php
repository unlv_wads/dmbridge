<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * <p>Represents an authenticated dmBridge user.</p>
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMUser extends DMModel {

	/**
	 * @var string
	 */
	private $username;


	/**
	 * @param string username
	 */
	public function __construct($username) {
		$this->setUsername($username);
	}

	/**
	 * @return string The user's username
	 * @since 0.1
	 */
	public function __toString() {
		return $this->getUsername();
	}

	/**
	 * @return string
	 * @since 0.1
	 */
	public final function getUsername() {
		return $this->username;
	}

	private function setUsername($username) {
		$this->username = $username;
	}


}
