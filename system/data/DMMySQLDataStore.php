<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * An instance of this class will be returned by
 * DMDataStoreFactory::getDataStore() when pdo_mysql is the current data store.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMMySQLDataStore extends DMPDODataStore implements DMDataStore {

	private static $instance;

	protected $dbcon, $driver = 'pdo_mysql';
	private $dbhost, $dbport = 3306, $dbname, $dbuser, $dbpass;


	/**
	 * Destroys the current Singleton object (not all your data)
	 * @return void
	 */
	public static function destroyInstance() {
		self::$instance = null;
	}

	/**
	 * @return self object
	 * @since 0.3
	 */
	public static function getInstance() {
		if (!self::$instance instanceof self) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	protected function __construct() {
		parent::__construct();
		DMDataStoreFactory::setDataStore('pdo_mysql');
		$this->dbhost = DMConfigIni::getInstance()->getString(
				"dmbridge.database.mysql.host");
		if (DMConfigIni::getInstance()->getString("dmbridge.database.mysql.port")) {
			$this->dbport = DMConfigIni::getInstance()->getString(
					"dmbridge.database.mysql.port");
		}
		$this->dbname = DMConfigIni::getInstance()->getString(
				"dmbridge.database.mysql.name");
		$this->dbuser = DMConfigIni::getInstance()->getString(
				"dmbridge.database.mysql.username");
		$this->dbpass = DMConfigIni::getInstance()->getString(
				"dmbridge.database.mysql.password");
	}

	public function __sleep() {
		return array('driver', 'dbhost', 'dbport', 'dbname', 'dbuser', 'dbpass');
	}

	/**
	 * @return PDO handle
	 * @throws DMPDOException
	 */
	public function getConnection() {
		if (!$this->dbcon) {
			try {
				if ($this->dbhost == "localhost" && $this->dbport = 3306) {
					$socket_path = ini_get("mysql.default_socket");
					if (!file_exists($socket_path)) {
						throw new PDOException(
								sprintf(
									DMLocalizedString::getString("MISSING_FILE"),
										$socket_path));
					}
				}
				$this->dbcon = @new PDO(
						$this->getDSN(), $this->dbuser, $this->dbpass);
				$this->dbcon->setAttribute(PDO::ATTR_ERRMODE,
					PDO::ERRMODE_EXCEPTION);
				$this->dbcon->setAttribute(PDO::ATTR_TIMEOUT, 10);
				$this->createTables();
			} catch (PDOException $e) {
				$e = new DMPDOException($e);
				throw $e;
			}
		}
		return $this->dbcon;
	}

	private function getDSN() {
		$dsn = "";
		$params = array();
		$params['host'] = $this->dbhost;
		$params['port'] = $this->dbport;
		$params['dbname'] = $this->dbname;
		foreach ($params as $key => $value) {
			if ($value) {
				$dsn .= $key . "=" . $value . ";";
			}
		}
		$dsn = "mysql:" . rtrim($dsn, ";");
		return $dsn;
	}

	/**
	 * @return string One of the constants in DMDataStoreType
	 */
	public function getType() {
		return DMDataStoreType::PDO_MySQL;
	}

}
