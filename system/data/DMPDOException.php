<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
# Copyright © 2009, 2010 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class DMPDOException extends DMDataStoreException {

	/**
	 * @param PDOException e
	 * @since 0.1
	 */
	public function __construct(PDOException $e) {
		// check the error code to make sure it's not a string, which it
		// sometimes is, which causes Exception to choke
		$code = is_int($e->getCode()) ? $e->getCode() : null;
		parent::__construct('PDO: ' . $e->getMessage(), $code);
	}

}
