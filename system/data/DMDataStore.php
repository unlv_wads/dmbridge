<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
interface DMDataStore {

	/**
	 * Begins a new transaction.
	 */
	function beginTransaction();

	/**
	 * Ends a transaction, committing all changes.
	 */
	function commit();

	/**
	 * @throws DMPDOException
	 * @return void
	 */
	function createTables();

	/**
	 * @return int One of the DMDataStoreType constants
	 */
	function getType();

	/**
	 * @param string sql
	 * @param array params Array of bound parameters, in :key => value format
	 * @throws DMPDOException
	 * @return array
	 */
	function read($sql, array $params);

	/**
	 * Rolls back all changes to the data store since the transaction was begun.
	 */
	function rollBack();

	/**
	 * @param string sql
	 * @param array params Array of bound parameters, in :key => value format
	 * @throws DMPDOException
	 * @return int Number of affected rows
	 */
	function write($sql, array $params);

	/**
	 * @param DMCommentQuery query
	 * @param bool count Whether to return the result count, or the results
	 * @return Array of DMComment objects
	 * @deprecated
	 */
	function getCommentsForQuery(DMCommentQuery $query, $count);

	/**
	 * @param DMTagQuery query
	 * @param bool count Whether to return the result count, or the results
	 * @return Associative array of tag value => count pairs
	 * @deprecated
	 */
	function getTagsForQueryAsCounts(DMTagQuery $query, $count);

	/**
	 * @param DMTagQuery query
	 * @param bool count Whether to return the result count, or the results
	 * @return Array of DMTag objects
	 * @deprecated
	 */
	function getTagsForQueryAsObjects(DMTagQuery $query, $count);

	/**
	 * @param DMComment comment
	 */
	function deleteComment(DMComment $comment);

	/**
	 * @param int id
	 * @return DMComment
	 * @throws DMUnavailableModelException
	 */
	function loadComment($id);

	/**
	 * Persists the supplied comment to the data store.
	 *
	 * @param DMComment c
	 * @return void
	 */
	function saveComment(DMComment $c);

	/**
	 * @param DMCollection col
	 * @return float
	 * @deprecated
	 */
	function getMeanNumCommentsPerObjectInCollection(DMCollection $col);

	/**
	 * @param DMCollection col
	 * @return int
	 * @deprecated
	 */
	function getMedianNumCommentsPerObjectInCollection(DMCollection $col);

	/**
	 * @param DMCollection col
	 * @return float
	 * @deprecated
	 */
	function getMeanNumRatingsPerObjectInCollection(DMCollection $col);

	/**
	 * @param DMCollection col
	 * @return int
	 * @deprecated
	 */
	function getMedianNumRatingsPerObjectInCollection(DMCollection $col);

	/**
	 * @param DMCollection col
	 * @return float
	 * @deprecated
	 */
	function getMeanNumTagsPerObjectInCollection(DMCollection $col);

	/**
	 * @param DMCollection col
	 * @return int
	 * @deprecated
	 */
	function getMedianNumTagsPerObjectInCollection(DMCollection $col);

	/**
	 * @return int The number of objects with approved comments.
	 * @deprecated
	 */
	function getNumObjectsWithApprovedComments();

	/**
	 * Ascribes the given comment to the given object and persists the
	 * relationship to the data store.
	 *
	 * @param DMObject obj
	 * @param DMComment comment
	 * @return void
	 */
	function addObjectComment(DMObject $obj, DMComment $comment);

	/**
	 * @param int limit
	 * @return array Array of DMObjects in descending order by comment count,
	 * up to $limit
	 * @deprecated
	 */
	function getObjectsWithMostComments($limit);

	/**
	 * @param DMObject o
	 * @return array Array of all ratings for the given object, as DMRating
	 * objects
	 * @deprecated
	 */
	function getAllRatingsForObject(DMObject $o);

	/**
	 * @return int The number of objects that have been rated.
	 * @deprecated
	 */
	function getNumObjectsWithRatings();

	/**
	 * @return int The number of ratings in the data store.
	 * @deprecated
	 */
	function getNumRatings();

	/**
	 * @param DMCollection c
	 * @return int The number of ratings in the given collection.
	 * @deprecated
	 */
	function getNumRatingsInCollection(DMCollection $c);

	/**
	 * @param DMObject o
	 * @return int The number of ratings for the given object.
	 * @deprecated
	 */
	function getNumRatingsForObject(DMObject $o);

	/**
	 * Appends the given rating to the given object and persists the
	 * relationship to the data store.
	 *
	 * @param DMObject o
	 * @param DMRating r
	 */
	function addObjectRating(DMObject $o, DMRating $r);

	/**
	 * @param int limit
	 * @return array Array of DMObjects
	 * @deprecated
	 */
	function getObjectsWithHighestRatings($limit);

	/**
	 * @param int limit
	 * @return array Array of DMObjects
	 * @deprecated
	 */
	function getObjectsWithLowestRatings($limit);

	/**
	 * @param int limit
	 * @return array Array of DMObjects
	 * @deprecated
	 */
	function getObjectsWithMostRatings($limit);

	/**
	 * @param DMObject o
	 * @return float
	 * @deprecated
	 */
	function getRatingForObject(DMObject $o);

	/**
	 * Deletes all tags with the given value.
	 *
	 * @param string value
	 * @return int The number of tags deleted
	 * @deprecated
	 */
	function deleteAllTagsWithValue($value);

	/**
	 * Deletes the given tag from the data store.
	 *
	 * @param DMTag tag
	 */
	function deleteTag(DMTag $tag);

	/**
	 * @param int id
	 * @return DMTag
	 * @throws DMUnavailableModelException
	 */
	function loadTag($id);

	/**
	 * Saves the given tag to the data store.
	 *
	 * @param DMTag tag The tag to save
	 * @return void
	 */
	function saveTag(DMTag $tag);

	/**
	 * @return int The number of objects with approved tags.
	 * @deprecated
	 */
	function getNumObjectsWithApprovedTags();

	/**
	 * Ascribes the given tag to the given object and persists the relationship
	 * to the data store.
	 *
	 * @param DMObject o
	 * @param DMTag t
	 * @return void
	 */
	function addObjectTag(DMObject $o, DMTag $t);

	/**
	 * @param int limit
	 * @return array Array of DMObjects with the most tags in descending order
	 * up to $limit.
	 * @deprecated
	 */
	function getObjectsWithMostTags($limit);

}
