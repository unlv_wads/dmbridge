<?php
#
# dmBridge: a data access framework for CONTENTdm(R)
#
# Copyright © 2009, 2010, 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * An enum-like class.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
abstract class DMDataStoreType {

	const PDO_MySQL = "pdo_mysql";
	const PDO_SQLite = "pdo_sqlite";

	/**
	 * @return Array of strings corresponding to the DMDataStoreType constants
	 */
	public static function getAll() {
		$rc = new ReflectionClass(__CLASS__);
		return $rc->getConstants();
	}

}
